package com.krio.gadgetcontroller.provider.panel;

import com.krio.gadgetcontroller.provider.base.BaseModel;

import java.util.Date;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Data model for the {@code panel} table.
 */
public interface PanelModel extends BaseModel {

    /**
     * Get the {@code project_id} value.
     */
    long getProjectId();

    /**
     * Get the {@code caption} value.
     * Cannot be {@code null}.
     */
    @NonNull
    String getCaption();

    /**
     * Get the {@code position_on_screen} value.
     * Can be {@code null}.
     */
    @Nullable
    Integer getPositionOnScreen();
}
