package com.krio.gadgetcontroller.provider.panel;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.krio.gadgetcontroller.provider.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code panel} table.
 */
public class PanelContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return PanelColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, @Nullable PanelSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(Context context, @Nullable PanelSelection where) {
        return context.getContentResolver().update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public PanelContentValues putProjectId(long value) {
        mContentValues.put(PanelColumns.PROJECT_ID, value);
        return this;
    }


    public PanelContentValues putCaption(@NonNull String value) {
        if (value == null) throw new IllegalArgumentException("caption must not be null");
        mContentValues.put(PanelColumns.CAPTION, value);
        return this;
    }


    public PanelContentValues putPositionOnScreen(@Nullable Integer value) {
        mContentValues.put(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public PanelContentValues putPositionOnScreenNull() {
        mContentValues.putNull(PanelColumns.POSITION_ON_SCREEN);
        return this;
    }
}
