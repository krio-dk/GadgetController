package com.krio.gadgetcontroller.provider.panel;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import com.krio.gadgetcontroller.provider.base.AbstractSelection;
import com.krio.gadgetcontroller.provider.project.*;

/**
 * Selection for the {@code panel} table.
 */
public class PanelSelection extends AbstractSelection<PanelSelection> {
    @Override
    protected Uri baseUri() {
        return PanelColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code PanelCursor} object, which is positioned before the first entry, or null.
     */
    public PanelCursor query(ContentResolver contentResolver, String[] projection) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new PanelCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, null)}.
     */
    public PanelCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null);
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param context The context to use for the query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code PanelCursor} object, which is positioned before the first entry, or null.
     */
    public PanelCursor query(Context context, String[] projection) {
        Cursor cursor = context.getContentResolver().query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new PanelCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(context, null)}.
     */
    public PanelCursor query(Context context) {
        return query(context, null);
    }


    public PanelSelection id(long... value) {
        addEquals("panel." + PanelColumns._ID, toObjectArray(value));
        return this;
    }

    public PanelSelection idNot(long... value) {
        addNotEquals("panel." + PanelColumns._ID, toObjectArray(value));
        return this;
    }

    public PanelSelection orderById(boolean desc) {
        orderBy("panel." + PanelColumns._ID, desc);
        return this;
    }

    public PanelSelection orderById() {
        return orderById(false);
    }

    public PanelSelection projectId(long... value) {
        addEquals(PanelColumns.PROJECT_ID, toObjectArray(value));
        return this;
    }

    public PanelSelection projectIdNot(long... value) {
        addNotEquals(PanelColumns.PROJECT_ID, toObjectArray(value));
        return this;
    }

    public PanelSelection projectIdGt(long value) {
        addGreaterThan(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public PanelSelection projectIdGtEq(long value) {
        addGreaterThanOrEquals(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public PanelSelection projectIdLt(long value) {
        addLessThan(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public PanelSelection projectIdLtEq(long value) {
        addLessThanOrEquals(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public PanelSelection orderByProjectId(boolean desc) {
        orderBy(PanelColumns.PROJECT_ID, desc);
        return this;
    }

    public PanelSelection orderByProjectId() {
        orderBy(PanelColumns.PROJECT_ID, false);
        return this;
    }

    public PanelSelection projectName(String... value) {
        addEquals(ProjectColumns.NAME, value);
        return this;
    }

    public PanelSelection projectNameNot(String... value) {
        addNotEquals(ProjectColumns.NAME, value);
        return this;
    }

    public PanelSelection projectNameLike(String... value) {
        addLike(ProjectColumns.NAME, value);
        return this;
    }

    public PanelSelection projectNameContains(String... value) {
        addContains(ProjectColumns.NAME, value);
        return this;
    }

    public PanelSelection projectNameStartsWith(String... value) {
        addStartsWith(ProjectColumns.NAME, value);
        return this;
    }

    public PanelSelection projectNameEndsWith(String... value) {
        addEndsWith(ProjectColumns.NAME, value);
        return this;
    }

    public PanelSelection orderByProjectName(boolean desc) {
        orderBy(ProjectColumns.NAME, desc);
        return this;
    }

    public PanelSelection orderByProjectName() {
        orderBy(ProjectColumns.NAME, false);
        return this;
    }

    public PanelSelection projectToken(String... value) {
        addEquals(ProjectColumns.TOKEN, value);
        return this;
    }

    public PanelSelection projectTokenNot(String... value) {
        addNotEquals(ProjectColumns.TOKEN, value);
        return this;
    }

    public PanelSelection projectTokenLike(String... value) {
        addLike(ProjectColumns.TOKEN, value);
        return this;
    }

    public PanelSelection projectTokenContains(String... value) {
        addContains(ProjectColumns.TOKEN, value);
        return this;
    }

    public PanelSelection projectTokenStartsWith(String... value) {
        addStartsWith(ProjectColumns.TOKEN, value);
        return this;
    }

    public PanelSelection projectTokenEndsWith(String... value) {
        addEndsWith(ProjectColumns.TOKEN, value);
        return this;
    }

    public PanelSelection orderByProjectToken(boolean desc) {
        orderBy(ProjectColumns.TOKEN, desc);
        return this;
    }

    public PanelSelection orderByProjectToken() {
        orderBy(ProjectColumns.TOKEN, false);
        return this;
    }

    public PanelSelection projectPositionOnList(Integer... value) {
        addEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public PanelSelection projectPositionOnListNot(Integer... value) {
        addNotEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public PanelSelection projectPositionOnListGt(int value) {
        addGreaterThan(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public PanelSelection projectPositionOnListGtEq(int value) {
        addGreaterThanOrEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public PanelSelection projectPositionOnListLt(int value) {
        addLessThan(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public PanelSelection projectPositionOnListLtEq(int value) {
        addLessThanOrEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public PanelSelection orderByProjectPositionOnList(boolean desc) {
        orderBy(ProjectColumns.POSITION_ON_LIST, desc);
        return this;
    }

    public PanelSelection orderByProjectPositionOnList() {
        orderBy(ProjectColumns.POSITION_ON_LIST, false);
        return this;
    }

    public PanelSelection caption(String... value) {
        addEquals(PanelColumns.CAPTION, value);
        return this;
    }

    public PanelSelection captionNot(String... value) {
        addNotEquals(PanelColumns.CAPTION, value);
        return this;
    }

    public PanelSelection captionLike(String... value) {
        addLike(PanelColumns.CAPTION, value);
        return this;
    }

    public PanelSelection captionContains(String... value) {
        addContains(PanelColumns.CAPTION, value);
        return this;
    }

    public PanelSelection captionStartsWith(String... value) {
        addStartsWith(PanelColumns.CAPTION, value);
        return this;
    }

    public PanelSelection captionEndsWith(String... value) {
        addEndsWith(PanelColumns.CAPTION, value);
        return this;
    }

    public PanelSelection orderByCaption(boolean desc) {
        orderBy(PanelColumns.CAPTION, desc);
        return this;
    }

    public PanelSelection orderByCaption() {
        orderBy(PanelColumns.CAPTION, false);
        return this;
    }

    public PanelSelection positionOnScreen(Integer... value) {
        addEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public PanelSelection positionOnScreenNot(Integer... value) {
        addNotEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public PanelSelection positionOnScreenGt(int value) {
        addGreaterThan(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public PanelSelection positionOnScreenGtEq(int value) {
        addGreaterThanOrEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public PanelSelection positionOnScreenLt(int value) {
        addLessThan(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public PanelSelection positionOnScreenLtEq(int value) {
        addLessThanOrEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public PanelSelection orderByPositionOnScreen(boolean desc) {
        orderBy(PanelColumns.POSITION_ON_SCREEN, desc);
        return this;
    }

    public PanelSelection orderByPositionOnScreen() {
        orderBy(PanelColumns.POSITION_ON_SCREEN, false);
        return this;
    }
}
