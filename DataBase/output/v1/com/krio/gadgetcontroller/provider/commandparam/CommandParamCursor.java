package com.krio.gadgetcontroller.provider.commandparam;

import java.util.Date;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.krio.gadgetcontroller.provider.base.AbstractCursor;
import com.krio.gadgetcontroller.provider.command.*;
import com.krio.gadgetcontroller.provider.widget.*;
import com.krio.gadgetcontroller.provider.panel.*;
import com.krio.gadgetcontroller.provider.project.*;

/**
 * Cursor wrapper for the {@code command_param} table.
 */
public class CommandParamCursor extends AbstractCursor implements CommandParamModel {
    public CommandParamCursor(Cursor cursor) {
        super(cursor);
    }

    /**
     * Primary key.
     */
    public long getId() {
        Long res = getLongOrNull(CommandParamColumns._ID);
        if (res == null)
            throw new NullPointerException("The value of '_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code name} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getName() {
        String res = getStringOrNull(CommandParamColumns.NAME);
        if (res == null)
            throw new NullPointerException("The value of 'name' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code integer_param} value.
     * Can be {@code null}.
     */
    @Nullable
    public Integer getIntegerParam() {
        Integer res = getIntegerOrNull(CommandParamColumns.INTEGER_PARAM);
        return res;
    }

    /**
     * Get the {@code double_param} value.
     * Can be {@code null}.
     */
    @Nullable
    public Double getDoubleParam() {
        Double res = getDoubleOrNull(CommandParamColumns.DOUBLE_PARAM);
        return res;
    }

    /**
     * Get the {@code string_param} value.
     * Can be {@code null}.
     */
    @Nullable
    public String getStringParam() {
        String res = getStringOrNull(CommandParamColumns.STRING_PARAM);
        return res;
    }

    /**
     * Get the {@code boolean_param} value.
     * Can be {@code null}.
     */
    @Nullable
    public Boolean getBooleanParam() {
        Boolean res = getBooleanOrNull(CommandParamColumns.BOOLEAN_PARAM);
        return res;
    }

    /**
     * Get the {@code date_param} value.
     * Can be {@code null}.
     */
    @Nullable
    public Date getDateParam() {
        Date res = getDateOrNull(CommandParamColumns.DATE_PARAM);
        return res;
    }

    /**
     * Get the {@code command_id} value.
     */
    public long getCommandId() {
        Long res = getLongOrNull(CommandParamColumns.COMMAND_ID);
        if (res == null)
            throw new NullPointerException("The value of 'command_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code type} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public CommandType getCommandType() {
        Integer intValue = getIntegerOrNull(CommandColumns.TYPE);
        if (intValue == null)
            throw new NullPointerException("The value of 'type' in the database was null, which is not allowed according to the model definition");
        return CommandType.values()[intValue];
    }

    /**
     * Get the {@code widget_id} value.
     */
    public long getCommandWidgetId() {
        Long res = getLongOrNull(CommandColumns.WIDGET_ID);
        if (res == null)
            throw new NullPointerException("The value of 'widget_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code position_on_panel} value.
     * Can be {@code null}.
     */
    @Nullable
    public Integer getCommandWidgetPositionOnPanel() {
        Integer res = getIntegerOrNull(WidgetColumns.POSITION_ON_PANEL);
        return res;
    }

    /**
     * Get the {@code caption} value.
     * Can be {@code null}.
     */
    @Nullable
    public String getCommandWidgetCaption() {
        String res = getStringOrNull(WidgetColumns.CAPTION);
        return res;
    }

    /**
     * Get the {@code panel_id} value.
     */
    public long getCommandWidgetPanelId() {
        Long res = getLongOrNull(WidgetColumns.PANEL_ID);
        if (res == null)
            throw new NullPointerException("The value of 'panel_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code project_id} value.
     */
    public long getCommandWidgetPanelProjectId() {
        Long res = getLongOrNull(PanelColumns.PROJECT_ID);
        if (res == null)
            throw new NullPointerException("The value of 'project_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code name} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getCommandWidgetPanelProjectName() {
        String res = getStringOrNull(ProjectColumns.NAME);
        if (res == null)
            throw new NullPointerException("The value of 'name' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code token} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getCommandWidgetPanelProjectToken() {
        String res = getStringOrNull(ProjectColumns.TOKEN);
        if (res == null)
            throw new NullPointerException("The value of 'token' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code position_on_list} value.
     * Can be {@code null}.
     */
    @Nullable
    public Integer getCommandWidgetPanelProjectPositionOnList() {
        Integer res = getIntegerOrNull(ProjectColumns.POSITION_ON_LIST);
        return res;
    }

    /**
     * Get the {@code caption} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getCommandWidgetPanelCaption() {
        String res = getStringOrNull(PanelColumns.CAPTION);
        if (res == null)
            throw new NullPointerException("The value of 'caption' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code position_on_screen} value.
     * Can be {@code null}.
     */
    @Nullable
    public Integer getCommandWidgetPanelPositionOnScreen() {
        Integer res = getIntegerOrNull(PanelColumns.POSITION_ON_SCREEN);
        return res;
    }

    /**
     * Get the {@code type} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public WidgetType getCommandWidgetType() {
        Integer intValue = getIntegerOrNull(WidgetColumns.TYPE);
        if (intValue == null)
            throw new NullPointerException("The value of 'type' in the database was null, which is not allowed according to the model definition");
        return WidgetType.values()[intValue];
    }

    /**
     * Get the {@code cmd} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getCommandCmd() {
        String res = getStringOrNull(CommandColumns.CMD);
        if (res == null)
            throw new NullPointerException("The value of 'cmd' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code type} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public CommandParamType getType() {
        Integer intValue = getIntegerOrNull(CommandParamColumns.TYPE);
        if (intValue == null)
            throw new NullPointerException("The value of 'type' in the database was null, which is not allowed according to the model definition");
        return CommandParamType.values()[intValue];
    }
}
