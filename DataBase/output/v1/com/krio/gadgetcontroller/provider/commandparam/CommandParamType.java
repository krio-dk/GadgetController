package com.krio.gadgetcontroller.provider.commandparam;

/**
 * Possible values for the {@code type} column of the {@code command_param} table.
 */
public enum CommandParamType {
    /**
     * 
     */
    INEGER,

    /**
     * 
     */
    DOUBLE,

    /**
     * 
     */
    STRING,

    /**
     * 
     */
    BOOLEAN,

    /**
     * 
     */
    DATE,

}