package com.krio.gadgetcontroller.provider.commandparam;

import android.net.Uri;
import android.provider.BaseColumns;

import com.krio.gadgetcontroller.provider.DataProvider;
import com.krio.gadgetcontroller.provider.command.CommandColumns;
import com.krio.gadgetcontroller.provider.commandparam.CommandParamColumns;
import com.krio.gadgetcontroller.provider.panel.PanelColumns;
import com.krio.gadgetcontroller.provider.project.ProjectColumns;
import com.krio.gadgetcontroller.provider.widget.WidgetColumns;
import com.krio.gadgetcontroller.provider.widgetattr.WidgetAttrColumns;

/**
 * Columns for the {@code command_param} table.
 */
public class CommandParamColumns implements BaseColumns {
    public static final String TABLE_NAME = "command_param";
    public static final Uri CONTENT_URI = Uri.parse(DataProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    /**
     * Primary key.
     */
    public static final String _ID = BaseColumns._ID;

    public static final String NAME = "command_param__name";

    public static final String INTEGER_PARAM = "integer_param";

    public static final String DOUBLE_PARAM = "double_param";

    public static final String STRING_PARAM = "string_param";

    public static final String BOOLEAN_PARAM = "boolean_param";

    public static final String DATE_PARAM = "date_param";

    public static final String COMMAND_ID = "command_id";

    public static final String TYPE = "command_param__type";


    public static final String DEFAULT_ORDER = TABLE_NAME + "." +_ID;

    // @formatter:off
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            NAME,
            INTEGER_PARAM,
            DOUBLE_PARAM,
            STRING_PARAM,
            BOOLEAN_PARAM,
            DATE_PARAM,
            COMMAND_ID,
            TYPE
    };
    // @formatter:on

    public static boolean hasColumns(String[] projection) {
        if (projection == null) return true;
        for (String c : projection) {
            if (c.equals(NAME) || c.contains("." + NAME)) return true;
            if (c.equals(INTEGER_PARAM) || c.contains("." + INTEGER_PARAM)) return true;
            if (c.equals(DOUBLE_PARAM) || c.contains("." + DOUBLE_PARAM)) return true;
            if (c.equals(STRING_PARAM) || c.contains("." + STRING_PARAM)) return true;
            if (c.equals(BOOLEAN_PARAM) || c.contains("." + BOOLEAN_PARAM)) return true;
            if (c.equals(DATE_PARAM) || c.contains("." + DATE_PARAM)) return true;
            if (c.equals(COMMAND_ID) || c.contains("." + COMMAND_ID)) return true;
            if (c.equals(TYPE) || c.contains("." + TYPE)) return true;
        }
        return false;
    }

    public static final String PREFIX_COMMAND = TABLE_NAME + "__" + CommandColumns.TABLE_NAME;
}
