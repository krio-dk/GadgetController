package com.krio.gadgetcontroller.provider.commandparam;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.krio.gadgetcontroller.provider.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code command_param} table.
 */
public class CommandParamContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return CommandParamColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, @Nullable CommandParamSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(Context context, @Nullable CommandParamSelection where) {
        return context.getContentResolver().update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public CommandParamContentValues putName(@NonNull String value) {
        if (value == null) throw new IllegalArgumentException("name must not be null");
        mContentValues.put(CommandParamColumns.NAME, value);
        return this;
    }


    public CommandParamContentValues putIntegerParam(@Nullable Integer value) {
        mContentValues.put(CommandParamColumns.INTEGER_PARAM, value);
        return this;
    }

    public CommandParamContentValues putIntegerParamNull() {
        mContentValues.putNull(CommandParamColumns.INTEGER_PARAM);
        return this;
    }

    public CommandParamContentValues putDoubleParam(@Nullable Double value) {
        mContentValues.put(CommandParamColumns.DOUBLE_PARAM, value);
        return this;
    }

    public CommandParamContentValues putDoubleParamNull() {
        mContentValues.putNull(CommandParamColumns.DOUBLE_PARAM);
        return this;
    }

    public CommandParamContentValues putStringParam(@Nullable String value) {
        mContentValues.put(CommandParamColumns.STRING_PARAM, value);
        return this;
    }

    public CommandParamContentValues putStringParamNull() {
        mContentValues.putNull(CommandParamColumns.STRING_PARAM);
        return this;
    }

    public CommandParamContentValues putBooleanParam(@Nullable Boolean value) {
        mContentValues.put(CommandParamColumns.BOOLEAN_PARAM, value);
        return this;
    }

    public CommandParamContentValues putBooleanParamNull() {
        mContentValues.putNull(CommandParamColumns.BOOLEAN_PARAM);
        return this;
    }

    public CommandParamContentValues putDateParam(@Nullable Date value) {
        mContentValues.put(CommandParamColumns.DATE_PARAM, value == null ? null : value.getTime());
        return this;
    }

    public CommandParamContentValues putDateParamNull() {
        mContentValues.putNull(CommandParamColumns.DATE_PARAM);
        return this;
    }

    public CommandParamContentValues putDateParam(@Nullable Long value) {
        mContentValues.put(CommandParamColumns.DATE_PARAM, value);
        return this;
    }

    public CommandParamContentValues putCommandId(long value) {
        mContentValues.put(CommandParamColumns.COMMAND_ID, value);
        return this;
    }


    public CommandParamContentValues putType(@NonNull CommandParamType value) {
        if (value == null) throw new IllegalArgumentException("type must not be null");
        mContentValues.put(CommandParamColumns.TYPE, value.ordinal());
        return this;
    }

}
