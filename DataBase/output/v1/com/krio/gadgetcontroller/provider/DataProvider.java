package com.krio.gadgetcontroller.provider;

import java.util.Arrays;

import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.krio.gadgetcontroller.BuildConfig;
import com.krio.gadgetcontroller.provider.base.BaseContentProvider;
import com.krio.gadgetcontroller.provider.command.CommandColumns;
import com.krio.gadgetcontroller.provider.commandparam.CommandParamColumns;
import com.krio.gadgetcontroller.provider.panel.PanelColumns;
import com.krio.gadgetcontroller.provider.project.ProjectColumns;
import com.krio.gadgetcontroller.provider.widget.WidgetColumns;
import com.krio.gadgetcontroller.provider.widgetattr.WidgetAttrColumns;

public class DataProvider extends BaseContentProvider {
    private static final String TAG = DataProvider.class.getSimpleName();

    private static final boolean DEBUG = BuildConfig.DEBUG;

    private static final String TYPE_CURSOR_ITEM = "vnd.android.cursor.item/";
    private static final String TYPE_CURSOR_DIR = "vnd.android.cursor.dir/";

    public static final String AUTHORITY = "com.krio.gadgetcontroller.provider";
    public static final String CONTENT_URI_BASE = "content://" + AUTHORITY;

    private static final int URI_TYPE_COMMAND = 0;
    private static final int URI_TYPE_COMMAND_ID = 1;

    private static final int URI_TYPE_COMMAND_PARAM = 2;
    private static final int URI_TYPE_COMMAND_PARAM_ID = 3;

    private static final int URI_TYPE_PANEL = 4;
    private static final int URI_TYPE_PANEL_ID = 5;

    private static final int URI_TYPE_PROJECT = 6;
    private static final int URI_TYPE_PROJECT_ID = 7;

    private static final int URI_TYPE_WIDGET = 8;
    private static final int URI_TYPE_WIDGET_ID = 9;

    private static final int URI_TYPE_WIDGET_ATTR = 10;
    private static final int URI_TYPE_WIDGET_ATTR_ID = 11;



    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        URI_MATCHER.addURI(AUTHORITY, CommandColumns.TABLE_NAME, URI_TYPE_COMMAND);
        URI_MATCHER.addURI(AUTHORITY, CommandColumns.TABLE_NAME + "/#", URI_TYPE_COMMAND_ID);
        URI_MATCHER.addURI(AUTHORITY, CommandParamColumns.TABLE_NAME, URI_TYPE_COMMAND_PARAM);
        URI_MATCHER.addURI(AUTHORITY, CommandParamColumns.TABLE_NAME + "/#", URI_TYPE_COMMAND_PARAM_ID);
        URI_MATCHER.addURI(AUTHORITY, PanelColumns.TABLE_NAME, URI_TYPE_PANEL);
        URI_MATCHER.addURI(AUTHORITY, PanelColumns.TABLE_NAME + "/#", URI_TYPE_PANEL_ID);
        URI_MATCHER.addURI(AUTHORITY, ProjectColumns.TABLE_NAME, URI_TYPE_PROJECT);
        URI_MATCHER.addURI(AUTHORITY, ProjectColumns.TABLE_NAME + "/#", URI_TYPE_PROJECT_ID);
        URI_MATCHER.addURI(AUTHORITY, WidgetColumns.TABLE_NAME, URI_TYPE_WIDGET);
        URI_MATCHER.addURI(AUTHORITY, WidgetColumns.TABLE_NAME + "/#", URI_TYPE_WIDGET_ID);
        URI_MATCHER.addURI(AUTHORITY, WidgetAttrColumns.TABLE_NAME, URI_TYPE_WIDGET_ATTR);
        URI_MATCHER.addURI(AUTHORITY, WidgetAttrColumns.TABLE_NAME + "/#", URI_TYPE_WIDGET_ATTR_ID);
    }

    @Override
    protected SQLiteOpenHelper createSqLiteOpenHelper() {
        return DataSQLiteOpenHelper.getInstance(getContext());
    }

    @Override
    protected boolean hasDebug() {
        return DEBUG;
    }

    @Override
    public String getType(Uri uri) {
        int match = URI_MATCHER.match(uri);
        switch (match) {
            case URI_TYPE_COMMAND:
                return TYPE_CURSOR_DIR + CommandColumns.TABLE_NAME;
            case URI_TYPE_COMMAND_ID:
                return TYPE_CURSOR_ITEM + CommandColumns.TABLE_NAME;

            case URI_TYPE_COMMAND_PARAM:
                return TYPE_CURSOR_DIR + CommandParamColumns.TABLE_NAME;
            case URI_TYPE_COMMAND_PARAM_ID:
                return TYPE_CURSOR_ITEM + CommandParamColumns.TABLE_NAME;

            case URI_TYPE_PANEL:
                return TYPE_CURSOR_DIR + PanelColumns.TABLE_NAME;
            case URI_TYPE_PANEL_ID:
                return TYPE_CURSOR_ITEM + PanelColumns.TABLE_NAME;

            case URI_TYPE_PROJECT:
                return TYPE_CURSOR_DIR + ProjectColumns.TABLE_NAME;
            case URI_TYPE_PROJECT_ID:
                return TYPE_CURSOR_ITEM + ProjectColumns.TABLE_NAME;

            case URI_TYPE_WIDGET:
                return TYPE_CURSOR_DIR + WidgetColumns.TABLE_NAME;
            case URI_TYPE_WIDGET_ID:
                return TYPE_CURSOR_ITEM + WidgetColumns.TABLE_NAME;

            case URI_TYPE_WIDGET_ATTR:
                return TYPE_CURSOR_DIR + WidgetAttrColumns.TABLE_NAME;
            case URI_TYPE_WIDGET_ATTR_ID:
                return TYPE_CURSOR_ITEM + WidgetAttrColumns.TABLE_NAME;

        }
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if (DEBUG) Log.d(TAG, "insert uri=" + uri + " values=" + values);
        return super.insert(uri, values);
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        if (DEBUG) Log.d(TAG, "bulkInsert uri=" + uri + " values.length=" + values.length);
        return super.bulkInsert(uri, values);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (DEBUG) Log.d(TAG, "update uri=" + uri + " values=" + values + " selection=" + selection + " selectionArgs=" + Arrays.toString(selectionArgs));
        return super.update(uri, values, selection, selectionArgs);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        if (DEBUG) Log.d(TAG, "delete uri=" + uri + " selection=" + selection + " selectionArgs=" + Arrays.toString(selectionArgs));
        return super.delete(uri, selection, selectionArgs);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (DEBUG)
            Log.d(TAG, "query uri=" + uri + " selection=" + selection + " selectionArgs=" + Arrays.toString(selectionArgs) + " sortOrder=" + sortOrder
                    + " groupBy=" + uri.getQueryParameter(QUERY_GROUP_BY) + " having=" + uri.getQueryParameter(QUERY_HAVING) + " limit=" + uri.getQueryParameter(QUERY_LIMIT));
        return super.query(uri, projection, selection, selectionArgs, sortOrder);
    }

    @Override
    protected QueryParams getQueryParams(Uri uri, String selection, String[] projection) {
        QueryParams res = new QueryParams();
        String id = null;
        int matchedId = URI_MATCHER.match(uri);
        switch (matchedId) {
            case URI_TYPE_COMMAND:
            case URI_TYPE_COMMAND_ID:
                res.table = CommandColumns.TABLE_NAME;
                res.idColumn = CommandColumns._ID;
                res.tablesWithJoins = CommandColumns.TABLE_NAME;
                if (WidgetColumns.hasColumns(projection) || PanelColumns.hasColumns(projection) || ProjectColumns.hasColumns(projection)) {
                    res.tablesWithJoins += " LEFT OUTER JOIN " + WidgetColumns.TABLE_NAME + " AS " + CommandColumns.PREFIX_WIDGET + " ON " + CommandColumns.TABLE_NAME + "." + CommandColumns.WIDGET_ID + "=" + CommandColumns.PREFIX_WIDGET + "." + WidgetColumns._ID;
                }
                if (PanelColumns.hasColumns(projection) || ProjectColumns.hasColumns(projection)) {
                    res.tablesWithJoins += " LEFT OUTER JOIN " + PanelColumns.TABLE_NAME + " AS " + WidgetColumns.PREFIX_PANEL + " ON " + CommandColumns.PREFIX_WIDGET + "." + WidgetColumns.PANEL_ID + "=" + WidgetColumns.PREFIX_PANEL + "." + PanelColumns._ID;
                }
                if (ProjectColumns.hasColumns(projection)) {
                    res.tablesWithJoins += " LEFT OUTER JOIN " + ProjectColumns.TABLE_NAME + " AS " + PanelColumns.PREFIX_PROJECT + " ON " + WidgetColumns.PREFIX_PANEL + "." + PanelColumns.PROJECT_ID + "=" + PanelColumns.PREFIX_PROJECT + "." + ProjectColumns._ID;
                }
                res.orderBy = CommandColumns.DEFAULT_ORDER;
                break;

            case URI_TYPE_COMMAND_PARAM:
            case URI_TYPE_COMMAND_PARAM_ID:
                res.table = CommandParamColumns.TABLE_NAME;
                res.idColumn = CommandParamColumns._ID;
                res.tablesWithJoins = CommandParamColumns.TABLE_NAME;
                if (CommandColumns.hasColumns(projection) || WidgetColumns.hasColumns(projection) || PanelColumns.hasColumns(projection) || ProjectColumns.hasColumns(projection)) {
                    res.tablesWithJoins += " LEFT OUTER JOIN " + CommandColumns.TABLE_NAME + " AS " + CommandParamColumns.PREFIX_COMMAND + " ON " + CommandParamColumns.TABLE_NAME + "." + CommandParamColumns.COMMAND_ID + "=" + CommandParamColumns.PREFIX_COMMAND + "." + CommandColumns._ID;
                }
                if (WidgetColumns.hasColumns(projection) || PanelColumns.hasColumns(projection) || ProjectColumns.hasColumns(projection)) {
                    res.tablesWithJoins += " LEFT OUTER JOIN " + WidgetColumns.TABLE_NAME + " AS " + CommandColumns.PREFIX_WIDGET + " ON " + CommandParamColumns.PREFIX_COMMAND + "." + CommandColumns.WIDGET_ID + "=" + CommandColumns.PREFIX_WIDGET + "." + WidgetColumns._ID;
                }
                if (PanelColumns.hasColumns(projection) || ProjectColumns.hasColumns(projection)) {
                    res.tablesWithJoins += " LEFT OUTER JOIN " + PanelColumns.TABLE_NAME + " AS " + WidgetColumns.PREFIX_PANEL + " ON " + CommandColumns.PREFIX_WIDGET + "." + WidgetColumns.PANEL_ID + "=" + WidgetColumns.PREFIX_PANEL + "." + PanelColumns._ID;
                }
                if (ProjectColumns.hasColumns(projection)) {
                    res.tablesWithJoins += " LEFT OUTER JOIN " + ProjectColumns.TABLE_NAME + " AS " + PanelColumns.PREFIX_PROJECT + " ON " + WidgetColumns.PREFIX_PANEL + "." + PanelColumns.PROJECT_ID + "=" + PanelColumns.PREFIX_PROJECT + "." + ProjectColumns._ID;
                }
                res.orderBy = CommandParamColumns.DEFAULT_ORDER;
                break;

            case URI_TYPE_PANEL:
            case URI_TYPE_PANEL_ID:
                res.table = PanelColumns.TABLE_NAME;
                res.idColumn = PanelColumns._ID;
                res.tablesWithJoins = PanelColumns.TABLE_NAME;
                if (ProjectColumns.hasColumns(projection)) {
                    res.tablesWithJoins += " LEFT OUTER JOIN " + ProjectColumns.TABLE_NAME + " AS " + PanelColumns.PREFIX_PROJECT + " ON " + PanelColumns.TABLE_NAME + "." + PanelColumns.PROJECT_ID + "=" + PanelColumns.PREFIX_PROJECT + "." + ProjectColumns._ID;
                }
                res.orderBy = PanelColumns.DEFAULT_ORDER;
                break;

            case URI_TYPE_PROJECT:
            case URI_TYPE_PROJECT_ID:
                res.table = ProjectColumns.TABLE_NAME;
                res.idColumn = ProjectColumns._ID;
                res.tablesWithJoins = ProjectColumns.TABLE_NAME;
                res.orderBy = ProjectColumns.DEFAULT_ORDER;
                break;

            case URI_TYPE_WIDGET:
            case URI_TYPE_WIDGET_ID:
                res.table = WidgetColumns.TABLE_NAME;
                res.idColumn = WidgetColumns._ID;
                res.tablesWithJoins = WidgetColumns.TABLE_NAME;
                if (PanelColumns.hasColumns(projection) || ProjectColumns.hasColumns(projection)) {
                    res.tablesWithJoins += " LEFT OUTER JOIN " + PanelColumns.TABLE_NAME + " AS " + WidgetColumns.PREFIX_PANEL + " ON " + WidgetColumns.TABLE_NAME + "." + WidgetColumns.PANEL_ID + "=" + WidgetColumns.PREFIX_PANEL + "." + PanelColumns._ID;
                }
                if (ProjectColumns.hasColumns(projection)) {
                    res.tablesWithJoins += " LEFT OUTER JOIN " + ProjectColumns.TABLE_NAME + " AS " + PanelColumns.PREFIX_PROJECT + " ON " + WidgetColumns.PREFIX_PANEL + "." + PanelColumns.PROJECT_ID + "=" + PanelColumns.PREFIX_PROJECT + "." + ProjectColumns._ID;
                }
                res.orderBy = WidgetColumns.DEFAULT_ORDER;
                break;

            case URI_TYPE_WIDGET_ATTR:
            case URI_TYPE_WIDGET_ATTR_ID:
                res.table = WidgetAttrColumns.TABLE_NAME;
                res.idColumn = WidgetAttrColumns._ID;
                res.tablesWithJoins = WidgetAttrColumns.TABLE_NAME;
                if (WidgetColumns.hasColumns(projection) || PanelColumns.hasColumns(projection) || ProjectColumns.hasColumns(projection)) {
                    res.tablesWithJoins += " LEFT OUTER JOIN " + WidgetColumns.TABLE_NAME + " AS " + WidgetAttrColumns.PREFIX_WIDGET + " ON " + WidgetAttrColumns.TABLE_NAME + "." + WidgetAttrColumns.WIDGET_ID + "=" + WidgetAttrColumns.PREFIX_WIDGET + "." + WidgetColumns._ID;
                }
                if (PanelColumns.hasColumns(projection) || ProjectColumns.hasColumns(projection)) {
                    res.tablesWithJoins += " LEFT OUTER JOIN " + PanelColumns.TABLE_NAME + " AS " + WidgetColumns.PREFIX_PANEL + " ON " + WidgetAttrColumns.PREFIX_WIDGET + "." + WidgetColumns.PANEL_ID + "=" + WidgetColumns.PREFIX_PANEL + "." + PanelColumns._ID;
                }
                if (ProjectColumns.hasColumns(projection)) {
                    res.tablesWithJoins += " LEFT OUTER JOIN " + ProjectColumns.TABLE_NAME + " AS " + PanelColumns.PREFIX_PROJECT + " ON " + WidgetColumns.PREFIX_PANEL + "." + PanelColumns.PROJECT_ID + "=" + PanelColumns.PREFIX_PROJECT + "." + ProjectColumns._ID;
                }
                res.orderBy = WidgetAttrColumns.DEFAULT_ORDER;
                break;

            default:
                throw new IllegalArgumentException("The uri '" + uri + "' is not supported by this ContentProvider");
        }

        switch (matchedId) {
            case URI_TYPE_COMMAND_ID:
            case URI_TYPE_COMMAND_PARAM_ID:
            case URI_TYPE_PANEL_ID:
            case URI_TYPE_PROJECT_ID:
            case URI_TYPE_WIDGET_ID:
            case URI_TYPE_WIDGET_ATTR_ID:
                id = uri.getLastPathSegment();
        }
        if (id != null) {
            if (selection != null) {
                res.selection = res.table + "." + res.idColumn + "=" + id + " and (" + selection + ")";
            } else {
                res.selection = res.table + "." + res.idColumn + "=" + id;
            }
        } else {
            res.selection = selection;
        }
        return res;
    }
}
