package com.krio.gadgetcontroller.provider.project;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import com.krio.gadgetcontroller.provider.base.AbstractSelection;

/**
 * Selection for the {@code project} table.
 */
public class ProjectSelection extends AbstractSelection<ProjectSelection> {
    @Override
    protected Uri baseUri() {
        return ProjectColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code ProjectCursor} object, which is positioned before the first entry, or null.
     */
    public ProjectCursor query(ContentResolver contentResolver, String[] projection) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new ProjectCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, null)}.
     */
    public ProjectCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null);
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param context The context to use for the query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code ProjectCursor} object, which is positioned before the first entry, or null.
     */
    public ProjectCursor query(Context context, String[] projection) {
        Cursor cursor = context.getContentResolver().query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new ProjectCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(context, null)}.
     */
    public ProjectCursor query(Context context) {
        return query(context, null);
    }


    public ProjectSelection id(long... value) {
        addEquals("project." + ProjectColumns._ID, toObjectArray(value));
        return this;
    }

    public ProjectSelection idNot(long... value) {
        addNotEquals("project." + ProjectColumns._ID, toObjectArray(value));
        return this;
    }

    public ProjectSelection orderById(boolean desc) {
        orderBy("project." + ProjectColumns._ID, desc);
        return this;
    }

    public ProjectSelection orderById() {
        return orderById(false);
    }

    public ProjectSelection name(String... value) {
        addEquals(ProjectColumns.NAME, value);
        return this;
    }

    public ProjectSelection nameNot(String... value) {
        addNotEquals(ProjectColumns.NAME, value);
        return this;
    }

    public ProjectSelection nameLike(String... value) {
        addLike(ProjectColumns.NAME, value);
        return this;
    }

    public ProjectSelection nameContains(String... value) {
        addContains(ProjectColumns.NAME, value);
        return this;
    }

    public ProjectSelection nameStartsWith(String... value) {
        addStartsWith(ProjectColumns.NAME, value);
        return this;
    }

    public ProjectSelection nameEndsWith(String... value) {
        addEndsWith(ProjectColumns.NAME, value);
        return this;
    }

    public ProjectSelection orderByName(boolean desc) {
        orderBy(ProjectColumns.NAME, desc);
        return this;
    }

    public ProjectSelection orderByName() {
        orderBy(ProjectColumns.NAME, false);
        return this;
    }

    public ProjectSelection token(String... value) {
        addEquals(ProjectColumns.TOKEN, value);
        return this;
    }

    public ProjectSelection tokenNot(String... value) {
        addNotEquals(ProjectColumns.TOKEN, value);
        return this;
    }

    public ProjectSelection tokenLike(String... value) {
        addLike(ProjectColumns.TOKEN, value);
        return this;
    }

    public ProjectSelection tokenContains(String... value) {
        addContains(ProjectColumns.TOKEN, value);
        return this;
    }

    public ProjectSelection tokenStartsWith(String... value) {
        addStartsWith(ProjectColumns.TOKEN, value);
        return this;
    }

    public ProjectSelection tokenEndsWith(String... value) {
        addEndsWith(ProjectColumns.TOKEN, value);
        return this;
    }

    public ProjectSelection orderByToken(boolean desc) {
        orderBy(ProjectColumns.TOKEN, desc);
        return this;
    }

    public ProjectSelection orderByToken() {
        orderBy(ProjectColumns.TOKEN, false);
        return this;
    }

    public ProjectSelection positionOnList(Integer... value) {
        addEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public ProjectSelection positionOnListNot(Integer... value) {
        addNotEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public ProjectSelection positionOnListGt(int value) {
        addGreaterThan(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public ProjectSelection positionOnListGtEq(int value) {
        addGreaterThanOrEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public ProjectSelection positionOnListLt(int value) {
        addLessThan(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public ProjectSelection positionOnListLtEq(int value) {
        addLessThanOrEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public ProjectSelection orderByPositionOnList(boolean desc) {
        orderBy(ProjectColumns.POSITION_ON_LIST, desc);
        return this;
    }

    public ProjectSelection orderByPositionOnList() {
        orderBy(ProjectColumns.POSITION_ON_LIST, false);
        return this;
    }
}
