package com.krio.gadgetcontroller.provider;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.DefaultDatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import com.krio.gadgetcontroller.BuildConfig;
import com.krio.gadgetcontroller.provider.command.CommandColumns;
import com.krio.gadgetcontroller.provider.commandparam.CommandParamColumns;
import com.krio.gadgetcontroller.provider.panel.PanelColumns;
import com.krio.gadgetcontroller.provider.project.ProjectColumns;
import com.krio.gadgetcontroller.provider.widget.WidgetColumns;
import com.krio.gadgetcontroller.provider.widgetattr.WidgetAttrColumns;

public class DataSQLiteOpenHelper extends SQLiteOpenHelper {
    private static final String TAG = DataSQLiteOpenHelper.class.getSimpleName();

    public static final String DATABASE_FILE_NAME = "gadgetcontroller_se.db";
    private static final int DATABASE_VERSION = 1;
    private static DataSQLiteOpenHelper sInstance;
    private final Context mContext;
    private final DataSQLiteOpenHelperCallbacks mOpenHelperCallbacks;

    // @formatter:off
    public static final String SQL_CREATE_TABLE_COMMAND = "CREATE TABLE IF NOT EXISTS "
            + CommandColumns.TABLE_NAME + " ( "
            + CommandColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CommandColumns.TYPE + " INTEGER NOT NULL, "
            + CommandColumns.WIDGET_ID + " INTEGER NOT NULL, "
            + CommandColumns.CMD + " TEXT NOT NULL "
            + ", CONSTRAINT fk_widget_id FOREIGN KEY (" + CommandColumns.WIDGET_ID + ") REFERENCES widget (_id) ON DELETE CASCADE"
            + " );";

    public static final String SQL_CREATE_TABLE_COMMAND_PARAM = "CREATE TABLE IF NOT EXISTS "
            + CommandParamColumns.TABLE_NAME + " ( "
            + CommandParamColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CommandParamColumns.NAME + " TEXT NOT NULL, "
            + CommandParamColumns.INTEGER_PARAM + " INTEGER, "
            + CommandParamColumns.DOUBLE_PARAM + " REAL, "
            + CommandParamColumns.STRING_PARAM + " TEXT, "
            + CommandParamColumns.BOOLEAN_PARAM + " INTEGER, "
            + CommandParamColumns.DATE_PARAM + " INTEGER, "
            + CommandParamColumns.COMMAND_ID + " INTEGER NOT NULL, "
            + CommandParamColumns.TYPE + " INTEGER NOT NULL "
            + ", CONSTRAINT fk_command_id FOREIGN KEY (" + CommandParamColumns.COMMAND_ID + ") REFERENCES command (_id) ON DELETE CASCADE"
            + " );";

    public static final String SQL_CREATE_TABLE_PANEL = "CREATE TABLE IF NOT EXISTS "
            + PanelColumns.TABLE_NAME + " ( "
            + PanelColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + PanelColumns.PROJECT_ID + " INTEGER NOT NULL, "
            + PanelColumns.CAPTION + " TEXT NOT NULL, "
            + PanelColumns.POSITION_ON_SCREEN + " INTEGER "
            + ", CONSTRAINT fk_project_id FOREIGN KEY (" + PanelColumns.PROJECT_ID + ") REFERENCES project (_id) ON DELETE CASCADE"
            + " );";

    public static final String SQL_CREATE_TABLE_PROJECT = "CREATE TABLE IF NOT EXISTS "
            + ProjectColumns.TABLE_NAME + " ( "
            + ProjectColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + ProjectColumns.NAME + " TEXT NOT NULL, "
            + ProjectColumns.TOKEN + " TEXT NOT NULL, "
            + ProjectColumns.POSITION_ON_LIST + " INTEGER "
            + " );";

    public static final String SQL_CREATE_TABLE_WIDGET = "CREATE TABLE IF NOT EXISTS "
            + WidgetColumns.TABLE_NAME + " ( "
            + WidgetColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + WidgetColumns.POSITION_ON_PANEL + " INTEGER, "
            + WidgetColumns.CAPTION + " TEXT, "
            + WidgetColumns.PANEL_ID + " INTEGER NOT NULL, "
            + WidgetColumns.TYPE + " INTEGER NOT NULL "
            + ", CONSTRAINT fk_panel_id FOREIGN KEY (" + WidgetColumns.PANEL_ID + ") REFERENCES panel (_id) ON DELETE CASCADE"
            + " );";

    public static final String SQL_CREATE_TABLE_WIDGET_ATTR = "CREATE TABLE IF NOT EXISTS "
            + WidgetAttrColumns.TABLE_NAME + " ( "
            + WidgetAttrColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + WidgetAttrColumns.NAME + " TEXT NOT NULL, "
            + WidgetAttrColumns.INTEGER_ATTR + " INTEGER, "
            + WidgetAttrColumns.DOUBLE_ATTR + " REAL, "
            + WidgetAttrColumns.STRING_ATTR + " TEXT, "
            + WidgetAttrColumns.BOOLEAN_ATTR + " INTEGER, "
            + WidgetAttrColumns.DATE_ATTR + " INTEGER, "
            + WidgetAttrColumns.WIDGET_ID + " INTEGER NOT NULL, "
            + WidgetAttrColumns.TYPE + " INTEGER NOT NULL "
            + ", CONSTRAINT fk_widget_id FOREIGN KEY (" + WidgetAttrColumns.WIDGET_ID + ") REFERENCES widget (_id) ON DELETE CASCADE"
            + " );";

    // @formatter:on

    public static DataSQLiteOpenHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = newInstance(context.getApplicationContext());
        }
        return sInstance;
    }

    private static DataSQLiteOpenHelper newInstance(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            return newInstancePreHoneycomb(context);
        }
        return newInstancePostHoneycomb(context);
    }


    /*
     * Pre Honeycomb.
     */
    private static DataSQLiteOpenHelper newInstancePreHoneycomb(Context context) {
        return new DataSQLiteOpenHelper(context);
    }

    private DataSQLiteOpenHelper(Context context) {
        super(context, DATABASE_FILE_NAME, null, DATABASE_VERSION);
        mContext = context;
        mOpenHelperCallbacks = new DataSQLiteOpenHelperCallbacks();
    }


    /*
     * Post Honeycomb.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static DataSQLiteOpenHelper newInstancePostHoneycomb(Context context) {
        return new DataSQLiteOpenHelper(context, new DefaultDatabaseErrorHandler());
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private DataSQLiteOpenHelper(Context context, DatabaseErrorHandler errorHandler) {
        super(context, DATABASE_FILE_NAME, null, DATABASE_VERSION, errorHandler);
        mContext = context;
        mOpenHelperCallbacks = new DataSQLiteOpenHelperCallbacks();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        if (BuildConfig.DEBUG) Log.d(TAG, "onCreate");
        mOpenHelperCallbacks.onPreCreate(mContext, db);
        db.execSQL(SQL_CREATE_TABLE_COMMAND);
        db.execSQL(SQL_CREATE_TABLE_COMMAND_PARAM);
        db.execSQL(SQL_CREATE_TABLE_PANEL);
        db.execSQL(SQL_CREATE_TABLE_PROJECT);
        db.execSQL(SQL_CREATE_TABLE_WIDGET);
        db.execSQL(SQL_CREATE_TABLE_WIDGET_ATTR);
        mOpenHelperCallbacks.onPostCreate(mContext, db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            setForeignKeyConstraintsEnabled(db);
        }
        mOpenHelperCallbacks.onOpen(mContext, db);
    }

    private void setForeignKeyConstraintsEnabled(SQLiteDatabase db) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            setForeignKeyConstraintsEnabledPreJellyBean(db);
        } else {
            setForeignKeyConstraintsEnabledPostJellyBean(db);
        }
    }

    private void setForeignKeyConstraintsEnabledPreJellyBean(SQLiteDatabase db) {
        db.execSQL("PRAGMA foreign_keys=ON;");
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setForeignKeyConstraintsEnabledPostJellyBean(SQLiteDatabase db) {
        db.setForeignKeyConstraintsEnabled(true);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        mOpenHelperCallbacks.onUpgrade(mContext, db, oldVersion, newVersion);
    }
}
