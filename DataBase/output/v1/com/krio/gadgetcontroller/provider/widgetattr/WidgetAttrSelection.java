package com.krio.gadgetcontroller.provider.widgetattr;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import com.krio.gadgetcontroller.provider.base.AbstractSelection;
import com.krio.gadgetcontroller.provider.widget.*;
import com.krio.gadgetcontroller.provider.panel.*;
import com.krio.gadgetcontroller.provider.project.*;

/**
 * Selection for the {@code widget_attr} table.
 */
public class WidgetAttrSelection extends AbstractSelection<WidgetAttrSelection> {
    @Override
    protected Uri baseUri() {
        return WidgetAttrColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code WidgetAttrCursor} object, which is positioned before the first entry, or null.
     */
    public WidgetAttrCursor query(ContentResolver contentResolver, String[] projection) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new WidgetAttrCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, null)}.
     */
    public WidgetAttrCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null);
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param context The context to use for the query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code WidgetAttrCursor} object, which is positioned before the first entry, or null.
     */
    public WidgetAttrCursor query(Context context, String[] projection) {
        Cursor cursor = context.getContentResolver().query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new WidgetAttrCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(context, null)}.
     */
    public WidgetAttrCursor query(Context context) {
        return query(context, null);
    }


    public WidgetAttrSelection id(long... value) {
        addEquals("widget_attr." + WidgetAttrColumns._ID, toObjectArray(value));
        return this;
    }

    public WidgetAttrSelection idNot(long... value) {
        addNotEquals("widget_attr." + WidgetAttrColumns._ID, toObjectArray(value));
        return this;
    }

    public WidgetAttrSelection orderById(boolean desc) {
        orderBy("widget_attr." + WidgetAttrColumns._ID, desc);
        return this;
    }

    public WidgetAttrSelection orderById() {
        return orderById(false);
    }

    public WidgetAttrSelection name(String... value) {
        addEquals(WidgetAttrColumns.NAME, value);
        return this;
    }

    public WidgetAttrSelection nameNot(String... value) {
        addNotEquals(WidgetAttrColumns.NAME, value);
        return this;
    }

    public WidgetAttrSelection nameLike(String... value) {
        addLike(WidgetAttrColumns.NAME, value);
        return this;
    }

    public WidgetAttrSelection nameContains(String... value) {
        addContains(WidgetAttrColumns.NAME, value);
        return this;
    }

    public WidgetAttrSelection nameStartsWith(String... value) {
        addStartsWith(WidgetAttrColumns.NAME, value);
        return this;
    }

    public WidgetAttrSelection nameEndsWith(String... value) {
        addEndsWith(WidgetAttrColumns.NAME, value);
        return this;
    }

    public WidgetAttrSelection orderByName(boolean desc) {
        orderBy(WidgetAttrColumns.NAME, desc);
        return this;
    }

    public WidgetAttrSelection orderByName() {
        orderBy(WidgetAttrColumns.NAME, false);
        return this;
    }

    public WidgetAttrSelection integerAttr(Integer... value) {
        addEquals(WidgetAttrColumns.INTEGER_ATTR, value);
        return this;
    }

    public WidgetAttrSelection integerAttrNot(Integer... value) {
        addNotEquals(WidgetAttrColumns.INTEGER_ATTR, value);
        return this;
    }

    public WidgetAttrSelection integerAttrGt(int value) {
        addGreaterThan(WidgetAttrColumns.INTEGER_ATTR, value);
        return this;
    }

    public WidgetAttrSelection integerAttrGtEq(int value) {
        addGreaterThanOrEquals(WidgetAttrColumns.INTEGER_ATTR, value);
        return this;
    }

    public WidgetAttrSelection integerAttrLt(int value) {
        addLessThan(WidgetAttrColumns.INTEGER_ATTR, value);
        return this;
    }

    public WidgetAttrSelection integerAttrLtEq(int value) {
        addLessThanOrEquals(WidgetAttrColumns.INTEGER_ATTR, value);
        return this;
    }

    public WidgetAttrSelection orderByIntegerAttr(boolean desc) {
        orderBy(WidgetAttrColumns.INTEGER_ATTR, desc);
        return this;
    }

    public WidgetAttrSelection orderByIntegerAttr() {
        orderBy(WidgetAttrColumns.INTEGER_ATTR, false);
        return this;
    }

    public WidgetAttrSelection doubleAttr(Double... value) {
        addEquals(WidgetAttrColumns.DOUBLE_ATTR, value);
        return this;
    }

    public WidgetAttrSelection doubleAttrNot(Double... value) {
        addNotEquals(WidgetAttrColumns.DOUBLE_ATTR, value);
        return this;
    }

    public WidgetAttrSelection doubleAttrGt(double value) {
        addGreaterThan(WidgetAttrColumns.DOUBLE_ATTR, value);
        return this;
    }

    public WidgetAttrSelection doubleAttrGtEq(double value) {
        addGreaterThanOrEquals(WidgetAttrColumns.DOUBLE_ATTR, value);
        return this;
    }

    public WidgetAttrSelection doubleAttrLt(double value) {
        addLessThan(WidgetAttrColumns.DOUBLE_ATTR, value);
        return this;
    }

    public WidgetAttrSelection doubleAttrLtEq(double value) {
        addLessThanOrEquals(WidgetAttrColumns.DOUBLE_ATTR, value);
        return this;
    }

    public WidgetAttrSelection orderByDoubleAttr(boolean desc) {
        orderBy(WidgetAttrColumns.DOUBLE_ATTR, desc);
        return this;
    }

    public WidgetAttrSelection orderByDoubleAttr() {
        orderBy(WidgetAttrColumns.DOUBLE_ATTR, false);
        return this;
    }

    public WidgetAttrSelection stringAttr(String... value) {
        addEquals(WidgetAttrColumns.STRING_ATTR, value);
        return this;
    }

    public WidgetAttrSelection stringAttrNot(String... value) {
        addNotEquals(WidgetAttrColumns.STRING_ATTR, value);
        return this;
    }

    public WidgetAttrSelection stringAttrLike(String... value) {
        addLike(WidgetAttrColumns.STRING_ATTR, value);
        return this;
    }

    public WidgetAttrSelection stringAttrContains(String... value) {
        addContains(WidgetAttrColumns.STRING_ATTR, value);
        return this;
    }

    public WidgetAttrSelection stringAttrStartsWith(String... value) {
        addStartsWith(WidgetAttrColumns.STRING_ATTR, value);
        return this;
    }

    public WidgetAttrSelection stringAttrEndsWith(String... value) {
        addEndsWith(WidgetAttrColumns.STRING_ATTR, value);
        return this;
    }

    public WidgetAttrSelection orderByStringAttr(boolean desc) {
        orderBy(WidgetAttrColumns.STRING_ATTR, desc);
        return this;
    }

    public WidgetAttrSelection orderByStringAttr() {
        orderBy(WidgetAttrColumns.STRING_ATTR, false);
        return this;
    }

    public WidgetAttrSelection booleanAttr(Boolean value) {
        addEquals(WidgetAttrColumns.BOOLEAN_ATTR, toObjectArray(value));
        return this;
    }

    public WidgetAttrSelection orderByBooleanAttr(boolean desc) {
        orderBy(WidgetAttrColumns.BOOLEAN_ATTR, desc);
        return this;
    }

    public WidgetAttrSelection orderByBooleanAttr() {
        orderBy(WidgetAttrColumns.BOOLEAN_ATTR, false);
        return this;
    }

    public WidgetAttrSelection dateAttr(Date... value) {
        addEquals(WidgetAttrColumns.DATE_ATTR, value);
        return this;
    }

    public WidgetAttrSelection dateAttrNot(Date... value) {
        addNotEquals(WidgetAttrColumns.DATE_ATTR, value);
        return this;
    }

    public WidgetAttrSelection dateAttr(Long... value) {
        addEquals(WidgetAttrColumns.DATE_ATTR, value);
        return this;
    }

    public WidgetAttrSelection dateAttrAfter(Date value) {
        addGreaterThan(WidgetAttrColumns.DATE_ATTR, value);
        return this;
    }

    public WidgetAttrSelection dateAttrAfterEq(Date value) {
        addGreaterThanOrEquals(WidgetAttrColumns.DATE_ATTR, value);
        return this;
    }

    public WidgetAttrSelection dateAttrBefore(Date value) {
        addLessThan(WidgetAttrColumns.DATE_ATTR, value);
        return this;
    }

    public WidgetAttrSelection dateAttrBeforeEq(Date value) {
        addLessThanOrEquals(WidgetAttrColumns.DATE_ATTR, value);
        return this;
    }

    public WidgetAttrSelection orderByDateAttr(boolean desc) {
        orderBy(WidgetAttrColumns.DATE_ATTR, desc);
        return this;
    }

    public WidgetAttrSelection orderByDateAttr() {
        orderBy(WidgetAttrColumns.DATE_ATTR, false);
        return this;
    }

    public WidgetAttrSelection widgetId(long... value) {
        addEquals(WidgetAttrColumns.WIDGET_ID, toObjectArray(value));
        return this;
    }

    public WidgetAttrSelection widgetIdNot(long... value) {
        addNotEquals(WidgetAttrColumns.WIDGET_ID, toObjectArray(value));
        return this;
    }

    public WidgetAttrSelection widgetIdGt(long value) {
        addGreaterThan(WidgetAttrColumns.WIDGET_ID, value);
        return this;
    }

    public WidgetAttrSelection widgetIdGtEq(long value) {
        addGreaterThanOrEquals(WidgetAttrColumns.WIDGET_ID, value);
        return this;
    }

    public WidgetAttrSelection widgetIdLt(long value) {
        addLessThan(WidgetAttrColumns.WIDGET_ID, value);
        return this;
    }

    public WidgetAttrSelection widgetIdLtEq(long value) {
        addLessThanOrEquals(WidgetAttrColumns.WIDGET_ID, value);
        return this;
    }

    public WidgetAttrSelection orderByWidgetId(boolean desc) {
        orderBy(WidgetAttrColumns.WIDGET_ID, desc);
        return this;
    }

    public WidgetAttrSelection orderByWidgetId() {
        orderBy(WidgetAttrColumns.WIDGET_ID, false);
        return this;
    }

    public WidgetAttrSelection widgetPositionOnPanel(Integer... value) {
        addEquals(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public WidgetAttrSelection widgetPositionOnPanelNot(Integer... value) {
        addNotEquals(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public WidgetAttrSelection widgetPositionOnPanelGt(int value) {
        addGreaterThan(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public WidgetAttrSelection widgetPositionOnPanelGtEq(int value) {
        addGreaterThanOrEquals(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public WidgetAttrSelection widgetPositionOnPanelLt(int value) {
        addLessThan(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public WidgetAttrSelection widgetPositionOnPanelLtEq(int value) {
        addLessThanOrEquals(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public WidgetAttrSelection orderByWidgetPositionOnPanel(boolean desc) {
        orderBy(WidgetColumns.POSITION_ON_PANEL, desc);
        return this;
    }

    public WidgetAttrSelection orderByWidgetPositionOnPanel() {
        orderBy(WidgetColumns.POSITION_ON_PANEL, false);
        return this;
    }

    public WidgetAttrSelection widgetCaption(String... value) {
        addEquals(WidgetColumns.CAPTION, value);
        return this;
    }

    public WidgetAttrSelection widgetCaptionNot(String... value) {
        addNotEquals(WidgetColumns.CAPTION, value);
        return this;
    }

    public WidgetAttrSelection widgetCaptionLike(String... value) {
        addLike(WidgetColumns.CAPTION, value);
        return this;
    }

    public WidgetAttrSelection widgetCaptionContains(String... value) {
        addContains(WidgetColumns.CAPTION, value);
        return this;
    }

    public WidgetAttrSelection widgetCaptionStartsWith(String... value) {
        addStartsWith(WidgetColumns.CAPTION, value);
        return this;
    }

    public WidgetAttrSelection widgetCaptionEndsWith(String... value) {
        addEndsWith(WidgetColumns.CAPTION, value);
        return this;
    }

    public WidgetAttrSelection orderByWidgetCaption(boolean desc) {
        orderBy(WidgetColumns.CAPTION, desc);
        return this;
    }

    public WidgetAttrSelection orderByWidgetCaption() {
        orderBy(WidgetColumns.CAPTION, false);
        return this;
    }

    public WidgetAttrSelection widgetPanelId(long... value) {
        addEquals(WidgetColumns.PANEL_ID, toObjectArray(value));
        return this;
    }

    public WidgetAttrSelection widgetPanelIdNot(long... value) {
        addNotEquals(WidgetColumns.PANEL_ID, toObjectArray(value));
        return this;
    }

    public WidgetAttrSelection widgetPanelIdGt(long value) {
        addGreaterThan(WidgetColumns.PANEL_ID, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelIdGtEq(long value) {
        addGreaterThanOrEquals(WidgetColumns.PANEL_ID, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelIdLt(long value) {
        addLessThan(WidgetColumns.PANEL_ID, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelIdLtEq(long value) {
        addLessThanOrEquals(WidgetColumns.PANEL_ID, value);
        return this;
    }

    public WidgetAttrSelection orderByWidgetPanelId(boolean desc) {
        orderBy(WidgetColumns.PANEL_ID, desc);
        return this;
    }

    public WidgetAttrSelection orderByWidgetPanelId() {
        orderBy(WidgetColumns.PANEL_ID, false);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectId(long... value) {
        addEquals(PanelColumns.PROJECT_ID, toObjectArray(value));
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectIdNot(long... value) {
        addNotEquals(PanelColumns.PROJECT_ID, toObjectArray(value));
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectIdGt(long value) {
        addGreaterThan(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectIdGtEq(long value) {
        addGreaterThanOrEquals(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectIdLt(long value) {
        addLessThan(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectIdLtEq(long value) {
        addLessThanOrEquals(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public WidgetAttrSelection orderByWidgetPanelProjectId(boolean desc) {
        orderBy(PanelColumns.PROJECT_ID, desc);
        return this;
    }

    public WidgetAttrSelection orderByWidgetPanelProjectId() {
        orderBy(PanelColumns.PROJECT_ID, false);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectName(String... value) {
        addEquals(ProjectColumns.NAME, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectNameNot(String... value) {
        addNotEquals(ProjectColumns.NAME, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectNameLike(String... value) {
        addLike(ProjectColumns.NAME, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectNameContains(String... value) {
        addContains(ProjectColumns.NAME, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectNameStartsWith(String... value) {
        addStartsWith(ProjectColumns.NAME, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectNameEndsWith(String... value) {
        addEndsWith(ProjectColumns.NAME, value);
        return this;
    }

    public WidgetAttrSelection orderByWidgetPanelProjectName(boolean desc) {
        orderBy(ProjectColumns.NAME, desc);
        return this;
    }

    public WidgetAttrSelection orderByWidgetPanelProjectName() {
        orderBy(ProjectColumns.NAME, false);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectToken(String... value) {
        addEquals(ProjectColumns.TOKEN, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectTokenNot(String... value) {
        addNotEquals(ProjectColumns.TOKEN, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectTokenLike(String... value) {
        addLike(ProjectColumns.TOKEN, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectTokenContains(String... value) {
        addContains(ProjectColumns.TOKEN, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectTokenStartsWith(String... value) {
        addStartsWith(ProjectColumns.TOKEN, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectTokenEndsWith(String... value) {
        addEndsWith(ProjectColumns.TOKEN, value);
        return this;
    }

    public WidgetAttrSelection orderByWidgetPanelProjectToken(boolean desc) {
        orderBy(ProjectColumns.TOKEN, desc);
        return this;
    }

    public WidgetAttrSelection orderByWidgetPanelProjectToken() {
        orderBy(ProjectColumns.TOKEN, false);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectPositionOnList(Integer... value) {
        addEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectPositionOnListNot(Integer... value) {
        addNotEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectPositionOnListGt(int value) {
        addGreaterThan(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectPositionOnListGtEq(int value) {
        addGreaterThanOrEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectPositionOnListLt(int value) {
        addLessThan(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelProjectPositionOnListLtEq(int value) {
        addLessThanOrEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public WidgetAttrSelection orderByWidgetPanelProjectPositionOnList(boolean desc) {
        orderBy(ProjectColumns.POSITION_ON_LIST, desc);
        return this;
    }

    public WidgetAttrSelection orderByWidgetPanelProjectPositionOnList() {
        orderBy(ProjectColumns.POSITION_ON_LIST, false);
        return this;
    }

    public WidgetAttrSelection widgetPanelCaption(String... value) {
        addEquals(PanelColumns.CAPTION, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelCaptionNot(String... value) {
        addNotEquals(PanelColumns.CAPTION, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelCaptionLike(String... value) {
        addLike(PanelColumns.CAPTION, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelCaptionContains(String... value) {
        addContains(PanelColumns.CAPTION, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelCaptionStartsWith(String... value) {
        addStartsWith(PanelColumns.CAPTION, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelCaptionEndsWith(String... value) {
        addEndsWith(PanelColumns.CAPTION, value);
        return this;
    }

    public WidgetAttrSelection orderByWidgetPanelCaption(boolean desc) {
        orderBy(PanelColumns.CAPTION, desc);
        return this;
    }

    public WidgetAttrSelection orderByWidgetPanelCaption() {
        orderBy(PanelColumns.CAPTION, false);
        return this;
    }

    public WidgetAttrSelection widgetPanelPositionOnScreen(Integer... value) {
        addEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelPositionOnScreenNot(Integer... value) {
        addNotEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelPositionOnScreenGt(int value) {
        addGreaterThan(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelPositionOnScreenGtEq(int value) {
        addGreaterThanOrEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelPositionOnScreenLt(int value) {
        addLessThan(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public WidgetAttrSelection widgetPanelPositionOnScreenLtEq(int value) {
        addLessThanOrEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public WidgetAttrSelection orderByWidgetPanelPositionOnScreen(boolean desc) {
        orderBy(PanelColumns.POSITION_ON_SCREEN, desc);
        return this;
    }

    public WidgetAttrSelection orderByWidgetPanelPositionOnScreen() {
        orderBy(PanelColumns.POSITION_ON_SCREEN, false);
        return this;
    }

    public WidgetAttrSelection widgetType(WidgetType... value) {
        addEquals(WidgetColumns.TYPE, value);
        return this;
    }

    public WidgetAttrSelection widgetTypeNot(WidgetType... value) {
        addNotEquals(WidgetColumns.TYPE, value);
        return this;
    }


    public WidgetAttrSelection orderByWidgetType(boolean desc) {
        orderBy(WidgetColumns.TYPE, desc);
        return this;
    }

    public WidgetAttrSelection orderByWidgetType() {
        orderBy(WidgetColumns.TYPE, false);
        return this;
    }

    public WidgetAttrSelection type(WidgetAttrType... value) {
        addEquals(WidgetAttrColumns.TYPE, value);
        return this;
    }

    public WidgetAttrSelection typeNot(WidgetAttrType... value) {
        addNotEquals(WidgetAttrColumns.TYPE, value);
        return this;
    }


    public WidgetAttrSelection orderByType(boolean desc) {
        orderBy(WidgetAttrColumns.TYPE, desc);
        return this;
    }

    public WidgetAttrSelection orderByType() {
        orderBy(WidgetAttrColumns.TYPE, false);
        return this;
    }
}
