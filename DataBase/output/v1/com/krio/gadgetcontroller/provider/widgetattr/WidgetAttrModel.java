package com.krio.gadgetcontroller.provider.widgetattr;

import com.krio.gadgetcontroller.provider.base.BaseModel;

import java.util.Date;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Data model for the {@code widget_attr} table.
 */
public interface WidgetAttrModel extends BaseModel {

    /**
     * Get the {@code name} value.
     * Cannot be {@code null}.
     */
    @NonNull
    String getName();

    /**
     * Get the {@code integer_attr} value.
     * Can be {@code null}.
     */
    @Nullable
    Integer getIntegerAttr();

    /**
     * Get the {@code double_attr} value.
     * Can be {@code null}.
     */
    @Nullable
    Double getDoubleAttr();

    /**
     * Get the {@code string_attr} value.
     * Can be {@code null}.
     */
    @Nullable
    String getStringAttr();

    /**
     * Get the {@code boolean_attr} value.
     * Can be {@code null}.
     */
    @Nullable
    Boolean getBooleanAttr();

    /**
     * Get the {@code date_attr} value.
     * Can be {@code null}.
     */
    @Nullable
    Date getDateAttr();

    /**
     * Get the {@code widget_id} value.
     */
    long getWidgetId();

    /**
     * Get the {@code type} value.
     * Cannot be {@code null}.
     */
    @NonNull
    WidgetAttrType getType();
}
