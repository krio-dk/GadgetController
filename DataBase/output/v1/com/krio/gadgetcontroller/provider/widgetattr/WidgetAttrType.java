package com.krio.gadgetcontroller.provider.widgetattr;

/**
 * Possible values for the {@code type} column of the {@code widget_attr} table.
 */
public enum WidgetAttrType {
    /**
     * 
     */
    INEGER,

    /**
     * 
     */
    DOUBLE,

    /**
     * 
     */
    STRING,

    /**
     * 
     */
    BOOLEAN,

    /**
     * 
     */
    DATE,

}