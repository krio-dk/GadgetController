package com.krio.gadgetcontroller.provider.widgetattr;

import java.util.Date;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.krio.gadgetcontroller.provider.base.AbstractCursor;
import com.krio.gadgetcontroller.provider.widget.*;
import com.krio.gadgetcontroller.provider.panel.*;
import com.krio.gadgetcontroller.provider.project.*;

/**
 * Cursor wrapper for the {@code widget_attr} table.
 */
public class WidgetAttrCursor extends AbstractCursor implements WidgetAttrModel {
    public WidgetAttrCursor(Cursor cursor) {
        super(cursor);
    }

    /**
     * Primary key.
     */
    public long getId() {
        Long res = getLongOrNull(WidgetAttrColumns._ID);
        if (res == null)
            throw new NullPointerException("The value of '_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code name} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getName() {
        String res = getStringOrNull(WidgetAttrColumns.NAME);
        if (res == null)
            throw new NullPointerException("The value of 'name' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code integer_attr} value.
     * Can be {@code null}.
     */
    @Nullable
    public Integer getIntegerAttr() {
        Integer res = getIntegerOrNull(WidgetAttrColumns.INTEGER_ATTR);
        return res;
    }

    /**
     * Get the {@code double_attr} value.
     * Can be {@code null}.
     */
    @Nullable
    public Double getDoubleAttr() {
        Double res = getDoubleOrNull(WidgetAttrColumns.DOUBLE_ATTR);
        return res;
    }

    /**
     * Get the {@code string_attr} value.
     * Can be {@code null}.
     */
    @Nullable
    public String getStringAttr() {
        String res = getStringOrNull(WidgetAttrColumns.STRING_ATTR);
        return res;
    }

    /**
     * Get the {@code boolean_attr} value.
     * Can be {@code null}.
     */
    @Nullable
    public Boolean getBooleanAttr() {
        Boolean res = getBooleanOrNull(WidgetAttrColumns.BOOLEAN_ATTR);
        return res;
    }

    /**
     * Get the {@code date_attr} value.
     * Can be {@code null}.
     */
    @Nullable
    public Date getDateAttr() {
        Date res = getDateOrNull(WidgetAttrColumns.DATE_ATTR);
        return res;
    }

    /**
     * Get the {@code widget_id} value.
     */
    public long getWidgetId() {
        Long res = getLongOrNull(WidgetAttrColumns.WIDGET_ID);
        if (res == null)
            throw new NullPointerException("The value of 'widget_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code position_on_panel} value.
     * Can be {@code null}.
     */
    @Nullable
    public Integer getWidgetPositionOnPanel() {
        Integer res = getIntegerOrNull(WidgetColumns.POSITION_ON_PANEL);
        return res;
    }

    /**
     * Get the {@code caption} value.
     * Can be {@code null}.
     */
    @Nullable
    public String getWidgetCaption() {
        String res = getStringOrNull(WidgetColumns.CAPTION);
        return res;
    }

    /**
     * Get the {@code panel_id} value.
     */
    public long getWidgetPanelId() {
        Long res = getLongOrNull(WidgetColumns.PANEL_ID);
        if (res == null)
            throw new NullPointerException("The value of 'panel_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code project_id} value.
     */
    public long getWidgetPanelProjectId() {
        Long res = getLongOrNull(PanelColumns.PROJECT_ID);
        if (res == null)
            throw new NullPointerException("The value of 'project_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code name} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getWidgetPanelProjectName() {
        String res = getStringOrNull(ProjectColumns.NAME);
        if (res == null)
            throw new NullPointerException("The value of 'name' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code token} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getWidgetPanelProjectToken() {
        String res = getStringOrNull(ProjectColumns.TOKEN);
        if (res == null)
            throw new NullPointerException("The value of 'token' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code position_on_list} value.
     * Can be {@code null}.
     */
    @Nullable
    public Integer getWidgetPanelProjectPositionOnList() {
        Integer res = getIntegerOrNull(ProjectColumns.POSITION_ON_LIST);
        return res;
    }

    /**
     * Get the {@code caption} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getWidgetPanelCaption() {
        String res = getStringOrNull(PanelColumns.CAPTION);
        if (res == null)
            throw new NullPointerException("The value of 'caption' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code position_on_screen} value.
     * Can be {@code null}.
     */
    @Nullable
    public Integer getWidgetPanelPositionOnScreen() {
        Integer res = getIntegerOrNull(PanelColumns.POSITION_ON_SCREEN);
        return res;
    }

    /**
     * Get the {@code type} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public WidgetType getWidgetType() {
        Integer intValue = getIntegerOrNull(WidgetColumns.TYPE);
        if (intValue == null)
            throw new NullPointerException("The value of 'type' in the database was null, which is not allowed according to the model definition");
        return WidgetType.values()[intValue];
    }

    /**
     * Get the {@code type} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public WidgetAttrType getType() {
        Integer intValue = getIntegerOrNull(WidgetAttrColumns.TYPE);
        if (intValue == null)
            throw new NullPointerException("The value of 'type' in the database was null, which is not allowed according to the model definition");
        return WidgetAttrType.values()[intValue];
    }
}
