package com.krio.gadgetcontroller.provider.widget;

import android.net.Uri;
import android.provider.BaseColumns;

import com.krio.gadgetcontroller.provider.DataProvider;
import com.krio.gadgetcontroller.provider.command.CommandColumns;
import com.krio.gadgetcontroller.provider.commandparam.CommandParamColumns;
import com.krio.gadgetcontroller.provider.panel.PanelColumns;
import com.krio.gadgetcontroller.provider.project.ProjectColumns;
import com.krio.gadgetcontroller.provider.widget.WidgetColumns;
import com.krio.gadgetcontroller.provider.widgetattr.WidgetAttrColumns;

/**
 * Columns for the {@code widget} table.
 */
public class WidgetColumns implements BaseColumns {
    public static final String TABLE_NAME = "widget";
    public static final Uri CONTENT_URI = Uri.parse(DataProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    /**
     * Primary key.
     */
    public static final String _ID = BaseColumns._ID;

    public static final String POSITION_ON_PANEL = "position_on_panel";

    public static final String CAPTION = "widget__caption";

    public static final String PANEL_ID = "panel_id";

    public static final String TYPE = "widget__type";


    public static final String DEFAULT_ORDER = TABLE_NAME + "." +_ID;

    // @formatter:off
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            POSITION_ON_PANEL,
            CAPTION,
            PANEL_ID,
            TYPE
    };
    // @formatter:on

    public static boolean hasColumns(String[] projection) {
        if (projection == null) return true;
        for (String c : projection) {
            if (c.equals(POSITION_ON_PANEL) || c.contains("." + POSITION_ON_PANEL)) return true;
            if (c.equals(CAPTION) || c.contains("." + CAPTION)) return true;
            if (c.equals(PANEL_ID) || c.contains("." + PANEL_ID)) return true;
            if (c.equals(TYPE) || c.contains("." + TYPE)) return true;
        }
        return false;
    }

    public static final String PREFIX_PANEL = TABLE_NAME + "__" + PanelColumns.TABLE_NAME;
}
