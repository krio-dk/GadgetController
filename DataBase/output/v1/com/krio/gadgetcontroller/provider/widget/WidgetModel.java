package com.krio.gadgetcontroller.provider.widget;

import com.krio.gadgetcontroller.provider.base.BaseModel;

import java.util.Date;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Data model for the {@code widget} table.
 */
public interface WidgetModel extends BaseModel {

    /**
     * Get the {@code position_on_panel} value.
     * Can be {@code null}.
     */
    @Nullable
    Integer getPositionOnPanel();

    /**
     * Get the {@code caption} value.
     * Can be {@code null}.
     */
    @Nullable
    String getCaption();

    /**
     * Get the {@code panel_id} value.
     */
    long getPanelId();

    /**
     * Get the {@code type} value.
     * Cannot be {@code null}.
     */
    @NonNull
    WidgetType getType();
}
