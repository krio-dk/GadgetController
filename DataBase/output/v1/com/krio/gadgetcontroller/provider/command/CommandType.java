package com.krio.gadgetcontroller.provider.command;

/**
 * Possible values for the {@code type} column of the {@code command} table.
 */
public enum CommandType {
    /**
     * 
     */
    COMMAND_LED_ENABLE,

    /**
     * 
     */
    COMMAND_LED_DISABLE,

    /**
     * 
     */
    COMMAND_LED_BRIGHTNESS,

    /**
     * 
     */
    COMMAND_SWITCH_ENABLE,

    /**
     * 
     */
    COMMAND_SWITCH_DISABLE,

    /**
     * 
     */
    COMMAND_DISPLAY,

    /**
     * 
     */
    COMMAND_BUTTON_DOWN,

    /**
     * 
     */
    COMMAND_BUTTON_UP,

    /**
     * 
     */
    COMMAND_SEEKBAR,

    /**
     * 
     */
    COMMAND_JOYSTICK,

}