package com.krio.gadgetcontroller.ui.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.ui.adapter.CommandListPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommandListActivity extends BaseActivity {

    @BindView(R.id.container)
    ViewPager viewPager;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    CommandListPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_command_list);
        ButterKnife.bind(this);

        initActionBar();
        initComponents();
    }

    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.title_api);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initComponents() {
        pagerAdapter = new CommandListPagerAdapter(getSupportFragmentManager(), this);

        viewPager.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.view_pager_separator_size));
        viewPager.setPageMarginDrawable(R.color.colorDarkHintBackground);
        viewPager.setAdapter(pagerAdapter);

        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
