package com.krio.gadgetcontroller.ui.activity;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.ui.fragment.DeviceListBluetoothFragment;
import com.krio.gadgetcontroller.ui.fragment.TCPConnectionFragmentOld;

import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

public class LocalConnectionActivity extends BaseActivity {

    public static final String REQUEST_CODE = "request_code";
    protected int requestCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_connection);

        requestCode = getIntent().getIntExtra(REQUEST_CODE, 0);

        initActionBar(requestCode);

        if (savedInstanceState == null) {
            selectFragment(requestCode);
        }
    }

    private void initActionBar(int requestCode) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);

            switch (requestCode) {
                case ProjectActivity.REQUEST_CONNECTION_BLUETOOTH:
                    setTitle(R.string.title_bluetooth);
                    break;

                case ProjectActivity.REQUEST_CONNECTION_TCP:
                    setTitle(R.string.title_tcp);
                    break;
            }
        }
    }

    private void selectFragment(int requestCode) {
        switch (requestCode) {
            case ProjectActivity.REQUEST_CONNECTION_BLUETOOTH:
                showFragment(DeviceListBluetoothFragment.newInstance());
                break;

            case ProjectActivity.REQUEST_CONNECTION_TCP:
                // showFragment(DeviceListWiFiP2PFragment.newInstance());
                showFragment(TCPConnectionFragmentOld.newInstance());
                break;
        }
    }

    private void showFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_connection_container, fragment)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (requestCode == ProjectActivity.REQUEST_CONNECTION_BLUETOOTH) {
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            bluetoothAdapter.cancelDiscovery();
        }
    }
}
