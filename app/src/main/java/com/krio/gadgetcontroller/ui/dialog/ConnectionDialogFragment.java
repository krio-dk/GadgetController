package com.krio.gadgetcontroller.ui.dialog;


import com.krio.gadgetcontroller.R;

import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConnectionDialogFragment extends DialogFragment implements View.OnClickListener {

    public static final String TAG = "ConnectionDialogFragment";

    private onConnectionSelectedListener listener;

    public interface onConnectionSelectedListener {
        void onConnectionBluetoothSelected();

        void onConnectionTCPSelected();

        void onConnectionGCServerSelected();
    }

    public void setItemClickListener(onConnectionSelectedListener listener) {
        this.listener = listener;
    }

    public static ConnectionDialogFragment newInstance() {
        ConnectionDialogFragment fragment = new ConnectionDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.fragment_dialog_connection, null);

        View optionBluetooth = view.findViewById(R.id.option_bluetooth);
        View optionWiFi = view.findViewById(R.id.option_wifi);
        View optionInternet = view.findViewById(R.id.option_internet);

        optionBluetooth.setOnClickListener(this);
        optionWiFi.setOnClickListener(this);
        optionInternet.setOnClickListener(this);

        return buildDialog(view);
    }

    private Dialog buildDialog(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.DialogTheme);
        builder.setTitle(R.string.alert_title_connection);
        builder.setView(view);
        builder.setNegativeButton(R.string.button_cancel, (dialog, which) -> dismiss());
        return builder.create();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.option_bluetooth:
                listener.onConnectionBluetoothSelected();
                break;

            case R.id.option_wifi:
                listener.onConnectionTCPSelected();
                break;

            case R.id.option_internet:
                listener.onConnectionGCServerSelected();
                break;
        }

        dismiss();
    }
}
