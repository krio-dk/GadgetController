package com.krio.gadgetcontroller.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.krio.gadgetcontroller.App;
import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.project.Project;
import com.krio.gadgetcontroller.logic.projectmanager.ProjectManager;
import com.krio.gadgetcontroller.ui.utils.EditTextUtils;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProjectSettingsActivity extends BaseActivity {

    public static final String EDIT_PROJECT_ID = "edit_project_id";

    @BindView(R.id.text_input_layout)
    TextInputLayout textInputLayout;

    @BindView(R.id.project_name)
    EditText projectName;

    @BindView(R.id.project_token)
    TextView projectToken;

    EditTextUtils projectNameUtils;

    ProjectManager projectManager;
    Project editProject;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_settings);

        ButterKnife.bind(this);

        projectManager = App.getProjectManager();

        initActionBar();
        initEditTextUtils();

        long projectId = getIntent().getLongExtra(EDIT_PROJECT_ID, 0);

        if (projectId == 0) {
            initNewMode();
        } else {
            initEditMode(projectId);
        }
    }

    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initEditTextUtils() {
        LinearLayout parentLayout = ButterKnife.findById(this, R.id.parent_layout);
        projectNameUtils = new EditTextUtils(this, parentLayout, textInputLayout, projectName);
    }


    private void initNewMode() {
        setProjectToken(generateProjectToken());
    }

    private void initEditMode(long projectId) {
        editProject = projectManager.getProject(projectId);
        setProjectName(editProject.getName());
        setProjectToken(editProject.getToken());
    }


    private String generateProjectToken() {
        return UUID.randomUUID().toString().toUpperCase();
    }

    public void setProjectToken(String token) {
        this.projectToken.setText(token);
    }

    public void setProjectName(String name) {
        this.projectName.setText(name);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_project, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.menu_done:
                String projectName = this.projectName.getText().toString();
                String projectToken = this.projectToken.getText().toString();
                onMenuDoneSelected(projectName, projectToken);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.how_it_work_layout)
    public void onHelpClick() {
        startHelpActivity();
    }

    private void onMenuDoneSelected(String projectName, String projectToken) {
        if (validateName(projectName)) {
            if (editProject == null) {
                addProject(projectName, projectToken);
            } else {
                updateProject(projectName, projectToken);
            }
            finish();
        }
    }

    private boolean validateName(String projectName) {
        if (projectName.isEmpty()) {
            projectNameUtils.showEmptyWarn();
            return false;
        }

        if (!projectNameUtils.isLengthCorrect()) {
            return false;
        }

        return true;
    }

    private void updateProject(String projectName, String projectToken) {
        editProject.setName(projectName);
    }

    private void addProject(String projectName, String projectToken) {
        projectManager.addProject(projectName, projectToken);
    }


    public void startHelpActivity() {
        startActivity(new Intent(ProjectSettingsActivity.this, HowItWorksActivity.class));
    }
}
