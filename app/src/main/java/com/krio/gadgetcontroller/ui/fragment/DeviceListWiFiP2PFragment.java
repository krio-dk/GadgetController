package com.krio.gadgetcontroller.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.ui.adapter.DeviceListWiFiP2PAdapter;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@Deprecated
public class DeviceListWiFiP2PFragment extends BaseFragment implements WifiP2pManager.PeerListListener, DeviceListWiFiP2PAdapter.OnWiFiDeviceSelectedListener {

    @BindView(R.id.refresh)
    ImageButton refreshButton;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    @BindView(R.id.devices)
    RecyclerView deviceList;

    DeviceListWiFiP2PAdapter deviceAdapter;

    private final IntentFilter intentFilter = new IntentFilter();

    WifiP2pManager mManager;
    WifiP2pManager.Channel mChannel;

    private List<WifiP2pDevice> peers = new ArrayList<>();

    public static DeviceListWiFiP2PFragment newInstance() {
        DeviceListWiFiP2PFragment fragment = new DeviceListWiFiP2PFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_device_list_wi_fi, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initDeviceList();

        initIntentFilter();
        initWifiP2pManager();

        discoverPeers();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(wifiStateChangeReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(wifiStateChangeReceiver);
    }

    private void initDeviceList() {
        deviceList.setLayoutManager(new LinearLayoutManager(getActivity()));
        deviceList.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                .build());

        deviceAdapter = new DeviceListWiFiP2PAdapter(DeviceListWiFiP2PAdapter.DEVICE_NEW);
        deviceAdapter.setOnItemClickListener(this);
        deviceList.setAdapter(deviceAdapter);
    }

    private void initIntentFilter () {
        //  Indicates a change in the Wi-Fi P2P status.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);

        // Indicates a change in the list of available peers.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);

        // Indicates the state of Wi-Fi P2P connectivity has changed.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);

        // Indicates this device's details have changed.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
    }

    private void initWifiP2pManager() {
        mManager = (WifiP2pManager) getActivity().getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(getActivity(), getActivity().getMainLooper(), null);
    }

    private void discoverPeers() {
        mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                // Code for when the discovery initiation is successful goes here.
                // No services have actually been discovered yet, so this method
                // can often be left blank.  Code for peer discovery goes in the
                // onReceive method, detailed below.
                Log.d("krio_wifi", "discoverPeers onSuccess");
            }

            @Override
            public void onFailure(int reasonCode) {
                // Code for when the discovery initiation fails goes here.
                // Alert the user that something went wrong.
                Log.d("krio_wifi", "discoverPeers onFailure");
            }
        });
    }

    private final BroadcastReceiver wifiStateChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            switch (action) {
                case WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION:
                    // Determine if Wifi P2P mode is enabled or not, alert the Activity.
                    int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
                    if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                        Log.d("krio_wifi", "P2P Enabled");
                    } else {
                        Log.d("krio_wifi", "P2P DISABLED");
                    }
                    break;

                case WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION:
                    // The peer list has changed

                    // Request available peers from the wifi p2p manager. This is an
                    // asynchronous call and the calling activity is notified with a
                    // callback on PeerListListener.onPeersAvailable()
                    if (mManager != null) {
                        mManager.requestPeers(mChannel, DeviceListWiFiP2PFragment.this);
                    }
                    Log.d("krio_wifi", "P2P peers changed");
                    break;

                case WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION:
                    // Connection state changed

                    if (mManager == null) {
                        return;
                    }

                    NetworkInfo networkInfo = intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);

                    if (networkInfo.isConnected()) {

                        // We are connected with the other device, request connection
                        // info to find group owner IP

                        mManager.requestConnectionInfo(mChannel, new WifiP2pManager.ConnectionInfoListener() {
                            @Override
                            public void onConnectionInfoAvailable(WifiP2pInfo info) {
                                // InetAddress from WifiP2pInfo struct.
                                // InetAddress groupOwnerAddress = info.groupOwnerAddress.getHostAddress();

                                // After the group negotiation, we can determine the group owner.
                                if (info.groupFormed && info.isGroupOwner) {
                                    // Do whatever tasks are specific to the group owner.
                                    // One common case is creating a server thread and accepting
                                    // incoming connections.
                                } else if (info.groupFormed) {
                                    // The other device acts as the client. In this case,
                                    // you'll want to create a client thread that connects to the group
                                    // owner.
                                }
                            }
                        });
                    }
                    Log.d("krio_wifi", "P2P connection state changed: " + networkInfo.isConnected());
                    break;

                case WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION:
                    /*
                    DeviceListFragment fragment = (DeviceListFragment) activity.getFragmentManager()
                            .findFragmentById(R.id.frag_list);
                    fragment.updateThisDevice((WifiP2pDevice) intent.getParcelableExtra(
                            WifiP2pManager.EXTRA_WIFI_P2P_DEVICE));
                    */

                    WifiP2pDevice device = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);

                    Log.d("krio_wifi", "P2P Device changed action: " + device.deviceName + ", " + device.deviceAddress);
                    break;
            }
        }
    };

    @Override
    public void onPeersAvailable(WifiP2pDeviceList peerList) {

        Log.d("krio_wifi", "onPeersAvailable");

        // Out with the old, in with the new.
        peers.clear();
        peers.addAll(peerList.getDeviceList());

        // If an AdapterView is backed by this data, notify it
        // of the change.  For instance, if you have a ListView of available
        // peers, trigger an update.
        deviceAdapter.notifyDataSetChanged();
        if (peers.size() == 0) {
            Log.d("krio_wifi", "No devices found");
            return;
        }
    }

    @Override
    public void onWiFiDeviceSelected(WifiP2pDevice device) {
        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = device.deviceAddress;
        config.wps.setup = WpsInfo.PBC;

        mManager.connect(mChannel, config, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                // WiFiDirectBroadcastReceiver will notify us. Ignore for now.
            }

            @Override
            public void onFailure(int reason) {
                Log.d("krio_wifi", "Connect failed. Reson  = " + reason);
            }
        });
    }
}
