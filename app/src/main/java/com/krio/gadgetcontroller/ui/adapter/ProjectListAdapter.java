package com.krio.gadgetcontroller.ui.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.project.Project;
import com.krio.gadgetcontroller.ui.utils.ProjectTouchHelperCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;

/**
 * Created by krio on 30.10.15.
 */
public class ProjectListAdapter extends RecyclerView.Adapter<ProjectListAdapter.ViewHolder> implements ProjectTouchHelperCallback.ItemTouchHelperAdapter {

    public static final int TYPE_OPTIONS = 0;
    public static final int TYPE_PROJECT = 1;

    RecyclerView recyclerView;

    List<Project> projectList = new ArrayList<>();
    OnProjectActionListener actionListener;

    Project removedProject;
    int removedProjectPosition;

    public interface OnProjectActionListener {
        void onProjectSelected(long projectId);

        void onProjectEditSelected(long projectId);

        void onProjectRemoved(long projectId);
    }

    public void setActionListener(OnProjectActionListener actionListener) {
        this.actionListener = actionListener;
    }

    public ProjectListAdapter(RecyclerView recyclerView, List<Project> projectList) {
        this.recyclerView = recyclerView;
        this.projectList = projectList;
    }

    @Override
    public int getItemViewType(int position) {
        if (projectList.get(position) == null) {
            return TYPE_OPTIONS;
        } else {
            return TYPE_PROJECT;
        }
    }

    @Override
    public ProjectListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(viewType == TYPE_PROJECT ? R.layout.item_project_list : R.layout.item_project_list_confirm, parent, false);
        return new ViewHolder(v, viewType);
    }

    @Override
    public void onBindViewHolder(ProjectListAdapter.ViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_PROJECT) {
            holder.name.setText(projectList.get(position).getName());
            holder.token.setText(projectList.get(position).getToken());
        }
    }

    @Override
    public int getItemCount() {
        return projectList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  implements ProjectTouchHelperCallback.ItemTouchHelperViewHolder{

        @Nullable
        @BindView(R.id.primaryText)
        public TextView name;

        @Nullable
        @BindView(R.id.drag_overlay)
        View dragOverlay;

        @Nullable
        @BindView(R.id.secondaryText)
        public TextView token;

        @Nullable
        @BindView(R.id.edit)
        public ImageButton edit;

        @Nullable
        @BindView(R.id.delete)
        public ImageButton delete;

        @Nullable
        @BindView(R.id.cancel)
        public Button cancel;

        public ViewHolder(View v, int viewType)
        {
            super(v);
            ButterKnife.bind(this, v);

            if (viewType == TYPE_PROJECT) {
                v.setOnClickListener(selectListener);
                v.setTag(this);

            } else {
                if (cancel != null) {
                    cancel.setOnClickListener(cancelListener);
                    cancel.setTag(this);
                }

                if (edit != null) {
                    edit.setOnClickListener(editListener);
                    edit.setTag(this);
                }

                if (delete != null) {
                    delete.setOnClickListener(deleteListener);
                    delete.setTag(this);
                }
            }
        }

        @Override
        public void onItemSelected() {
            dragOverlay.setVisibility(View.VISIBLE);
        }

        @Override
        public void onItemClear() {
            dragOverlay.setVisibility(View.GONE);
        }
    }

    public View.OnClickListener cancelListener = v -> cancelRemoveItem();

    public View.OnClickListener editListener = v -> {
        long id = removedProject.getId();
        cancelRemoveItem();

        if (actionListener != null) {
            actionListener.onProjectEditSelected(id);
        }
    };

    public View.OnClickListener deleteListener = v -> {
        long id = removedProject.getId();

        if (actionListener != null) {
            actionListener.onProjectRemoved(id);
        }
    };

    public View.OnClickListener selectListener = v -> {
        if (actionListener != null) {
            ViewHolder viewHolder = (ViewHolder) v.getTag();
            int position = viewHolder.getLayoutPosition();
            actionListener.onProjectSelected(projectList.get(position).getId());
        }
    };

    private boolean removeItem(int position) {
        if (removedProject == null) {
            removedProject = projectList.get(position);
            removedProjectPosition = position;

            projectList.remove(position);
            projectList.add(position, null);

            recyclerView.setItemAnimator(new FadeInAnimator());
            recyclerView.getItemAnimator().setAddDuration(100);
            notifyItemChanged(position);
            return true;
        }
        return false;
    }

    public boolean deleteItem() {
        if (removedProject != null) {
            projectList.remove(removedProjectPosition);
            removedProject = null;

            for (int i = 0; i < projectList.size(); i ++) {
                projectList.get(i).setPosition(i);
            }

            recyclerView.setItemAnimator(new FadeInAnimator());
            recyclerView.getItemAnimator().setRemoveDuration(100);
            notifyItemRemoved(removedProjectPosition);
            return true;
        }
        return false;
    }

    private void moveItem(int fromPosition, int toPosition) {

        if (fromPosition != toPosition) {
            projectList.add(toPosition, projectList.remove(fromPosition));

            for (int i = 0; i < projectList.size(); i++) {
                projectList.get(i).setPosition(i);
            }

            notifyItemMoved(fromPosition, toPosition);
        }
    }

    @Override
    public void onItemSwiped(int position) {
        removeItem(position);
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        moveItem(fromPosition, toPosition);
    }

    @Override
    public void cancelRemoveItem() {
        if (removedProject != null) {
            projectList.remove(removedProjectPosition);
            projectList.add(removedProjectPosition, removedProject);
            removedProject = null;

            recyclerView.setItemAnimator(new SlideInLeftAnimator());
            recyclerView.getItemAnimator().setAddDuration(300);
            notifyItemChanged(removedProjectPosition);
        }
    }

    public void updateProject (int position) {
        notifyItemChanged(position);
    }

    public void addProject (Project project) {
        projectList.add(0, project);
        notifyItemInserted(0);

        for (int i = 1; i < projectList.size(); i++) {
            projectList.get(i).setPosition(i);
        }
    }
}
