package com.krio.gadgetcontroller.ui.utils;

import android.animation.Animator;
import android.view.View;
import android.widget.ImageView;

import com.krio.gadgetcontroller.App;

import jp.wasabeef.blurry.Blurry;

public class AnimationUtils {

    public interface onAnimationEndListener {
        void onAnimationEnd();
    }

    public static void prepareOverlay(View captureView, ImageView overlayView) {
        Blurry.with(App.getContext()).async().capture(captureView).into(overlayView);
    }

    public static void switchView(View hideView, View showView, int duration, onAnimationEndListener listener) {
        AnimationUtils.hideView(hideView, duration);
        AnimationUtils.showView(showView, duration, listener);
    }

    public static void hideView(View view, int duration) {
        view.animate().alpha(0).setDuration(duration).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public static void showView(View view, int duration, onAnimationEndListener listener) {
        view.animate().alpha(1).setDuration(duration).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                view.setAlpha(0);
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (listener != null)
                    listener.onAnimationEnd();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }
}
