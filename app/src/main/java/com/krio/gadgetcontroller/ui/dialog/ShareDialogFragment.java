package com.krio.gadgetcontroller.ui.dialog;


import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.ui.adapter.ShareAdapter;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

public class ShareDialogFragment extends DialogFragment implements ShareAdapter.OnItemClickListener {

    public static final String TAG = "ShareDialogFragment";

    RecyclerView recyclerView;
    ShareAdapter shareAdapter;

    public static ShareDialogFragment newInstance() {
        ShareDialogFragment fragment = new ShareDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.fragment_dialog_share, null);

        initAdapter();
        initRecyclerView(view);

        return buildDialog(view);
    }

    private void initAdapter() {
        shareAdapter = new ShareAdapter(getActivity());
        shareAdapter.setOnItemClickListener(this);
    }

    private void initRecyclerView(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                .showLastDivider()
                .build());

        recyclerView.setAdapter(shareAdapter);
    }

    private Dialog buildDialog(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.DialogTheme);
        builder.setTitle(R.string.alert_title_share);
        builder.setView(view);
        builder.setNegativeButton(R.string.button_cancel,  (dialog, which) -> dismiss());
        return builder.create();
    }

    @Override
    public void onAppSelected(ResolveInfo appInfo) {
        Intent sendIntent = new Intent();

        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, getActivity().getString(R.string.app_name));
        sendIntent.putExtra(Intent.EXTRA_TEXT, getActivity().getString(R.string.share_text));
        sendIntent.setType("text/plain");

        if (appInfo != null) {
            sendIntent.setComponent(new ComponentName(appInfo.activityInfo.packageName, appInfo.activityInfo.name));
        }

        getActivity().startActivity(sendIntent);
        dismiss();
    }
}
