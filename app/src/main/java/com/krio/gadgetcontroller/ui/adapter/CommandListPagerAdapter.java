package com.krio.gadgetcontroller.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.ui.fragment.CommandListFragment;

/**
 * Created by krio on 04.06.16.
 */
public class CommandListPagerAdapter extends FragmentPagerAdapter {

    Context ctx;

    public CommandListPagerAdapter(FragmentManager fm, Context ctx) {
        super(fm);
        this.ctx = ctx;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return CommandListFragment.newInstance(CommandListFragment.TYPE_INPUT_COMMANDS);
            case 1:
                return CommandListFragment.newInstance(CommandListFragment.TYPE_OUTPUT_COMMANDS);
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return ctx.getString(R.string.tab_input_command);
            case 1:
                return ctx.getString(R.string.tab_output_command);
        }
        return null;
    }
}
