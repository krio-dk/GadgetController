package com.krio.gadgetcontroller.ui.fragment;


import com.krio.gadgetcontroller.App;
import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.command.InputCommandParser;
import com.krio.gadgetcontroller.logic.panel.Panel;
import com.krio.gadgetcontroller.logic.project.Project;
import com.krio.gadgetcontroller.logic.project.ProjectModeWrapper;
import com.krio.gadgetcontroller.logic.widget.Widget;
import com.krio.gadgetcontroller.provider.widget.WidgetType;
import com.krio.gadgetcontroller.ui.activity.ComponentEditActivity;
import com.krio.gadgetcontroller.ui.activity.ProjectActivity;
import com.krio.gadgetcontroller.ui.adapter.WidgetListAdapter;
import com.krio.gadgetcontroller.ui.dialog.EditDialogFragment;
import com.krio.gadgetcontroller.ui.utils.DialogUtils;
import com.krio.gadgetcontroller.ui.utils.WidgetTouchHelperCallback;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class PanelFragment extends BaseFragment implements WidgetListAdapter.OnWidgetEditSelectedListener, EditDialogFragment.onEditDialogItemSelectedListener, WidgetListAdapter.OnStartDragListener, Observer {

    private static final String PANEL_ID = "panel_id";

    @Inject
    Project project;

    @Inject
    ProjectModeWrapper projectModeWrapper;

    @Inject
    InputCommandParser inputCommandParser;

    @BindView(R.id.panel_name)
    TextView panelName;

    @BindView(R.id.element_list)
    RecyclerView widgetList;

    ItemTouchHelper itemTouchHelper;
    Panel panel;

    OnPanelDeleteListener deleteListener;

    WidgetListAdapter widgetAdapter;
    EditDialogFragment editDialog;

    public static PanelFragment newInstance(long id) {
        PanelFragment fragment = new PanelFragment();
        Bundle args = new Bundle();
        args.putLong(PANEL_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_panel, container, false);
        unbinder = ButterKnife.bind(this, view);
        App.getMainComponent().inject(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.d("krio", "PanelFragment onViewCreated");

        long panelId = getArguments().getLong(PANEL_ID);

        panel = project.getPanel(panelId);
        panelName.setText(panel.getName());
        projectModeWrapper.addObserver(this);

        initWidgetAdapter();
        initWidgetList();
        initTouchHelper();
        initEditDialog();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        updateViewsVisibility();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        projectModeWrapper.deleteObserver(this);
        inputCommandParser.deleteObserver(widgetAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            deleteListener = (OnPanelDeleteListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "Activity must implement fragment's callbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        deleteListener = null;
    }

    private void initWidgetAdapter() {

        List<Widget> panelWidgets = project.getPanelWidgets(panel.getId());

        widgetAdapter = new WidgetListAdapter(panelWidgets, widgetList);
        widgetAdapter.setEditSelectedListener(this);
        widgetAdapter.setDragStartListener(this);
        inputCommandParser.addObserver(widgetAdapter);
    }

    private void initWidgetList() {
        widgetList.setLayoutManager(new LinearLayoutManager(getActivity()));
        widgetList.setAdapter(widgetAdapter);
    }

    private void initTouchHelper() {
        ItemTouchHelper.Callback callback = new WidgetTouchHelperCallback(widgetAdapter);
        itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(widgetList);

        widgetList.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                .showLastDivider()
                .build());
    }

    private void initEditDialog() {
        Fragment dialog = getFragmentManager().findFragmentByTag(EditDialogFragment.TAG);
        if (dialog == null) {
            editDialog = EditDialogFragment.newInstance();
        } else {
            editDialog = (EditDialogFragment) dialog;
        }
        editDialog.setListener(this);
    }

    private void updateViewsVisibility() {
        if (projectModeWrapper.isEditMode()) {
            getView().findViewById(R.id.panel_edit).setVisibility(View.VISIBLE);
            getView().findViewById(R.id.empty_panel_view).setVisibility(View.GONE);
            getView().findViewById(R.id.empty_panel_view_edit_mode).setVisibility(View.VISIBLE);

        } else {
            getView().findViewById(R.id.panel_edit).setVisibility(View.GONE);
            getView().findViewById(R.id.empty_panel_view_edit_mode).setVisibility(View.GONE);
            getView().findViewById(R.id.empty_panel_view).setVisibility(View.VISIBLE);
        }
        updateWidgetListVisibility();
    }

    private void updateWidgetListVisibility() {
        if (widgetAdapter.getItemCount() == 0) {
            widgetList.setVisibility(View.GONE);
        } else {
            getView().findViewById(R.id.empty_panel_view_edit_mode).setVisibility(View.GONE);
            getView().findViewById(R.id.empty_panel_view).setVisibility(View.GONE);
            widgetList.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.panel_edit)
    public void onPanelEditSelected() {
        showEditPanelDialog();
    }

    @Override
    public void onWidgetEditSelected(Widget widget) {
        showEditWidgetDialog(widget);
    }


    @Override
    public void onEditPanelSelected() {
        showComponentEditActivity(
                ProjectActivity.REQUEST_EDIT_PANEL,
                ComponentEditActivity.PANEL_ID,
                panel.getId()
        );
    }

    @Override
    public void onDeletePanelSelected() {
        deleteListener.onPanelDelete(panel.getId());
    }

    @Override
    public void onEditWidgetAttrsSelected(Widget widget) {
        showComponentEditActivity(
                ProjectActivity.REQUEST_EDIT_WIDGET_ATTR,
                ComponentEditActivity.WIDGET_ID,
                widget.getId()
        );
    }

    @Override
    public void onEditWidgetCommandsSelected(Widget widget) {
        showComponentEditActivity(
                ProjectActivity.REQUEST_EDIT_WIDGET_COMMANDS,
                ComponentEditActivity.WIDGET_ID,
                widget.getId()
        );
    }

    @Override
    public void onDeleteWidgetSelected(Widget widget) {
        DialogUtils.showDialogDeleteWidget(getActivity(), () -> {
            project.deleteWidget(widget.getId());
            widgetAdapter.removeWidget(widget);
            updateViewsVisibility();
        });
    }

    @Override
    public void update(Observable observable, Object data) {
        updateViewsVisibility();
        widgetAdapter.update();
    }

    public void showEditPanelDialog() {
        editDialog.setType(EditDialogFragment.TYPE_PANEL);
        if (project.getPanelCount() > 1) {
            editDialog.setDeleteEnabled(true);
        } else {
            editDialog.setDeleteEnabled(false);
        }
        editDialog.show(getFragmentManager(), EditDialogFragment.TAG);
    }

    public void showEditWidgetDialog(Widget widget) {
        if (widget.getWidgetType() == WidgetType.LABEL) {
            editDialog.setType(EditDialogFragment.TYPE_WIDGET_NO_COMMANDS);
        } else {
            editDialog.setType(EditDialogFragment.TYPE_WIDGET);
        }
        editDialog.setWidget(widget);
        editDialog.show(getFragmentManager(), EditDialogFragment.TAG);
    }

    public void showComponentEditActivity(int requestCode, String extraName, long extraId) {
        Intent intent = new Intent(getActivity(), ComponentEditActivity.class);
        intent.putExtra(ComponentEditActivity.REQUEST_CODE, requestCode);
        intent.putExtra(extraName, extraId);
        getActivity().startActivityForResult(intent, requestCode);
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        itemTouchHelper.startDrag(viewHolder);
    }

    public interface OnPanelDeleteListener {
        void onPanelDelete(long panelId);
    }
}