package com.krio.gadgetcontroller.ui.adapter;

import com.krio.gadgetcontroller.R;

import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by krio on 30.10.15.
 */
public class TCPHistoryAdapter extends RecyclerView.Adapter<TCPHistoryAdapter.ViewHolder> {

    public static final int TYPE_EMPTY = 0;
    public static final int TYPE_ITEM = 1;

    List<String> historyList = new ArrayList<>();
    OnHistoryItemClickListener clickListener;

    public interface OnHistoryItemClickListener {

        void onHistoryItemClick(String host, String port);
    }

    public void setClickListener(OnHistoryItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public TCPHistoryAdapter(Collection<String> historySet) {
        historyList.addAll(historySet);

        if (historyList.isEmpty()) {
            historyList.add(null);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (historyList.get(position) == null) {
            return TYPE_EMPTY;
        } else {
            return TYPE_ITEM;
        }
    }

    @Override
    public TCPHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                               .inflate(R.layout.item_single_line_list, parent, false);
        return new ViewHolder(v, viewType);
    }

    @Override
    public void onBindViewHolder(TCPHistoryAdapter.ViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_ITEM) {
            holder.textView.setText(historyList.get(position));
            holder.textView.setTextColor(ContextCompat.getColor(holder.textView.getContext(), R.color.colorDarkPrimaryText));
        } else {
            holder.textView.setText(holder.textView.getResources().getString(R.string.text_empty_tcp_history));
        }
    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        @BindView(R.id.textView)
        public TextView textView;

        public ViewHolder(View v, int viewType) {
            super(v);
            ButterKnife.bind(this, v);

            if (viewType == TYPE_ITEM) {
                v.setOnClickListener(selectListener);
                v.setTag(this);
            }
        }
    }

    public View.OnClickListener selectListener = v -> {
        if (clickListener != null) {
            ViewHolder viewHolder = (ViewHolder) v.getTag();
            int position = viewHolder.getLayoutPosition();

            String item = historyList.get(position);

            String host = item.substring(0, item.indexOf(":"));
            String port = item.substring(item.indexOf(":") + 1);

            clickListener.onHistoryItemClick(host, port);
        }
    };

    public void clear() {
        historyList.clear();
        historyList.add(null);
        notifyDataSetChanged();
    }
}
