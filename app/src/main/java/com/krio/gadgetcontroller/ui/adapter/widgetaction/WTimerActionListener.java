package com.krio.gadgetcontroller.ui.adapter.widgetaction;

import android.widget.CompoundButton;

import com.krio.gadgetcontroller.logic.widget.WSwitch;
import com.krio.gadgetcontroller.logic.widget.WTimer;
import com.krio.gadgetcontroller.logic.widget.Widget;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.ui.adapter.WidgetListAdapter;

import java.util.List;

/**
 * Created by krio on 25.04.16.
 */
public class WTimerActionListener implements CompoundButton.OnCheckedChangeListener {

    List<Widget> widgetList;
    boolean fromUser = false;

    public WTimerActionListener(List<Widget> widgetList) {
        this.widgetList = widgetList;
    }

    public void setFromUser(boolean fromUser) {
        this.fromUser = fromUser;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (fromUser) {
            WidgetListAdapter.MainViewHolder mainViewHolder = (WidgetListAdapter.MainViewHolder) buttonView.getTag();
            int position = mainViewHolder.getLayoutPosition();

            Widget widget = widgetList.get(position);

            if (isChecked) {
                ((WTimer) widget).start();
            } else {
                ((WTimer) widget).stop();
            }
        }
    }
}
