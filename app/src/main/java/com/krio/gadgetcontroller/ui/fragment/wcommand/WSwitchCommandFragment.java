package com.krio.gadgetcontroller.ui.fragment.wcommand;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.krio.gadgetcontroller.App;
import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.command.CommandValidator;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.ui.fragment.BaseFragment;
import com.krio.gadgetcontroller.ui.listener.OnComponentInteractionListener;
import com.krio.gadgetcontroller.ui.utils.EditTextUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by krio on 11.05.16.
 */
public class WSwitchCommandFragment extends BaseFragment {

    public static final String IS_NEW_COMMANDS = "is_new_commands";

    public static final String COMMAND_ON = "commandOn";
    public static final String COMMAND_OFF = "commandOff";

    @BindView(R.id.command_on_layout)
    TextInputLayout commandOnLayout;

    @BindView(R.id.command_on)
    EditText commandOn;

    @BindView(R.id.command_off_layout)
    TextInputLayout commandOffLayout;

    @BindView(R.id.command_off)
    EditText commandOff;

    EditTextUtils commandOnTextUtils;
    EditTextUtils commandOffTextUtils;

    Bundle defaultValues;
    boolean isNewCommands;

    OnComponentInteractionListener listener;

    public static WSwitchCommandFragment newInstance(boolean isNewCommands, String commandOn, String commandOff) {
        WSwitchCommandFragment fragment = new WSwitchCommandFragment();
        Bundle args = new Bundle();
        args.putBoolean(IS_NEW_COMMANDS, isNewCommands);
        args.putString(COMMAND_ON, commandOn);
        args.putString(COMMAND_OFF, commandOff);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_command_switch, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initEditTextUtils(view);

        defaultValues = getArguments();

        isNewCommands = defaultValues.getBoolean(IS_NEW_COMMANDS);

        commandOn.setText(defaultValues.getString(COMMAND_ON));
        commandOff.setText(defaultValues.getString(COMMAND_OFF));
    }

    private void initEditTextUtils(View view) {
        LinearLayout parentLayout = ButterKnife.findById(view, R.id.parent_layout);

        commandOnTextUtils = new EditTextUtils(getActivity(), parentLayout, commandOnLayout, commandOn);
        commandOffTextUtils = new EditTextUtils(getActivity(), parentLayout, commandOffLayout, commandOff);

        commandOnTextUtils.setMaxLen(10);
        commandOffTextUtils.setMaxLen(10);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_add_element, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_done:
                onMenuDoneSelected();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Context ctx) {
        super.onAttach(ctx);
        try {
            listener = (OnComponentInteractionListener) ctx;
        } catch (ClassCastException e) {
            throw new ClassCastException(ctx.toString() + "Activity must implement fragment's listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void onMenuDoneSelected() {
        String cmdOn = commandOn.getText().toString().trim();
        String cmdOff = commandOff.getText().toString().trim();

        if (hasChanged(cmdOn, cmdOff)) {
            if (validateCommands(cmdOn, cmdOff)) {
                createCommand(CommandType.COMMAND_SWITCH_ENABLE, cmdOn);
                createCommand(CommandType.COMMAND_SWITCH_DISABLE, cmdOff);
                listener.onFinishWidgetBuild();
            }
        } else if (!isNewCommands) {
            getActivity().finish();
        }
    }

    private boolean validateCommands(String commandOn, String commandOff) {

        CommandValidator commandValidator = App.getMainComponent().getCommandValidator();
        boolean emptyCheckSuccess = true;

        if (commandOn.isEmpty()) {
            commandOnTextUtils.showEmptyWarn();
            emptyCheckSuccess = false;
        }

        if (commandOff.isEmpty()) {
            commandOffTextUtils.showEmptyWarn();
            emptyCheckSuccess = false;
        }

        if (!emptyCheckSuccess) {
            return false;
        }

        if (commandOn.equals(commandOff)) {
            commandOnTextUtils.showWarn(getString(R.string.warning_command_equals));
            commandOffTextUtils.showWarn(getString(R.string.warning_command_equals));
            return false;
        }

        if (hasCommandChanged(commandOn, COMMAND_ON)) {
            if (!commandValidator.validateOutputCommand(commandOn)) {
                commandOnTextUtils.showWarn(getString(R.string.warning_command_exist));
                return false;
            }
        }

        if (hasCommandChanged(commandOff, COMMAND_OFF)) {
            if (!commandValidator.validateOutputCommand(commandOff)) {
                commandOffTextUtils.showWarn(getString(R.string.warning_command_exist));
                return false;
            }
        }

        return true;
    }

    private boolean hasChanged (String commandOn, String commandOff) {
        boolean hasChanged = false;

        if (!commandOn.equals(defaultValues.getString(COMMAND_ON))) {
            hasChanged = true;
        }

        if (!commandOff.equals(defaultValues.getString(COMMAND_OFF))) {
            hasChanged = true;
        }

        return hasChanged;
    }

    private boolean hasCommandChanged (String command, String defaultValuesKey) {
        return !command.equals(defaultValues.getString(defaultValuesKey));
    }

    private void createCommand(CommandType commandType, String command) {
        listener.onCreateCommand(commandType, command, null);
    }
}
