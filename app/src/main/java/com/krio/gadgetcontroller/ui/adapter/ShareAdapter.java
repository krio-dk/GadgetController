package com.krio.gadgetcontroller.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.krio.gadgetcontroller.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by krio on 30.10.15.
 */
public class ShareAdapter extends RecyclerView.Adapter<ShareAdapter.ViewHolder> implements View.OnClickListener {

    Context mContext;
    List<ResolveInfo> mApps = new ArrayList<>();
    PackageManager pManager;

    OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onAppSelected(ResolveInfo appInfo);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public ShareAdapter(Context ctx) {
        mContext = ctx;

        Intent intent = new Intent(Intent.ACTION_SEND, null);
        intent.putExtra(Intent.EXTRA_SUBJECT, mContext.getString(R.string.app_name));
        intent.putExtra(Intent.EXTRA_TEXT, mContext.getString(R.string.share_text));
        intent.setType("text/plain");

        pManager = ctx.getPackageManager();
        mApps = pManager.queryIntentActivities(intent, PackageManager.COMPONENT_ENABLED_STATE_DEFAULT);
    }

    @Override
    public ShareAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_share_list, parent, false);
        return new ViewHolder(v, this);
    }

    @Override
    public void onBindViewHolder(ShareAdapter.ViewHolder holder, int position) {
        holder.appName.setText(mApps.get(position).loadLabel(pManager));
        holder.appIcon.setImageDrawable(mApps.get(position).loadIcon(pManager));
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView appName;
        public ImageView appIcon;

        public ViewHolder(View v, View.OnClickListener onClickListener) {
            super(v);

            v.setOnClickListener(onClickListener);
            v.setTag(this);

            appName = (TextView) v.findViewById(R.id.textView);
            appIcon = (ImageView) v.findViewById(R.id.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return mApps.size();
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            ViewHolder viewHolder = (ViewHolder) v.getTag();
            int position = viewHolder.getLayoutPosition();
            mOnItemClickListener.onAppSelected(mApps.get(position));
        }
    }

}
