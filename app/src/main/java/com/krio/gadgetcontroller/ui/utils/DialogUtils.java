package com.krio.gadgetcontroller.ui.utils;

import android.content.Context;
import android.support.v7.app.AlertDialog;

import com.krio.gadgetcontroller.R;

/**
 * Created by krio on 30.10.15.
 */
public class DialogUtils {

    public static int dialogTheme = R.style.DialogTheme;

    public interface OnPositiveButtonClickListener {
        void onPositiveButtonClick();
    }

    public static void showDialog(final Context context, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, dialogTheme);

        builder.setMessage(msg)
                .setCancelable(true)
                .setPositiveButton(R.string.button_ok, null)
                .show();
    }

    public static void showDialogDeletePanel(final Context context, final OnPositiveButtonClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, dialogTheme);

        builder.setMessage(R.string.alert_message_remove_panel)
                .setIcon(R.drawable.ic_delete_forever_black_24dp)
                .setTitle(R.string.alert_title_remove_panel)
                .setCancelable(true)
                .setPositiveButton(R.string.button_remove, (dialog, id) -> listener.onPositiveButtonClick())
                .setNegativeButton(R.string.button_cancel, null)
                .show();
    }

    public static void showDialogDeleteProject(final Context context, final OnPositiveButtonClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, dialogTheme);

        builder.setMessage(R.string.alert_message_remove_project)
                .setTitle(R.string.alert_title_remove_project)
                .setCancelable(true)
                .setPositiveButton(R.string.button_remove, (dialog, id) -> listener.onPositiveButtonClick())
                .setNegativeButton(R.string.button_cancel, null)
                .show();
    }

    public static void showDialogDeleteWidget(final Context context, final OnPositiveButtonClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, dialogTheme);

        builder.setMessage(R.string.alert_message_remove_element)
                .setTitle(R.string.alert_title_remove_element)
                .setCancelable(true)
                .setPositiveButton(R.string.button_remove, (dialog, id) -> listener.onPositiveButtonClick())
                .setNegativeButton(R.string.button_cancel, null)
                .show();
    }

    public static void showDialogDisconnect(final Context context, final OnPositiveButtonClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, dialogTheme);

        builder.setMessage(R.string.alert_message_disconnect_device)
                .setTitle(R.string.alert_title_disconnect_device)
                .setCancelable(false)
                .setPositiveButton(R.string.button_disconnect, (dialog, id) -> listener.onPositiveButtonClick())
                .setNegativeButton(R.string.button_cancel, null)
                .show();
    }

    public static AlertDialog showDialogConnectionLost(final Context context, final OnPositiveButtonClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, dialogTheme);

        return builder.setMessage(R.string.alert_message_connection_lost)
                .setTitle(R.string.alert_title_connection_lost)
                .setCancelable(false)
                .setPositiveButton(R.string.button_reconnect, (dialog, id) -> listener.onPositiveButtonClick())
                .setNegativeButton(R.string.button_cancel, null)
                .show();
    }

    public static AlertDialog showDialogConnectionFailed(final Context context, final OnPositiveButtonClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, dialogTheme);

        return builder.setMessage(R.string.alert_message_connection_faild)
                .setTitle(R.string.alert_title_connection_faild)
                .setCancelable(false)
                .setPositiveButton(R.string.button_connect, (dialog, id) -> listener.onPositiveButtonClick())
                .setNegativeButton(R.string.button_cancel, null)
                .show();
    }


    public static void showDialogClearLog(final Context context, final OnPositiveButtonClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, dialogTheme);

        builder.setMessage(R.string.alert_message_clear_log)
               .setTitle(R.string.alert_title_clear_log)
               .setCancelable(true)
               .setPositiveButton(R.string.button_clear, (dialog, id) -> listener.onPositiveButtonClick())
               .setNegativeButton(R.string.button_cancel, null)
               .show();
    }

    public static void showDialogClearTCPHistory(final Context context, final OnPositiveButtonClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, dialogTheme);

        builder.setMessage(R.string.alert_message_clear_tcp_history)
               .setTitle(R.string.alert_title_clear_tcp_history)
               .setCancelable(true)
               .setPositiveButton(R.string.button_clear, (dialog, id) -> listener.onPositiveButtonClick())
               .setNegativeButton(R.string.button_cancel, null)
               .show();
    }
}
