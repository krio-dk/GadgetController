package com.krio.gadgetcontroller.ui.adapter;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.ui.fragment.ConnectionTCPHistoryFragment;
import com.krio.gadgetcontroller.ui.fragment.ConnectionTCPNewFragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by krio on 04.06.16.
 */
public class TCPConnectionPagerAdapter extends FragmentPagerAdapter {

    Context ctx;

    public TCPConnectionPagerAdapter(FragmentManager fm, Context ctx) {
        super(fm);
        this.ctx = ctx;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ConnectionTCPNewFragment.newInstance();
            case 1:
                return ConnectionTCPHistoryFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public int getItemPosition(Object object) {
        // refresh all fragments when data set changed
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return ctx.getString(R.string.tab_tcp_connection_new);
            case 1:
                return ctx.getString(R.string.tab_tcp_connection_history);
        }
        return null;
    }
}
