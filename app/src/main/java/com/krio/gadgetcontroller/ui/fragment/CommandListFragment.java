package com.krio.gadgetcontroller.ui.fragment;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.krio.gadgetcontroller.App;
import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.project.Project;
import com.krio.gadgetcontroller.ui.adapter.CommandListAdapter;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommandListFragment extends BaseFragment {

    public static final String TYPE = "type";
    public static final int TYPE_INPUT_COMMANDS = 1;
    public static final int TYPE_OUTPUT_COMMANDS = 2;

    @BindView(R.id.empty_layout)
    View emptyLayout;

    @BindView(R.id.recyclerView)
    RecyclerView commandList;

    @Inject
    Project project;

    CommandListAdapter commandAdapter;
    int type;

    public static CommandListFragment newInstance(int type) {
        CommandListFragment fragment = new CommandListFragment();
        Bundle args = new Bundle();
        args.putInt(TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getInt(TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_command_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        App.getMainComponent().inject(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initCommandAdapter();
        initCommandList();
    }

    private void initCommandAdapter() {
        commandAdapter = new CommandListAdapter();
        switch (type) {
            case TYPE_INPUT_COMMANDS:
                commandAdapter.setInputCommandItems(project.getInputCommandMap());
                break;

            case TYPE_OUTPUT_COMMANDS:
                commandAdapter.setOutputCommandItems(project.getWidgetMap());
                break;
        }

        checkCommandPresent();
    }

    private void initCommandList() {
        commandList.setLayoutManager(new LinearLayoutManager(getActivity()));
        commandList.setItemAnimator(new DefaultItemAnimator());
        commandList.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                .showLastDivider()
                .build());
        commandList.setAdapter(commandAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void checkCommandPresent() {
        if (commandAdapter.getItemCount() == 0) {
            setEmptyLayoutVisible(true);
        } else {
            setEmptyLayoutVisible(false);
        }
    }

    private void setEmptyLayoutVisible(boolean visible) {
        emptyLayout.setVisibility(visible ? View.VISIBLE : View.GONE);
    }
}
