package com.krio.gadgetcontroller.ui.listener;

import android.content.SharedPreferences;
import android.view.MenuItem;

/**
 * Created by krio on 01.05.17.
 */
public interface TCPInteractionProvider {
    void addMenuItem(MenuItem item);
    void connect(String host, String port);
    SharedPreferences getSharedPreferences();
}
