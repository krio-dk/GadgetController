package com.krio.gadgetcontroller.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.krio.gadgetcontroller.App;
import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.project.Project;
import com.krio.gadgetcontroller.logic.projectmanager.ProjectManager;
import com.krio.gadgetcontroller.ui.activity.ProjectActivity;
import com.krio.gadgetcontroller.ui.activity.ProjectSettingsActivity;
import com.krio.gadgetcontroller.ui.adapter.ProjectListAdapter;
import com.krio.gadgetcontroller.ui.utils.DialogUtils;
import com.krio.gadgetcontroller.ui.utils.ProjectTouchHelperCallback;

import java.util.Observable;
import java.util.Observer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;


public class ProjectListFragment extends BaseFragment implements ProjectListAdapter.OnProjectActionListener, Observer {

    @BindView(R.id.project_list)
    RecyclerView projectList;

    @BindView(R.id.empty_layout)
    View emptyLayout;

    ProjectListAdapter projectAdapter;
    RecyclerView.ItemAnimator itemAnimator;

    ProjectManager projectManager;

    public static ProjectListFragment newInstance() {
        ProjectListFragment fragment = new ProjectListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_project_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        projectManager = App.getProjectManager();
        projectManager.addObserver(this);

        initProjectAdapter();
        initItemAnimator();
        initProjectList();
        initTouchHelper();
    }

    private void initProjectAdapter() {
        projectAdapter = new ProjectListAdapter(projectList, projectManager.getProjectList());
        projectAdapter.setActionListener(this);
        checkProjectPresent();
    }

    private void initItemAnimator() {
        itemAnimator = new SlideInLeftAnimator();
        itemAnimator.setAddDuration(300);
    }

    private void initProjectList() {
        projectList.setLayoutManager(new LinearLayoutManager(getActivity()));
        projectList.setItemAnimator(itemAnimator);
        projectList.setAdapter(projectAdapter);
    }

    private void initTouchHelper() {
        ProjectTouchHelperCallback callback = new ProjectTouchHelperCallback(getActivity(), projectAdapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(projectList);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        projectManager.deleteObserver(this);
    }

    @OnClick(R.id.add_project)
    public void onAddProjectClick() {
        projectAdapter.cancelRemoveItem();
        projectList.setItemAnimator(itemAnimator);
        startProjectSettingsActivity(0);
    }

    @Override
    public void onProjectSelected(long projectId) {
        startProjectActivity(projectId);
    }

    @Override
    public void onProjectEditSelected(long projectId) {
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            startProjectSettingsActivity(projectId);
        }, 500);
    }

    @Override
    public void onProjectRemoved(long projectId) {
        DialogUtils.showDialogDeleteProject(getActivity(), () -> {
            if (projectAdapter.deleteItem()) {
                projectManager.deleteProject(projectId);
                checkProjectPresent();
            }
        });
    }

    @Override
    public void update(Observable observable, Object data) {
        ProjectManager.Notifier notifier = (ProjectManager.Notifier) data;
        Project project = notifier.getProject();

        Handler handler = new Handler();
        handler.postDelayed(() -> {
            switch (notifier.getType()) {
                case ProjectManager.Notifier.TYPE_PROJECT_CHANGED:
                    projectAdapter.updateProject(project.getPosition());
                    break;

                case ProjectManager.Notifier.TYPE_PROJECT_INSERTED:
                    projectAdapter.addProject(project);
                    checkProjectPresent();
                    break;
            }
        }, 500);
    }

    private void checkProjectPresent() {
        if (projectAdapter.getItemCount() == 0) {
            setEmptyLayoutVisible(true);
        } else {
            setEmptyLayoutVisible(false);
        }
    }

    public void startProjectSettingsActivity(long projectId) {
        Intent intent = new Intent(getActivity(), ProjectSettingsActivity.class);
        intent.putExtra(ProjectSettingsActivity.EDIT_PROJECT_ID, projectId);
        startActivity(intent);
    }

    public void startProjectActivity(long projectId) {
        if (getActivity() != null) {
            Intent intent = new Intent(getActivity(), ProjectActivity.class);
            intent.putExtra(ProjectActivity.EXTRA_PROJECT_ID, projectId);
            startActivity(intent);
        }
    }

    private void setEmptyLayoutVisible(boolean visible) {
        emptyLayout.setVisibility(visible ? View.VISIBLE : View.GONE);
    }
}
