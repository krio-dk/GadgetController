package com.krio.gadgetcontroller.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.provider.widget.WidgetType;
import com.krio.gadgetcontroller.ui.listener.OnComponentInteractionListener;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class ComponentListFragment extends BaseFragment {

    private OnComponentInteractionListener listener;

    public static ComponentListFragment newInstance() {
        ComponentListFragment fragment = new ComponentListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_component_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnComponentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "Activity must implement fragment's callbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @OnClick({
            R.id.widget_button,
            R.id.widget_switch,
            R.id.widget_seekbar,
            R.id.widget_joystick,
            R.id.widget_monitor,
            R.id.widget_led,
            R.id.widget_text_label,
            R.id.widget_text_timer,
            R.id.control_panel
    })
    public void onWidgetClick(View view) {
        switch (view.getId()) {
            case R.id.widget_monitor:
                listener.onWidgetTypeSelected(WidgetType.DISPLAY);
                break;
            case R.id.widget_led:
                listener.onWidgetTypeSelected(WidgetType.LED);
                break;

            case R.id.widget_button:
                listener.onWidgetTypeSelected(WidgetType.BUTTON);
                break;
            case R.id.widget_seekbar:
                listener.onWidgetTypeSelected(WidgetType.SEEKBAR);
                break;
            case R.id.widget_switch:
                listener.onWidgetTypeSelected(WidgetType.SWITCH);
                break;
            case R.id.widget_joystick:
                listener.onWidgetTypeSelected(WidgetType.JOYSTICK);
                break;

            case R.id.widget_text_label:
                listener.onWidgetTypeSelected(WidgetType.LABEL);
                break;

            case R.id.widget_text_timer:
                listener.onWidgetTypeSelected(WidgetType.TIMER);
                break;

            case R.id.control_panel:
                listener.onPanelAddSelected();
                break;
        }
    }
}
