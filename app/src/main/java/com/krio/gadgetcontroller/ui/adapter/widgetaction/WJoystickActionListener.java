package com.krio.gadgetcontroller.ui.adapter.widgetaction;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.krio.gadgetcontroller.logic.widget.WJoystick;
import com.krio.gadgetcontroller.logic.widget.Widget;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.ui.adapter.WidgetListAdapter;
import com.krio.gadgetcontroller.ui.myview.joystick.JoystickListener;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by krio on 25.04.16.
 */
public class WJoystickActionListener implements JoystickListener {

    List<Widget> widgetList;
    RecyclerView recyclerView;

    public WJoystickActionListener(List<Widget> widgetList, RecyclerView recyclerView) {
        this.widgetList = widgetList;
        this.recyclerView = recyclerView;
    }

    @Override
    public void onDown() {
        recyclerView.requestDisallowInterceptTouchEvent(true);
    }

    @Override
    public void onDrag(View v, float degrees, float offset) {
        WidgetListAdapter.MainViewHolder mainViewHolder = (WidgetListAdapter.MainViewHolder) v.getTag();
        int position = mainViewHolder.getLayoutPosition();

        Widget widget = widgetList.get(position);

        Map<String, Object> params = new HashMap<>();
        params.put(WJoystick.CMD_PARAM_ANGLE, roundDegrees(degrees));
        params.put(WJoystick.CMD_PARAM_SPEED, roundOffset(offset));

        widget.performCommand(CommandType.COMMAND_JOYSTICK, params);
    }

    @Override
    public void onUp(View v) {
        WidgetListAdapter.MainViewHolder mainViewHolder = (WidgetListAdapter.MainViewHolder) v.getTag();
        int position = mainViewHolder.getLayoutPosition();

        Widget widget = widgetList.get(position);

        Map<String, Object> params = new HashMap<>();
        params.put(WJoystick.CMD_PARAM_ANGLE, 0);
        params.put(WJoystick.CMD_PARAM_SPEED, 0);

        widget.performCommand(CommandType.COMMAND_JOYSTICK, params);
    }

    private int roundDegrees (float degrees) {
        int roundedDegrees = BigDecimal.valueOf(degrees).setScale(0, BigDecimal.ROUND_CEILING).intValue();

        if (roundedDegrees < 0) {
            roundedDegrees = 360 + roundedDegrees;
        }

        return roundedDegrees;
    }

    private int roundOffset (float offset) {
        return BigDecimal.valueOf(offset * 100).setScale(0, BigDecimal.ROUND_CEILING).intValue();
    }
}
