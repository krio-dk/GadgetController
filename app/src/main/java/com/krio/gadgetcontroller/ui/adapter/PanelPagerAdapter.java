package com.krio.gadgetcontroller.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.krio.gadgetcontroller.logic.panel.Panel;
import com.krio.gadgetcontroller.ui.fragment.PanelFragment;

import java.util.List;

/**
 * Created by krio on 25.04.16.
 */
public class PanelPagerAdapter extends FragmentStatePagerAdapter {

    List<Panel> panelList;

    public PanelPagerAdapter(FragmentManager fm, List<Panel> panelList) {
        super(fm);
        this.panelList = panelList;
    }

    @Override
    public Fragment getItem(int position) {
        return PanelFragment.newInstance(panelList.get(position).getId());
    }

    @Override
    public int getCount() {
        return panelList.size();
    }

    @Override
    public int getItemPosition(Object object) {
        // refresh all fragments when data set changed
        return POSITION_NONE;
    }

    public long getPanelId (int position) {
        return panelList.get(position).getId();
    }

    public void update(List<Panel> panelList) {
        this.panelList = panelList;
        notifyDataSetChanged();
    }

    public void update() {
        notifyDataSetChanged();
    }
}