package com.krio.gadgetcontroller.ui.fragment.wattr;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.widget.WButton;
import com.krio.gadgetcontroller.ui.fragment.BaseFragment;
import com.krio.gadgetcontroller.ui.listener.OnComponentInteractionListener;
import com.krio.gadgetcontroller.ui.utils.EditTextUtils;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by krio on 11.05.16.
 */
public class WButtonAttrFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener {

    public static final String IS_NEW_WIDGET = "is_new_widget";

    @BindView(R.id.caption_layout)
    TextInputLayout captionLayout;

    @BindView(R.id.name)
    EditText captionText;

    @BindView(R.id.caption_button_layout)
    TextInputLayout captionButtonLayout;

    @BindView(R.id.caption_button)
    EditText captionButtonText;

    @BindView(R.id.show_caption)
    CheckBox isCaptionVisible;

    @BindView(R.id.element_caption)
    TextView exampleCaptionText;

    @BindView(R.id.button)
    Button exampleCaptionButtonText;

    EditTextUtils captionTextUtils;
    EditTextUtils captionButtonTextUtils;

    Bundle defaultValues;
    boolean isNewWidget;

    OnComponentInteractionListener callbacks;

    public static WButtonAttrFragment newInstance(boolean isNewWidget, String caption, boolean captionVisible, String btnText) {
        WButtonAttrFragment fragment = new WButtonAttrFragment();
        Bundle args = new Bundle();
        args.putBoolean(IS_NEW_WIDGET, isNewWidget);
        args.putString(WButton.ATTR_CAPTION_TEXT, caption);
        args.putBoolean(WButton.ATTR_IS_CAPTION_VISIBLE, captionVisible);
        args.putString(WButton.ATTR_BUTTON_TEXT, btnText);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_attr_button, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        isCaptionVisible.setOnCheckedChangeListener(this);
        initEditTextUtils(view);

        defaultValues = getArguments();

        isNewWidget = defaultValues.getBoolean(IS_NEW_WIDGET);

        captionText.setText(defaultValues.getString(WButton.ATTR_CAPTION_TEXT));
        isCaptionVisible.setChecked(defaultValues.getBoolean(WButton.ATTR_IS_CAPTION_VISIBLE));
        captionButtonText.setText(defaultValues.getString(WButton.ATTR_BUTTON_TEXT));
    }

    private void initEditTextUtils(View view) {
        LinearLayout parentLayout = ButterKnife.findById(view, R.id.parent_layout);

        captionTextUtils = new EditTextUtils(getActivity(), parentLayout, captionLayout, captionText);
        captionButtonTextUtils = new EditTextUtils(getActivity(), parentLayout, captionButtonLayout, captionButtonText);

        captionTextUtils.setMaxLen(15);
        captionButtonTextUtils.setMaxLen(15);

        captionTextUtils.setChangeListener(str -> {
            if (str.isEmpty()) {
                exampleCaptionText.setText(R.string.widget_name_button);
            } else {
                exampleCaptionText.setText(str);
            }
        });

        captionButtonTextUtils.setChangeListener(str -> {
            if (str.isEmpty()) {
                exampleCaptionButtonText.setText(R.string.placeholder_widget_button_value);
            } else {
                exampleCaptionButtonText.setText(str);
            }
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            captionLayout.setVisibility(View.VISIBLE);
            exampleCaptionText.setVisibility(View.VISIBLE);
        } else {
            captionLayout.setVisibility(View.GONE);
            exampleCaptionText.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_add_element, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_done:
                String captionButton = captionButtonText.getText().toString();
                String caption = captionText.getText().toString();
                boolean showCaption = isCaptionVisible.isChecked();
                onMenuDoneSelected(captionButton, caption, showCaption);
        }
        return super.onOptionsItemSelected(item);
    }

    private void onMenuDoneSelected(String captionButton, String caption, boolean showCaption) {
        if (hasChanged(captionButton, caption, showCaption)) {
            if (validateAttrs(captionButton, caption, showCaption)) {
                setWidgetAttr();
            }
        } else if (!isNewWidget) {
            getActivity().finish();
        }
    }

    private boolean validateAttrs(String captionButton, String caption, boolean showCaption) {

        boolean emptyFlag = false;

        if (showCaption && caption.isEmpty()) {
            captionTextUtils.showEmptyWarn();
            emptyFlag = true;
        }

        if (captionButton.isEmpty()) {
            captionButtonTextUtils.showEmptyWarn();
            emptyFlag = true;
        }

        if (emptyFlag) {
            return false;
        }

        if (showCaption && !captionTextUtils.isLengthCorrect()) {
            return false;
        }

        if (!captionButtonTextUtils.isLengthCorrect()) {
            return false;
        }

        return true;
    }

    private boolean hasChanged(String captionButton, String caption, boolean showCaption) {
        boolean hasChanged = false;

        if (!captionButton.equals(defaultValues.getString(WButton.ATTR_BUTTON_TEXT))) {
            hasChanged = true;
        }

        if (!caption.equals(defaultValues.getString(WButton.ATTR_CAPTION_TEXT))) {
            hasChanged = true;
        }

        if (showCaption != defaultValues.getBoolean(WButton.ATTR_IS_CAPTION_VISIBLE)) {
            hasChanged = true;
        }

        return hasChanged;
    }

    @Override
    public void onAttach(Context ctx) {
        super.onAttach(ctx);
        try {
            callbacks = (OnComponentInteractionListener) ctx;
        } catch (ClassCastException e) {
            throw new ClassCastException(ctx.toString() + "Activity must implement fragment's listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void setWidgetAttr() {
        Map<String, Object> attr = new HashMap<>();
        attr.put(WButton.ATTR_CAPTION_TEXT, captionText.getText().toString());
        attr.put(WButton.ATTR_BUTTON_TEXT, captionButtonText.getText().toString());
        attr.put(WButton.ATTR_IS_CAPTION_VISIBLE, isCaptionVisible.isChecked());
        callbacks.onSetAttrs(attr);
    }
}
