package com.krio.gadgetcontroller.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import com.krio.gadgetcontroller.App;
import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.logic.panel.Panel;
import com.krio.gadgetcontroller.logic.project.Project;
import com.krio.gadgetcontroller.logic.widget.WButton;
import com.krio.gadgetcontroller.logic.widget.WLed;
import com.krio.gadgetcontroller.logic.widget.WTimer;
import com.krio.gadgetcontroller.logic.widget.Widget;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;
import com.krio.gadgetcontroller.ui.fragment.PanelAttrFragment;
import com.krio.gadgetcontroller.ui.fragment.wattr.WButtonAttrFragment;
import com.krio.gadgetcontroller.ui.fragment.wattr.WDisplayAttrFragment;
import com.krio.gadgetcontroller.ui.fragment.wattr.WJoystickAttrFragment;
import com.krio.gadgetcontroller.ui.fragment.wattr.WLedAttrFragment;
import com.krio.gadgetcontroller.ui.fragment.wattr.WSeekBarAttrFragment;
import com.krio.gadgetcontroller.ui.fragment.wattr.WSwitchAttrFragment;
import com.krio.gadgetcontroller.ui.fragment.wattr.WLabelAttrFragment;
import com.krio.gadgetcontroller.ui.fragment.wattr.WTimerAttrFragment;
import com.krio.gadgetcontroller.ui.fragment.wcommand.WButtonCommandFragment;
import com.krio.gadgetcontroller.ui.fragment.wcommand.WJoystickCommandFragment;
import com.krio.gadgetcontroller.ui.fragment.wcommand.WLedCommandFragment;
import com.krio.gadgetcontroller.ui.fragment.wcommand.WSeekBarCommandFragment;
import com.krio.gadgetcontroller.ui.fragment.wcommand.WSwitchCommandFragment;
import com.krio.gadgetcontroller.ui.fragment.wcommand.WTextDisplayCommandFragment;
import com.krio.gadgetcontroller.ui.fragment.wcommand.WTimerCommandFragment;

import java.util.Map;

import javax.inject.Inject;

public class ComponentEditActivity extends ComponentEditInterfaceAdapter  {

    public static final String PANEL_ID = "panel_id";
    public static final String WIDGET_ID = "widget_id";

    long panelId;
    long widgetId;

    @Inject
    Project project;

    Panel panel;
    Widget widget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_component_edit);

        App.getMainComponent().inject(this);

        requestCode = getIntent().getIntExtra(REQUEST_CODE, 0);

        initActionBar();
        checkRequestCode(requestCode);
    }

    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.title_edit_mode);
        }
    }

    private void checkRequestCode (int requestCode) {
        if (requestCode != 0) {
            switch (requestCode) {
                case ProjectActivity.REQUEST_EDIT_PANEL:
                    panelId = getIntent().getLongExtra(PANEL_ID, 0);
                    panel = project.getPanel(panelId);
                    showFragment(PanelAttrFragment.newInstance(panel.getName()));
                    break;

                case ProjectActivity.REQUEST_EDIT_WIDGET_ATTR:
                    widgetId = getIntent().getLongExtra(WIDGET_ID, 0);
                    widget = project.getWidget(widgetId);
                    showWidgetAttrFragment(widget.getWidgetType());
                    break;

                case ProjectActivity.REQUEST_EDIT_WIDGET_COMMANDS:
                    widgetId = getIntent().getLongExtra(WIDGET_ID, 0);
                    widget = project.getWidget(widgetId);
                    showWidgetCommandFragment(widget.getWidgetType());

                    break;
            }
        }
    }

    private void showWidgetAttrFragment (WidgetType widgetType) {
        switch (widgetType) {
            case BUTTON:
                showFragment(WButtonAttrFragment.newInstance(
                        false,
                        widget.getCaption(),
                        widget.isCaptionVisible(),
                        ((WButton) widget).getButtonText())
                );
                break;

            case SWITCH:
                showFragment(WSwitchAttrFragment.newInstance(false, widget.getCaption()));
                break;

            case SEEKBAR:
                showFragment(WSeekBarAttrFragment.newInstance(
                        false,
                        widget.getCaption(),
                        widget.isCaptionVisible())
                );
                break;

            case JOYSTICK:
                showFragment(WJoystickAttrFragment.newInstance(
                        false,
                        widget.getCaption(),
                        widget.isCaptionVisible())
                );
                break;

            case DISPLAY:
                showFragment(WDisplayAttrFragment.newInstance(
                        false,
                        widget.getCaption(),
                        widget.isCaptionVisible())
                );
                break;

            case LED:
                showFragment(WLedAttrFragment.newInstance(false, widget.getCaption(), ((WLed) widget).getColor()));
                break;

            case LABEL:
                showFragment(WLabelAttrFragment.newInstance(false, widget.getCaption()));
                break;

            case TIMER:
                showFragment(WTimerAttrFragment.newInstance(false, ((WTimer) widget).getTimerInterval()));
                break;
        }
    }

    private void showWidgetCommandFragment(WidgetType widgetType) {

        Command cmd1;
        Command cmd2;

        switch (widgetType) {
            case BUTTON:

                cmd1 = widget.getOutputCommand(CommandType.COMMAND_BUTTON_DOWN);
                cmd2 = widget.getOutputCommand(CommandType.COMMAND_BUTTON_UP);

                showFragment(WButtonCommandFragment.newInstance(false, cmd1.getCmd(), cmd2.getCmd()));
                break;

            case SWITCH:

                cmd1 = widget.getOutputCommand(CommandType.COMMAND_SWITCH_ENABLE);
                cmd2 = widget.getOutputCommand(CommandType.COMMAND_SWITCH_DISABLE);

                showFragment(WSwitchCommandFragment.newInstance(false, cmd1.getCmd(), cmd2.getCmd()));
                break;

            case SEEKBAR:

                cmd1 = widget.getOutputCommand(CommandType.COMMAND_SEEKBAR);

                showFragment(WSeekBarCommandFragment.newInstance(false, cmd1.getCmd()));
                break;

            case JOYSTICK:

                cmd1 = widget.getOutputCommand(CommandType.COMMAND_JOYSTICK);

                showFragment(WJoystickCommandFragment.newInstance(false, cmd1.getCmd()));
                break;

            case DISPLAY:

                cmd1 = project.getInputCommand(widgetId, CommandType.COMMAND_DISPLAY);

                showFragment(WTextDisplayCommandFragment.newInstance(false, cmd1.getCmd()));
                break;

            case LED:

                cmd1 = project.getInputCommand(widgetId, CommandType.COMMAND_LED_ENABLE);
                cmd2 = project.getInputCommand(widgetId, CommandType.COMMAND_LED_DISABLE);

                showFragment(WLedCommandFragment.newInstance(false, cmd1.getCmd(), cmd2.getCmd()));
                break;

            case LABEL:
                onFinishWidgetBuild(); // label has no commands
                break;

            case TIMER:

                cmd1 = widget.getOutputCommand(CommandType.COMMAND_TIMER);

                showFragment(WTimerCommandFragment.newInstance(false, cmd1.getCmd()));
                break;
        }
    }

    private void showFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
    }

    @Override
    public void onEditPanelAttr(String caption) {
        panel.setName(caption);
        finishActivity(PANEL_ID, panelId);
    }

    @Override
    public void onEditWidgetAttr(Map<String, Object> attrs) {
        widget.setAttributes(attrs);
        finishActivity(WIDGET_ID, widgetId);
    }

    @Override
    public void onEditCommand(CommandType commandType, String cmd, Map<String, Object> params) {
        Command outputCommand = widget.getOutputCommand(commandType);
        if (outputCommand != null) {
            outputCommand.setCmd(cmd);
            outputCommand.setParams(params);
        }

        Command inputCommand  = project.getInputCommand(widgetId, commandType);
        if (inputCommand != null) {
            inputCommand.setCmd(cmd);
            inputCommand.setParams(params);
        }
    }

    @Override
    public void onFinishEditCommands() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    public void finishActivity (String extraName, long extraValue) {
        Intent intent = new Intent();
        intent.putExtra(extraName, extraValue);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}
