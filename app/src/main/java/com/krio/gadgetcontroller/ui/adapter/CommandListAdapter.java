package com.krio.gadgetcontroller.ui.adapter;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.logic.command.InputCommand;
import com.krio.gadgetcontroller.logic.widget.WTimer;
import com.krio.gadgetcontroller.logic.widget.Widget;

import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.krio.gadgetcontroller.provider.widget.WidgetType.TIMER;

/**
 * Created by krio on 01.06.16.
 */
public class CommandListAdapter extends RecyclerView.Adapter<CommandListAdapter.ViewHolder> {

    public static final int TYPE_COMMAND = 1;
    public static final int TYPE_WIDGET = 2;

    List<Item> item = new ArrayList<>();

    public void setInputCommandItems(Map<String, InputCommand> inputCommandMap) {
        Widget widget = null;

        for (InputCommand command : inputCommandMap.values()) {
            if (widget != command.getWidget()) {
                widget = command.getWidget();
                item.add(new Item(TYPE_WIDGET, widget));
            }
            item.add(new Item(TYPE_COMMAND, command));
        }
        notifyItemRangeInserted(0, item.size());
    }

    public void setOutputCommandItems(Map<Long, Widget> widgetMap) {
        for (Widget widget : widgetMap.values()) {
            List<Command> commandList = widget.getOutputCommands();
            if (!commandList.isEmpty()) {
                item.add(new Item(TYPE_WIDGET, widget));
                for (Command command : commandList) {
                    item.add(new Item(TYPE_COMMAND, command));
                }
            }
        }
        notifyItemRangeInserted(0, item.size());
    }

    class Item {
        int type;
        Object object;

        public Item(int type, Object object) {
            this.type = type;
            this.object = object;
        }

        public int getType() {
            return type;
        }

        public Object getObject() {
            return object;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return item.get(position).getType();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(viewType == TYPE_WIDGET ? R.layout.item_command_list_widget : R.layout.item_command_list, parent, false);
        return new ViewHolder(v, viewType);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_WIDGET:
                bindWidgetViewHolder(holder, (Widget) item.get(position).getObject());
                break;

            case TYPE_COMMAND:
                bindCommandViewHolder(holder, (Command) item.get(position).getObject());
                break;
        }
    }

    private void bindWidgetViewHolder(ViewHolder holder, Widget widget) {
        holder.parentLayout.setBackgroundColor(ContextCompat.getColor(holder.parentLayout.getContext(), R.color.colorWidgetInCommandListBackground));

        String widgetType = "";
        switch (widget.getWidgetType()) {
            case BUTTON:
                widgetType = holder.primaryText.getResources().getString(R.string.widget_name_button);
                break;
            case SEEKBAR:
                widgetType = holder.primaryText.getResources().getString(R.string.widget_name_seekbar);
                break;
            case SWITCH:
                widgetType = holder.primaryText.getResources().getString(R.string.widget_name_switch);
                break;
            case JOYSTICK:
                widgetType = holder.primaryText.getResources().getString(R.string.widget_name_joystick);
                break;
            case LED:
                widgetType = holder.primaryText.getResources().getString(R.string.widget_name_led);
                break;
            case DISPLAY:
                widgetType = holder.primaryText.getResources().getString(R.string.widget_name_display);
                break;
            case LABEL:
                widgetType = holder.primaryText.getResources().getString(R.string.widget_name_label);
                break;
            case TIMER:
                widgetType = holder.primaryText.getResources().getString(R.string.widget_name_timer);
                break;
        }

        String caption = holder.primaryText.getResources().getString(R.string.widget);

        holder.primaryText.setText(caption + " " + widgetType.toLowerCase());


        Resources recources =  holder.primaryText.getResources();
        if (widget.getWidgetType() != TIMER) {
            holder.secondaryText.setText(
                    (widget.getCaption() != null && !widget.getCaption().isEmpty() ? widget
                            .getCaption() : recources.getString(R.string.text_none_label_full)));
        } else {
            WTimer timer = (WTimer) widget;
            holder.secondaryText.setText(recources.getString(R.string.timer_interval, WTimer
                    .getFormattedCountDownTime(timer.getTimerInterval() * 1000)));
        }
    }

    private void bindCommandViewHolder(ViewHolder holder, Command command) {

        holder.command.setText(command.getCmd());

        String commandType = "";

        switch (command.getType()) {
            case COMMAND_LED_ENABLE:
                commandType = holder.command.getContext().getString(R.string.command_led_enable);
                break;

            case COMMAND_LED_DISABLE:
                commandType = holder.command.getContext().getString(R.string.command_led_disable);
                break;

            case COMMAND_DISPLAY:
                commandType = holder.command.getContext().getString(R.string.command_display);
                break;


            case COMMAND_SWITCH_ENABLE:
                commandType = holder.command.getContext().getString(R.string.command_switch_on);
                break;


            case COMMAND_SWITCH_DISABLE:
                commandType = holder.command.getContext().getString(R.string.command_switch_off);
                break;


            case COMMAND_BUTTON_DOWN:
                commandType = holder.command.getContext().getString(R.string.command_button_down);
                break;


            case COMMAND_BUTTON_UP:
                commandType = holder.command.getContext().getString(R.string.command_button_up);
                break;


            case COMMAND_SEEKBAR:
                commandType = holder.command.getContext().getString(R.string.command_seekbar);
                break;


            case COMMAND_JOYSTICK:
                commandType = holder.command.getContext().getString(R.string.command_joystick);
                break;

            case COMMAND_TIMER:
                commandType = holder.command.getContext().getString(R.string.command_timer);
                break;
        }

        holder.commandType.setText(commandType);
    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        @BindView(R.id.primaryText)
        TextView primaryText;

        @Nullable
        @BindView(R.id.secondaryText)
        TextView secondaryText;

        @Nullable
        @BindView(R.id.command)
        TextView command;

        @Nullable
        @BindView(R.id.command_type)
        TextView commandType;

        @Nullable
        @BindView(R.id.parent_layout)
        View parentLayout;


        public ViewHolder(View view, int viewType) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
