package com.krio.gadgetcontroller.ui.fragment.wcommand;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.krio.gadgetcontroller.App;
import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.command.CommandValidator;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.ui.fragment.BaseFragment;
import com.krio.gadgetcontroller.ui.listener.OnComponentInteractionListener;
import com.krio.gadgetcontroller.ui.utils.EditTextUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by krio on 11.05.16.
 */
public class WSeekBarCommandFragment extends BaseFragment {

    public static final String IS_NEW_COMMANDS = "is_new_commands";

    public static final String COMMAND = "command";

    @BindView(R.id.command_layout)
    TextInputLayout commandLayout;

    @BindView(R.id.command)
    EditText command;

    EditTextUtils commandTextUtils;

    Bundle defaultValues;
    boolean isNewCommands;

    OnComponentInteractionListener listener;

    public static WSeekBarCommandFragment newInstance(boolean isNewCommands, String command) {
        WSeekBarCommandFragment fragment = new WSeekBarCommandFragment();
        Bundle args = new Bundle();
        args.putBoolean(IS_NEW_COMMANDS, isNewCommands);
        args.putString(COMMAND, command);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_command_seekbar, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initEditTextUtils(view);

        defaultValues = getArguments();

        isNewCommands = defaultValues.getBoolean(IS_NEW_COMMANDS);

        command.setText(defaultValues.getString(COMMAND));
    }

    private void initEditTextUtils(View view) {
        LinearLayout parentLayout = ButterKnife.findById(view, R.id.parent_layout);
        commandTextUtils = new EditTextUtils(getActivity(), parentLayout, commandLayout, command);
        commandTextUtils.setMaxLen(10);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_add_element, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_done:
                onMenuDoneSelected();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Context ctx) {
        super.onAttach(ctx);
        try {
            listener = (OnComponentInteractionListener) ctx;
        } catch (ClassCastException e) {
            throw new ClassCastException(ctx.toString() + "Activity must implement fragment's listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void onMenuDoneSelected() {
        String cmd = command.getText().toString().trim();

        if (hasChanged(cmd)) {
            if (validateCommand(cmd)) {
                createCommand(CommandType.COMMAND_SEEKBAR, cmd);
                listener.onFinishWidgetBuild();
            }
        } else if (!isNewCommands) {
            getActivity().finish();
        }
    }

    private boolean validateCommand(String command) {

        CommandValidator commandValidator = App.getMainComponent().getCommandValidator();

        if (command.isEmpty()) {
            commandTextUtils.showEmptyWarn();
            return false;
        }

        if (!commandValidator.validateOutputCommand(command)) {
            commandTextUtils.showWarn(getString(R.string.warning_command_exist));
            return false;
        }

        return true;
    }

    private boolean hasChanged (String command) {
        boolean hasChanged = false;

        if (!command.equals(defaultValues.getString(COMMAND))) {
            hasChanged = true;
        }

        return hasChanged;
    }

    private void createCommand(CommandType commandType, String command) {
        listener.onCreateCommand(commandType, command, null);
    }
}
