package com.krio.gadgetcontroller.ui.fragment;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.connection.core.ConnectPerformer;
import com.krio.gadgetcontroller.logic.connection.core.Connection;
import com.krio.gadgetcontroller.logic.connection.core.ConnectionFactory;
import com.krio.gadgetcontroller.ui.adapter.DeviceListBluetoothAdapter;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.AnimRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DeviceListBluetoothFragment extends BaseFragment implements DeviceListBluetoothAdapter.OnBluetoothDeviceSelectedListener {

    private static final String NEW_DEVICE_LIST = "new_device_list";

    @BindView(R.id.refresh)
    ImageButton refreshButton;

    @BindView(R.id.main_content)
    View mainContent;

    @BindView(R.id.btconfig)
    View btConfig;

    @BindView(R.id.no_bluetooth)
    View noBluetooth;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    @BindView(R.id.paired_devices)
    RecyclerView pairedDeviceList;

    @BindView(R.id.new_devices)
    RecyclerView newDevicesList;

    private MenuItem menuItemLoader;

    private DeviceListBluetoothAdapter pairedDeviceAdapter;
    private DeviceListBluetoothAdapter newDeviceAdapter;

    private BluetoothAdapter bluetoothAdapter;

    public static DeviceListBluetoothFragment newInstance() {
        DeviceListBluetoothFragment fragment = new DeviceListBluetoothFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        saveNewDeviceListState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_device_list_bluetooth, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (bluetoothAdapter != null) {

            setSubtitle(R.string.subtitle_select_device);

            initNewDeviceComponents();
            initPairedDeviceComponents();

            initMakeDiscoveryButton();

            registerDiscoveryReceiver(BluetoothDevice.ACTION_FOUND);
            registerDiscoveryReceiver(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

            checkDiscovering();
        }
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
        inflater.inflate(R.menu.menu_loader, menu);
        menuItemLoader = menu.findItem(R.id.menu_loader);
        menuItemLoader.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void initNewDeviceComponents() {
        newDevicesList.setLayoutManager(new LinearLayoutManager(getActivity()));
        newDevicesList.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                .build());

        newDeviceAdapter = new DeviceListBluetoothAdapter(DeviceListBluetoothAdapter.DEVICE_NEW);
        newDeviceAdapter.setOnItemClickListener(this);
        newDevicesList.setAdapter(newDeviceAdapter);
    }

    private void initPairedDeviceComponents() {
        pairedDeviceList.setLayoutManager(new LinearLayoutManager(getActivity()));
        pairedDeviceList.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                .build());

        pairedDeviceAdapter = new DeviceListBluetoothAdapter(DeviceListBluetoothAdapter.DEVICE_PAIRED);
        pairedDeviceAdapter.setOnItemClickListener(this);
        pairedDeviceList.setAdapter(pairedDeviceAdapter);
    }

    private void initMakeDiscoveryButton() {
        refreshButton.setOnTouchListener((v, event) -> bluetoothAdapter.isDiscovering());
        refreshButton.setOnClickListener((v) -> doDiscovery());
    }


    private void registerDiscoveryReceiver(String actionFilter) {
        IntentFilter filter = new IntentFilter(actionFilter);
        getActivity().registerReceiver(discoveryReceiver, filter);
    }

    private void registerBluetoothStateChangeReceiver() {
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        getActivity().registerReceiver(bluetoothStateChangeReceiver, filter);
    }


    private void unregisterDiscoveryReceiver() {
        getActivity().unregisterReceiver(discoveryReceiver);
    }

    private void unregisterBluetoothStateChangeReceiver() {
        getActivity().unregisterReceiver(bluetoothStateChangeReceiver);
    }

    private void checkDiscovering() {
        if (bluetoothAdapter.isEnabled()) {
            if (bluetoothAdapter.isDiscovering()) {
                ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
                if (actionBar != null) {
                    actionBar.setSubtitle(R.string.subtitle_scanning_devices);
                }
                startRotateAnimation(refreshButton, R.anim.anim_refresh);
            }
        }
    }


    private void saveNewDeviceListState(Bundle savedInstanceState) {
        if (bluetoothAdapter != null && newDeviceAdapter.getItemCount() != 0) {
            savedInstanceState.putParcelableArrayList(NEW_DEVICE_LIST, newDeviceAdapter.getDevices());
        }
    }

    private void restoreNewDeviceListState(Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.containsKey(NEW_DEVICE_LIST)) {
            List<BluetoothDevice> newDeviceList = savedInstanceState.getParcelableArrayList(NEW_DEVICE_LIST);
            newDeviceAdapter.addAll(newDeviceList);
        }
    }

    private void setBluetoothDisabledState() {
        setSubtitle(R.string.subtitle_bluetooth_disabled);
        stopRotateAnimation(refreshButton);

        mainContent.setVisibility(View.GONE);
        btConfig.setVisibility(View.VISIBLE);
    }

    private void setBluetoothEnabledState() {
        Handler myHandler = new Handler();
        myHandler.postDelayed(() -> {
            setSubtitle(R.string.subtitle_select_device);

            btConfig.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.GONE);
            mainContent.setVisibility(View.VISIBLE);

        }, 2000);
    }

    private void setBluetoothTurningOnState () {
        setSubtitle(R.string.subtitle_turning_on_bluetooth);

        mainContent.setVisibility(View.GONE);
        btConfig.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void updatePairedDeviceList() {
        pairedDeviceAdapter.clear();

        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            pairedDeviceAdapter.addAll(pairedDevices);
        } else {
            pairedDeviceAdapter.add(null);
        }
    }

    private void setSubtitle(int resId) {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setSubtitle(resId);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        restoreNewDeviceListState(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (bluetoothAdapter != null) {
            if (bluetoothAdapter.isEnabled()) {
                updatePairedDeviceList();
            } else {
                setBluetoothDisabledState();
            }
            registerBluetoothStateChangeReceiver();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (bluetoothAdapter != null) {
            unregisterBluetoothStateChangeReceiver();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (bluetoothAdapter != null) {
            unregisterDiscoveryReceiver();
        }
    }

    public void startRotateAnimation(View view, @AnimRes int id) {
        Animation rotation = AnimationUtils.loadAnimation(getActivity(), id);
        rotation.setRepeatCount(Animation.INFINITE);
        view.startAnimation(rotation);
    }

    public void stopRotateAnimation(View v) {
        v.clearAnimation();
    }

    private void doDiscovery() {
        setSubtitle(R.string.subtitle_scanning_devices);
        startRotateAnimation(refreshButton, R.anim.anim_refresh);

        if (bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
        }

        newDeviceAdapter.clear();
        bluetoothAdapter.startDiscovery();
    }

    /**
     * The BroadcastReceiver that listens for discovered devices;
     * changes the title when discovery is finished.
     */
    private final BroadcastReceiver discoveryReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            switch (intent.getAction()) {
                case BluetoothDevice.ACTION_FOUND:
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                        newDeviceAdapter.add(device);
                    }
                    break;

                case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                    setSubtitle(R.string.subtitle_select_device);
                    stopRotateAnimation(refreshButton);

                    if (newDeviceAdapter.getItemCount() == 0) {
                        newDeviceAdapter.add(null);
                    }
                    break;
            }
        }
    };


    private final BroadcastReceiver bluetoothStateChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(intent.getAction())) {
                int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        setBluetoothDisabledState();
                        break;

                    case BluetoothAdapter.STATE_ON:
                        setBluetoothEnabledState();
                        updatePairedDeviceList();
                        break;

                    case BluetoothAdapter.STATE_TURNING_ON:
                        setBluetoothTurningOnState();
                        break;
                }
            }
        }
    };

    @OnClick(R.id.enable_bt)
    public void onEnableBluetoothClick() {
        bluetoothAdapter.enable();
    }

    @Override
    public void onBluetoothDeviceSelected(BluetoothDevice device) {
        bluetoothAdapter.cancelDiscovery();

        Connection connection = ConnectionFactory.createBluetoothConnection(device);

        ConnectPerformer connectPerformer = new ConnectPerformer(
                getActivity(), connection,
                menuItemLoader, null
        );

        connectPerformer.setOnConnectionEventListener(
                new ConnectPerformer.OnConnectionEventListener() {
                    @Override
                    public void onConnecting() {
                        refreshButton.setVisibility(View.GONE);
                    }

                    @Override
                    public void onConnectionFailed() {
                        refreshButton.setVisibility(View.VISIBLE);
                    }
                });

        connectPerformer.performConnect(() -> {
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
        });
    }
}
