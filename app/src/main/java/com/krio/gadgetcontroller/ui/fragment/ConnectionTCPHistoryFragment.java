package com.krio.gadgetcontroller.ui.fragment;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.ui.adapter.TCPHistoryAdapter;
import com.krio.gadgetcontroller.ui.listener.TCPInteractionProvider;
import com.krio.gadgetcontroller.ui.utils.DialogUtils;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedHashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.krio.gadgetcontroller.ui.activity.TCPConnectionActivity.HISTORY_SET;

public class ConnectionTCPHistoryFragment extends BaseFragment implements
        TCPHistoryAdapter.OnHistoryItemClickListener {

    @BindView(R.id.history_list)
    RecyclerView historyListView;

    private TCPHistoryAdapter tcpHistoryAdapter;

    private TCPInteractionProvider interactionProvider;

    public static ConnectionTCPHistoryFragment newInstance() {
        ConnectionTCPHistoryFragment fragment = new ConnectionTCPHistoryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_connection_tcp_history, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initHistoryList();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            interactionProvider = (TCPInteractionProvider) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "Activity must implement fragment's callbacks");
        }
    }

    @Override
    public void onHistoryItemClick(final String host, final String port) {
        interactionProvider.connect(host, port);
    }

    private void initHistoryList() {

        SharedPreferences sharedPreferences = interactionProvider.getSharedPreferences();

        Set<String> historySet = sharedPreferences.getStringSet(HISTORY_SET, new LinkedHashSet<>());

        tcpHistoryAdapter = new TCPHistoryAdapter(historySet);
        tcpHistoryAdapter.setClickListener(this);

        historyListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        historyListView.setAdapter(tcpHistoryAdapter);

        historyListView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                .showLastDivider()
                .build());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_connection_tcp_history, menu);
        interactionProvider.addMenuItem(menu.findItem(R.id.menu_clear));
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_clear:
                SharedPreferences sharedPreferences = interactionProvider.getSharedPreferences();
                DialogUtils.showDialogClearTCPHistory(getActivity(), () -> {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.remove(HISTORY_SET);
                    editor.apply();

                    tcpHistoryAdapter.clear();
                });
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
