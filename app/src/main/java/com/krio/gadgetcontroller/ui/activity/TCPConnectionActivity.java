package com.krio.gadgetcontroller.ui.activity;

import com.google.gson.Gson;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.connection.TCPHistoryItem;
import com.krio.gadgetcontroller.logic.connection.core.ConnectPerformer;
import com.krio.gadgetcontroller.logic.connection.core.Connection;
import com.krio.gadgetcontroller.logic.connection.core.ConnectionFactory;
import com.krio.gadgetcontroller.ui.adapter.TCPConnectionPagerAdapter;
import com.krio.gadgetcontroller.ui.listener.TCPInteractionProvider;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.net.ConnectivityManager.CONNECTIVITY_ACTION;
import static android.net.ConnectivityManager.TYPE_WIFI;

public class TCPConnectionActivity extends BaseActivity implements TCPInteractionProvider {

    public static final String HISTORY_SET = "history_set";

    @BindView(R.id.empty_layout)
    View emptyLayout;

    @BindView(R.id.main_content)
    View mainContent;

    @BindView(R.id.progress_layout)
    View progressLayout;

    @BindView(R.id.container)
    ViewPager viewPager;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    SharedPreferences sharedPreferences;

    TCPConnectionPagerAdapter pagerAdapter;

    List<MenuItem> menuItemList = new ArrayList<>();

    private MenuItem menuItemLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tcp_connection);
        ButterKnife.bind(this);

        initActionBar();
        initComponents();
    }

    // region Initialization

    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.title_tcp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initComponents() {
        pagerAdapter = new TCPConnectionPagerAdapter(getSupportFragmentManager(), this);

        viewPager.setPageMargin(
                getResources().getDimensionPixelOffset(R.dimen.view_pager_separator_size));
        viewPager.setPageMarginDrawable(R.color.colorDarkHintBackground);
        viewPager.setAdapter(pagerAdapter);

        tabLayout.setupWithViewPager(viewPager);

        sharedPreferences = getPreferences(Context.MODE_PRIVATE);
    }

    // endregion

    @Override
    public void onResume() {
        super.onResume();
        checkNetworkConnection();
        registerNetworkStateChangeReceiver();
    }

    @Override
    public void onStop() {
        super.onStop();
        unregisterNetworkStateChangeReceiver();
    }

    // region Menu

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_loader, menu);
        menuItemLoader = menu.findItem(R.id.menu_loader);
        menuItemLoader.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setMenuItemsVisible(boolean visible) {
        for (MenuItem item : menuItemList) {
            item.setVisible(visible);
        }
    }

    @Override
    public void addMenuItem(final MenuItem item) {
        menuItemList.add(item);
    }

    // endregion

    // region Network State Check

    private void checkNetworkConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE
        );

        NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();

        if (activeNetworkInfo != null) {
            switch (activeNetworkInfo.getState()) {

                case CONNECTED:
                    setSubtitle(activeNetworkInfo);
                    setNetworkConnectedState();
                    break;

                case CONNECTING:
                    setNetworkConnectingState();
                    break;

                default:
                    setNetworkDisconnectedState();
                    break;
            }
        } else {
            setNetworkDisconnectedState();
        }
    }

    // endregion

    // region UI State

    private void setSubtitle(NetworkInfo networkInfo) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (networkInfo != null) {

                String subtitle = networkInfo.getTypeName();

                /*
                if (!TextUtils.isEmpty(networkInfo.getSubtypeName())) {
                    subtitle += ", " + networkInfo.getSubtypeName();
                }
                */

                if (networkInfo.getType() == TYPE_WIFI) {
                    subtitle += ", " + networkInfo.getExtraInfo();
                }

                actionBar.setSubtitle(subtitle);
            } else {
                actionBar.setSubtitle(null);
            }
        }
    }

    private void setNetworkConnectedState() {
        pagerAdapter.notifyDataSetChanged();
        setMainContentVisible(true);
        setMenuItemsVisible(true);
    }

    private void setNetworkConnectingState() {
        setSubtitle(null);
        emptyLayout.setVisibility(View.GONE);
        mainContent.setVisibility(View.GONE);
        progressLayout.setVisibility(View.VISIBLE);
        setMenuItemsVisible(false);
    }

    private void setNetworkDisconnectedState() {
        setSubtitle(null);
        setMainContentVisible(false);
        setMenuItemsVisible(false);
    }

    void setMainContentVisible(boolean visible) {
        progressLayout.setVisibility(View.GONE);
        if (visible) {
            emptyLayout.setVisibility(View.GONE);
            mainContent.setVisibility(View.VISIBLE);
        } else {
            mainContent.setVisibility(View.GONE);
            emptyLayout.setVisibility(View.VISIBLE);
        }
    }

    // endregion

    // region Connect

    @Override
    public void connect(final String host, final String port) {
        if (validateData(host, port)) {
            Connection connection = ConnectionFactory.createTCPClientConnection(
                    host,
                    Integer.parseInt(port)
            );

            ConnectPerformer connectPerformer = new ConnectPerformer(
                    this, connection,
                    menuItemLoader, menuItemList
            );

            connectPerformer.performConnect(() -> {
                setResult(Activity.RESULT_OK);
                finish();
            });

            saveToHistory(host, port);
        }
    }

    private boolean validateData(String host, String port) {
        if (TextUtils.isEmpty(host) || TextUtils.isEmpty(port)) {
            return false;
        }

        return true;
    }

    // endregion

    // region History Processing

    @Override
    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    private void saveToHistory(String host, String port) {

        List<String> historyList = new ArrayList<>();
        historyList.addAll(sharedPreferences.getStringSet(HISTORY_SET, new LinkedHashSet<>()));

        historyList.add(0, buildHistoryItem(host, port));

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(HISTORY_SET, new LinkedHashSet<>(historyList));
        editor.apply();

        pagerAdapter.notifyDataSetChanged();
    }


    private String buildHistoryItem(String host, String port) {
        final TCPHistoryItem item = new TCPHistoryItem(host, port);
        final Gson gson = new Gson();
        return gson.toJson(item, TCPHistoryItem.class);
    }

    // endregion

    // region Network State Change Receiver

    private void registerNetworkStateChangeReceiver() {
        final IntentFilter filters = new IntentFilter();
        filters.addAction(CONNECTIVITY_ACTION);
        registerReceiver(networkStateChangeReceiver, filters);
    }

    private void unregisterNetworkStateChangeReceiver() {
        unregisterReceiver(networkStateChangeReceiver);
    }

    private final BroadcastReceiver networkStateChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            checkNetworkConnection();
        }
    };

    // endregion

}
