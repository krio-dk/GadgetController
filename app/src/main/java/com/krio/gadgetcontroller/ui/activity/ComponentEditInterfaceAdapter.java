package com.krio.gadgetcontroller.ui.activity;

import com.krio.gadgetcontroller.logic.panel.Panel;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;
import com.krio.gadgetcontroller.ui.listener.OnComponentInteractionListener;

import java.util.Map;

/**
 * Created by krio on 01.06.16.
 */
public abstract class ComponentEditInterfaceAdapter extends BaseActivity implements OnComponentInteractionListener {

    public static final String REQUEST_CODE = "request_code";
    protected int requestCode;

    @Override
    public void onPanelAddSelected() {
        // do nothing
    }

    @Override
    public void onWidgetTypeSelected(WidgetType widgetType) {
        // do nothing
    }

    @Override
    public void onSetAttrs(Map<String, Object> attrs) {
        if (requestCode == ProjectActivity.REQUEST_EDIT_PANEL) {
            onEditPanelAttr((String) attrs.get(Panel.CAPTION));
        } else {
            onEditWidgetAttr(attrs);
        }
    }

    @Override
    public void onCreateCommand(CommandType commandType, String cmd, Map<String, Object> params) {
        onEditCommand(commandType, cmd, params);
    }

    @Override
    public void onFinishWidgetBuild() {
        onFinishEditCommands();
    }

    public abstract void onEditPanelAttr(String caption);
    public abstract void onEditWidgetAttr(Map<String, Object> attrs);
    public abstract void onEditCommand(CommandType commandType, String cmd, Map<String, Object> params);
    public abstract void onFinishEditCommands();
}
