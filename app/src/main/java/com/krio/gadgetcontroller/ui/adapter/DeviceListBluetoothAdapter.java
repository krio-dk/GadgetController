package com.krio.gadgetcontroller.ui.adapter;

import android.bluetooth.BluetoothDevice;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.krio.gadgetcontroller.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by krio on 30.10.15.
 */
public class DeviceListBluetoothAdapter extends RecyclerView.Adapter<DeviceListBluetoothAdapter.ViewHolder> implements View.OnClickListener {

    public static final int TYPE_EMPTY = 0;
    public static final int TYPE_DEVICE = 1;

    public static final int DEVICE_NEW = 2;
    public static final int DEVICE_PAIRED = 3;

    private int mDeviceType;

    List<BluetoothDevice> mDevices = new ArrayList<>();
    OnBluetoothDeviceSelectedListener mOnItemClickListener;

    public interface OnBluetoothDeviceSelectedListener {
        void onBluetoothDeviceSelected(BluetoothDevice device);
    }

    public void setOnItemClickListener(OnBluetoothDeviceSelectedListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public DeviceListBluetoothAdapter(int deviceType) {
        mDeviceType = deviceType;
    }

    public void addAll(Set<BluetoothDevice> devices) {
        mDevices.addAll(devices);
        notifyDataSetChanged();
    }

    public void addAll(List<BluetoothDevice> devices) {
        mDevices.addAll(devices);
        notifyDataSetChanged();
    }

    public void add(BluetoothDevice device) {
        mDevices.add(device);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (mDevices.get(position) == null) {
            return TYPE_EMPTY;
        } else {
            return TYPE_DEVICE;
        }
    }

    @Override
    public DeviceListBluetoothAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(viewType == TYPE_EMPTY ? R.layout.item_single_line_list : R.layout.item_two_line_list, parent, false);
        return new ViewHolder(v, viewType, this);
    }

    @Override
    public void onBindViewHolder(DeviceListBluetoothAdapter.ViewHolder holder, int position) {

        if (getItemViewType(position) == TYPE_DEVICE) {
            holder.name.setText(mDevices.get(position).getName() == null ? holder.name.getContext().getString(R.string.text_none_defined_device_name) : mDevices.get(position).getName());
            holder.mac.setText(mDevices.get(position).getAddress());
        } else {
            if (mDeviceType == DEVICE_NEW) {
                holder.empty.setText(holder.empty.getContext().getString(R.string.text_none_found_device));
            } else {
                holder.empty.setText(holder.empty.getContext().getString(R.string.text_none_paired_devices));
            }
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView mac;
        public TextView empty;

        public ViewHolder(View v, int viewType, View.OnClickListener onClickListener) {
            super(v);

            if (viewType == TYPE_DEVICE) {
                v.setOnClickListener(onClickListener);
                v.setTag(this);

                name = (TextView) v.findViewById(R.id.primaryText);
                mac = (TextView) v.findViewById(R.id.secondaryText);
            } else {
                empty = (TextView) v.findViewById(R.id.textView);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mDevices.size();
    }

    public void clear () {
        mDevices.clear();
        notifyDataSetChanged();
    }

    public ArrayList<BluetoothDevice> getDevices () {
        return (ArrayList<BluetoothDevice>) mDevices;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            ViewHolder viewHolder = (ViewHolder) v.getTag();
            int position = viewHolder.getLayoutPosition();
            mOnItemClickListener.onBluetoothDeviceSelected(mDevices.get(position));
        }
    }
}
