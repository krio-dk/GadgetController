package com.krio.gadgetcontroller.ui.fragment;

import android.support.v4.app.Fragment;

import butterknife.Unbinder;

/**
 * Created by krio on 21.04.16.
 */
public abstract class BaseFragment extends Fragment {

    protected Unbinder unbinder;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // unbinder.unbind();
    }
}
