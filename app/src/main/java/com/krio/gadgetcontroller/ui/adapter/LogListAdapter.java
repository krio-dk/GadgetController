package com.krio.gadgetcontroller.ui.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.connection.log.LogItem;

import java.util.List;

/**
 * Created by krio on 30.10.15.
 */
public class LogListAdapter extends RecyclerView.Adapter<LogListAdapter.ViewHolder> {

    List<LogItem> logItemList;

    public LogListAdapter(List<LogItem> logItemList) {
        this.logItemList = logItemList;
    }

    @Override
    public LogListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_log_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(LogListAdapter.ViewHolder holder, int position) {

        holder.time.setText(logItemList.get(position).getTime());
        holder.value.setText(logItemList.get(position).getValue());

        switch (logItemList.get(position).getItemType()) {
            case STATE_CHANGE:
                holder.value.setBackgroundColor(ContextCompat.getColor(holder.value.getContext(), R.color.colorLogConnectState));
                holder.arrow.setVisibility(View.GONE);
                break;


            case CONNECTION_PROBLEM:
                holder.value.setBackgroundColor(ContextCompat.getColor(holder.value.getContext(), R.color.colorLogConnectProblem));
                holder.arrow.setVisibility(View.GONE);
                break;


            case EXECUTE_OUTPUT_COMMAND:
                holder.value.setBackgroundColor(ContextCompat.getColor(holder.value.getContext(), R.color.colorLogControlCommand));
                holder.arrow.setVisibility(View.GONE);
                break;


            case EXECUTE_INPUT_COMMAND:
                holder.value.setBackgroundColor(ContextCompat.getColor(holder.value.getContext(), R.color.colorLogDisplayCommand));
                holder.arrow.setVisibility(View.GONE);
                break;


            case RECEIVE_DATA:
                holder.value.setBackgroundColor(ContextCompat.getColor(holder.value.getContext(), R.color.colorWhiteBaseBackground));
                holder.arrow.setVisibility(View.VISIBLE);
                holder.arrow.setImageResource(R.drawable.ic_arrow_back_black_24dp);
                break;

            case SEND_DATA:
                holder.value.setBackgroundColor(ContextCompat.getColor(holder.value.getContext(), R.color.colorLogOutputData));
                holder.arrow.setVisibility(View.VISIBLE);
                holder.arrow.setImageResource(R.drawable.ic_arrow_forward_black_24dp);
                break;

        }

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView time;
        public TextView value;
        public ImageView arrow;

        public ViewHolder(View v) {
            super(v);
            time = (TextView) v.findViewById(R.id.time);
            value = (TextView) v.findViewById(R.id.value);
            arrow = (ImageView) v.findViewById(R.id.arrow);
        }
    }

    @Override
    public int getItemCount() {
        return logItemList.size();
    }

    public void notifyAddItem() {
        notifyItemInserted(logItemList.size()-1);
    }

    public void notifyClearList(int itemCount) {
        notifyItemRangeRemoved(0, itemCount);
    }

}
