package com.krio.gadgetcontroller.ui.utils;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.krio.gadgetcontroller.R;

/**
 * Created by krio on 21.05.16.
 */
public class EditTextUtils implements View.OnFocusChangeListener, TextView.OnEditorActionListener, TextWatcher {

    Context context;
    InputMethodManager inputMethodManager;

    View parentView;
    TextInputLayout textInputLayout;
    EditText editText;

    int maxLen = 25;

    onTextChangeListener changeListener;

    public EditTextUtils(Context context, View parentView, TextInputLayout textInputLayout, EditText editText) {
        this.context = context;
        this.parentView = parentView;
        this.textInputLayout = textInputLayout;
        this.editText = editText;

        inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

        textInputLayout.setCounterMaxLength(maxLen);

        parentView.setOnFocusChangeListener(this);
        editText.setOnFocusChangeListener(this);
        editText.setOnEditorActionListener(this);
        editText.addTextChangedListener(this);
    }

    public void setMaxLen(int maxLen) {
        this.maxLen = maxLen;
        textInputLayout.setCounterMaxLength(maxLen);
    }

    public void setChangeListener(onTextChangeListener changeListener) {
        this.changeListener = changeListener;
    }

    public void focus() {
        editText.requestFocus();
        showKeyboard();
    }

    public void showWarn(String warn) {
        textInputLayout.setErrorEnabled(true);
        textInputLayout.setError(warn);
    }

    public void showEmptyWarn() {
        textInputLayout.setErrorEnabled(true);
        textInputLayout.setError(context.getString(R.string.warning_field_empty));
    }

    public boolean isLengthCorrect() {
        return editText.getText().length() <= maxLen;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (v.getId() == parentView.getId()) {
            onParentFocusChange(hasFocus);
        } else {
            onEditTextFocusChange(hasFocus);
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            parentView.requestFocus();
            return true;
        }
        return false;
    }

    private void onParentFocusChange (boolean hasFocus) {
        if (hasFocus) {
            hideKeyboard();
        }
    }

    private void onEditTextFocusChange (boolean hasFocus) {
        if (hasFocus) {
            if (!textInputLayout.isCounterEnabled()) {
                textInputLayout.setCounterEnabled(true);
            }
            textInputLayout.setErrorEnabled(false);
            textInputLayout.setError("");
        } else {
            if (isLengthCorrect()) {
                textInputLayout.setErrorEnabled(false);
                textInputLayout.setCounterEnabled(false);
            } else {
                textInputLayout.setErrorEnabled(true);
                textInputLayout.setError(context.getString(R.string.warning_value_too_big));
            }
        }
    }

    private void showKeyboard() {
        inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    private void hideKeyboard() {
        inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (changeListener != null) {
            changeListener.onTextChange(s.toString());
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public interface onTextChangeListener {
        void onTextChange (String str);
    }
}
