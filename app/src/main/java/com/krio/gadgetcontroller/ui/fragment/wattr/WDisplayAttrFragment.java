package com.krio.gadgetcontroller.ui.fragment.wattr;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.widget.WSeekBar;
import com.krio.gadgetcontroller.logic.widget.WDisplay;
import com.krio.gadgetcontroller.ui.fragment.BaseFragment;
import com.krio.gadgetcontroller.ui.listener.OnComponentInteractionListener;
import com.krio.gadgetcontroller.ui.utils.EditTextUtils;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by krio on 11.05.16.
 */
public class WDisplayAttrFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener {

    public static final String IS_NEW_WIDGET = "is_new_widget";

    @BindView(R.id.caption_layout)
    TextInputLayout captionLayout;

    @BindView(R.id.name)
    EditText captionText;

    @BindView(R.id.show_caption)
    CheckBox isCaptionVisible;

    @BindView(R.id.element_caption)
    TextView exampleCaptionText;

    EditTextUtils captionTextUtils;

    Bundle defaultValues;
    boolean isNewWidget;

    OnComponentInteractionListener callbacks;

    public static WDisplayAttrFragment newInstance(boolean isNewWidget, String caption, boolean captionVisible) {
        WDisplayAttrFragment fragment = new WDisplayAttrFragment();
        Bundle args = new Bundle();
        args.putBoolean(IS_NEW_WIDGET, isNewWidget);
        args.putString(WDisplay.ATTR_CAPTION_TEXT, caption);
        args.putBoolean(WDisplay.ATTR_IS_CAPTION_VISIBLE, captionVisible);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_attr_display, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        isCaptionVisible.setOnCheckedChangeListener(this);
        initEditTextUtils(view);

        defaultValues = getArguments();

        isNewWidget = defaultValues.getBoolean(IS_NEW_WIDGET);

        captionText.setText( defaultValues.getString(WSeekBar.ATTR_CAPTION_TEXT));
        isCaptionVisible.setChecked( defaultValues.getBoolean(WSeekBar.ATTR_IS_CAPTION_VISIBLE));
    }

    private void initEditTextUtils(View view) {
        LinearLayout parentLayout = ButterKnife.findById(view, R.id.parent_layout);

        captionTextUtils = new EditTextUtils(getActivity(), parentLayout, captionLayout, captionText);
        captionTextUtils.setMaxLen(15);

        captionTextUtils.setChangeListener(str -> {
            if (str.isEmpty()) {
                exampleCaptionText.setText(R.string.widget_name_display);
            } else {
                exampleCaptionText.setText(str);
            }
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            captionLayout.setVisibility(View.VISIBLE);
            exampleCaptionText.setVisibility(View.VISIBLE);
        } else {
            captionLayout.setVisibility(View.GONE);
            exampleCaptionText.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_add_element, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_done:
                String caption = captionText.getText().toString();
                boolean showCaption = isCaptionVisible.isChecked();
                onMenuDoneSelected(caption, showCaption);
        }
        return super.onOptionsItemSelected(item);
    }

    private void onMenuDoneSelected(String caption, boolean showCaption) {
        if (hasChanged(caption, showCaption)) {
            if (validateAttrs(caption, showCaption)) {
                setWidgetAttr();
            }
        } else if (!isNewWidget) {
            getActivity().finish();
        }
    }

    private boolean validateAttrs(String caption, boolean showCaption) {

        if (showCaption && caption.isEmpty()) {
            captionTextUtils.showEmptyWarn();
            return false;
        }

        if (showCaption && !captionTextUtils.isLengthCorrect()) {
            return false;
        }

        return true;
    }

    private boolean hasChanged(String caption, boolean showCaption) {
        boolean hasChanged = false;

        if (!caption.equals(defaultValues.getString(WDisplay.ATTR_CAPTION_TEXT))) {
            hasChanged = true;
        }

        if (showCaption != defaultValues.getBoolean(WDisplay.ATTR_IS_CAPTION_VISIBLE)) {
            hasChanged = true;
        }

        return hasChanged;
    }

    @Override
    public void onAttach(Context ctx) {
        super.onAttach(ctx);
        try {
            callbacks = (OnComponentInteractionListener) ctx;
        } catch (ClassCastException e) {
            throw new ClassCastException(ctx.toString() + "Activity must implement fragment's listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void setWidgetAttr() {
        Map<String, Object> attr = new HashMap<>();
        attr.put(WDisplay.ATTR_CAPTION_TEXT, captionText.getText().toString());
        attr.put(WDisplay.ATTR_IS_CAPTION_VISIBLE, isCaptionVisible.isChecked());
        callbacks.onSetAttrs(attr);
    }
}
