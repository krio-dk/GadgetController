package com.krio.gadgetcontroller.ui.dialog;


import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.widget.Widget;

import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditDialogFragment extends DialogFragment implements View.OnClickListener {

    public static final String TAG = "EditDialogFragment";

    public static final int TYPE_PANEL = 1;
    public static final int TYPE_WIDGET = 2;
    public static final int TYPE_WIDGET_NO_COMMANDS = 3;

    private onEditDialogItemSelectedListener listener;

    private static Widget widget;
    private static int type = 0;
    private static boolean deleteEnabled = true;

    public interface onEditDialogItemSelectedListener {
        void onEditPanelSelected();

        void onDeletePanelSelected();

        void onEditWidgetAttrsSelected(Widget widget);

        void onEditWidgetCommandsSelected(Widget widget);

        void onDeleteWidgetSelected(Widget widget);
    }

    public void setListener(onEditDialogItemSelectedListener listener) {
        this.listener = listener;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setWidget(Widget widget) {
        this.widget = widget;
    }

    public void setDeleteEnabled(boolean deleteEnabled) {
        this.deleteEnabled = deleteEnabled;
    }

    public static EditDialogFragment newInstance() {
        EditDialogFragment fragment = new EditDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.fragment_dialog_edit, null);

        TextView optionAttrs = (TextView) view.findViewById(R.id.edit_option_attrs);
        TextView optionDelete = (TextView) view.findViewById(R.id.edit_option_delete);
        TextView optionCommands = (TextView) view.findViewById(R.id.edit_option_commands);

        optionAttrs.setOnClickListener(this);
        optionCommands.setOnClickListener(this);
        optionDelete.setOnClickListener(this);

        switch (type) {
            case TYPE_PANEL:
                optionAttrs.setText(R.string.edit_option_panel_attr);
                optionDelete.setText(R.string.edit_option_panel_delete);
                optionDelete.setEnabled(deleteEnabled);
                optionCommands.setVisibility(View.GONE);
                break;

            case TYPE_WIDGET:
                optionAttrs.setText(R.string.edit_option_widget_attr);
                optionDelete.setText(R.string.edit_option_widget_delete);
                optionCommands.setText(R.string.edit_option_widget_commands);
                optionCommands.setVisibility(View.VISIBLE);
                break;

            case TYPE_WIDGET_NO_COMMANDS:
                optionAttrs.setText(R.string.edit_option_widget_attr);
                optionDelete.setText(R.string.edit_option_widget_delete);
                optionCommands.setVisibility(View.GONE);
                break;
        }

        return buildDialog(view);
    }

    private Dialog buildDialog(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.DialogTheme);
        builder.setTitle(type == TYPE_PANEL ? R.string.alert_title_edit_panel : R.string.alert_title_edit_widget);
        builder.setView(view);
        builder.setNegativeButton(R.string.button_cancel, (dialog, which) -> dismiss());
        return builder.create();
    }

    @Override
    public void onClick(View v) {

        if (listener != null) {
            switch (v.getId()) {
                case R.id.edit_option_attrs:
                    if (type == TYPE_PANEL) {
                        listener.onEditPanelSelected();
                    } else {
                        listener.onEditWidgetAttrsSelected(widget);
                    }
                    break;

                case R.id.edit_option_commands:
                    listener.onEditWidgetCommandsSelected(widget);
                    break;

                case R.id.edit_option_delete:
                    if (type == TYPE_PANEL) {
                        listener.onDeletePanelSelected();
                    } else {
                        listener.onDeleteWidgetSelected(widget);
                    }
                    break;
            }
        }

        dismiss();
    }
}
