package com.krio.gadgetcontroller.ui.myview.joystick;

import android.view.View;

/**
 * Interface definition for joystick callbacks from user touch interactions.
 */
public interface JoystickListener {
    void onDown();

    /**
     * @param degrees -180 -> 180.
     * @param offset  normalized, 0 -> 1.
     */
    void onDrag(View v, float degrees, float offset);

    void onUp(View v);
}