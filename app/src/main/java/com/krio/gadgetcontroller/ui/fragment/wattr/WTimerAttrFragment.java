package com.krio.gadgetcontroller.ui.fragment.wattr;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.widget.WTimer;
import com.krio.gadgetcontroller.ui.fragment.BaseFragment;
import com.krio.gadgetcontroller.ui.listener.OnComponentInteractionListener;
import com.krio.gadgetcontroller.ui.utils.EditTextUtils;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by krio on 11.05.16.
 */
public class WTimerAttrFragment extends BaseFragment {

    public static final String IS_NEW_WIDGET = "is_new_widget";


    @BindView(R.id.interval_value_layout)
    TextInputLayout mIntervalValueLayout;

    @BindView(R.id.interval_value)
    EditText timerIntervalValue;

    @BindView(R.id.element_caption)
    TextView exampleCaptionText;

    @BindView (R.id.timer_command)
    TextView timerCommand;

    @BindView(R.id.timer_switch)
    SwitchCompat timerSwitch;

    EditTextUtils captionTextUtils;

    Bundle defaultValues;

    boolean isNewWidget;

    OnComponentInteractionListener callbacks;

    public static WTimerAttrFragment newInstance(boolean isNewWidget, int interval) {
        WTimerAttrFragment fragment = new WTimerAttrFragment();
        Bundle args = new Bundle();
        args.putBoolean(IS_NEW_WIDGET, isNewWidget);
        args.putInt(WTimer.ATTR_INTERVAL, interval);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_attr_timer, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        timerSwitch.setEnabled(false);
        timerCommand.setText("< command >");

        initEditTextUtils(view);

        defaultValues = getArguments();

        isNewWidget = defaultValues.getBoolean(IS_NEW_WIDGET);

        int defaultInterval = defaultValues.getInt(WTimer.ATTR_INTERVAL);
        if (defaultInterval != 0) {
            timerIntervalValue.setText(String.valueOf(defaultInterval));
        }
    }

    private void initEditTextUtils(View view) {
        LinearLayout parentLayout = ButterKnife.findById(view, R.id.parent_layout);
        int maxLen = 5;

        captionTextUtils = new EditTextUtils(getActivity(), parentLayout, mIntervalValueLayout,
                timerIntervalValue);
        captionTextUtils.setMaxLen(maxLen);

        captionTextUtils.setChangeListener(str -> {
            if (str.isEmpty()) {
                exampleCaptionText.setText(R.string.widget_name_timer);
            } else if (str.length() <= maxLen){
                exampleCaptionText.setText(getString(R.string.timer_caption_value,
                        WTimer.getFormattedCountDownTime(Long.valueOf(str) * 1000)));
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_add_element, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_done:
                String timerValue = timerIntervalValue.getText().toString();
                onMenuDoneSelected(timerValue);
        }
        return super.onOptionsItemSelected(item);
    }

    private void onMenuDoneSelected(String timerValue) {
        if (hasChanged(timerValue)) {
            if (validateAttrs(timerValue)) {
                setWidgetAttr();
            }
        } else if (!isNewWidget) {
            getActivity().finish();
        }
    }

    private boolean validateAttrs(String timerValue) {
        if (timerValue.isEmpty()) {
            captionTextUtils.showEmptyWarn();
            return false;
        }

        if (!captionTextUtils.isLengthCorrect()) {
            return false;
        }

        return true;
    }

    private boolean hasChanged(String timerValue) {
        boolean hasChanged = false;

        if (!timerValue.equals(defaultValues.getString(WTimer.ATTR_INTERVAL))) {
            hasChanged = true;
        }

        return hasChanged;
    }


    @Override
    public void onAttach(Context ctx) {
        super.onAttach(ctx);
        try {
            callbacks = (OnComponentInteractionListener) ctx;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    ctx.toString() + "Activity must implement fragment's listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void setWidgetAttr() {
        Map<String, Object> attr = new HashMap<>();
        attr.put(WTimer.ATTR_INTERVAL, Integer.parseInt(timerIntervalValue.getText().toString()));
        callbacks.onSetAttrs(attr);
    }
}
