package com.krio.gadgetcontroller.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import com.krio.gadgetcontroller.App;
import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.di.builder.WidgetBuilderComponent;
import com.krio.gadgetcontroller.logic.command.InputCommand;
import com.krio.gadgetcontroller.logic.project.Project;
import com.krio.gadgetcontroller.logic.widget.Widget;
import com.krio.gadgetcontroller.logic.widgetbuilder.WidgetBuilder;
import com.krio.gadgetcontroller.logic.widgetbuilder.WidgetBuilderFactory;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;
import com.krio.gadgetcontroller.ui.fragment.ComponentListFragment;
import com.krio.gadgetcontroller.ui.fragment.wattr.WButtonAttrFragment;
import com.krio.gadgetcontroller.ui.fragment.wattr.WDisplayAttrFragment;
import com.krio.gadgetcontroller.ui.fragment.wattr.WJoystickAttrFragment;
import com.krio.gadgetcontroller.ui.fragment.wattr.WLedAttrFragment;
import com.krio.gadgetcontroller.ui.fragment.wattr.WSeekBarAttrFragment;
import com.krio.gadgetcontroller.ui.fragment.wattr.WSwitchAttrFragment;
import com.krio.gadgetcontroller.ui.fragment.wattr.WLabelAttrFragment;
import com.krio.gadgetcontroller.ui.fragment.wattr.WTimerAttrFragment;
import com.krio.gadgetcontroller.ui.fragment.wcommand.WButtonCommandFragment;
import com.krio.gadgetcontroller.ui.fragment.wcommand.WJoystickCommandFragment;
import com.krio.gadgetcontroller.ui.fragment.wcommand.WLedCommandFragment;
import com.krio.gadgetcontroller.ui.fragment.wcommand.WSeekBarCommandFragment;
import com.krio.gadgetcontroller.ui.fragment.wcommand.WSwitchCommandFragment;
import com.krio.gadgetcontroller.ui.fragment.wcommand.WTextDisplayCommandFragment;
import com.krio.gadgetcontroller.ui.fragment.wcommand.WTimerCommandFragment;
import com.krio.gadgetcontroller.ui.listener.OnComponentInteractionListener;
import com.krio.gadgetcontroller.ui.utils.KeyboardUtils;

import java.util.Map;

import javax.inject.Inject;

public class ComponentAddActivity extends BaseActivity implements OnComponentInteractionListener {

    public static final String PANEL_ID = "panel_id";
    public static final String WIDGET_ID = "widget_id";

    public static final String COMPONENT_TYPE = "component_type";
    public static final int TYPE_WIDGET = 1;
    public static final int TYPE_PANEL = 2;

    @Inject
    Project project;
    long panelId;

    WidgetBuilder widgetBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_component_add);

        App.getMainComponent().inject(this);
        panelId = getIntent().getLongExtra(PANEL_ID, 0);

        initActionBar();

        WidgetBuilderComponent widgetBuilderComponent = App.getWidgetBuilderComponent();
        if (widgetBuilderComponent != null) {
            widgetBuilder = widgetBuilderComponent.getWidgetBuilder();
        }

        if (widgetBuilder == null) {
            showComponentListFragment();
        }
    }

    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.title_add_component);
        }
    }

    private void showComponentListFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, ComponentListFragment.newInstance())
                .commit();
    }

    private void showInteractionFragment(Fragment fragment) {
        KeyboardUtils.hide(this, getCurrentFocus());
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_out_right)
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void setSubtitle(int redId) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setSubtitle(redId);
        }
    }

    private void showWidgetAttrFragment(WidgetType widgetType) {
        switch (widgetType) {
            case BUTTON:
                showInteractionFragment(WButtonAttrFragment.newInstance(true, null, true, null));
                break;

            case SWITCH:
                showInteractionFragment(WSwitchAttrFragment.newInstance(true, null));
                break;

            case SEEKBAR:
                showInteractionFragment(WSeekBarAttrFragment.newInstance(true, null, true));
                break;

            case JOYSTICK:
                showInteractionFragment(WJoystickAttrFragment.newInstance(true, null, true));
                break;

            case DISPLAY:
                showInteractionFragment(WDisplayAttrFragment.newInstance(true, null, true));
                break;

            case LED:
                showInteractionFragment(WLedAttrFragment.newInstance(true, null, 0));
                break;

            case LABEL:
                showInteractionFragment(WLabelAttrFragment.newInstance(true, null));
                break;

            case TIMER:
                showInteractionFragment(WTimerAttrFragment.newInstance(true, 0));
                break;
        }

    }

    private void showWidgetCommandFragment(WidgetType widgetType) {
        switch (widgetType) {
            case BUTTON:
                showInteractionFragment(WButtonCommandFragment.newInstance(true, null, null));
                break;

            case SWITCH:
                showInteractionFragment(WSwitchCommandFragment.newInstance(true, null, null));
                break;

            case SEEKBAR:
                showInteractionFragment(WSeekBarCommandFragment.newInstance(true, null));
                break;

            case JOYSTICK:
                showInteractionFragment(WJoystickCommandFragment.newInstance(true, null));
                break;

            case DISPLAY:
                showInteractionFragment(WTextDisplayCommandFragment.newInstance(true, null));
                break;

            case LED:
                showInteractionFragment(WLedCommandFragment.newInstance(true, null, null));
                break;

            case LABEL:
                onFinishWidgetBuild(); // label has no commands
                break;

            case TIMER:
                showInteractionFragment(WTimerCommandFragment.newInstance(true, null));
                break;
        }
    }

    private void createWidgetBuilder(WidgetType widgetType) {
        widgetBuilder = WidgetBuilderFactory.createWidgetBuilder(widgetType);
        if (widgetBuilder != null) {
            widgetBuilder.setPanelId(panelId);
            widgetBuilder.setPosition(project.getPanelWidgets(panelId).size());
            App.createWidgetBuilderComponent(widgetBuilder);
        }
    }

    private boolean addWidgetToProject() {
        if (!widgetBuilder.createWidget()) {
            return false;
        }

        Widget widget = widgetBuilder.getWidget();
        project.addWidget(widget);

        for (InputCommand command : widgetBuilder.getInputCommandList()) {
            command.setWidgetId(widget.getId());
            project.addInputCommand(command);
        }

        return true;
    }

    @Override
    public void onPanelAddSelected() {
        project.addPanel(getString(R.string.header_new_panel));

        Intent intent = new Intent();
        intent.putExtra(COMPONENT_TYPE, TYPE_PANEL);
        setResult(RESULT_OK, intent);
        finishActivity();
    }

    @Override
    public void onWidgetTypeSelected(WidgetType widgetType) {
        createWidgetBuilder(widgetType);
        showWidgetAttrFragment(widgetType);
    }

    @Override
    public void onSetAttrs(Map<String, Object> attrs) {
        widgetBuilder.setAttrs(attrs);
        showWidgetCommandFragment(widgetBuilder.getWidgetType());
    }

    @Override
    public void onCreateCommand(CommandType commandType, String cmd, Map<String, Object> params) {
        widgetBuilder.createCommand(commandType, cmd, params);
    }

    @Override
    public void onFinishWidgetBuild() {
        if (addWidgetToProject()) {
            Intent intent = new Intent();
            intent.putExtra(COMPONENT_TYPE, TYPE_WIDGET);
            intent.putExtra(WIDGET_ID, widgetBuilder.getWidget().getId());
            setResult(RESULT_OK, intent);
            finishActivity();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            finishActivity();
        } else {
            KeyboardUtils.hide(this, getCurrentFocus());
            getSupportFragmentManager().popBackStack();
        }
    }

    private void finishActivity() {
        App.clearWidgetBuilderComponent();
        finish();
    }

}
