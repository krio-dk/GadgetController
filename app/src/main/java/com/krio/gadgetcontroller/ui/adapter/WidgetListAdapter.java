package com.krio.gadgetcontroller.ui.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.krio.gadgetcontroller.App;
import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.widget.WButton;
import com.krio.gadgetcontroller.logic.widget.WLed;
import com.krio.gadgetcontroller.logic.widget.WDisplay;
import com.krio.gadgetcontroller.logic.widget.WSeekBar;
import com.krio.gadgetcontroller.logic.widget.WSwitch;
import com.krio.gadgetcontroller.logic.widget.WTimer;
import com.krio.gadgetcontroller.logic.widget.Widget;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;
import com.krio.gadgetcontroller.ui.adapter.widgetaction.WButtonActionListener;
import com.krio.gadgetcontroller.ui.adapter.widgetaction.WJoystickActionListener;
import com.krio.gadgetcontroller.ui.adapter.widgetaction.WSeekBarActionListener;
import com.krio.gadgetcontroller.ui.adapter.widgetaction.WSwitchActionListener;
import com.krio.gadgetcontroller.ui.adapter.widgetaction.WTimerActionListener;
import com.krio.gadgetcontroller.ui.myview.circle.CircleView;
import com.krio.gadgetcontroller.ui.myview.joystick.Joystick;
import com.krio.gadgetcontroller.ui.utils.WidgetTouchHelperCallback;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WidgetListAdapter extends RecyclerView.Adapter<WidgetListAdapter.MainViewHolder> implements View.OnClickListener, Observer, WidgetTouchHelperCallback.ItemTouchHelperAdapter {

    List<Widget> widgetList;
    RecyclerView recyclerView;

    OnWidgetEditSelectedListener editSelectedListener;
    OnStartDragListener dragStartListener;

    WButtonActionListener buttonActionListener;
    WSeekBarActionListener seekBarActionListener;
    WSwitchActionListener switchActionListener;
    WJoystickActionListener joystickActionListener;
    WTimerActionListener timerActionListener;

    public interface OnWidgetEditSelectedListener {
        void onWidgetEditSelected(Widget widget);
    }

    public interface OnStartDragListener {
        void onStartDrag(RecyclerView.ViewHolder viewHolder);
    }

    public void setEditSelectedListener(OnWidgetEditSelectedListener editSelectedListener) {
        this.editSelectedListener = editSelectedListener;
    }

    public void setDragStartListener(OnStartDragListener dragStartListener) {
        this.dragStartListener = dragStartListener;
    }

    public WidgetListAdapter(List<Widget> widgetList, RecyclerView recyclerView) {
        this.widgetList = widgetList;
        this.recyclerView = recyclerView;

        initWidgetActionListeners();
    }

    private void initWidgetActionListeners() {
        buttonActionListener = new WButtonActionListener(widgetList, recyclerView);
        joystickActionListener = new WJoystickActionListener(widgetList, recyclerView);
        seekBarActionListener = new WSeekBarActionListener(widgetList);
        switchActionListener = new WSwitchActionListener(widgetList);
        timerActionListener = new WTimerActionListener(widgetList);
    }

    @Override
    public int getItemViewType(int position) {
        return widgetList.get(position).getWidgetType().ordinal();
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        WidgetType widgetType = WidgetType.fromInteger(viewType);
        @LayoutRes int layoutResource;

        switch (widgetType) {
            case BUTTON:
                layoutResource = R.layout.widget_button;
                break;

            case SWITCH:
                layoutResource = R.layout.widget_switch;
                break;

            case SEEKBAR:
                layoutResource = R.layout.widget_seekbar;
                break;

            case JOYSTICK:
                layoutResource = R.layout.widget_joystick;
                break;

            case LED:
                layoutResource = R.layout.widget_led;
                break;

            case DISPLAY:
                layoutResource = R.layout.widget_display;
                break;

            case LABEL:
                layoutResource = R.layout.widget_label;
                break;

            case TIMER:
                layoutResource = R.layout.widget_timer;
                break;

            default:
                layoutResource = R.layout.item_single_line_list;
        }

        View view = LayoutInflater.from(parent.getContext()).inflate(layoutResource, parent, false);
        return new MainViewHolder(view, widgetType, this);
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {

        holder.dragHandle.setOnTouchListener((v, event) -> {
            if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                if (dragStartListener != null) {
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    dragStartListener.onStartDrag(holder);
                }
            }
            return false;
        });

        Widget widget = widgetList.get(position);

        setEditButtonsVisible(holder);
        setCaption(holder, widget);

        switch (widget.getWidgetType()) {
            case LED:
                bindLedView(holder, widget);
                break;

            case DISPLAY:
                bindTextDisplayView(holder, widget);
                break;

            case SEEKBAR:
                bindSeekBarView(holder, widget);
                break;

            case JOYSTICK:
                bindJoystickView(holder, widget);
                break;

            case BUTTON:
                bindButtonView(holder, widget);
                break;

            case SWITCH:
                bindSwitchView(holder, widget);
                break;

            case TIMER:
                bindTimerView(holder, widget);
                break;
        }
    }

    private void setEditButtonsVisible(MainViewHolder holder) {
        if (App.getMainComponent().getProjectModeWrapper().isEditMode()) {
            holder.editButton.setVisibility(View.VISIBLE);
            holder.dragHandle.setVisibility(View.VISIBLE);
        } else {
            holder.editButton.setVisibility(View.GONE);
            holder.dragHandle.setVisibility(View.GONE);
        }
    }

    private void setCaption(MainViewHolder holder, Widget widget) {
        if (widget.isCaptionVisible()) {
            holder.elementCaption.setText(widget.getCaption());
            holder.elementCaption.setVisibility(View.VISIBLE);
        } else {
            holder.elementCaption.setVisibility(View.GONE);
        }
    }

    private void bindLedView(MainViewHolder holder, Widget widget) {
        if (holder.led != null) {
            if (((WLed) widget).isEnabled()) {
                holder.led.setBackgroundColor(((WLed) widget).getColor());
            } else {
                holder.led.setBackgroundColor(ContextCompat.getColor(holder.led.getContext(), R.color.colorDarkDividersBackground));
            }
        }
    }

    private void bindTextDisplayView(MainViewHolder holder, Widget widget) {
        if (holder.monitor != null) {
            holder.monitor.setText(((WDisplay) widget).getText());
        }
    }

    private void bindSeekBarView(MainViewHolder holder, Widget widget) {
        if (holder.seekBar != null) {
            holder.seekBar.setProgress(((WSeekBar) widget).getProgress());
        }
    }

    private void bindJoystickView(MainViewHolder holder, Widget widget) {

    }


    private void bindButtonView(MainViewHolder holder, Widget widget) {
        if (holder.button != null) {
            holder.button.setText(((WButton) widget).getButtonText());
        }
    }

    private void bindSwitchView(MainViewHolder holder, Widget widget) {
        switchActionListener.setFromUser(false);
        if (holder.switchCompat != null) {
            holder.switchCompat.setChecked(((WSwitch) widget).isChecked());
        }
        switchActionListener.setFromUser(true);
    }

    private void bindTimerView(MainViewHolder holder, Widget widget) {

        WTimer timer = (WTimer) widget;

        timer.setCallback(new WTimer.WTimerCallback() {
            @Override
            public void onTick(String formattedCountDownTime) {
                holder.elementCaption.setText(holder.elementCaption.getResources().getString(R.string.timer_caption_value, formattedCountDownTime));
            }

            @Override
            public void onSend() {
                if (holder.timerCommand != null) {
                    holder.timerCommand.setTextColor(ContextCompat.getColor(holder.timerCommand.getContext(), R.color.colorAccent));
                    holder.timerCommand.postDelayed(() -> {
                        holder.timerCommand.setTextColor(ContextCompat.getColor(holder.timerCommand.getContext(), R.color.colorDarkHintText));
                    }, 750);
                }
            }

            @Override
            public void onStop() {
                holder.elementCaption.setText(holder.elementCaption.getResources().getString(R.string.timer_caption_off));
            }
        });

        if (!timer.isRun()) {
            holder.elementCaption.setText(holder.elementCaption.getResources().getString(R.string.timer_caption_off));
        }

        if (holder.timerCommand != null) {
            String cmd = timer.getOutputCommand(CommandType.COMMAND_TIMER).getCmd();
            holder.timerCommand.setText("< " + cmd + " >");
        }

        if (holder.timerInterval != null) {
            holder.timerInterval.setText(String.valueOf(timer.getTimerInterval()));
        }

        timerActionListener.setFromUser(false);
        if (holder.timerSwitch != null) {
            holder.timerSwitch.setChecked(timer.isRun());
        }
        timerActionListener.setFromUser(true);

    }

    @Override
    public int getItemCount() {
        return widgetList.size();
    }

    public class MainViewHolder extends RecyclerView.ViewHolder implements
            WidgetTouchHelperCallback.ItemTouchHelperViewHolder {


        @BindView(R.id.element_caption)
        TextView elementCaption;

        @BindView(R.id.drag_handle)
        ImageButton dragHandle;

        @BindView(R.id.edit_button)
        ImageButton editButton;

        @Nullable
        @BindView(R.id.led)
        CircleView led;

        @Nullable
        @BindView(R.id.button)
        Button button;

        @Nullable
        @BindView(R.id.switch_compat)
        SwitchCompat switchCompat;

        @Nullable
        @BindView(R.id.seekbar)
        SeekBar seekBar;

        @Nullable
        @BindView(R.id.monitor)
        TextView monitor;

        @Nullable
        @BindView(R.id.joystick)
        Joystick joystick;

        @Nullable
        @BindView(R.id.timer_interval)
        TextView timerInterval;

        @Nullable
        @BindView(R.id.timer_command)
        TextView timerCommand;

        @Nullable
        @BindView(R.id.timer_switch)
        SwitchCompat timerSwitch;

        public MainViewHolder(View view, WidgetType widgetType, View.OnClickListener onClickListener) {
            super(view);
            ButterKnife.bind(this, view);

            if (editButton != null) {
                editButton.setOnClickListener(onClickListener);
                editButton.setTag(this);
            }

            switch (widgetType) {

                case SEEKBAR:
                    if (seekBar != null) {
                        seekBar.setTag(this);
                        seekBar.setOnSeekBarChangeListener(seekBarActionListener);
                    }
                    break;

                case BUTTON:
                    if (button != null) {
                        button.setTag(this);
                        button.setOnTouchListener(buttonActionListener);
                    }
                    break;

                case SWITCH:
                    if (switchCompat != null) {
                        switchCompat.setTag(this);
                        switchCompat.setOnCheckedChangeListener(switchActionListener);
                    }
                    break;

                case JOYSTICK:
                    if (joystick != null) {
                        joystick.setTag(this);
                        joystick.setJoystickListener(joystickActionListener);
                    }
                    break;

                case TIMER:
                    if (timerSwitch != null) {
                        timerSwitch.setTag(this);
                        timerSwitch.setOnCheckedChangeListener(timerActionListener);
                    }
                    break;

            }
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(ContextCompat.getColor(recyclerView.getContext(), R.color.colorDragBackground));
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(ContextCompat.getColor(recyclerView.getContext(), R.color.colorDarkElementsBackground));
        }

    }

    @Override
    public void onClick(View v) {
        if (editSelectedListener != null) {
            MainViewHolder mainViewHolder = (MainViewHolder) v.getTag();
            int position = mainViewHolder.getLayoutPosition();

            editSelectedListener.onWidgetEditSelected(widgetList.get(position));
        }
    }

    public void add(Widget widget) {
        int insertPosition = this.widgetList.size();
        widgetList.add(widget);
        notifyItemInserted(insertPosition);
    }

    public void addAll(List<Widget> widgetList) {
        int positionStart = this.widgetList.size();
        this.widgetList.addAll(widgetList);
        notifyItemRangeInserted(positionStart, widgetList.size());
    }

    public void removeWidget(Widget widget) {
        int position = widgetList.indexOf(widget);
        widgetList.remove(position);
        notifyItemRemoved(position);
    }

    public void removeAll() {
        int itemCount = widgetList.size();
        widgetList.clear();
        notifyItemRangeRemoved(0, itemCount);
    }

    public List<Widget> getWidgetList() {
        return widgetList;
    }

    public void moveItem(int fromPosition, int toPosition) {
        if (fromPosition != toPosition) {
            widgetList.add(toPosition, widgetList.remove(fromPosition));

            for (int i = 0; i < widgetList.size(); i++) {
                widgetList.get(i).setPosition(i);
            }

            notifyItemMoved(fromPosition, toPosition);
        }
    }

    public void update() {
        notifyItemRangeChanged(0, getItemCount());
    }

    @Override
    public void onItemSwiped(int position) {

    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        moveItem(fromPosition, toPosition);
    }

    @Override
    public void cancelRemoveItem() {

    }

    @Override
    public void update(Observable observable, Object data) {
        Widget widget = (Widget) data;

        if (widgetList.size() > widget.getPosition()) {
            if (widgetList.get(widget.getPosition()).getId() == widget.getId()) {
                recyclerView.setItemAnimator(null);
                notifyItemChanged(widget.getPosition());
            }
        }
    }
}
