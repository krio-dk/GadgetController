package com.krio.gadgetcontroller.ui.adapter.widgetaction;

import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;

import com.krio.gadgetcontroller.logic.widget.Widget;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.ui.adapter.WidgetListAdapter;

import java.util.List;

/**
 * Created by krio on 25.04.16.
 */
public class WButtonActionListener implements View.OnTouchListener {

    List<Widget> widgetList;
    RecyclerView recyclerView;

    public WButtonActionListener(List<Widget> widgetList, RecyclerView recyclerView) {
        this.widgetList = widgetList;
        this.recyclerView = recyclerView;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        WidgetListAdapter.MainViewHolder mainViewHolder = (WidgetListAdapter.MainViewHolder) v.getTag();
        int position = mainViewHolder.getLayoutPosition();

        Widget widget = widgetList.get(position);

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                recyclerView.requestDisallowInterceptTouchEvent(true);
                widget.performCommand(CommandType.COMMAND_BUTTON_DOWN, null);
                break;

            case MotionEvent.ACTION_UP:
                widget.performCommand(CommandType.COMMAND_BUTTON_UP, null);
                break;

        }
        return false;
    }
}
