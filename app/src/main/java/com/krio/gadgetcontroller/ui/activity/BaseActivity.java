package com.krio.gadgetcontroller.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.krio.gadgetcontroller.R;

/**
 * Created by krio on 21.04.16.
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_left, R.anim.fade_out);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.slide_out_right);
    }
}
