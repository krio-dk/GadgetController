package com.krio.gadgetcontroller.ui.utils;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.ui.dialog.ShareDialogFragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

public class NavigationItemsUtils {

    static ShareDialogFragment shareDialog = ShareDialogFragment.newInstance();

    public static void onNavigationItemSelected(MenuItem item, Context context) {
        switch (item.getItemId()) {
            case R.id.nav_share:
                showShareDialog(((AppCompatActivity) context).getSupportFragmentManager());
                break;

            case R.id.nav_wiki:
                showActionView(context.getString(R.string.url_wiki), context);
                break;

            case R.id.nav_write_developer:
                sendMail(context);
                break;
        }
    }

    private static void showShareDialog(FragmentManager fragmentManager) {
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            shareDialog.show(fragmentManager, ShareDialogFragment.TAG);
        }, 200);
    }

    private static void showActionView(final String url, Context context) {
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }, 200);
    }

    private static void sendMail(Context context) {

        String supportEmail = context.getString(R.string.support_email);
        String[] email = {supportEmail};

        Intent intent = new Intent(Intent.ACTION_SENDTO,
                Uri.fromParts("mailto", supportEmail, null));
        intent.putExtra(Intent.EXTRA_EMAIL, email);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Gadget Controller Support");

        context.startActivity(Intent.createChooser(intent,
                context.getString(R.string.text_label_write_developer)));
    }
}
