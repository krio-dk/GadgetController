package com.krio.gadgetcontroller.ui.fragment;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.ui.listener.TCPInteractionProvider;
import com.krio.gadgetcontroller.ui.utils.EditTextUtils;
import com.krio.gadgetcontroller.ui.utils.NetworkConnectionUtils;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConnectionTCPNewFragment extends BaseFragment {

    @BindView(R.id.main_content)
    View mainContent;

    @BindView(R.id.network_ip)
    TextView networkIP;

    @BindView(R.id.mac_container)
    View macContainer;

    @BindView(R.id.network_mac)
    TextView networkMAC;

    @BindView(R.id.host_input_layout)
    TextInputLayout hostInputLayout;

    @BindView(R.id.host)
    EditText hostEditText;

    @BindView(R.id.port_input_layout)
    TextInputLayout portInputLayout;

    @BindView(R.id.port)
    EditText portEditText;

    private TCPInteractionProvider interactionProvider;

    public static ConnectionTCPNewFragment newInstance() {
        ConnectionTCPNewFragment fragment = new ConnectionTCPNewFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_connection_tcp_new, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initEditTextUtils();

        networkIP.setText(NetworkConnectionUtils.getIPAddress(true));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            interactionProvider = (TCPInteractionProvider) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "Activity must implement fragment's callbacks");
        }
    }

    private void initEditTextUtils() {
        EditTextUtils hostEditTextUtils =
                new EditTextUtils(getActivity(), mainContent, hostInputLayout, hostEditText);
        EditTextUtils portEditTextUtils =
                new EditTextUtils(getActivity(), mainContent, portInputLayout, portEditText);

        hostEditTextUtils.setMaxLen(24);
        portEditTextUtils.setMaxLen(5);
    }

    public void onDone() {
        String host = hostEditText.getText().toString();
        String port = portEditText.getText().toString();
        interactionProvider.connect(host, port);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_connection_tcp_new, menu);
        interactionProvider.addMenuItem(menu.findItem(R.id.menu_done));
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_done:
                onDone();
        }
        return super.onOptionsItemSelected(item);
    }
}
