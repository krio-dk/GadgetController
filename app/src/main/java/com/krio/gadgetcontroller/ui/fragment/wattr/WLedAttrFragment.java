package com.krio.gadgetcontroller.ui.fragment.wattr;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.widget.WLed;
import com.krio.gadgetcontroller.ui.fragment.BaseFragment;
import com.krio.gadgetcontroller.ui.listener.OnComponentInteractionListener;
import com.krio.gadgetcontroller.ui.utils.EditTextUtils;
import com.larswerkman.lobsterpicker.sliders.LobsterShadeSlider;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by krio on 11.05.16.
 */
public class WLedAttrFragment extends BaseFragment {

    public static final String IS_NEW_WIDGET = "is_new_widget";

    @BindView(R.id.caption_layout)
    TextInputLayout captionLayout;

    @BindView(R.id.name)
    EditText captionText;

    @BindView(R.id.element_caption)
    TextView exampleCaptionText;

    @BindView(R.id.shadeslider)
    LobsterShadeSlider shadeSlider;

    EditTextUtils captionTextUtils;

    Bundle defaultValues;
    boolean isNewWidget;

    OnComponentInteractionListener callbacks;

    public static WLedAttrFragment newInstance(boolean isNewWidget, String caption, int color) {
        WLedAttrFragment fragment = new WLedAttrFragment();
        Bundle args = new Bundle();
        args.putBoolean(IS_NEW_WIDGET, isNewWidget);
        args.putString(WLed.ATTR_CAPTION_TEXT, caption);
        args.putInt(WLed.ATTR_COLOR, color);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_attr_led, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initEditTextUtils(view);

        defaultValues = getArguments();

        isNewWidget = defaultValues.getBoolean(IS_NEW_WIDGET);

        captionText.setText(defaultValues.getString(WLed.ATTR_CAPTION_TEXT));

        for (int i=0; i < 6; i++) {
            if (shadeSlider.getColor() == defaultValues.getInt(WLed.ATTR_COLOR)) {
                shadeSlider.setShadePosition(i);
                break;
            }
        }
    }

    private void initEditTextUtils(View view) {
        LinearLayout parentLayout = ButterKnife.findById(view, R.id.parent_layout);

        captionTextUtils = new EditTextUtils(getActivity(), parentLayout, captionLayout, captionText);
        captionTextUtils.setMaxLen(15);

        captionTextUtils.setChangeListener(str -> {
            if (str.isEmpty()) {
                exampleCaptionText.setText(R.string.widget_name_led);
            } else {
                exampleCaptionText.setText(str);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_add_element, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_done:
                String caption = captionText.getText().toString();
                onMenuDoneSelected(caption);
        }
        return super.onOptionsItemSelected(item);
    }

    private void onMenuDoneSelected(String caption) {
        if (hasChanged(caption)) {
            if (validateAttrs(caption)) {
                setWidgetAttr();
            }
        } else if (!isNewWidget) {
            getActivity().finish();
        }
    }

    private boolean validateAttrs(String caption) {
        if (caption.isEmpty()) {
            captionTextUtils.showEmptyWarn();
            return false;
        }

        if (!captionTextUtils.isLengthCorrect()) {
            return false;
        }

        return true;
    }

    private boolean hasChanged(String caption) {
        boolean hasChanged = false;

        if (!caption.equals(defaultValues.getString(WLed.ATTR_CAPTION_TEXT))) {
            hasChanged = true;
        }

        if (shadeSlider.getColor() != defaultValues.getInt(WLed.ATTR_COLOR)) {
            hasChanged = true;
        }

        return hasChanged;
    }


    @Override
    public void onAttach(Context ctx) {
        super.onAttach(ctx);
        try {
            callbacks = (OnComponentInteractionListener) ctx;
        } catch (ClassCastException e) {
            throw new ClassCastException(ctx.toString() + "Activity must implement fragment's listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void setWidgetAttr() {
        Map<String, Object> attr = new HashMap<>();
        attr.put(WLed.ATTR_CAPTION_TEXT, captionText.getText().toString());
        attr.put(WLed.ATTR_COLOR, shadeSlider.getColor());
        callbacks.onSetAttrs(attr);
    }
}
