package com.krio.gadgetcontroller.ui.activity;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.krio.gadgetcontroller.App;
import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.connection.log.ConnectionLog;
import com.krio.gadgetcontroller.ui.adapter.LogListAdapter;
import com.krio.gadgetcontroller.ui.utils.DialogUtils;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LogActivity extends BaseActivity implements ConnectionLog.LogUpdateListener {

    @BindView(R.id.empty_layout)
    View emptyLayout;

    @BindView(R.id.recyclerView)
    RecyclerView recycleView;

    LogListAdapter logListAdapter;

    ConnectionLog connectionLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
        ButterKnife.bind(this);

        connectionLog = App.getMainComponent().getConnectionLog();
        connectionLog.setUpdateListener(this);

        initActionBar();
        initAdapter();
        initRecycleView();

        scrollBottom();
    }

    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.title_log);
        }
    }

    private void initAdapter() {
        logListAdapter = new LogListAdapter(connectionLog.getItemList());
        checkLogItemsPresent();
    }

    private void initRecycleView() {
        recycleView.setLayoutManager(new LinearLayoutManager(this));
        recycleView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this)
                .showLastDivider()
                .size(getResources().getDimensionPixelSize(R.dimen.divider_size))
                .color(ContextCompat.getColor(this, R.color.colorDarkHintBackground))
                .build());

        recycleView.setAdapter(logListAdapter);
    }

    @Override
    public void onAddItem() {
        // TODO: fix crash here
        // logListAdapter.notifyAddItem();
        scrollBottom();
        setEmptyLayoutVisible(false);
    }

    @Override
    public void onClearList(int itemCount) {
        logListAdapter.notifyClearList(itemCount);
        setEmptyLayoutVisible(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        connectionLog.resetUpdateListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_log, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.menu_clear:
                DialogUtils.showDialogClearLog(this, () -> {
                    connectionLog.clear();
                });
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.to_start)
    public void toStartSelected() {
        scrollTop();
    }

    @OnClick(R.id.to_end)
    public void toEndSelected() {
        scrollBottom();
    }

    private void checkLogItemsPresent() {
        if (logListAdapter.getItemCount() == 0) {
            setEmptyLayoutVisible(true);
        } else {
            setEmptyLayoutVisible(false);
        }
    }

    private void setEmptyLayoutVisible(boolean visible) {
        emptyLayout.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    private void scrollTop() {
        recycleView.scrollToPosition(0);
    }

    private void scrollBottom() {
        recycleView.scrollToPosition(logListAdapter.getItemCount() - 1);
    }
}
