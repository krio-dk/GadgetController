package com.krio.gadgetcontroller.ui.listener;

import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;

import java.util.Map;

/**
 * Created by krio on 29.05.16.
 */
public interface OnComponentInteractionListener {
    void onPanelAddSelected();
    void onWidgetTypeSelected(WidgetType widgetType);
    void onSetAttrs(Map<String, Object> attrs);
    void onCreateCommand (CommandType commandType, String cmd, Map<String, Object> params);
    void onFinishWidgetBuild();
}
