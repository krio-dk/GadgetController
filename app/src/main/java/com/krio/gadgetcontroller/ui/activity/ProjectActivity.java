package com.krio.gadgetcontroller.ui.activity;

import com.krio.gadgetcontroller.App;
import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.connection.core.Connection;
import com.krio.gadgetcontroller.logic.connection.core.ConnectionFactory;
import com.krio.gadgetcontroller.logic.connection.core.ConnectionWrapper;
import com.krio.gadgetcontroller.logic.connection.events.ConnectionEventHandler;
import com.krio.gadgetcontroller.logic.connection.events.ConnectionEventListener;
import com.krio.gadgetcontroller.logic.project.Project;
import com.krio.gadgetcontroller.logic.project.ProjectModeWrapper;
import com.krio.gadgetcontroller.ui.adapter.PanelPagerAdapter;
import com.krio.gadgetcontroller.ui.dialog.ConnectionDialogFragment;
import com.krio.gadgetcontroller.ui.fragment.PanelFragment;
import com.krio.gadgetcontroller.ui.utils.DialogUtils;
import com.krio.gadgetcontroller.ui.utils.NavigationItemsUtils;
import com.viewpagerindicator.CirclePageIndicator;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.Observable;
import java.util.Observer;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProjectActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, Observer, PanelFragment.OnPanelDeleteListener, ConnectionDialogFragment.onConnectionSelectedListener, ConnectionEventListener {

    public static final String EXTRA_PROJECT_ID = "extra_project_id";

    public static final int REQUEST_ADD_COMPONENT = 1;
    public static final int REQUEST_EDIT_PROJECT = 2;
    public static final int REQUEST_EDIT_PANEL = 3;
    public static final int REQUEST_EDIT_WIDGET_ATTR = 4;
    public static final int REQUEST_EDIT_WIDGET_COMMANDS = 5;
    public static final int REQUEST_CONNECTION_BLUETOOTH = 6;
    public static final int REQUEST_CONNECTION_TCP = 7;

    @Inject
    Project project;

    @Inject
    ProjectModeWrapper projectModeWrapper;

    @Inject
    ConnectionWrapper connectionWrapper;

    @Inject
    ConnectionEventHandler connectionEventHandler;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.uuid)
    TextView uuid;

    @BindView(R.id.panel_pager)
    ViewPager panelViewPager;

    @BindView(R.id.add_component)
    FloatingActionButton addComponent;

    PanelPagerAdapter panelPagerAdapter;

    Menu menu;

    ConnectionDialogFragment connectionDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
        ButterKnife.bind(this);

        initMainComponent();

        App.getMainComponent().inject(this);

        connectionEventHandler.setEventListener(this);
        projectModeWrapper.addObserver(this);

        setSupportActionBar(toolbar);

        initDrawerToggle();
        initNavigationView();

        initPanelPagerAdapter();
        initPanelViewPager();
        initPanelPagerIndicator();

        initConnectionDialog();

        updateToolbar();
    }

    private void initMainComponent() {
        long projectId = getIntent().getLongExtra(EXTRA_PROJECT_ID, 0);

        if (projectId == 0)
            finish();

        if (App.getMainComponent() == null) {
            App.createMainComponent(projectId);
        }
    }

    private void initDrawerToggle() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                super.onDrawerSlide(drawerView, 0); // this disables the arrow @ completed state
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, 0); // this disables the animation
            }
        };

        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void initNavigationView() {
        uuid.setText(project.getToken());
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void initEditModeSwitch() {
        View actionView = navigationView.getMenu().findItem(R.id.nav_mode).getActionView();
        SwitchCompat editModeSwitch = ButterKnife.findById(actionView, R.id.edit_mode_switcher);

        editModeSwitch.setOnCheckedChangeListener((CompoundButton buttonView, boolean isChecked) -> {
            projectModeWrapper.setEditMode(isChecked);
            drawer.postDelayed(() -> drawer.closeDrawer(GravityCompat.START), 300);
        });
    }

    private void initPanelPagerAdapter() {
        panelPagerAdapter = new PanelPagerAdapter(getSupportFragmentManager(), project.getPanelList());
    }

    private void initPanelViewPager() {
        panelViewPager.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.view_pager_separator_size));
        panelViewPager.setPageMarginDrawable(R.color.colorDarkHintBackground);
        panelViewPager.setAdapter(panelPagerAdapter);
    }

    private void initPanelPagerIndicator() {
        CirclePageIndicator circleIndicator = (CirclePageIndicator) findViewById(R.id.pager_indicator);
        circleIndicator.setRadius(getResources().getDimensionPixelSize(R.dimen.view_pager_circle_indicator_radius));
        circleIndicator.setFillColor(ContextCompat.getColor(ProjectActivity.this, R.color.colorFillPagerIndicator));
        circleIndicator.setPageColor(ContextCompat.getColor(ProjectActivity.this, R.color.colorDarkDividersBackground));
        circleIndicator.setStrokeColor(ContextCompat.getColor(ProjectActivity.this, R.color.colorDarkHintBackground));
        circleIndicator.setViewPager(panelViewPager);
    }

    private void initConnectionDialog() {
        Fragment dialog = getSupportFragmentManager().findFragmentByTag(ConnectionDialogFragment.TAG);
        if (dialog == null) {
            connectionDialog = ConnectionDialogFragment.newInstance();
        } else {
            connectionDialog = (ConnectionDialogFragment) dialog;
        }
        connectionDialog.setItemClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateToolbar();
        updateMenuActionItem();
    }

    private void updateToolbar() {
        if (toolbar != null) {
            switch (connectionWrapper.getConnection().getState()) {
                case Connection.STATE_CONNECTED:
                    toolbar.setSubtitle(R.string.subtitle_connected);
                    break;

                case Connection.STATE_CONNECTING:
                    toolbar.setSubtitle(R.string.subtitle_connecting);
                    break;

                default:
                    toolbar.setSubtitle(R.string.subtitle_not_connected);
            }
        }
    }

    private void updateMenuActionItem() {
        if (menu != null) {
            MenuItem actionItem = menu.findItem(R.id.connect_device);

            switch (connectionWrapper.getConnection().getState()) {
                case Connection.STATE_CONNECTED:
                    actionItem.setActionView(null);
                    actionItem.setIcon(R.drawable.ic_stop_white_48dp);
                    break;

                case Connection.STATE_CONNECTING:
                    actionItem.setActionView(R.layout.progress_bar);
                    break;

                default:
                    actionItem.setActionView(null);
                    actionItem.setIcon(R.drawable.ic_play_arrow_white_48dp);
            }
        }
    }

    private void setEditMode(boolean editMode) {
        if (editMode) {
            editModeOn();
        } else {
            editModeOff();
        }
        // DialogUtils.setEditMode(editMode);
    }

    private void editModeOn() {
        setTitle(R.string.title_edit_mode);
        addComponent.setVisibility(View.VISIBLE);

        /*
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, R.color.colorEditModePrimary)));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorEditModePrimaryDark));
        }
        */
    }

    private void editModeOff() {
        setTitle(project.getName());
        addComponent.setVisibility(View.GONE);

        /*
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, R.color.colorPrimary)));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
        */
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finishActivity();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_project, menu);
        this.menu = menu;

        initEditModeSwitch();
        updateMenuActionItem();
        setEditMode(projectModeWrapper.isEditMode());

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.connect_device:
                if (connectionWrapper.getConnection().getState() == Connection.STATE_DISCONNECTED) {
                    connectionDialog.show(getSupportFragmentManager(), ConnectionDialogFragment.TAG);
                } else {
                    connectionWrapper.getConnection().performDisconnect();
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_commands:
                drawer.closeDrawer(GravityCompat.START);
                showCommandsActivity();
                break;

            case R.id.nav_log:
                drawer.closeDrawer(GravityCompat.START);
                showLogActivity();
                break;

            case R.id.nav_mode:
                break;

            default:
                drawer.closeDrawer(GravityCompat.START);
                NavigationItemsUtils.onNavigationItemSelected(item, this);
        }

        return true;
    }

    @OnClick(R.id.add_component)
    public void onAddComponentClick() {
        showComponentAddActivity();
    }

    @OnClick(R.id.close_project)
    public void onCloseProjectSelected() {
        drawer.closeDrawer(GravityCompat.START);
        Handler handler = new Handler();
        handler.postDelayed(this::finishActivity, 200);
    }

    @Override
    public void onPanelDelete(long panelId) {
        DialogUtils.showDialogDeletePanel(this, () -> {
            project.deletePanel(panelId);
            panelPagerAdapter.update(project.getPanelList());
        });
    }

    private void showCommandsActivity() {
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            startActivity(new Intent(this, CommandListActivity.class));
        }, 200);
    }

    private void showLogActivity() {
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            startActivity(new Intent(this, LogActivity.class));
        }, 200);
    }

    private void showComponentAddActivity() {
        long panelId = panelPagerAdapter.getPanelId(panelViewPager.getCurrentItem());

        Intent intent = new Intent(this, ComponentAddActivity.class);
        intent.putExtra(ComponentAddActivity.PANEL_ID, panelId);
        startActivityForResult(intent, REQUEST_ADD_COMPONENT);
    }

    @Override
    public void update(Observable observable, Object data) {
        setEditMode(projectModeWrapper.isEditMode());
    }

    @Override
    public void onConnectionBluetoothSelected() {
        Intent intent = new Intent(ProjectActivity.this, LocalConnectionActivity.class);
        intent.putExtra(LocalConnectionActivity.REQUEST_CODE, REQUEST_CONNECTION_BLUETOOTH);
        startActivityForResult(intent, REQUEST_CONNECTION_BLUETOOTH);
    }

    @Override
    public void onConnectionTCPSelected() {
        Intent intent = new Intent(ProjectActivity.this, TCPConnectionActivity.class);
        startActivityForResult(intent, REQUEST_CONNECTION_TCP);
    }

    @Override
    public void onConnectionGCServerSelected() {
        // No need to user ConnectPerformer because of this activity process all itself
        Connection connection = ConnectionFactory.createSocketIOConnection(project.getToken());
        connectionWrapper.setConnection(connection);
        connectionWrapper.getConnection().performConnect();
    }

    @Override
    public void onChangeState(int state) {
        updateToolbar();
        updateMenuActionItem();
    }

    @Override
    public void onSendData(byte[] data) {

    }

    @Override
    public void onReceiveData(byte[] data, int byteCount) {

    }

    @Override
    public void onConnectionFailed() {
        DialogUtils.showDialogConnectionFailed(this, () -> {
            connectionWrapper.getConnection().performConnect();
        });
    }

    @Override
    public void onConnectionLost() {
        DialogUtils.showDialogConnectionLost(this, () -> {
            connectionWrapper.getConnection().performConnect();
        });
    }

    @Override
    public void onRemoteEvent(String event) {
        switch (event) {
            case Connection.REMOTE_EVENT_DEVICE_CONNECT:

                break;

            case Connection.REMOTE_EVENT_DEVICE_DISCONNECT:

                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (App.getMainComponent() != null) {
            connectionEventHandler.resetEventListener();
            projectModeWrapper.deleteObserver(this);
        }
    }

    private void finishActivity() {
        connectionEventHandler.resetEventListener();
        projectModeWrapper.deleteObserver(this);
        connectionWrapper.getConnection().performDisconnect();
        App.clearMainComponent();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        connectionEventHandler.setEventListener(this);

        if (resultCode == RESULT_OK) {

            switch (requestCode) {
                case REQUEST_ADD_COMPONENT:
                    int type = data.getIntExtra(ComponentAddActivity.COMPONENT_TYPE, ComponentAddActivity.TYPE_PANEL);

                    if (type == ComponentAddActivity.TYPE_PANEL) {
                        panelPagerAdapter.update(project.getPanelList());

                        Handler handler = new Handler();
                        handler.postDelayed(() -> {
                            panelViewPager.setCurrentItem(panelPagerAdapter.getCount(), true);
                        }, 500);

                    } else {
                        // TODO: optimize widget adding via insertItem in concrete widgetListAdapter
                        long widgetId = data.getLongExtra(ComponentAddActivity.WIDGET_ID, 0);
                        panelPagerAdapter.update();
                    }
                    break;

                case REQUEST_EDIT_PANEL:
                    panelPagerAdapter.update();
                    break;

                case REQUEST_EDIT_WIDGET_ATTR:
                    // TODO: optimize widget adding via updateItem in concrete widgetListAdapter
                    long widgetId = data.getLongExtra(ComponentEditActivity.WIDGET_ID, 0);
                    panelPagerAdapter.update();
                    break;

                case REQUEST_EDIT_WIDGET_COMMANDS:
                    // TODO: optimize
                    panelPagerAdapter.update();
                    break;

                case REQUEST_CONNECTION_BLUETOOTH:
                case REQUEST_CONNECTION_TCP:
                    // do nothing
                    break;
            }
        }
    }
}
