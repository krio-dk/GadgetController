package com.krio.gadgetcontroller.ui.fragment;

import com.krio.gadgetcontroller.App;
import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.connection.core.Connection;
import com.krio.gadgetcontroller.logic.connection.core.ConnectionFactory;
import com.krio.gadgetcontroller.ui.adapter.TCPHistoryAdapter;
import com.krio.gadgetcontroller.ui.utils.DialogUtils;
import com.krio.gadgetcontroller.ui.utils.EditTextUtils;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Deprecated
public class TCPConnectionFragmentOld extends BaseFragment implements
                                                        TCPHistoryAdapter.OnHistoryItemClickListener {


    private static final String HISTORY_SET = "history_set";

    @BindView(R.id.main_content)
    View mainContent;

    @BindView(R.id.wifi_config)
    View wifiConfig;

    @BindView(R.id.network_ssid)
    TextView networkSSID;

    @BindView(R.id.network_ip)
    TextView networkIP;

    @BindView(R.id.host_input_layout)
    TextInputLayout hostInputLayout;

    @BindView(R.id.host)
    EditText hostEditText;

    @BindView(R.id.port_input_layout)
    TextInputLayout portInputLayout;

    @BindView(R.id.port)
    EditText portEditText;

    @BindView(R.id.history_list)
    RecyclerView historyListView;

    WifiManager wifiManager;

    SharedPreferences sharedPreferences;
    TCPHistoryAdapter tcpHistoryAdapter;

    public static TCPConnectionFragmentOld newInstance() {
        TCPConnectionFragmentOld fragment = new TCPConnectionFragmentOld();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tcp_connection_old, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        initEditTextUtils();
        initHistoryList();
    }

    @Override
    public void onResume() {
        super.onResume();

        checkNetworkConnection();
        registerNetworkStateChangeReceiver();

        if (wifiManager != null) {
            checkWiFiState();
            registerWiFiStateChangeReceiver();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        unregisterNetworkStateChangeReceiver();

        if (wifiManager != null) {
            unregisterWiFiStateChangeReceiver();
        }
    }

    @Override
    public void onHistoryItemClick(final String host, final String port) {
        hostEditText.setText(host);
        portEditText.setText(port);
        onDone();
    }

    private void initEditTextUtils() {
        EditTextUtils hostEditTextUtils =
                new EditTextUtils(getActivity(), mainContent, hostInputLayout, hostEditText);
        EditTextUtils portEditTextUtils =
                new EditTextUtils(getActivity(), mainContent, portInputLayout, portEditText);

        hostEditTextUtils.setMaxLen(24);
        portEditTextUtils.setMaxLen(5);
    }

    private void initHistoryList() {
        Set<String> historySet = sharedPreferences.getStringSet(HISTORY_SET, new LinkedHashSet<>());

        tcpHistoryAdapter = new TCPHistoryAdapter(historySet);
        tcpHistoryAdapter.setClickListener(this);

        historyListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        historyListView.setAdapter(tcpHistoryAdapter);

        historyListView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                                                  .showLastDivider()
                                                  .build());
    }

    private void checkWiFiState() {
        if (wifiManager.isWifiEnabled()) {
            checkWiFiConnection();
        } else {
            setWiFiDisabledState();
        }

    }

    private void checkWiFiConnection() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (activeNetwork != null && activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
            setWifiConnectedState();
        } else {
            setWifiNotConnectedState();
        }
    }

    private void checkNetworkConnection() {
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (activeNetwork != null && activeNetwork.isConnected()) {
            Log.d("KRIO", "Network connection: connected");
        } else {
            Log.d("KRIO", "Network connection: not connected");
        }
    }


    private void setWifiConnectedState() {
        setSubtitle(R.string.subtitle_connected);
        setConfigVisible(false);
        updateNetworkInfo();

        hostEditText.setEnabled(true);
        portEditText.setEnabled(true);
        tcpHistoryAdapter.setClickListener(this);
    }

    private void setWifiNotConnectedState() {
        setSubtitle(R.string.subtitle_not_connected);
        setConfigVisible(false);
        networkSSID.setText("");
        networkIP.setText("");

        hostEditText.setEnabled(false);
        portEditText.setEnabled(false);
        tcpHistoryAdapter.setClickListener(null);
    }

    private void setWiFiDisabledState() {
        setSubtitle(R.string.subtitle_wifi_disabled);
        setConfigVisible(true);
    }


    void setConfigVisible(boolean visible) {
        if (visible) {
            mainContent.setVisibility(View.GONE);
            wifiConfig.setVisibility(View.VISIBLE);
        } else {
            wifiConfig.setVisibility(View.GONE);
            mainContent.setVisibility(View.VISIBLE);
        }
    }

    private void setSubtitle(int resId) {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setSubtitle(resId);
        }
    }

    private void updateNetworkInfo() {
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        networkSSID.setText(wifiInfo.getSSID());
        networkIP.setText(getIp());
    }

    private String getIp() {
        String ipString = null;
        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();

        // Convert little-endian to big-endianif needed
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ipAddress = Integer.reverseBytes(ipAddress);
        }

        byte[] ipByteArray = BigInteger.valueOf(ipAddress).toByteArray();

        try {
            ipString = InetAddress.getByAddress(ipByteArray).getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        return ipString;
    }

    @OnClick(R.id.enable_wifi)
    public void onEnableWiFiClick() {
        wifiManager.setWifiEnabled(true);
    }


    public void onDone() {
        String host = hostEditText.getText().toString();
        String port = portEditText.getText().toString();

        if (validateData(host, port)) {
            Connection connection =
                    ConnectionFactory.createTCPClientConnection(host, Integer.parseInt(port));
            App.setConnection(connection);

            saveToHistory(host, port);

            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
        }
    }

    private boolean validateData(String host, String port) {
        if (TextUtils.isEmpty(host) || TextUtils.isEmpty(port)) {
            return false;
        }

        return true;
    }

    private void saveToHistory(String host, String port) {

        List<String> historyList = new ArrayList<>();
        historyList.addAll(sharedPreferences.getStringSet(HISTORY_SET, new LinkedHashSet<>()));

        historyList.add(0, buildHistoryItem(host, port));

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(HISTORY_SET, new LinkedHashSet<>(historyList));
        editor.apply();
    }


    private String buildHistoryItem(String host, String port) {
        return host + ':' + port;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_add_element, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_done:
                onDone();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.clearHistory)
    public void onClearHistoryClick() {
        DialogUtils.showDialogClearTCPHistory(getActivity(), () -> {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove(HISTORY_SET);
            editor.apply();

            tcpHistoryAdapter.clear();
        });
    }

    private void registerWiFiStateChangeReceiver() {
        final IntentFilter filters = new IntentFilter();
        filters.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        // filters.addAction("android.net.wifi.STATE_CHANGE");
        filters.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        getActivity().registerReceiver(wifiStateChangeReceiver, filters);
    }

    private void registerNetworkStateChangeReceiver() {
        final IntentFilter filters = new IntentFilter();
        filters.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        getActivity().registerReceiver(networkStateChangeReceiver, filters);
    }

    private void unregisterWiFiStateChangeReceiver() {
        getActivity().unregisterReceiver(wifiStateChangeReceiver);
    }

    private void unregisterNetworkStateChangeReceiver() {
        getActivity().unregisterReceiver(networkStateChangeReceiver);
    }

    private final BroadcastReceiver wifiStateChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            checkWiFiState();
        }
    };

    private final BroadcastReceiver networkStateChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            checkNetworkConnection();
        }
    };
}
