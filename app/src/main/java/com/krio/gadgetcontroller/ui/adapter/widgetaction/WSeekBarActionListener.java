package com.krio.gadgetcontroller.ui.adapter.widgetaction;

import android.widget.SeekBar;

import com.krio.gadgetcontroller.logic.widget.WSeekBar;
import com.krio.gadgetcontroller.logic.widget.Widget;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.ui.adapter.WidgetListAdapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by krio on 25.04.16.
 */
public class WSeekBarActionListener implements SeekBar.OnSeekBarChangeListener {

    List<Widget> widgetList;

    public WSeekBarActionListener(List<Widget> widgetList) {
        this.widgetList = widgetList;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            WidgetListAdapter.MainViewHolder mainViewHolder = (WidgetListAdapter.MainViewHolder) seekBar.getTag();
            int position = mainViewHolder.getLayoutPosition();

            Widget widget = widgetList.get(position);

            Map<String, Object> params = new HashMap<>();
            params.put(WSeekBar.CMD_PARAM_VALUE, progress);

            ((WSeekBar) widget).setProgress(progress);
            widget.performCommand(CommandType.COMMAND_SEEKBAR, params);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }
}
