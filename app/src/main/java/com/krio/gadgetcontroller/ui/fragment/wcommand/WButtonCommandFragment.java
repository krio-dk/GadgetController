package com.krio.gadgetcontroller.ui.fragment.wcommand;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.krio.gadgetcontroller.App;
import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.command.CommandValidator;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.ui.fragment.BaseFragment;
import com.krio.gadgetcontroller.ui.listener.OnComponentInteractionListener;
import com.krio.gadgetcontroller.ui.utils.EditTextUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by krio on 11.05.16.
 */
public class WButtonCommandFragment extends BaseFragment {

    public static final String IS_NEW_COMMANDS = "is_new_commands";

    public static final String COMMAND_DOWN = "commandDown";
    public static final String COMMAND_UP = "commandUp";

    @BindView(R.id.command_down_layout)
    TextInputLayout commandDownLayout;

    @BindView(R.id.command_down)
    EditText commandDown;

    @BindView(R.id.command_up_layout)
    TextInputLayout commandUpLayout;

    @BindView(R.id.command_up)
    EditText commandUp;

    EditTextUtils commandDownTextUtils;
    EditTextUtils commandUpTextUtils;

    Bundle defaultValues;
    boolean isNewCommands;

    OnComponentInteractionListener listener;

    public static WButtonCommandFragment newInstance(boolean isNewCommands, String commandDown, String commandUp) {
        WButtonCommandFragment fragment = new WButtonCommandFragment();
        Bundle args = new Bundle();
        args.putBoolean(IS_NEW_COMMANDS, isNewCommands);
        args.putString(COMMAND_DOWN, commandDown);
        args.putString(COMMAND_UP, commandUp);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_command_button, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initEditTextUtils(view);

        defaultValues = getArguments();

        isNewCommands = defaultValues.getBoolean(IS_NEW_COMMANDS);

        commandDown.setText(defaultValues.getString(COMMAND_DOWN));
        commandUp.setText(defaultValues.getString(COMMAND_UP));
    }

    private void initEditTextUtils(View view) {
        LinearLayout parentLayout = ButterKnife.findById(view, R.id.parent_layout);

        commandDownTextUtils = new EditTextUtils(getActivity(), parentLayout, commandDownLayout, commandDown);
        commandUpTextUtils = new EditTextUtils(getActivity(), parentLayout, commandUpLayout, commandUp);

        commandDownTextUtils.setMaxLen(10);
        commandUpTextUtils.setMaxLen(10);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_add_element, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_done:
                onMenuDoneSelected();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Context ctx) {
        super.onAttach(ctx);
        try {
            listener = (OnComponentInteractionListener) ctx;
        } catch (ClassCastException e) {
            throw new ClassCastException(ctx.toString() + "Activity must implement fragment's listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void onMenuDoneSelected() {
        String cmdDown = commandDown.getText().toString().trim();
        String cmdUp = commandUp.getText().toString().trim();

        if (hasChanged(cmdDown, cmdUp)) {
            if (validateCommands(cmdDown, cmdUp)) {
                createCommand(CommandType.COMMAND_BUTTON_DOWN, cmdDown);
                createCommand(CommandType.COMMAND_BUTTON_UP, cmdUp);
                listener.onFinishWidgetBuild();
            }
        } else if (!isNewCommands) {
            getActivity().finish();
        }

    }

    private boolean validateCommands(String commandDown, String commandUp) {

        CommandValidator commandValidator = App.getMainComponent().getCommandValidator();

        boolean emptyCheckSuccess = true;

        if (commandDown.isEmpty()) {
            commandDownTextUtils.showEmptyWarn();
            emptyCheckSuccess = false;
        }

        if (commandUp.isEmpty()) {
            commandUpTextUtils.showEmptyWarn();
            emptyCheckSuccess = false;
        }

        if (!emptyCheckSuccess) {
            return false;
        }

        if (commandDown.equals(commandUp)) {
            commandDownTextUtils.showWarn(getString(R.string.warning_command_equals));
            commandUpTextUtils.showWarn(getString(R.string.warning_command_equals));
            return false;
        }

        if (hasCommandChanged(commandDown, COMMAND_DOWN)) {
            if (!commandValidator.validateOutputCommand(commandDown)) {
                commandDownTextUtils.showWarn(getString(R.string.warning_command_exist));
                return false;
            }
        }

        if (hasCommandChanged(commandUp, COMMAND_UP)) {
            if (!commandValidator.validateOutputCommand(commandUp)) {
                commandUpTextUtils.showWarn(getString(R.string.warning_command_exist));
                return false;
            }
        }

        return true;
    }

    private boolean hasChanged(String commandDown, String commandUp) {
        boolean hasChanged = false;

        if (!commandDown.equals(defaultValues.getString(COMMAND_UP))) {
            hasChanged = true;
        }

        if (!commandUp.equals(defaultValues.getString(COMMAND_DOWN))) {
            hasChanged = true;
        }

        return hasChanged;
    }

    private boolean hasCommandChanged (String command, String defaultValuesKey) {
        return !command.equals(defaultValues.getString(defaultValuesKey));
    }


    private void createCommand(CommandType commandType, String command) {
        // As we can have only one command for button ...
        if (!command.isEmpty()) {
            listener.onCreateCommand(commandType, command, null);
        }
    }
}
