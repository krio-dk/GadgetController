package com.krio.gadgetcontroller.ui.activity;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.ui.utils.NavigationItemsUtils;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        initDrawerToggle();

        navigationView.setNavigationItemSelectedListener(this);
    }

    private void initDrawerToggle() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                super.onDrawerSlide(drawerView, 0); // this disables the arrow @ completed state
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, 0); // this disables the animation
            }
        };

        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // no menu
        // getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        drawer.closeDrawer(GravityCompat.START);
        NavigationItemsUtils.onNavigationItemSelected(item, this);
        return true;
    }

    @Optional
    @OnClick({R.id.website, R.id.vk, R.id.facebook, R.id.youtube})
    public void onSocialItemSelected(View view) {
        drawer.closeDrawer(GravityCompat.START);

        String url = null;
        switch (view.getId()) {
            case R.id.website:
                url = getString(R.string.url_website);
                break;

            case R.id.vk:
                url = getString(R.string.url_vk);
                break;

            case R.id.facebook:
                url = getString(R.string.url_facebook);
                break;

            case R.id.youtube:
                url = getString(R.string.url_youtube);
                break;
        }

        if (url != null) {
            startSocialActivity(url);
        }
    }



    private void startSocialActivity(final String url) {
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }, 200);
    }
}
