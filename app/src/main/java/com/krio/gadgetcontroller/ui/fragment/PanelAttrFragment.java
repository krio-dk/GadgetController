package com.krio.gadgetcontroller.ui.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.panel.Panel;
import com.krio.gadgetcontroller.ui.listener.OnComponentInteractionListener;
import com.krio.gadgetcontroller.ui.utils.EditTextUtils;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PanelAttrFragment extends BaseFragment {

    @BindView(R.id.caption_layout)
    TextInputLayout captionLayout;

    @BindView(R.id.name)
    EditText captionText;

    EditTextUtils captionTextUtils;

    OnComponentInteractionListener callbacks;

    public static PanelAttrFragment newInstance(String caption) {
        PanelAttrFragment fragment = new PanelAttrFragment();
        Bundle args = new Bundle();
        args.putString(Panel.CAPTION, caption);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_panel_attr, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initEditTextUtils(view);

        Bundle args = getArguments();
        if (args != null) {
            captionText.setText(args.getString(Panel.CAPTION));
        }
    }

    private void initEditTextUtils(View view) {
        LinearLayout parentLayout = ButterKnife.findById(view, R.id.parent_layout);
        captionTextUtils = new EditTextUtils(getActivity(), parentLayout, captionLayout, captionText);
        captionTextUtils.setMaxLen(25);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_add_element, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_done:
                String caption = captionText.getText().toString();
                onMenuDoneSelected(caption);
        }
        return super.onOptionsItemSelected(item);
    }

    private void onMenuDoneSelected(String caption) {
        if (validateAttrs(caption)) {
            setPanelAttr();
        }
    }

    private boolean validateAttrs(String caption) {
        if (caption.isEmpty()) {
            captionTextUtils.showEmptyWarn();
            return false;
        }
        return true;
    }

    @Override
    public void onAttach(Context ctx) {
        super.onAttach(ctx);
        try {
            callbacks = (OnComponentInteractionListener) ctx;
        } catch (ClassCastException e) {
            throw new ClassCastException(ctx.toString() + "Activity must implement fragment's listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    private void setPanelAttr() {
        Map<String, Object> attr = new HashMap<>();
        attr.put(Panel.CAPTION, captionText.getText().toString());
        callbacks.onSetAttrs(attr);
    }
}
