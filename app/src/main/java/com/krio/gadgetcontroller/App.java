package com.krio.gadgetcontroller;

import android.app.Application;
import android.content.Context;

import com.krio.gadgetcontroller.di.app.AppComponent;
import com.krio.gadgetcontroller.di.app.AppModule;
import com.krio.gadgetcontroller.di.app.DaggerAppComponent;
import com.krio.gadgetcontroller.di.builder.DaggerWidgetBuilderComponent;
import com.krio.gadgetcontroller.di.builder.WidgetBuilderComponent;
import com.krio.gadgetcontroller.di.builder.WidgetBuilderModule;
import com.krio.gadgetcontroller.di.main.CommandModule;
import com.krio.gadgetcontroller.di.main.ConnectionModule;
import com.krio.gadgetcontroller.di.main.MainComponent;
import com.krio.gadgetcontroller.di.main.ProjectModule;
import com.krio.gadgetcontroller.logic.connection.core.Connection;
import com.krio.gadgetcontroller.logic.connection.log.ConnectionLog;
import com.krio.gadgetcontroller.logic.projectmanager.ProjectManager;
import com.krio.gadgetcontroller.logic.widgetbuilder.WidgetBuilder;
import com.squareup.sqlbrite.BriteDatabase;

/**
 * Created by krio on 21.05.16.
 */
public class App extends Application {

    private static AppComponent appComponent;
    private static MainComponent mainComponent;
    private static WidgetBuilderComponent widgetBuilderComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    //App Component
    public static Context getContext() {
        return appComponent.getContext();
    }

    public static BriteDatabase getDataBase() {
        return appComponent.getDataBase();
    }

    public static ProjectManager getProjectManager() {
        return appComponent.getProjectManager();
    }


    // Main Component
    public static MainComponent createMainComponent(long projectId) {
        mainComponent = appComponent.plusProjectComponent(new ProjectModule(projectId), new ConnectionModule(), new CommandModule());
        return mainComponent;
    }

    public static MainComponent getMainComponent() {
        return mainComponent;
    }

    public static void clearMainComponent() {
        mainComponent = null;
    }


    // Connection Helper
    public static Connection getConnection() {
        return mainComponent.getConnectionWrapper().getConnection();
    }

    public static void setConnection(Connection connection) {
        mainComponent.getConnectionWrapper().setConnection(connection);
    }


    // Log Helper
    public static ConnectionLog getConnectionLog() {
        return mainComponent.getConnectionLog();
    }


    // WidgetBuilder Component
    public static WidgetBuilderComponent createWidgetBuilderComponent(WidgetBuilder widgetBuilder) {
        widgetBuilderComponent = DaggerWidgetBuilderComponent.builder()
                .widgetBuilderModule(new WidgetBuilderModule(widgetBuilder))
                .build();

        return widgetBuilderComponent;
    }

    public static WidgetBuilderComponent getWidgetBuilderComponent() {
        return widgetBuilderComponent;
    }

    public static void clearWidgetBuilderComponent() {
        widgetBuilderComponent = null;
    }

}
