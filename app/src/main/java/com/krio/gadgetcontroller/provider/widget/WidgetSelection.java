package com.krio.gadgetcontroller.provider.widget;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import com.krio.gadgetcontroller.provider.base.AbstractSelection;
import com.krio.gadgetcontroller.provider.panel.*;
import com.krio.gadgetcontroller.provider.project.*;

/**
 * Selection for the {@code widget} table.
 */
public class WidgetSelection extends AbstractSelection<WidgetSelection> {
    @Override
    protected Uri baseUri() {
        return WidgetColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code WidgetCursor} object, which is positioned before the first entry, or null.
     */
    public WidgetCursor query(ContentResolver contentResolver, String[] projection) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new WidgetCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, null)}.
     */
    public WidgetCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null);
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param context The context to use for the query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code WidgetCursor} object, which is positioned before the first entry, or null.
     */
    public WidgetCursor query(Context context, String[] projection) {
        Cursor cursor = context.getContentResolver().query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new WidgetCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(context, null)}.
     */
    public WidgetCursor query(Context context) {
        return query(context, null);
    }


    public WidgetSelection id(long... value) {
        addEquals("widget." + WidgetColumns._ID, toObjectArray(value));
        return this;
    }

    public WidgetSelection idNot(long... value) {
        addNotEquals("widget." + WidgetColumns._ID, toObjectArray(value));
        return this;
    }

    public WidgetSelection orderById(boolean desc) {
        orderBy("widget." + WidgetColumns._ID, desc);
        return this;
    }

    public WidgetSelection orderById() {
        return orderById(false);
    }

    public WidgetSelection positionOnPanel(Integer... value) {
        addEquals(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public WidgetSelection positionOnPanelNot(Integer... value) {
        addNotEquals(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public WidgetSelection positionOnPanelGt(int value) {
        addGreaterThan(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public WidgetSelection positionOnPanelGtEq(int value) {
        addGreaterThanOrEquals(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public WidgetSelection positionOnPanelLt(int value) {
        addLessThan(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public WidgetSelection positionOnPanelLtEq(int value) {
        addLessThanOrEquals(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public WidgetSelection orderByPositionOnPanel(boolean desc) {
        orderBy(WidgetColumns.POSITION_ON_PANEL, desc);
        return this;
    }

    public WidgetSelection orderByPositionOnPanel() {
        orderBy(WidgetColumns.POSITION_ON_PANEL, false);
        return this;
    }

    public WidgetSelection caption(String... value) {
        addEquals(WidgetColumns.CAPTION, value);
        return this;
    }

    public WidgetSelection captionNot(String... value) {
        addNotEquals(WidgetColumns.CAPTION, value);
        return this;
    }

    public WidgetSelection captionLike(String... value) {
        addLike(WidgetColumns.CAPTION, value);
        return this;
    }

    public WidgetSelection captionContains(String... value) {
        addContains(WidgetColumns.CAPTION, value);
        return this;
    }

    public WidgetSelection captionStartsWith(String... value) {
        addStartsWith(WidgetColumns.CAPTION, value);
        return this;
    }

    public WidgetSelection captionEndsWith(String... value) {
        addEndsWith(WidgetColumns.CAPTION, value);
        return this;
    }

    public WidgetSelection orderByCaption(boolean desc) {
        orderBy(WidgetColumns.CAPTION, desc);
        return this;
    }

    public WidgetSelection orderByCaption() {
        orderBy(WidgetColumns.CAPTION, false);
        return this;
    }

    public WidgetSelection panelId(long... value) {
        addEquals(WidgetColumns.PANEL_ID, toObjectArray(value));
        return this;
    }

    public WidgetSelection panelIdNot(long... value) {
        addNotEquals(WidgetColumns.PANEL_ID, toObjectArray(value));
        return this;
    }

    public WidgetSelection panelIdGt(long value) {
        addGreaterThan(WidgetColumns.PANEL_ID, value);
        return this;
    }

    public WidgetSelection panelIdGtEq(long value) {
        addGreaterThanOrEquals(WidgetColumns.PANEL_ID, value);
        return this;
    }

    public WidgetSelection panelIdLt(long value) {
        addLessThan(WidgetColumns.PANEL_ID, value);
        return this;
    }

    public WidgetSelection panelIdLtEq(long value) {
        addLessThanOrEquals(WidgetColumns.PANEL_ID, value);
        return this;
    }

    public WidgetSelection orderByPanelId(boolean desc) {
        orderBy(WidgetColumns.PANEL_ID, desc);
        return this;
    }

    public WidgetSelection orderByPanelId() {
        orderBy(WidgetColumns.PANEL_ID, false);
        return this;
    }

    public WidgetSelection panelProjectId(long... value) {
        addEquals(PanelColumns.PROJECT_ID, toObjectArray(value));
        return this;
    }

    public WidgetSelection panelProjectIdNot(long... value) {
        addNotEquals(PanelColumns.PROJECT_ID, toObjectArray(value));
        return this;
    }

    public WidgetSelection panelProjectIdGt(long value) {
        addGreaterThan(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public WidgetSelection panelProjectIdGtEq(long value) {
        addGreaterThanOrEquals(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public WidgetSelection panelProjectIdLt(long value) {
        addLessThan(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public WidgetSelection panelProjectIdLtEq(long value) {
        addLessThanOrEquals(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public WidgetSelection orderByPanelProjectId(boolean desc) {
        orderBy(PanelColumns.PROJECT_ID, desc);
        return this;
    }

    public WidgetSelection orderByPanelProjectId() {
        orderBy(PanelColumns.PROJECT_ID, false);
        return this;
    }

    public WidgetSelection panelProjectName(String... value) {
        addEquals(ProjectColumns.NAME, value);
        return this;
    }

    public WidgetSelection panelProjectNameNot(String... value) {
        addNotEquals(ProjectColumns.NAME, value);
        return this;
    }

    public WidgetSelection panelProjectNameLike(String... value) {
        addLike(ProjectColumns.NAME, value);
        return this;
    }

    public WidgetSelection panelProjectNameContains(String... value) {
        addContains(ProjectColumns.NAME, value);
        return this;
    }

    public WidgetSelection panelProjectNameStartsWith(String... value) {
        addStartsWith(ProjectColumns.NAME, value);
        return this;
    }

    public WidgetSelection panelProjectNameEndsWith(String... value) {
        addEndsWith(ProjectColumns.NAME, value);
        return this;
    }

    public WidgetSelection orderByPanelProjectName(boolean desc) {
        orderBy(ProjectColumns.NAME, desc);
        return this;
    }

    public WidgetSelection orderByPanelProjectName() {
        orderBy(ProjectColumns.NAME, false);
        return this;
    }

    public WidgetSelection panelProjectToken(String... value) {
        addEquals(ProjectColumns.TOKEN, value);
        return this;
    }

    public WidgetSelection panelProjectTokenNot(String... value) {
        addNotEquals(ProjectColumns.TOKEN, value);
        return this;
    }

    public WidgetSelection panelProjectTokenLike(String... value) {
        addLike(ProjectColumns.TOKEN, value);
        return this;
    }

    public WidgetSelection panelProjectTokenContains(String... value) {
        addContains(ProjectColumns.TOKEN, value);
        return this;
    }

    public WidgetSelection panelProjectTokenStartsWith(String... value) {
        addStartsWith(ProjectColumns.TOKEN, value);
        return this;
    }

    public WidgetSelection panelProjectTokenEndsWith(String... value) {
        addEndsWith(ProjectColumns.TOKEN, value);
        return this;
    }

    public WidgetSelection orderByPanelProjectToken(boolean desc) {
        orderBy(ProjectColumns.TOKEN, desc);
        return this;
    }

    public WidgetSelection orderByPanelProjectToken() {
        orderBy(ProjectColumns.TOKEN, false);
        return this;
    }

    public WidgetSelection panelProjectPositionOnList(Integer... value) {
        addEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public WidgetSelection panelProjectPositionOnListNot(Integer... value) {
        addNotEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public WidgetSelection panelProjectPositionOnListGt(int value) {
        addGreaterThan(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public WidgetSelection panelProjectPositionOnListGtEq(int value) {
        addGreaterThanOrEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public WidgetSelection panelProjectPositionOnListLt(int value) {
        addLessThan(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public WidgetSelection panelProjectPositionOnListLtEq(int value) {
        addLessThanOrEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public WidgetSelection orderByPanelProjectPositionOnList(boolean desc) {
        orderBy(ProjectColumns.POSITION_ON_LIST, desc);
        return this;
    }

    public WidgetSelection orderByPanelProjectPositionOnList() {
        orderBy(ProjectColumns.POSITION_ON_LIST, false);
        return this;
    }

    public WidgetSelection panelCaption(String... value) {
        addEquals(PanelColumns.CAPTION, value);
        return this;
    }

    public WidgetSelection panelCaptionNot(String... value) {
        addNotEquals(PanelColumns.CAPTION, value);
        return this;
    }

    public WidgetSelection panelCaptionLike(String... value) {
        addLike(PanelColumns.CAPTION, value);
        return this;
    }

    public WidgetSelection panelCaptionContains(String... value) {
        addContains(PanelColumns.CAPTION, value);
        return this;
    }

    public WidgetSelection panelCaptionStartsWith(String... value) {
        addStartsWith(PanelColumns.CAPTION, value);
        return this;
    }

    public WidgetSelection panelCaptionEndsWith(String... value) {
        addEndsWith(PanelColumns.CAPTION, value);
        return this;
    }

    public WidgetSelection orderByPanelCaption(boolean desc) {
        orderBy(PanelColumns.CAPTION, desc);
        return this;
    }

    public WidgetSelection orderByPanelCaption() {
        orderBy(PanelColumns.CAPTION, false);
        return this;
    }

    public WidgetSelection panelPositionOnScreen(Integer... value) {
        addEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public WidgetSelection panelPositionOnScreenNot(Integer... value) {
        addNotEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public WidgetSelection panelPositionOnScreenGt(int value) {
        addGreaterThan(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public WidgetSelection panelPositionOnScreenGtEq(int value) {
        addGreaterThanOrEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public WidgetSelection panelPositionOnScreenLt(int value) {
        addLessThan(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public WidgetSelection panelPositionOnScreenLtEq(int value) {
        addLessThanOrEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public WidgetSelection orderByPanelPositionOnScreen(boolean desc) {
        orderBy(PanelColumns.POSITION_ON_SCREEN, desc);
        return this;
    }

    public WidgetSelection orderByPanelPositionOnScreen() {
        orderBy(PanelColumns.POSITION_ON_SCREEN, false);
        return this;
    }

    public WidgetSelection type(WidgetType... value) {
        addEquals(WidgetColumns.TYPE, value);
        return this;
    }

    public WidgetSelection typeNot(WidgetType... value) {
        addNotEquals(WidgetColumns.TYPE, value);
        return this;
    }


    public WidgetSelection orderByType(boolean desc) {
        orderBy(WidgetColumns.TYPE, desc);
        return this;
    }

    public WidgetSelection orderByType() {
        orderBy(WidgetColumns.TYPE, false);
        return this;
    }
}
