package com.krio.gadgetcontroller.provider.commandparam;

import com.krio.gadgetcontroller.provider.base.BaseModel;

import java.util.Date;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Data model for the {@code command_param} table.
 */
public interface CommandParamModel extends BaseModel {

    /**
     * Get the {@code name} value.
     * Cannot be {@code null}.
     */
    @NonNull
    String getName();

    /**
     * Get the {@code integer_param} value.
     * Can be {@code null}.
     */
    @Nullable
    Integer getIntegerParam();

    /**
     * Get the {@code double_param} value.
     * Can be {@code null}.
     */
    @Nullable
    Double getDoubleParam();

    /**
     * Get the {@code string_param} value.
     * Can be {@code null}.
     */
    @Nullable
    String getStringParam();

    /**
     * Get the {@code boolean_param} value.
     * Can be {@code null}.
     */
    @Nullable
    Boolean getBooleanParam();

    /**
     * Get the {@code date_param} value.
     * Can be {@code null}.
     */
    @Nullable
    Date getDateParam();

    /**
     * Get the {@code command_id} value.
     */
    long getCommandId();

    /**
     * Get the {@code type} value.
     * Cannot be {@code null}.
     */
    @NonNull
    CommandParamType getType();
}
