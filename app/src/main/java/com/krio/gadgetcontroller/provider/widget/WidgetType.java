package com.krio.gadgetcontroller.provider.widget;

/**
 * Possible values for the {@code type} column of the {@code widget} table.
 */
public enum WidgetType {
    /**
     * 
     */
    LED,

    /**
     * 
     */
    DISPLAY,

    /**
     * 
     */
    BUTTON,

    /**
     * 
     */
    SEEKBAR,

    /**
     * 
     */
    SWITCH,

    /**
     * 
     */
    LABEL,

    /**
     * 
     */
    JOYSTICK,

    /**
     *
     */
    TIMER;

    public static WidgetType fromInteger(int x) {
        return WidgetType.values()[x];
    }
}