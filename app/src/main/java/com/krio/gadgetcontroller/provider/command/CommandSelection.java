package com.krio.gadgetcontroller.provider.command;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import com.krio.gadgetcontroller.provider.base.AbstractSelection;
import com.krio.gadgetcontroller.provider.widget.*;
import com.krio.gadgetcontroller.provider.panel.*;
import com.krio.gadgetcontroller.provider.project.*;

/**
 * Selection for the {@code command} table.
 */
public class CommandSelection extends AbstractSelection<CommandSelection> {
    @Override
    protected Uri baseUri() {
        return CommandColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code CommandCursor} object, which is positioned before the first entry, or null.
     */
    public CommandCursor query(ContentResolver contentResolver, String[] projection) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new CommandCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, null)}.
     */
    public CommandCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null);
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param context The context to use for the query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code CommandCursor} object, which is positioned before the first entry, or null.
     */
    public CommandCursor query(Context context, String[] projection) {
        Cursor cursor = context.getContentResolver().query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new CommandCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(context, null)}.
     */
    public CommandCursor query(Context context) {
        return query(context, null);
    }


    public CommandSelection id(long... value) {
        addEquals("command." + CommandColumns._ID, toObjectArray(value));
        return this;
    }

    public CommandSelection idNot(long... value) {
        addNotEquals("command." + CommandColumns._ID, toObjectArray(value));
        return this;
    }

    public CommandSelection orderById(boolean desc) {
        orderBy("command." + CommandColumns._ID, desc);
        return this;
    }

    public CommandSelection orderById() {
        return orderById(false);
    }

    public CommandSelection type(CommandType... value) {
        addEquals(CommandColumns.TYPE, value);
        return this;
    }

    public CommandSelection typeNot(CommandType... value) {
        addNotEquals(CommandColumns.TYPE, value);
        return this;
    }


    public CommandSelection orderByType(boolean desc) {
        orderBy(CommandColumns.TYPE, desc);
        return this;
    }

    public CommandSelection orderByType() {
        orderBy(CommandColumns.TYPE, false);
        return this;
    }

    public CommandSelection widgetId(long... value) {
        addEquals(CommandColumns.WIDGET_ID, toObjectArray(value));
        return this;
    }

    public CommandSelection widgetIdNot(long... value) {
        addNotEquals(CommandColumns.WIDGET_ID, toObjectArray(value));
        return this;
    }

    public CommandSelection widgetIdGt(long value) {
        addGreaterThan(CommandColumns.WIDGET_ID, value);
        return this;
    }

    public CommandSelection widgetIdGtEq(long value) {
        addGreaterThanOrEquals(CommandColumns.WIDGET_ID, value);
        return this;
    }

    public CommandSelection widgetIdLt(long value) {
        addLessThan(CommandColumns.WIDGET_ID, value);
        return this;
    }

    public CommandSelection widgetIdLtEq(long value) {
        addLessThanOrEquals(CommandColumns.WIDGET_ID, value);
        return this;
    }

    public CommandSelection orderByWidgetId(boolean desc) {
        orderBy(CommandColumns.WIDGET_ID, desc);
        return this;
    }

    public CommandSelection orderByWidgetId() {
        orderBy(CommandColumns.WIDGET_ID, false);
        return this;
    }

    public CommandSelection widgetPositionOnPanel(Integer... value) {
        addEquals(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public CommandSelection widgetPositionOnPanelNot(Integer... value) {
        addNotEquals(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public CommandSelection widgetPositionOnPanelGt(int value) {
        addGreaterThan(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public CommandSelection widgetPositionOnPanelGtEq(int value) {
        addGreaterThanOrEquals(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public CommandSelection widgetPositionOnPanelLt(int value) {
        addLessThan(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public CommandSelection widgetPositionOnPanelLtEq(int value) {
        addLessThanOrEquals(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public CommandSelection orderByWidgetPositionOnPanel(boolean desc) {
        orderBy(WidgetColumns.POSITION_ON_PANEL, desc);
        return this;
    }

    public CommandSelection orderByWidgetPositionOnPanel() {
        orderBy(WidgetColumns.POSITION_ON_PANEL, false);
        return this;
    }

    public CommandSelection widgetCaption(String... value) {
        addEquals(WidgetColumns.CAPTION, value);
        return this;
    }

    public CommandSelection widgetCaptionNot(String... value) {
        addNotEquals(WidgetColumns.CAPTION, value);
        return this;
    }

    public CommandSelection widgetCaptionLike(String... value) {
        addLike(WidgetColumns.CAPTION, value);
        return this;
    }

    public CommandSelection widgetCaptionContains(String... value) {
        addContains(WidgetColumns.CAPTION, value);
        return this;
    }

    public CommandSelection widgetCaptionStartsWith(String... value) {
        addStartsWith(WidgetColumns.CAPTION, value);
        return this;
    }

    public CommandSelection widgetCaptionEndsWith(String... value) {
        addEndsWith(WidgetColumns.CAPTION, value);
        return this;
    }

    public CommandSelection orderByWidgetCaption(boolean desc) {
        orderBy(WidgetColumns.CAPTION, desc);
        return this;
    }

    public CommandSelection orderByWidgetCaption() {
        orderBy(WidgetColumns.CAPTION, false);
        return this;
    }

    public CommandSelection widgetPanelId(long... value) {
        addEquals(WidgetColumns.PANEL_ID, toObjectArray(value));
        return this;
    }

    public CommandSelection widgetPanelIdNot(long... value) {
        addNotEquals(WidgetColumns.PANEL_ID, toObjectArray(value));
        return this;
    }

    public CommandSelection widgetPanelIdGt(long value) {
        addGreaterThan(WidgetColumns.PANEL_ID, value);
        return this;
    }

    public CommandSelection widgetPanelIdGtEq(long value) {
        addGreaterThanOrEquals(WidgetColumns.PANEL_ID, value);
        return this;
    }

    public CommandSelection widgetPanelIdLt(long value) {
        addLessThan(WidgetColumns.PANEL_ID, value);
        return this;
    }

    public CommandSelection widgetPanelIdLtEq(long value) {
        addLessThanOrEquals(WidgetColumns.PANEL_ID, value);
        return this;
    }

    public CommandSelection orderByWidgetPanelId(boolean desc) {
        orderBy(WidgetColumns.PANEL_ID, desc);
        return this;
    }

    public CommandSelection orderByWidgetPanelId() {
        orderBy(WidgetColumns.PANEL_ID, false);
        return this;
    }

    public CommandSelection widgetPanelProjectId(long... value) {
        addEquals(PanelColumns.PROJECT_ID, toObjectArray(value));
        return this;
    }

    public CommandSelection widgetPanelProjectIdNot(long... value) {
        addNotEquals(PanelColumns.PROJECT_ID, toObjectArray(value));
        return this;
    }

    public CommandSelection widgetPanelProjectIdGt(long value) {
        addGreaterThan(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public CommandSelection widgetPanelProjectIdGtEq(long value) {
        addGreaterThanOrEquals(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public CommandSelection widgetPanelProjectIdLt(long value) {
        addLessThan(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public CommandSelection widgetPanelProjectIdLtEq(long value) {
        addLessThanOrEquals(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public CommandSelection orderByWidgetPanelProjectId(boolean desc) {
        orderBy(PanelColumns.PROJECT_ID, desc);
        return this;
    }

    public CommandSelection orderByWidgetPanelProjectId() {
        orderBy(PanelColumns.PROJECT_ID, false);
        return this;
    }

    public CommandSelection widgetPanelProjectName(String... value) {
        addEquals(ProjectColumns.NAME, value);
        return this;
    }

    public CommandSelection widgetPanelProjectNameNot(String... value) {
        addNotEquals(ProjectColumns.NAME, value);
        return this;
    }

    public CommandSelection widgetPanelProjectNameLike(String... value) {
        addLike(ProjectColumns.NAME, value);
        return this;
    }

    public CommandSelection widgetPanelProjectNameContains(String... value) {
        addContains(ProjectColumns.NAME, value);
        return this;
    }

    public CommandSelection widgetPanelProjectNameStartsWith(String... value) {
        addStartsWith(ProjectColumns.NAME, value);
        return this;
    }

    public CommandSelection widgetPanelProjectNameEndsWith(String... value) {
        addEndsWith(ProjectColumns.NAME, value);
        return this;
    }

    public CommandSelection orderByWidgetPanelProjectName(boolean desc) {
        orderBy(ProjectColumns.NAME, desc);
        return this;
    }

    public CommandSelection orderByWidgetPanelProjectName() {
        orderBy(ProjectColumns.NAME, false);
        return this;
    }

    public CommandSelection widgetPanelProjectToken(String... value) {
        addEquals(ProjectColumns.TOKEN, value);
        return this;
    }

    public CommandSelection widgetPanelProjectTokenNot(String... value) {
        addNotEquals(ProjectColumns.TOKEN, value);
        return this;
    }

    public CommandSelection widgetPanelProjectTokenLike(String... value) {
        addLike(ProjectColumns.TOKEN, value);
        return this;
    }

    public CommandSelection widgetPanelProjectTokenContains(String... value) {
        addContains(ProjectColumns.TOKEN, value);
        return this;
    }

    public CommandSelection widgetPanelProjectTokenStartsWith(String... value) {
        addStartsWith(ProjectColumns.TOKEN, value);
        return this;
    }

    public CommandSelection widgetPanelProjectTokenEndsWith(String... value) {
        addEndsWith(ProjectColumns.TOKEN, value);
        return this;
    }

    public CommandSelection orderByWidgetPanelProjectToken(boolean desc) {
        orderBy(ProjectColumns.TOKEN, desc);
        return this;
    }

    public CommandSelection orderByWidgetPanelProjectToken() {
        orderBy(ProjectColumns.TOKEN, false);
        return this;
    }

    public CommandSelection widgetPanelProjectPositionOnList(Integer... value) {
        addEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public CommandSelection widgetPanelProjectPositionOnListNot(Integer... value) {
        addNotEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public CommandSelection widgetPanelProjectPositionOnListGt(int value) {
        addGreaterThan(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public CommandSelection widgetPanelProjectPositionOnListGtEq(int value) {
        addGreaterThanOrEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public CommandSelection widgetPanelProjectPositionOnListLt(int value) {
        addLessThan(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public CommandSelection widgetPanelProjectPositionOnListLtEq(int value) {
        addLessThanOrEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public CommandSelection orderByWidgetPanelProjectPositionOnList(boolean desc) {
        orderBy(ProjectColumns.POSITION_ON_LIST, desc);
        return this;
    }

    public CommandSelection orderByWidgetPanelProjectPositionOnList() {
        orderBy(ProjectColumns.POSITION_ON_LIST, false);
        return this;
    }

    public CommandSelection widgetPanelCaption(String... value) {
        addEquals(PanelColumns.CAPTION, value);
        return this;
    }

    public CommandSelection widgetPanelCaptionNot(String... value) {
        addNotEquals(PanelColumns.CAPTION, value);
        return this;
    }

    public CommandSelection widgetPanelCaptionLike(String... value) {
        addLike(PanelColumns.CAPTION, value);
        return this;
    }

    public CommandSelection widgetPanelCaptionContains(String... value) {
        addContains(PanelColumns.CAPTION, value);
        return this;
    }

    public CommandSelection widgetPanelCaptionStartsWith(String... value) {
        addStartsWith(PanelColumns.CAPTION, value);
        return this;
    }

    public CommandSelection widgetPanelCaptionEndsWith(String... value) {
        addEndsWith(PanelColumns.CAPTION, value);
        return this;
    }

    public CommandSelection orderByWidgetPanelCaption(boolean desc) {
        orderBy(PanelColumns.CAPTION, desc);
        return this;
    }

    public CommandSelection orderByWidgetPanelCaption() {
        orderBy(PanelColumns.CAPTION, false);
        return this;
    }

    public CommandSelection widgetPanelPositionOnScreen(Integer... value) {
        addEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public CommandSelection widgetPanelPositionOnScreenNot(Integer... value) {
        addNotEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public CommandSelection widgetPanelPositionOnScreenGt(int value) {
        addGreaterThan(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public CommandSelection widgetPanelPositionOnScreenGtEq(int value) {
        addGreaterThanOrEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public CommandSelection widgetPanelPositionOnScreenLt(int value) {
        addLessThan(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public CommandSelection widgetPanelPositionOnScreenLtEq(int value) {
        addLessThanOrEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public CommandSelection orderByWidgetPanelPositionOnScreen(boolean desc) {
        orderBy(PanelColumns.POSITION_ON_SCREEN, desc);
        return this;
    }

    public CommandSelection orderByWidgetPanelPositionOnScreen() {
        orderBy(PanelColumns.POSITION_ON_SCREEN, false);
        return this;
    }

    public CommandSelection widgetType(WidgetType... value) {
        addEquals(WidgetColumns.TYPE, value);
        return this;
    }

    public CommandSelection widgetTypeNot(WidgetType... value) {
        addNotEquals(WidgetColumns.TYPE, value);
        return this;
    }


    public CommandSelection orderByWidgetType(boolean desc) {
        orderBy(WidgetColumns.TYPE, desc);
        return this;
    }

    public CommandSelection orderByWidgetType() {
        orderBy(WidgetColumns.TYPE, false);
        return this;
    }

    public CommandSelection cmd(String... value) {
        addEquals(CommandColumns.CMD, value);
        return this;
    }

    public CommandSelection cmdNot(String... value) {
        addNotEquals(CommandColumns.CMD, value);
        return this;
    }

    public CommandSelection cmdLike(String... value) {
        addLike(CommandColumns.CMD, value);
        return this;
    }

    public CommandSelection cmdContains(String... value) {
        addContains(CommandColumns.CMD, value);
        return this;
    }

    public CommandSelection cmdStartsWith(String... value) {
        addStartsWith(CommandColumns.CMD, value);
        return this;
    }

    public CommandSelection cmdEndsWith(String... value) {
        addEndsWith(CommandColumns.CMD, value);
        return this;
    }

    public CommandSelection orderByCmd(boolean desc) {
        orderBy(CommandColumns.CMD, desc);
        return this;
    }

    public CommandSelection orderByCmd() {
        orderBy(CommandColumns.CMD, false);
        return this;
    }
}
