package com.krio.gadgetcontroller.provider.command;

import com.krio.gadgetcontroller.provider.base.BaseModel;

import java.util.Date;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Data model for the {@code command} table.
 */
public interface CommandModel extends BaseModel {

    /**
     * Get the {@code type} value.
     * Cannot be {@code null}.
     */
    @NonNull
    CommandType getType();

    /**
     * Get the {@code widget_id} value.
     */
    long getWidgetId();

    /**
     * Get the {@code cmd} value.
     * Cannot be {@code null}.
     */
    @NonNull
    String getCmd();
}
