package com.krio.gadgetcontroller.provider.project;

import com.krio.gadgetcontroller.provider.base.BaseModel;

import java.util.Date;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Data model for the {@code project} table.
 */
public interface ProjectModel extends BaseModel {

    /**
     * Get the {@code name} value.
     * Cannot be {@code null}.
     */
    @NonNull
    String getName();

    /**
     * Get the {@code token} value.
     * Cannot be {@code null}.
     */
    @NonNull
    String getToken();

    /**
     * Get the {@code position_on_list} value.
     * Can be {@code null}.
     */
    @Nullable
    Integer getPositionOnList();
}
