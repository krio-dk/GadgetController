package com.krio.gadgetcontroller.provider.command;

import android.net.Uri;
import android.provider.BaseColumns;

import com.krio.gadgetcontroller.provider.DataProvider;
import com.krio.gadgetcontroller.provider.command.CommandColumns;
import com.krio.gadgetcontroller.provider.commandparam.CommandParamColumns;
import com.krio.gadgetcontroller.provider.panel.PanelColumns;
import com.krio.gadgetcontroller.provider.project.ProjectColumns;
import com.krio.gadgetcontroller.provider.widget.WidgetColumns;
import com.krio.gadgetcontroller.provider.widgetattr.WidgetAttrColumns;

/**
 * Columns for the {@code command} table.
 */
public class CommandColumns implements BaseColumns {
    public static final String TABLE_NAME = "command";
    public static final Uri CONTENT_URI = Uri.parse(DataProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    /**
     * Primary key.
     */
    public static final String _ID = BaseColumns._ID;

    public static final String TYPE = "command__type";

    public static final String WIDGET_ID = "widget_id";

    public static final String CMD = "cmd";


    public static final String DEFAULT_ORDER = TABLE_NAME + "." +_ID;

    // @formatter:off
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            TYPE,
            WIDGET_ID,
            CMD
    };
    // @formatter:on

    public static boolean hasColumns(String[] projection) {
        if (projection == null) return true;
        for (String c : projection) {
            if (c.equals(TYPE) || c.contains("." + TYPE)) return true;
            if (c.equals(WIDGET_ID) || c.contains("." + WIDGET_ID)) return true;
            if (c.equals(CMD) || c.contains("." + CMD)) return true;
        }
        return false;
    }

    public static final String PREFIX_WIDGET = TABLE_NAME + "__" + WidgetColumns.TABLE_NAME;
}
