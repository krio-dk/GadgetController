package com.krio.gadgetcontroller.provider.commandparam;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import com.krio.gadgetcontroller.provider.base.AbstractSelection;
import com.krio.gadgetcontroller.provider.command.*;
import com.krio.gadgetcontroller.provider.widget.*;
import com.krio.gadgetcontroller.provider.panel.*;
import com.krio.gadgetcontroller.provider.project.*;

/**
 * Selection for the {@code command_param} table.
 */
public class CommandParamSelection extends AbstractSelection<CommandParamSelection> {
    @Override
    protected Uri baseUri() {
        return CommandParamColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code CommandParamCursor} object, which is positioned before the first entry, or null.
     */
    public CommandParamCursor query(ContentResolver contentResolver, String[] projection) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new CommandParamCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, null)}.
     */
    public CommandParamCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null);
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param context The context to use for the query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code CommandParamCursor} object, which is positioned before the first entry, or null.
     */
    public CommandParamCursor query(Context context, String[] projection) {
        Cursor cursor = context.getContentResolver().query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new CommandParamCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(context, null)}.
     */
    public CommandParamCursor query(Context context) {
        return query(context, null);
    }


    public CommandParamSelection id(long... value) {
        addEquals("command_param." + CommandParamColumns._ID, toObjectArray(value));
        return this;
    }

    public CommandParamSelection idNot(long... value) {
        addNotEquals("command_param." + CommandParamColumns._ID, toObjectArray(value));
        return this;
    }

    public CommandParamSelection orderById(boolean desc) {
        orderBy("command_param." + CommandParamColumns._ID, desc);
        return this;
    }

    public CommandParamSelection orderById() {
        return orderById(false);
    }

    public CommandParamSelection name(String... value) {
        addEquals(CommandParamColumns.NAME, value);
        return this;
    }

    public CommandParamSelection nameNot(String... value) {
        addNotEquals(CommandParamColumns.NAME, value);
        return this;
    }

    public CommandParamSelection nameLike(String... value) {
        addLike(CommandParamColumns.NAME, value);
        return this;
    }

    public CommandParamSelection nameContains(String... value) {
        addContains(CommandParamColumns.NAME, value);
        return this;
    }

    public CommandParamSelection nameStartsWith(String... value) {
        addStartsWith(CommandParamColumns.NAME, value);
        return this;
    }

    public CommandParamSelection nameEndsWith(String... value) {
        addEndsWith(CommandParamColumns.NAME, value);
        return this;
    }

    public CommandParamSelection orderByName(boolean desc) {
        orderBy(CommandParamColumns.NAME, desc);
        return this;
    }

    public CommandParamSelection orderByName() {
        orderBy(CommandParamColumns.NAME, false);
        return this;
    }

    public CommandParamSelection integerParam(Integer... value) {
        addEquals(CommandParamColumns.INTEGER_PARAM, value);
        return this;
    }

    public CommandParamSelection integerParamNot(Integer... value) {
        addNotEquals(CommandParamColumns.INTEGER_PARAM, value);
        return this;
    }

    public CommandParamSelection integerParamGt(int value) {
        addGreaterThan(CommandParamColumns.INTEGER_PARAM, value);
        return this;
    }

    public CommandParamSelection integerParamGtEq(int value) {
        addGreaterThanOrEquals(CommandParamColumns.INTEGER_PARAM, value);
        return this;
    }

    public CommandParamSelection integerParamLt(int value) {
        addLessThan(CommandParamColumns.INTEGER_PARAM, value);
        return this;
    }

    public CommandParamSelection integerParamLtEq(int value) {
        addLessThanOrEquals(CommandParamColumns.INTEGER_PARAM, value);
        return this;
    }

    public CommandParamSelection orderByIntegerParam(boolean desc) {
        orderBy(CommandParamColumns.INTEGER_PARAM, desc);
        return this;
    }

    public CommandParamSelection orderByIntegerParam() {
        orderBy(CommandParamColumns.INTEGER_PARAM, false);
        return this;
    }

    public CommandParamSelection doubleParam(Double... value) {
        addEquals(CommandParamColumns.DOUBLE_PARAM, value);
        return this;
    }

    public CommandParamSelection doubleParamNot(Double... value) {
        addNotEquals(CommandParamColumns.DOUBLE_PARAM, value);
        return this;
    }

    public CommandParamSelection doubleParamGt(double value) {
        addGreaterThan(CommandParamColumns.DOUBLE_PARAM, value);
        return this;
    }

    public CommandParamSelection doubleParamGtEq(double value) {
        addGreaterThanOrEquals(CommandParamColumns.DOUBLE_PARAM, value);
        return this;
    }

    public CommandParamSelection doubleParamLt(double value) {
        addLessThan(CommandParamColumns.DOUBLE_PARAM, value);
        return this;
    }

    public CommandParamSelection doubleParamLtEq(double value) {
        addLessThanOrEquals(CommandParamColumns.DOUBLE_PARAM, value);
        return this;
    }

    public CommandParamSelection orderByDoubleParam(boolean desc) {
        orderBy(CommandParamColumns.DOUBLE_PARAM, desc);
        return this;
    }

    public CommandParamSelection orderByDoubleParam() {
        orderBy(CommandParamColumns.DOUBLE_PARAM, false);
        return this;
    }

    public CommandParamSelection stringParam(String... value) {
        addEquals(CommandParamColumns.STRING_PARAM, value);
        return this;
    }

    public CommandParamSelection stringParamNot(String... value) {
        addNotEquals(CommandParamColumns.STRING_PARAM, value);
        return this;
    }

    public CommandParamSelection stringParamLike(String... value) {
        addLike(CommandParamColumns.STRING_PARAM, value);
        return this;
    }

    public CommandParamSelection stringParamContains(String... value) {
        addContains(CommandParamColumns.STRING_PARAM, value);
        return this;
    }

    public CommandParamSelection stringParamStartsWith(String... value) {
        addStartsWith(CommandParamColumns.STRING_PARAM, value);
        return this;
    }

    public CommandParamSelection stringParamEndsWith(String... value) {
        addEndsWith(CommandParamColumns.STRING_PARAM, value);
        return this;
    }

    public CommandParamSelection orderByStringParam(boolean desc) {
        orderBy(CommandParamColumns.STRING_PARAM, desc);
        return this;
    }

    public CommandParamSelection orderByStringParam() {
        orderBy(CommandParamColumns.STRING_PARAM, false);
        return this;
    }

    public CommandParamSelection booleanParam(Boolean value) {
        addEquals(CommandParamColumns.BOOLEAN_PARAM, toObjectArray(value));
        return this;
    }

    public CommandParamSelection orderByBooleanParam(boolean desc) {
        orderBy(CommandParamColumns.BOOLEAN_PARAM, desc);
        return this;
    }

    public CommandParamSelection orderByBooleanParam() {
        orderBy(CommandParamColumns.BOOLEAN_PARAM, false);
        return this;
    }

    public CommandParamSelection dateParam(Date... value) {
        addEquals(CommandParamColumns.DATE_PARAM, value);
        return this;
    }

    public CommandParamSelection dateParamNot(Date... value) {
        addNotEquals(CommandParamColumns.DATE_PARAM, value);
        return this;
    }

    public CommandParamSelection dateParam(Long... value) {
        addEquals(CommandParamColumns.DATE_PARAM, value);
        return this;
    }

    public CommandParamSelection dateParamAfter(Date value) {
        addGreaterThan(CommandParamColumns.DATE_PARAM, value);
        return this;
    }

    public CommandParamSelection dateParamAfterEq(Date value) {
        addGreaterThanOrEquals(CommandParamColumns.DATE_PARAM, value);
        return this;
    }

    public CommandParamSelection dateParamBefore(Date value) {
        addLessThan(CommandParamColumns.DATE_PARAM, value);
        return this;
    }

    public CommandParamSelection dateParamBeforeEq(Date value) {
        addLessThanOrEquals(CommandParamColumns.DATE_PARAM, value);
        return this;
    }

    public CommandParamSelection orderByDateParam(boolean desc) {
        orderBy(CommandParamColumns.DATE_PARAM, desc);
        return this;
    }

    public CommandParamSelection orderByDateParam() {
        orderBy(CommandParamColumns.DATE_PARAM, false);
        return this;
    }

    public CommandParamSelection commandId(long... value) {
        addEquals(CommandParamColumns.COMMAND_ID, toObjectArray(value));
        return this;
    }

    public CommandParamSelection commandIdNot(long... value) {
        addNotEquals(CommandParamColumns.COMMAND_ID, toObjectArray(value));
        return this;
    }

    public CommandParamSelection commandIdGt(long value) {
        addGreaterThan(CommandParamColumns.COMMAND_ID, value);
        return this;
    }

    public CommandParamSelection commandIdGtEq(long value) {
        addGreaterThanOrEquals(CommandParamColumns.COMMAND_ID, value);
        return this;
    }

    public CommandParamSelection commandIdLt(long value) {
        addLessThan(CommandParamColumns.COMMAND_ID, value);
        return this;
    }

    public CommandParamSelection commandIdLtEq(long value) {
        addLessThanOrEquals(CommandParamColumns.COMMAND_ID, value);
        return this;
    }

    public CommandParamSelection orderByCommandId(boolean desc) {
        orderBy(CommandParamColumns.COMMAND_ID, desc);
        return this;
    }

    public CommandParamSelection orderByCommandId() {
        orderBy(CommandParamColumns.COMMAND_ID, false);
        return this;
    }

    public CommandParamSelection commandType(CommandType... value) {
        addEquals(CommandColumns.TYPE, value);
        return this;
    }

    public CommandParamSelection commandTypeNot(CommandType... value) {
        addNotEquals(CommandColumns.TYPE, value);
        return this;
    }


    public CommandParamSelection orderByCommandType(boolean desc) {
        orderBy(CommandColumns.TYPE, desc);
        return this;
    }

    public CommandParamSelection orderByCommandType() {
        orderBy(CommandColumns.TYPE, false);
        return this;
    }

    public CommandParamSelection commandWidgetId(long... value) {
        addEquals(CommandColumns.WIDGET_ID, toObjectArray(value));
        return this;
    }

    public CommandParamSelection commandWidgetIdNot(long... value) {
        addNotEquals(CommandColumns.WIDGET_ID, toObjectArray(value));
        return this;
    }

    public CommandParamSelection commandWidgetIdGt(long value) {
        addGreaterThan(CommandColumns.WIDGET_ID, value);
        return this;
    }

    public CommandParamSelection commandWidgetIdGtEq(long value) {
        addGreaterThanOrEquals(CommandColumns.WIDGET_ID, value);
        return this;
    }

    public CommandParamSelection commandWidgetIdLt(long value) {
        addLessThan(CommandColumns.WIDGET_ID, value);
        return this;
    }

    public CommandParamSelection commandWidgetIdLtEq(long value) {
        addLessThanOrEquals(CommandColumns.WIDGET_ID, value);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetId(boolean desc) {
        orderBy(CommandColumns.WIDGET_ID, desc);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetId() {
        orderBy(CommandColumns.WIDGET_ID, false);
        return this;
    }

    public CommandParamSelection commandWidgetPositionOnPanel(Integer... value) {
        addEquals(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public CommandParamSelection commandWidgetPositionOnPanelNot(Integer... value) {
        addNotEquals(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public CommandParamSelection commandWidgetPositionOnPanelGt(int value) {
        addGreaterThan(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public CommandParamSelection commandWidgetPositionOnPanelGtEq(int value) {
        addGreaterThanOrEquals(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public CommandParamSelection commandWidgetPositionOnPanelLt(int value) {
        addLessThan(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public CommandParamSelection commandWidgetPositionOnPanelLtEq(int value) {
        addLessThanOrEquals(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetPositionOnPanel(boolean desc) {
        orderBy(WidgetColumns.POSITION_ON_PANEL, desc);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetPositionOnPanel() {
        orderBy(WidgetColumns.POSITION_ON_PANEL, false);
        return this;
    }

    public CommandParamSelection commandWidgetCaption(String... value) {
        addEquals(WidgetColumns.CAPTION, value);
        return this;
    }

    public CommandParamSelection commandWidgetCaptionNot(String... value) {
        addNotEquals(WidgetColumns.CAPTION, value);
        return this;
    }

    public CommandParamSelection commandWidgetCaptionLike(String... value) {
        addLike(WidgetColumns.CAPTION, value);
        return this;
    }

    public CommandParamSelection commandWidgetCaptionContains(String... value) {
        addContains(WidgetColumns.CAPTION, value);
        return this;
    }

    public CommandParamSelection commandWidgetCaptionStartsWith(String... value) {
        addStartsWith(WidgetColumns.CAPTION, value);
        return this;
    }

    public CommandParamSelection commandWidgetCaptionEndsWith(String... value) {
        addEndsWith(WidgetColumns.CAPTION, value);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetCaption(boolean desc) {
        orderBy(WidgetColumns.CAPTION, desc);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetCaption() {
        orderBy(WidgetColumns.CAPTION, false);
        return this;
    }

    public CommandParamSelection commandWidgetPanelId(long... value) {
        addEquals(WidgetColumns.PANEL_ID, toObjectArray(value));
        return this;
    }

    public CommandParamSelection commandWidgetPanelIdNot(long... value) {
        addNotEquals(WidgetColumns.PANEL_ID, toObjectArray(value));
        return this;
    }

    public CommandParamSelection commandWidgetPanelIdGt(long value) {
        addGreaterThan(WidgetColumns.PANEL_ID, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelIdGtEq(long value) {
        addGreaterThanOrEquals(WidgetColumns.PANEL_ID, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelIdLt(long value) {
        addLessThan(WidgetColumns.PANEL_ID, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelIdLtEq(long value) {
        addLessThanOrEquals(WidgetColumns.PANEL_ID, value);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetPanelId(boolean desc) {
        orderBy(WidgetColumns.PANEL_ID, desc);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetPanelId() {
        orderBy(WidgetColumns.PANEL_ID, false);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectId(long... value) {
        addEquals(PanelColumns.PROJECT_ID, toObjectArray(value));
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectIdNot(long... value) {
        addNotEquals(PanelColumns.PROJECT_ID, toObjectArray(value));
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectIdGt(long value) {
        addGreaterThan(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectIdGtEq(long value) {
        addGreaterThanOrEquals(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectIdLt(long value) {
        addLessThan(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectIdLtEq(long value) {
        addLessThanOrEquals(PanelColumns.PROJECT_ID, value);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetPanelProjectId(boolean desc) {
        orderBy(PanelColumns.PROJECT_ID, desc);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetPanelProjectId() {
        orderBy(PanelColumns.PROJECT_ID, false);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectName(String... value) {
        addEquals(ProjectColumns.NAME, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectNameNot(String... value) {
        addNotEquals(ProjectColumns.NAME, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectNameLike(String... value) {
        addLike(ProjectColumns.NAME, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectNameContains(String... value) {
        addContains(ProjectColumns.NAME, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectNameStartsWith(String... value) {
        addStartsWith(ProjectColumns.NAME, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectNameEndsWith(String... value) {
        addEndsWith(ProjectColumns.NAME, value);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetPanelProjectName(boolean desc) {
        orderBy(ProjectColumns.NAME, desc);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetPanelProjectName() {
        orderBy(ProjectColumns.NAME, false);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectToken(String... value) {
        addEquals(ProjectColumns.TOKEN, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectTokenNot(String... value) {
        addNotEquals(ProjectColumns.TOKEN, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectTokenLike(String... value) {
        addLike(ProjectColumns.TOKEN, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectTokenContains(String... value) {
        addContains(ProjectColumns.TOKEN, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectTokenStartsWith(String... value) {
        addStartsWith(ProjectColumns.TOKEN, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectTokenEndsWith(String... value) {
        addEndsWith(ProjectColumns.TOKEN, value);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetPanelProjectToken(boolean desc) {
        orderBy(ProjectColumns.TOKEN, desc);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetPanelProjectToken() {
        orderBy(ProjectColumns.TOKEN, false);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectPositionOnList(Integer... value) {
        addEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectPositionOnListNot(Integer... value) {
        addNotEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectPositionOnListGt(int value) {
        addGreaterThan(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectPositionOnListGtEq(int value) {
        addGreaterThanOrEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectPositionOnListLt(int value) {
        addLessThan(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelProjectPositionOnListLtEq(int value) {
        addLessThanOrEquals(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetPanelProjectPositionOnList(boolean desc) {
        orderBy(ProjectColumns.POSITION_ON_LIST, desc);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetPanelProjectPositionOnList() {
        orderBy(ProjectColumns.POSITION_ON_LIST, false);
        return this;
    }

    public CommandParamSelection commandWidgetPanelCaption(String... value) {
        addEquals(PanelColumns.CAPTION, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelCaptionNot(String... value) {
        addNotEquals(PanelColumns.CAPTION, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelCaptionLike(String... value) {
        addLike(PanelColumns.CAPTION, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelCaptionContains(String... value) {
        addContains(PanelColumns.CAPTION, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelCaptionStartsWith(String... value) {
        addStartsWith(PanelColumns.CAPTION, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelCaptionEndsWith(String... value) {
        addEndsWith(PanelColumns.CAPTION, value);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetPanelCaption(boolean desc) {
        orderBy(PanelColumns.CAPTION, desc);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetPanelCaption() {
        orderBy(PanelColumns.CAPTION, false);
        return this;
    }

    public CommandParamSelection commandWidgetPanelPositionOnScreen(Integer... value) {
        addEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelPositionOnScreenNot(Integer... value) {
        addNotEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelPositionOnScreenGt(int value) {
        addGreaterThan(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelPositionOnScreenGtEq(int value) {
        addGreaterThanOrEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelPositionOnScreenLt(int value) {
        addLessThan(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public CommandParamSelection commandWidgetPanelPositionOnScreenLtEq(int value) {
        addLessThanOrEquals(PanelColumns.POSITION_ON_SCREEN, value);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetPanelPositionOnScreen(boolean desc) {
        orderBy(PanelColumns.POSITION_ON_SCREEN, desc);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetPanelPositionOnScreen() {
        orderBy(PanelColumns.POSITION_ON_SCREEN, false);
        return this;
    }

    public CommandParamSelection commandWidgetType(WidgetType... value) {
        addEquals(WidgetColumns.TYPE, value);
        return this;
    }

    public CommandParamSelection commandWidgetTypeNot(WidgetType... value) {
        addNotEquals(WidgetColumns.TYPE, value);
        return this;
    }


    public CommandParamSelection orderByCommandWidgetType(boolean desc) {
        orderBy(WidgetColumns.TYPE, desc);
        return this;
    }

    public CommandParamSelection orderByCommandWidgetType() {
        orderBy(WidgetColumns.TYPE, false);
        return this;
    }

    public CommandParamSelection commandCmd(String... value) {
        addEquals(CommandColumns.CMD, value);
        return this;
    }

    public CommandParamSelection commandCmdNot(String... value) {
        addNotEquals(CommandColumns.CMD, value);
        return this;
    }

    public CommandParamSelection commandCmdLike(String... value) {
        addLike(CommandColumns.CMD, value);
        return this;
    }

    public CommandParamSelection commandCmdContains(String... value) {
        addContains(CommandColumns.CMD, value);
        return this;
    }

    public CommandParamSelection commandCmdStartsWith(String... value) {
        addStartsWith(CommandColumns.CMD, value);
        return this;
    }

    public CommandParamSelection commandCmdEndsWith(String... value) {
        addEndsWith(CommandColumns.CMD, value);
        return this;
    }

    public CommandParamSelection orderByCommandCmd(boolean desc) {
        orderBy(CommandColumns.CMD, desc);
        return this;
    }

    public CommandParamSelection orderByCommandCmd() {
        orderBy(CommandColumns.CMD, false);
        return this;
    }

    public CommandParamSelection type(CommandParamType... value) {
        addEquals(CommandParamColumns.TYPE, value);
        return this;
    }

    public CommandParamSelection typeNot(CommandParamType... value) {
        addNotEquals(CommandParamColumns.TYPE, value);
        return this;
    }


    public CommandParamSelection orderByType(boolean desc) {
        orderBy(CommandParamColumns.TYPE, desc);
        return this;
    }

    public CommandParamSelection orderByType() {
        orderBy(CommandParamColumns.TYPE, false);
        return this;
    }
}
