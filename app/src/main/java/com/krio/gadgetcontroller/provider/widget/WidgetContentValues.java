package com.krio.gadgetcontroller.provider.widget;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.krio.gadgetcontroller.provider.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code widget} table.
 */
public class WidgetContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return WidgetColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, @Nullable WidgetSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(Context context, @Nullable WidgetSelection where) {
        return context.getContentResolver().update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public WidgetContentValues putPositionOnPanel(@Nullable Integer value) {
        mContentValues.put(WidgetColumns.POSITION_ON_PANEL, value);
        return this;
    }

    public WidgetContentValues putPositionOnPanelNull() {
        mContentValues.putNull(WidgetColumns.POSITION_ON_PANEL);
        return this;
    }

    public WidgetContentValues putCaption(@Nullable String value) {
        mContentValues.put(WidgetColumns.CAPTION, value);
        return this;
    }

    public WidgetContentValues putCaptionNull() {
        mContentValues.putNull(WidgetColumns.CAPTION);
        return this;
    }

    public WidgetContentValues putPanelId(long value) {
        mContentValues.put(WidgetColumns.PANEL_ID, value);
        return this;
    }


    public WidgetContentValues putType(@NonNull WidgetType value) {
        if (value == null) throw new IllegalArgumentException("type must not be null");
        mContentValues.put(WidgetColumns.TYPE, value.ordinal());
        return this;
    }

}
