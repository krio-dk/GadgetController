package com.krio.gadgetcontroller.provider.panel;

import java.util.Date;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.krio.gadgetcontroller.provider.base.AbstractCursor;
import com.krio.gadgetcontroller.provider.project.*;

/**
 * Cursor wrapper for the {@code panel} table.
 */
public class PanelCursor extends AbstractCursor implements PanelModel {
    public PanelCursor(Cursor cursor) {
        super(cursor);
    }

    /**
     * Primary key.
     */
    public long getId() {
        Long res = getLongOrNull(PanelColumns._ID);
        if (res == null)
            throw new NullPointerException("The value of '_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code project_id} value.
     */
    public long getProjectId() {
        Long res = getLongOrNull(PanelColumns.PROJECT_ID);
        if (res == null)
            throw new NullPointerException("The value of 'project_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code name} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getProjectName() {
        String res = getStringOrNull(ProjectColumns.NAME);
        if (res == null)
            throw new NullPointerException("The value of 'name' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code token} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getProjectToken() {
        String res = getStringOrNull(ProjectColumns.TOKEN);
        if (res == null)
            throw new NullPointerException("The value of 'token' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code position_on_list} value.
     * Can be {@code null}.
     */
    @Nullable
    public Integer getProjectPositionOnList() {
        Integer res = getIntegerOrNull(ProjectColumns.POSITION_ON_LIST);
        return res;
    }

    /**
     * Get the {@code caption} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getCaption() {
        String res = getStringOrNull(PanelColumns.CAPTION);
        if (res == null)
            throw new NullPointerException("The value of 'caption' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code position_on_screen} value.
     * Can be {@code null}.
     */
    @Nullable
    public Integer getPositionOnScreen() {
        Integer res = getIntegerOrNull(PanelColumns.POSITION_ON_SCREEN);
        return res;
    }
}
