package com.krio.gadgetcontroller.provider.panel;

import android.net.Uri;
import android.provider.BaseColumns;

import com.krio.gadgetcontroller.provider.DataProvider;
import com.krio.gadgetcontroller.provider.command.CommandColumns;
import com.krio.gadgetcontroller.provider.commandparam.CommandParamColumns;
import com.krio.gadgetcontroller.provider.panel.PanelColumns;
import com.krio.gadgetcontroller.provider.project.ProjectColumns;
import com.krio.gadgetcontroller.provider.widget.WidgetColumns;
import com.krio.gadgetcontroller.provider.widgetattr.WidgetAttrColumns;

/**
 * Columns for the {@code panel} table.
 */
public class PanelColumns implements BaseColumns {
    public static final String TABLE_NAME = "panel";
    public static final Uri CONTENT_URI = Uri.parse(DataProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    /**
     * Primary key.
     */
    public static final String _ID = BaseColumns._ID;

    public static final String PROJECT_ID = "project_id";

    public static final String CAPTION = "panel__caption";

    public static final String POSITION_ON_SCREEN = "position_on_screen";


    public static final String DEFAULT_ORDER = TABLE_NAME + "." +_ID;

    // @formatter:off
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            PROJECT_ID,
            CAPTION,
            POSITION_ON_SCREEN
    };
    // @formatter:on

    public static boolean hasColumns(String[] projection) {
        if (projection == null) return true;
        for (String c : projection) {
            if (c.equals(PROJECT_ID) || c.contains("." + PROJECT_ID)) return true;
            if (c.equals(CAPTION) || c.contains("." + CAPTION)) return true;
            if (c.equals(POSITION_ON_SCREEN) || c.contains("." + POSITION_ON_SCREEN)) return true;
        }
        return false;
    }

    public static final String PREFIX_PROJECT = TABLE_NAME + "__" + ProjectColumns.TABLE_NAME;
}
