package com.krio.gadgetcontroller.provider.command;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.krio.gadgetcontroller.provider.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code command} table.
 */
public class CommandContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return CommandColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, @Nullable CommandSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(Context context, @Nullable CommandSelection where) {
        return context.getContentResolver().update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public CommandContentValues putType(@NonNull CommandType value) {
        if (value == null) throw new IllegalArgumentException("type must not be null");
        mContentValues.put(CommandColumns.TYPE, value.ordinal());
        return this;
    }


    public CommandContentValues putWidgetId(long value) {
        mContentValues.put(CommandColumns.WIDGET_ID, value);
        return this;
    }


    public CommandContentValues putCmd(@NonNull String value) {
        if (value == null) throw new IllegalArgumentException("cmd must not be null");
        mContentValues.put(CommandColumns.CMD, value);
        return this;
    }

}
