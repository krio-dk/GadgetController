package com.krio.gadgetcontroller.provider.project;

import android.net.Uri;
import android.provider.BaseColumns;

import com.krio.gadgetcontroller.provider.DataProvider;
import com.krio.gadgetcontroller.provider.command.CommandColumns;
import com.krio.gadgetcontroller.provider.commandparam.CommandParamColumns;
import com.krio.gadgetcontroller.provider.panel.PanelColumns;
import com.krio.gadgetcontroller.provider.project.ProjectColumns;
import com.krio.gadgetcontroller.provider.widget.WidgetColumns;
import com.krio.gadgetcontroller.provider.widgetattr.WidgetAttrColumns;

/**
 * Columns for the {@code project} table.
 */
public class ProjectColumns implements BaseColumns {
    public static final String TABLE_NAME = "project";
    public static final Uri CONTENT_URI = Uri.parse(DataProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    /**
     * Primary key.
     */
    public static final String _ID = BaseColumns._ID;

    public static final String NAME = "project__name";

    public static final String TOKEN = "token";

    public static final String POSITION_ON_LIST = "position_on_list";


    public static final String DEFAULT_ORDER = TABLE_NAME + "." +_ID;

    // @formatter:off
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            NAME,
            TOKEN,
            POSITION_ON_LIST
    };
    // @formatter:on

    public static boolean hasColumns(String[] projection) {
        if (projection == null) return true;
        for (String c : projection) {
            if (c.equals(NAME) || c.contains("." + NAME)) return true;
            if (c.equals(TOKEN) || c.contains("." + TOKEN)) return true;
            if (c.equals(POSITION_ON_LIST) || c.contains("." + POSITION_ON_LIST)) return true;
        }
        return false;
    }

}
