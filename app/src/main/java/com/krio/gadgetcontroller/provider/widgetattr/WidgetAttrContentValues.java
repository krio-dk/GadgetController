package com.krio.gadgetcontroller.provider.widgetattr;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.krio.gadgetcontroller.provider.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code widget_attr} table.
 */
public class WidgetAttrContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return WidgetAttrColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, @Nullable WidgetAttrSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(Context context, @Nullable WidgetAttrSelection where) {
        return context.getContentResolver().update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public WidgetAttrContentValues putName(@NonNull String value) {
        if (value == null) throw new IllegalArgumentException("name must not be null");
        mContentValues.put(WidgetAttrColumns.NAME, value);
        return this;
    }


    public WidgetAttrContentValues putIntegerAttr(@Nullable Integer value) {
        mContentValues.put(WidgetAttrColumns.INTEGER_ATTR, value);
        return this;
    }

    public WidgetAttrContentValues putIntegerAttrNull() {
        mContentValues.putNull(WidgetAttrColumns.INTEGER_ATTR);
        return this;
    }

    public WidgetAttrContentValues putDoubleAttr(@Nullable Double value) {
        mContentValues.put(WidgetAttrColumns.DOUBLE_ATTR, value);
        return this;
    }

    public WidgetAttrContentValues putDoubleAttrNull() {
        mContentValues.putNull(WidgetAttrColumns.DOUBLE_ATTR);
        return this;
    }

    public WidgetAttrContentValues putStringAttr(@Nullable String value) {
        mContentValues.put(WidgetAttrColumns.STRING_ATTR, value);
        return this;
    }

    public WidgetAttrContentValues putStringAttrNull() {
        mContentValues.putNull(WidgetAttrColumns.STRING_ATTR);
        return this;
    }

    public WidgetAttrContentValues putBooleanAttr(@Nullable Boolean value) {
        mContentValues.put(WidgetAttrColumns.BOOLEAN_ATTR, value);
        return this;
    }

    public WidgetAttrContentValues putBooleanAttrNull() {
        mContentValues.putNull(WidgetAttrColumns.BOOLEAN_ATTR);
        return this;
    }

    public WidgetAttrContentValues putDateAttr(@Nullable Date value) {
        mContentValues.put(WidgetAttrColumns.DATE_ATTR, value == null ? null : value.getTime());
        return this;
    }

    public WidgetAttrContentValues putDateAttrNull() {
        mContentValues.putNull(WidgetAttrColumns.DATE_ATTR);
        return this;
    }

    public WidgetAttrContentValues putDateAttr(@Nullable Long value) {
        mContentValues.put(WidgetAttrColumns.DATE_ATTR, value);
        return this;
    }

    public WidgetAttrContentValues putWidgetId(long value) {
        mContentValues.put(WidgetAttrColumns.WIDGET_ID, value);
        return this;
    }


    public WidgetAttrContentValues putType(@NonNull WidgetAttrType value) {
        if (value == null) throw new IllegalArgumentException("type must not be null");
        mContentValues.put(WidgetAttrColumns.TYPE, value.ordinal());
        return this;
    }

}
