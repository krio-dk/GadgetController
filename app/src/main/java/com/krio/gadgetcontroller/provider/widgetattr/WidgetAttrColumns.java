package com.krio.gadgetcontroller.provider.widgetattr;

import android.net.Uri;
import android.provider.BaseColumns;

import com.krio.gadgetcontroller.provider.DataProvider;
import com.krio.gadgetcontroller.provider.command.CommandColumns;
import com.krio.gadgetcontroller.provider.commandparam.CommandParamColumns;
import com.krio.gadgetcontroller.provider.panel.PanelColumns;
import com.krio.gadgetcontroller.provider.project.ProjectColumns;
import com.krio.gadgetcontroller.provider.widget.WidgetColumns;
import com.krio.gadgetcontroller.provider.widgetattr.WidgetAttrColumns;

/**
 * Columns for the {@code widget_attr} table.
 */
public class WidgetAttrColumns implements BaseColumns {
    public static final String TABLE_NAME = "widget_attr";
    public static final Uri CONTENT_URI = Uri.parse(DataProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    /**
     * Primary key.
     */
    public static final String _ID = BaseColumns._ID;

    public static final String NAME = "widget_attr__name";

    public static final String INTEGER_ATTR = "integer_attr";

    public static final String DOUBLE_ATTR = "double_attr";

    public static final String STRING_ATTR = "string_attr";

    public static final String BOOLEAN_ATTR = "boolean_attr";

    public static final String DATE_ATTR = "date_attr";

    public static final String WIDGET_ID = "widget_id";

    public static final String TYPE = "widget_attr__type";


    public static final String DEFAULT_ORDER = TABLE_NAME + "." +_ID;

    // @formatter:off
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            NAME,
            INTEGER_ATTR,
            DOUBLE_ATTR,
            STRING_ATTR,
            BOOLEAN_ATTR,
            DATE_ATTR,
            WIDGET_ID,
            TYPE
    };
    // @formatter:on

    public static boolean hasColumns(String[] projection) {
        if (projection == null) return true;
        for (String c : projection) {
            if (c.equals(NAME) || c.contains("." + NAME)) return true;
            if (c.equals(INTEGER_ATTR) || c.contains("." + INTEGER_ATTR)) return true;
            if (c.equals(DOUBLE_ATTR) || c.contains("." + DOUBLE_ATTR)) return true;
            if (c.equals(STRING_ATTR) || c.contains("." + STRING_ATTR)) return true;
            if (c.equals(BOOLEAN_ATTR) || c.contains("." + BOOLEAN_ATTR)) return true;
            if (c.equals(DATE_ATTR) || c.contains("." + DATE_ATTR)) return true;
            if (c.equals(WIDGET_ID) || c.contains("." + WIDGET_ID)) return true;
            if (c.equals(TYPE) || c.contains("." + TYPE)) return true;
        }
        return false;
    }

    public static final String PREFIX_WIDGET = TABLE_NAME + "__" + WidgetColumns.TABLE_NAME;
}
