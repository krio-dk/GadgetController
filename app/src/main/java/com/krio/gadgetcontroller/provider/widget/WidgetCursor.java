package com.krio.gadgetcontroller.provider.widget;

import java.util.Date;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.krio.gadgetcontroller.provider.base.AbstractCursor;
import com.krio.gadgetcontroller.provider.panel.*;
import com.krio.gadgetcontroller.provider.project.*;

/**
 * Cursor wrapper for the {@code widget} table.
 */
public class WidgetCursor extends AbstractCursor implements WidgetModel {
    public WidgetCursor(Cursor cursor) {
        super(cursor);
    }

    /**
     * Primary key.
     */
    public long getId() {
        Long res = getLongOrNull(WidgetColumns._ID);
        if (res == null)
            throw new NullPointerException("The value of '_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code position_on_panel} value.
     * Can be {@code null}.
     */
    @Nullable
    public Integer getPositionOnPanel() {
        Integer res = getIntegerOrNull(WidgetColumns.POSITION_ON_PANEL);
        return res;
    }

    /**
     * Get the {@code caption} value.
     * Can be {@code null}.
     */
    @Nullable
    public String getCaption() {
        String res = getStringOrNull(WidgetColumns.CAPTION);
        return res;
    }

    /**
     * Get the {@code panel_id} value.
     */
    public long getPanelId() {
        Long res = getLongOrNull(WidgetColumns.PANEL_ID);
        if (res == null)
            throw new NullPointerException("The value of 'panel_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code project_id} value.
     */
    public long getPanelProjectId() {
        Long res = getLongOrNull(PanelColumns.PROJECT_ID);
        if (res == null)
            throw new NullPointerException("The value of 'project_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code name} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getPanelProjectName() {
        String res = getStringOrNull(ProjectColumns.NAME);
        if (res == null)
            throw new NullPointerException("The value of 'name' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code token} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getPanelProjectToken() {
        String res = getStringOrNull(ProjectColumns.TOKEN);
        if (res == null)
            throw new NullPointerException("The value of 'token' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code position_on_list} value.
     * Can be {@code null}.
     */
    @Nullable
    public Integer getPanelProjectPositionOnList() {
        Integer res = getIntegerOrNull(ProjectColumns.POSITION_ON_LIST);
        return res;
    }

    /**
     * Get the {@code caption} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getPanelCaption() {
        String res = getStringOrNull(PanelColumns.CAPTION);
        if (res == null)
            throw new NullPointerException("The value of 'caption' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code position_on_screen} value.
     * Can be {@code null}.
     */
    @Nullable
    public Integer getPanelPositionOnScreen() {
        Integer res = getIntegerOrNull(PanelColumns.POSITION_ON_SCREEN);
        return res;
    }

    /**
     * Get the {@code type} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public WidgetType getType() {
        Integer intValue = getIntegerOrNull(WidgetColumns.TYPE);
        if (intValue == null)
            throw new NullPointerException("The value of 'type' in the database was null, which is not allowed according to the model definition");
        return WidgetType.values()[intValue];
    }
}
