package com.krio.gadgetcontroller.provider.project;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.krio.gadgetcontroller.provider.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code project} table.
 */
public class ProjectContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return ProjectColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, @Nullable ProjectSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(Context context, @Nullable ProjectSelection where) {
        return context.getContentResolver().update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public ProjectContentValues putName(@NonNull String value) {
        if (value == null) throw new IllegalArgumentException("name must not be null");
        mContentValues.put(ProjectColumns.NAME, value);
        return this;
    }


    public ProjectContentValues putToken(@NonNull String value) {
        if (value == null) throw new IllegalArgumentException("token must not be null");
        mContentValues.put(ProjectColumns.TOKEN, value);
        return this;
    }


    public ProjectContentValues putPositionOnList(@Nullable Integer value) {
        mContentValues.put(ProjectColumns.POSITION_ON_LIST, value);
        return this;
    }

    public ProjectContentValues putPositionOnListNull() {
        mContentValues.putNull(ProjectColumns.POSITION_ON_LIST);
        return this;
    }
}
