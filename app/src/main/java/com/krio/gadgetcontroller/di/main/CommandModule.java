package com.krio.gadgetcontroller.di.main;

import android.support.annotation.NonNull;

import com.krio.gadgetcontroller.logic.command.CommandValidator;
import com.krio.gadgetcontroller.logic.command.InputCommandParser;
import com.krio.gadgetcontroller.logic.project.Project;

import dagger.Module;
import dagger.Provides;

@Module
public class CommandModule {

    @Provides
    @NonNull
    @MainScope
    public InputCommandParser provideInputCommandParser (Project project) {
        return new InputCommandParser(project.getInputCommandMap());
    }

    @Provides
    @NonNull
    @MainScope
    public CommandValidator provideCommandValidator (Project project) {
        return new CommandValidator(project);
    }
}
