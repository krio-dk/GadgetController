package com.krio.gadgetcontroller.di.main;

import android.content.Context;
import android.support.annotation.NonNull;

import com.krio.gadgetcontroller.logic.command.InputCommandParser;
import com.krio.gadgetcontroller.logic.connection.core.Connection;
import com.krio.gadgetcontroller.logic.connection.core.ConnectionWrapper;
import com.krio.gadgetcontroller.logic.connection.core.NoConnection;
import com.krio.gadgetcontroller.logic.connection.events.ConnectionEventHandler;
import com.krio.gadgetcontroller.logic.connection.events.ConnectionEventListener;
import com.krio.gadgetcontroller.logic.connection.events.ConnectionEventManager;
import com.krio.gadgetcontroller.logic.connection.log.ConnectionLog;

import dagger.Module;
import dagger.Provides;

@Module
public class ConnectionModule {


    @Provides
    @NonNull
    @MainScope
    public ConnectionEventHandler provideEventHandler(InputCommandParser inputCommandParser, ConnectionLog connectionLog) {
        return new ConnectionEventHandler(inputCommandParser, connectionLog);
    }

    @Provides
    @NonNull
    @MainScope
    public ConnectionEventListener provideEventRedirector(ConnectionEventHandler handler) {
        return new ConnectionEventManager(handler);
    }

    @Provides
    @NonNull
    @MainScope
    public ConnectionWrapper provideConnectionWrapper (ConnectionEventListener eventRedirector) {
        Connection connection = new NoConnection(eventRedirector);
        return new ConnectionWrapper(connection);
    }

    @Provides
    @NonNull
    @MainScope
    public ConnectionLog provideConnectionLog(Context context) {
        return new ConnectionLog(context);
    }
}
