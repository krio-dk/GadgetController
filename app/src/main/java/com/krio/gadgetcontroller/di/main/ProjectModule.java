package com.krio.gadgetcontroller.di.main;

import android.content.Context;
import android.support.annotation.NonNull;

import com.krio.gadgetcontroller.logic.project.Project;
import com.krio.gadgetcontroller.logic.project.ProjectCreator;
import com.krio.gadgetcontroller.logic.project.ProjectModeWrapper;
import com.krio.gadgetcontroller.logic.project.ProjectSimpleLoader;
import com.krio.gadgetcontroller.logic.project.ProjectSimpleSynchronizer;

import dagger.Module;
import dagger.Provides;

@Module
public class ProjectModule {

    private long projectId;

    public ProjectModule(long projectId) {
        this.projectId = projectId;
    }

    @Provides
    @NonNull
    @MainScope
    public Project provideProject(Project.ProjectLoader projectLoader, Project.DataBaseSynchronizer synchronizer) {
        ProjectCreator projectCreator = new ProjectCreator(projectLoader, synchronizer);
        return projectCreator.createProject(projectId);
    }

    @Provides
    @NonNull
    @MainScope
    Project.ProjectLoader provideProjectLoader(Context context) {
        return new ProjectSimpleLoader(context, false);
    }

    @Provides
    @NonNull
    @MainScope
    Project.DataBaseSynchronizer provideDataBaseSynchronizer(Context context) {
        return new ProjectSimpleSynchronizer(context);
    }

    @Provides
    @NonNull
    @MainScope
    public ProjectModeWrapper provideProjectModeWrapper() {
        return new ProjectModeWrapper();
    }

}
