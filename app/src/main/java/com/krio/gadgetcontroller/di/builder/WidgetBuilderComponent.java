package com.krio.gadgetcontroller.di.builder;

import com.krio.gadgetcontroller.logic.widgetbuilder.WidgetBuilder;

import dagger.Component;

@Component(modules = WidgetBuilderModule.class)
@WidgetBuilderScope
public interface WidgetBuilderComponent {
    WidgetBuilder getWidgetBuilder();
}
