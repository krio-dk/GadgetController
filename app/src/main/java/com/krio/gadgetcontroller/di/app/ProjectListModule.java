package com.krio.gadgetcontroller.di.app;

import android.content.Context;
import android.support.annotation.NonNull;

import com.krio.gadgetcontroller.logic.project.Project;
import com.krio.gadgetcontroller.logic.project.ProjectCreator;
import com.krio.gadgetcontroller.logic.project.ProjectSimpleLoader;
import com.krio.gadgetcontroller.logic.projectmanager.ProjectManager;
import com.krio.gadgetcontroller.logic.projectmanager.ProjectManagerSimpleLoader;
import com.krio.gadgetcontroller.logic.projectmanager.ProjectManagerSimpleSynchronizer;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ProjectListModule {

    @Provides
    @NonNull
    @Singleton
    ProjectManager provideProjectManager(Context context, ProjectManager.ProjectManagerLoader projectManagerLoader, ProjectManager.DataBaseSynchronizer synchronizer) {
        Project.ProjectLoader projectLoader = new ProjectSimpleLoader(context, true);
        ProjectCreator projectCreator = new ProjectCreator(projectLoader);

        ProjectManager projectManager = new ProjectManager(synchronizer, projectCreator);
        projectManagerLoader.load(projectManager);

        return projectManager;
    }

    @Provides
    @NonNull
    @Singleton
    ProjectManager.ProjectManagerLoader provideProjectListLoader(Context context) {
        return new ProjectManagerSimpleLoader(context);
    }

    @Provides
    @NonNull
    @Singleton
    ProjectManager.DataBaseSynchronizer provideDataBaseSynchronizer(Context context) {
        return new ProjectManagerSimpleSynchronizer(context);
    }
}
