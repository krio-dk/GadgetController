package com.krio.gadgetcontroller.di.main;

import com.krio.gadgetcontroller.logic.command.CommandValidator;
import com.krio.gadgetcontroller.logic.connection.core.ConnectPerformer;
import com.krio.gadgetcontroller.logic.connection.core.ConnectionWrapper;
import com.krio.gadgetcontroller.logic.connection.events.ConnectionEventListener;
import com.krio.gadgetcontroller.logic.connection.log.ConnectionLog;
import com.krio.gadgetcontroller.logic.project.ProjectModeWrapper;
import com.krio.gadgetcontroller.ui.activity.ComponentAddActivity;
import com.krio.gadgetcontroller.ui.activity.ComponentEditActivity;
import com.krio.gadgetcontroller.ui.activity.ProjectActivity;
import com.krio.gadgetcontroller.ui.fragment.CommandListFragment;
import com.krio.gadgetcontroller.ui.fragment.PanelFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {ProjectModule.class, ConnectionModule.class, CommandModule.class})
@MainScope
public interface MainComponent {

    void inject(ProjectActivity activity);

    void inject(ComponentAddActivity activity);

    void inject(ComponentEditActivity activity);

    void inject(PanelFragment fragment);

    void inject(CommandListFragment fragment);

    void inject(ConnectPerformer connectPerformer);

    ConnectionWrapper getConnectionWrapper();

    ConnectionEventListener getConnectionEventListener();

    ConnectionLog getConnectionLog();

    ProjectModeWrapper getProjectModeWrapper();

    CommandValidator getCommandValidator();
}
