package com.krio.gadgetcontroller.di.builder;

import android.support.annotation.NonNull;

import com.krio.gadgetcontroller.logic.widgetbuilder.WidgetBuilder;

import dagger.Module;
import dagger.Provides;

@Module
public class WidgetBuilderModule {

    WidgetBuilder widgetBuilder;

    public WidgetBuilderModule(@NonNull WidgetBuilder widgetBuilder) {
        this.widgetBuilder = widgetBuilder;
    }

    @Provides
    @NonNull
    @WidgetBuilderScope
    WidgetBuilder provideWidgetBuilder() {
        return widgetBuilder;
    }
}
