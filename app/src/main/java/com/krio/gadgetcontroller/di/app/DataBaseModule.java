package com.krio.gadgetcontroller.di.app;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

import com.krio.gadgetcontroller.provider.DataSQLiteOpenHelper;
import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.schedulers.Schedulers;

@Module
public class DataBaseModule {

    @Provides
    @Singleton
    SQLiteOpenHelper provideOpenHelper(Context context) {
        return DataSQLiteOpenHelper.getInstance(context);
    }

    @Provides
    @Singleton
    SqlBrite provideSqlBrite() {
        return SqlBrite.create();
    }

    @Provides @Singleton
    BriteDatabase provideDatabase(SqlBrite sqlBrite, SQLiteOpenHelper helper) {
        BriteDatabase db = sqlBrite.wrapDatabaseHelper(helper, Schedulers.io());
        db.setLoggingEnabled(true);
        return db;
    }
}
