package com.krio.gadgetcontroller.di.app;

import android.content.Context;

import com.krio.gadgetcontroller.di.main.CommandModule;
import com.krio.gadgetcontroller.di.main.ConnectionModule;
import com.krio.gadgetcontroller.di.main.MainComponent;
import com.krio.gadgetcontroller.di.main.ProjectModule;
import com.krio.gadgetcontroller.logic.projectmanager.ProjectManager;
import com.squareup.sqlbrite.BriteDatabase;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {AppModule.class, ProjectListModule.class})
@Singleton
public interface AppComponent {
    Context getContext();
    BriteDatabase getDataBase();
    ProjectManager getProjectManager();
    MainComponent plusProjectComponent(ProjectModule projectModule, ConnectionModule connectionModule, CommandModule commandModule);
}
