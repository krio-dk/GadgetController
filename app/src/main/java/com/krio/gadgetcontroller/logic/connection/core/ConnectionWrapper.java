package com.krio.gadgetcontroller.logic.connection.core;

import android.util.Log;

/**
 * Created by krio on 03.05.16.
 */
public class ConnectionWrapper {

    Connection connection;

    public ConnectionWrapper(Connection connection) {
        Log.d("krio", "new ConnectionWrapper");
        this.connection = connection;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
