package com.krio.gadgetcontroller.logic.widgetbuilder;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.logic.command.InputCommand;
import com.krio.gadgetcontroller.logic.widget.WDisplay;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;

import java.util.Map;

/**
 * Created by krio on 16.04.16.
 */
public class WDisplayBuilder extends WidgetBuilder {

    public WDisplayBuilder() {
        widgetType = WidgetType.DISPLAY;
    }

    @Override
    public boolean createConcreteWidget() {
        widget = new WDisplay();
        return true;
    }

    @Override
    public Command createCommand(CommandType type, String cmd, Map<String, Object> params) {

        if (type != CommandType.COMMAND_DISPLAY) {
            return null;
        }

        if (widget == null) {
            createWidget();
        }

        InputCommand command = new InputCommand(widget, type, cmd);
        inputCommandList.add(command);
        return command;
    }

}
