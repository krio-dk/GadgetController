package com.krio.gadgetcontroller.logic.projectmanager;

import android.content.Context;
import android.util.Log;

import com.krio.gadgetcontroller.logic.project.Project;
import com.krio.gadgetcontroller.provider.project.ProjectCursor;
import com.krio.gadgetcontroller.provider.project.ProjectSelection;

/**
 * Created by krio on 02.06.16.
 */
public class ProjectManagerSimpleLoader implements ProjectManager.ProjectManagerLoader {

    Context context;

    public ProjectManagerSimpleLoader(Context context) {
        Log.d("krio", "new ProjectManagerSimpleLoader");
        this.context = context;
    }

    @Override
    public void load(ProjectManager projectManager) {
        ProjectSelection projectSelection = new ProjectSelection();
        ProjectCursor projectCursor = projectSelection.query(context);
        while (projectCursor.moveToNext()) {
            Project project = projectManager.projectCreator.createProject(projectCursor.getId());
            projectManager.projectMap.put(project.getId(), project);
            project.setChangeListener(projectManager);
        }
        projectCursor.close();
    }
}
