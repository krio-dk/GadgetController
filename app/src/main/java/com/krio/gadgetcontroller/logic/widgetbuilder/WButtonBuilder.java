package com.krio.gadgetcontroller.logic.widgetbuilder;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.logic.command.OutputCommand;
import com.krio.gadgetcontroller.logic.widget.WButton;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;

import java.util.Map;

/**
 * Created by krio on 16.04.16.
 */
public class WButtonBuilder extends WidgetBuilder {

    Command btnDownCmd;
    Command btnUpCmd;

    public WButtonBuilder() {
        widgetType = WidgetType.BUTTON;
    }

    @Override
    public boolean createConcreteWidget() {
        if (btnDownCmd == null && btnUpCmd == null) {
            return false;
        }

        widget = new WButton(btnDownCmd, btnUpCmd);
        return true;
    }

    @Override
    public Command createCommand(CommandType type, String cmd, Map<String, Object> params) {
        switch (type) {
            case COMMAND_BUTTON_DOWN:
                btnDownCmd = new OutputCommand(type, cmd);
                return btnDownCmd;

            case COMMAND_BUTTON_UP:
                btnUpCmd = new OutputCommand(type, cmd);
                return btnUpCmd;

            default:
                return null;
        }
    }


}
