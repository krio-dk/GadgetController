package com.krio.gadgetcontroller.logic.widgetbuilder;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.logic.command.OutputCommand;
import com.krio.gadgetcontroller.logic.widget.WTimer;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;

import java.util.Map;

/**
 * Created by krio on 22.01.17.
 */
public class WTimerBuilder extends WidgetBuilder {

    Command cmd;

    public WTimerBuilder() {
        widgetType = WidgetType.TIMER;
    }

    @Override
    public boolean createConcreteWidget() {
        if (cmd == null) {
            return false;
        }

        widget = new WTimer(cmd);
        return true;
    }

    @Override
    public Command createCommand(CommandType type, String cmd, Map<String, Object> params) {
        switch (type) {
            case COMMAND_TIMER:
                this.cmd = new OutputCommand(type, cmd);
                return this.cmd;

            default:
                return null;
        }
    }
}
