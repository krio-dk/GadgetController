package com.krio.gadgetcontroller.logic.command;

import android.util.Log;

import com.krio.gadgetcontroller.logic.widget.Widget;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;

/**
 * Created by krio on 28.04.16.
 */
public class InputCommandParser extends Observable {

    ByteArrayOutputStream byteArrayOutputStream;
    Map<String, InputCommand> inputCommandMap;

    public InputCommandParser(Map<String, InputCommand> inputCommandMap) {
        Log.d("krio", "new InputCommandParser");
        byteArrayOutputStream = new ByteArrayOutputStream();
        this.inputCommandMap = inputCommandMap;
    }

    public void parseCommand(byte[] readBuf, int byteCount) {
        for (int i = 0; i < byteCount; i++) {
            if (readBuf[i] != 10) {
                byteArrayOutputStream.write(readBuf[i]);
            } else {
                try {
                    JSONObject jsonCommand = new JSONObject(byteArrayOutputStream.toString());
                    executeCommand(jsonCommand);
                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    byteArrayOutputStream.reset();
                }
            }
        }
    }

    private void executeCommand(JSONObject jsonCommand) throws JSONException {
        InputCommand command = inputCommandMap.get(jsonCommand.getString("cmd"));
        if (command != null) {
            Map<String, Object> params = getCommandParams(jsonCommand);

            command.execute(params);
            notifyAdapters(command.getWidget());
        }
    }

    private Map<String, Object> getCommandParams(JSONObject jsonCommand) {
        Map<String, Object> params = new HashMap<>();

        try {
            JSONObject jsonParams = jsonCommand.getJSONObject("params");

            Iterator<String> iterator = jsonParams.keys();
            while (iterator.hasNext()) {
                try {
                    String key = iterator.next();
                    Object value = jsonParams.get(key);
                    params.put(key, value);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return params;
    }

    void notifyAdapters(Widget widget) {
        setChanged();
        notifyObservers(widget);
    }
}
