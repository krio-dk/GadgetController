package com.krio.gadgetcontroller.logic.project;

import java.util.Observable;

/**
 * Created by krio on 10.05.16.
 */
public class ProjectModeWrapper extends Observable {
    boolean editMode = false;

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        if (this.editMode != editMode) {
            this.editMode = editMode;
            setChanged();
            notifyObservers();
        }
    }
}
