package com.krio.gadgetcontroller.logic.connection.events;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;

/**
 * Created by krio on 23.04.16.
 */
public class ConnectionEventManager implements ConnectionEventListener {

    private Handler handler;

    public ConnectionEventManager(@NonNull Handler handler) {
        Log.d("krio", "new ConnectionEventManager");
        this.handler = handler;
    }

    @Override
    public void onChangeState(int state) {
        handler.obtainMessage(ConnectionEventHandler.STATE_CHANGE, state, -1).sendToTarget();
    }

    @Override
    public void onSendData(byte[] data) {
        handler.obtainMessage(ConnectionEventHandler.SEND_DATA, -1, -1, data).sendToTarget();
    }

    @Override
    public void onReceiveData(byte[] data, int byteCount) {
        handler.obtainMessage(ConnectionEventHandler.RECEIVE_DATA, byteCount, -1, data).sendToTarget();
    }

    @Override
    public void onConnectionFailed() {
        handler.obtainMessage(ConnectionEventHandler.CONNECTION_FAILED).sendToTarget();
    }

    @Override
    public void onConnectionLost() {
        handler.obtainMessage(ConnectionEventHandler.CONNECTION_LOST).sendToTarget();
    }

    @Override
    public void onRemoteEvent(String event) {
        handler.obtainMessage(ConnectionEventHandler.REMOTE_EVENT, event).sendToTarget();
    }
}
