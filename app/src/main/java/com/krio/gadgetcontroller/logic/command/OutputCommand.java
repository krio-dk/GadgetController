package com.krio.gadgetcontroller.logic.command;

import com.krio.gadgetcontroller.App;
import com.krio.gadgetcontroller.provider.command.CommandType;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by krio on 07.04.16.
 */
public class OutputCommand extends Command {

    public OutputCommand(CommandType commandType, String cmd) {
        super(commandType, cmd);
    }

    @Override
    public void execute() {
        App.getConnection().performSend(getJsonCommand().getBytes());
        App.getConnectionLog().notifyOutputCommandExecute(this);
    }

    @Override
    protected void onChangeCommand(String oldCommand, String newCommand) {
        if (changeListener != null) {
            changeListener.onChangeOutputCommand(newCommand, id);
        }
    }

    private String getJsonCommand() {
        JSONObject jsonCommand = new JSONObject();
        try {
            jsonCommand.put("cmd", cmd);
            if (params != null && params.size() > 0) {
                JSONObject jsonParams = new JSONObject(params);
                jsonCommand.put("params", jsonParams);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonCommand.toString() + '\n';
    }
}
