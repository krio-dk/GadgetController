package com.krio.gadgetcontroller.logic.widget;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by krio on 07.04.16.
 */
public class WDisplay extends Widget {

    public static final String ATTR_IS_CAPTION_VISIBLE = "isCaptionVisible";
    public static final String CMD_PARAM_TEXT = "text";

    String text;

    public WDisplay() {
        super(WidgetType.DISPLAY);
    }

    public String getText() {
        return text;
    }

    @Override
    public boolean isCaptionVisible() {
        return (boolean) attrs.get(ATTR_IS_CAPTION_VISIBLE);
    }

    @Override
    public Command getOutputCommand(CommandType commandType) {
        return null;
    }

    @Override
    public List<Command> getOutputCommands() {
        return Collections.emptyList();
    }

    @Override
    public void performCommand(CommandType commandType, Map<String, Object> params) {
        if (commandType == CommandType.COMMAND_DISPLAY) {
            text = String.valueOf(params.get(CMD_PARAM_TEXT));
        } else {
            throw new UnsupportedOperationException();
        }
    }
}
