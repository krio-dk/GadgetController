package com.krio.gadgetcontroller.logic.widget;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by krio on 07.04.16.
 */
public class WLabel extends Widget {

    public WLabel() {
        super(WidgetType.LABEL);
    }

    @Override
    public Command getOutputCommand(CommandType commandType) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isCaptionVisible() {
        return true;
    }

    @Override
    public List<Command> getOutputCommands() {
        return Collections.emptyList();
    }

    @Override
    public void performCommand(CommandType commandType, Map<String, Object> params) {
        throw new UnsupportedOperationException();
    }
}
