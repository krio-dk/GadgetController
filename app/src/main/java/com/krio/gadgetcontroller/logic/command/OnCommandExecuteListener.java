package com.krio.gadgetcontroller.logic.command;

/**
 * Created by krio on 06.06.16.
 */
public interface OnCommandExecuteListener {
    void onInputCommandExecute(Command command);
    void onOutputCommandExecute(Command command);
}
