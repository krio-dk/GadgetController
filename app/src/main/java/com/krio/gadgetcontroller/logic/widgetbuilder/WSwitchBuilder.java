package com.krio.gadgetcontroller.logic.widgetbuilder;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.logic.command.OutputCommand;
import com.krio.gadgetcontroller.logic.widget.WSwitch;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;

import java.util.Map;

/**
 * Created by krio on 17.04.16.
 */
public class WSwitchBuilder extends WidgetBuilder {

    Command swcOnCmd;
    Command swcOffCmd;

    public WSwitchBuilder() {
        widgetType = WidgetType.SWITCH;
    }

    @Override
    public boolean createConcreteWidget() {
        if (swcOnCmd == null || swcOffCmd == null) {
            return false;
        }

        widget = new WSwitch(swcOnCmd, swcOffCmd);
        return true;
    }

    @Override
    public Command createCommand(CommandType type, String cmd, Map<String, Object> params) {
        switch (type) {
            case COMMAND_SWITCH_ENABLE:
                swcOnCmd = new OutputCommand(type, cmd);
                return swcOnCmd;

            case COMMAND_SWITCH_DISABLE:
                swcOffCmd = new OutputCommand(type, cmd);
                return swcOffCmd;

            default:
                return null;
        }
    }
}
