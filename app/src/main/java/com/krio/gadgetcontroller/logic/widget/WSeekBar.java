package com.krio.gadgetcontroller.logic.widget;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by krio on 07.04.16.
 */
public class WSeekBar extends Widget {

    public static final String ATTR_IS_CAPTION_VISIBLE = "isCaptionVisible";
    public static final String CMD_PARAM_VALUE = "value";

    Command cmd;
    int progress;

    public WSeekBar(Command cmd) {
        super(WidgetType.SEEKBAR);
        this.cmd = cmd;
    }

    @Override
    public boolean isCaptionVisible() {
        return (boolean) attrs.get(ATTR_IS_CAPTION_VISIBLE);
    }

    @Override
    public Command getOutputCommand(CommandType commandType) {
        if (commandType == CommandType.COMMAND_SEEKBAR) {
            return cmd;
        } else {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public List<Command> getOutputCommands() {
        List<Command> commandList = new ArrayList<>();
        commandList.add(cmd);
        return commandList;
    }

    @Override
    public void performCommand(CommandType commandType, Map<String, Object> params) {
        if (commandType == CommandType.COMMAND_SEEKBAR) {
            cmd.execute(params);
        } else {
            throw new UnsupportedOperationException();
        }
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(final int progress) {
        this.progress = progress;
    }
}
