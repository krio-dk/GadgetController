package com.krio.gadgetcontroller.logic.widget;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by krio on 07.04.16.
 */
public class WButton extends Widget {

    public static final String ATTR_IS_CAPTION_VISIBLE = "BUTTON_SHOW_CAPTION";
    public static final String ATTR_BUTTON_TEXT = "BUTTON_CAPTION";

    Command btnDownCmd;
    Command btnUpCmd;

    public WButton(Command btnDownCmd, Command btnUpCmd) {
        super(WidgetType.BUTTON);
        this.btnDownCmd = btnDownCmd;
        this.btnUpCmd = btnUpCmd;
    }

    public String getButtonText () {
        return (String) attrs.get(ATTR_BUTTON_TEXT);
    }

    @Override
    public boolean isCaptionVisible() {
        return (boolean) attrs.get(ATTR_IS_CAPTION_VISIBLE);
    }

    @Override
    public Command getOutputCommand(CommandType commandType) {
        switch (commandType) {
            case COMMAND_BUTTON_DOWN:
                return btnDownCmd;
            case COMMAND_BUTTON_UP:
                return btnUpCmd;
            default:
                throw new UnsupportedOperationException();
        }
    }

    @Override
    public List<Command> getOutputCommands() {
        List<Command> commandList = new ArrayList<>();
        commandList.add(btnDownCmd);
        commandList.add(btnUpCmd);
        return commandList;
    }

    @Override
    public void performCommand(CommandType commandType, Map<String, Object> params) {
        switch (commandType) {
            case COMMAND_BUTTON_DOWN:
                btnDownCmd.execute();
                break;
            case COMMAND_BUTTON_UP:
                btnUpCmd.execute();
                break;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
