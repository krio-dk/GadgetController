package com.krio.gadgetcontroller.logic.connection;

import android.support.annotation.NonNull;

import java.io.Serializable;

public class TCPHistoryItem implements Serializable {

    private static final long serialVersionUID = -3980431419094299227L;

    int id;

    @NonNull
    final private String host;

    @NonNull
    final private String port;

    public TCPHistoryItem(@NonNull final String host, @NonNull final String port) {
        this.host = host;
        this.port = port;
    }

    @NonNull
    public String getHost() {
        return host;
    }

    @NonNull
    public String getPort() {
        return port;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }
}
