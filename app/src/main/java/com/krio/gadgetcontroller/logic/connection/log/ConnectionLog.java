package com.krio.gadgetcontroller.logic.connection.log;

import android.content.Context;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.logic.connection.core.Connection;
import com.krio.gadgetcontroller.provider.command.CommandType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by krio on 28.04.16.
 */
public class ConnectionLog {

    private List<LogItem> itemList = new ArrayList<>();
    private LogUpdateListener updateListener;

    Context context;

    public ConnectionLog(Context context) {
        this.context = context;
    }

    private void addItem(String value, ItemType itemType) {
        Date date = new Date();
        LogItem logItem = new LogItem(date, value, itemType);

        checkedInsert(logItem);

        if (updateListener != null) {
            updateListener.onAddItem();
        }
    }

    // TODO: make cache
    private void checkedInsert(LogItem item) {
        itemList.add(item);
    }

    public List<LogItem> getItemList() {
        return itemList;
    }

    public void clear() {
        int itemCount = itemList.size();
        itemList.clear();

        if (updateListener != null) {
            updateListener.onClearList(itemCount);
        }
    }

    public void setUpdateListener(LogUpdateListener updateListener) {
        this.updateListener = updateListener;
    }

    public void resetUpdateListener() {
        updateListener = null;
    }

    public void notifyChangeState(int state) {
        switch (state) {
            case Connection.STATE_DISCONNECTED:
                addItem(context.getString(R.string.log_state_disconnected), ItemType.STATE_CHANGE);
                break;

            case Connection.STATE_CONNECTING:
                addItem(context.getString(R.string.log_state_connecting), ItemType.STATE_CHANGE);
                break;

            case Connection.STATE_CONNECTED:
                addItem(context.getString(R.string.log_state_connected), ItemType.STATE_CHANGE);
                break;
        }
    }

    public void notifySendData(byte[] data) {
        String value = new String(data);

        if (value.charAt(value.length() - 1) == '\n') {
            value = value.substring(0, value.length() - 1);
        }

        addItem(value, ItemType.SEND_DATA);
    }

    public void notifyReceiveData(byte[] data, int byteCount) {
        String value = new String(data, 0, byteCount);

        if (value.charAt(value.length() - 1) == '\n') {
            value = value.substring(0, value.length() - 1);
        }

        addItem(value, ItemType.RECEIVE_DATA);

    }

    public void notifyConnectionFailed() {
        addItem(context.getString(R.string.log_connection_faild), ItemType.CONNECTION_PROBLEM);
    }

    public void notifyConnectionLost() {
        addItem(context.getString(R.string.log_connection_lost), ItemType.CONNECTION_PROBLEM);
    }

    public void notifyRemoteEvent(String event) {
        switch (event) {
            case Connection.REMOTE_EVENT_DEVICE_CONNECT:

                break;

            case Connection.REMOTE_EVENT_DEVICE_DISCONNECT:

                break;
        }
    }

    public void notifyInputCommandExecute(Command command) {
        String value = context.getString(R.string.log_display_command, command.getCmd(),
                                         getCommandTypeDescription(command.getType()));
        addItem(value, ItemType.EXECUTE_INPUT_COMMAND);
    }

    public void notifyOutputCommandExecute(Command command) {
        String value = context.getString(R.string.log_control_command, command.getCmd(),
                                         getCommandTypeDescription(command.getType()));
        addItem(value, ItemType.EXECUTE_OUTPUT_COMMAND);
    }

    private String getCommandTypeDescription(CommandType commandType) {

        switch (commandType) {
            case COMMAND_LED_ENABLE:
                return context.getString(R.string.command_led_enable);

            case COMMAND_LED_DISABLE:
                return context.getString(R.string.command_led_disable);

            case COMMAND_DISPLAY:
                return context.getString(R.string.command_display);

            case COMMAND_SWITCH_ENABLE:
                return context.getString(R.string.command_switch_on);

            case COMMAND_SWITCH_DISABLE:
                return context.getString(R.string.command_switch_off);

            case COMMAND_BUTTON_DOWN:
                return context.getString(R.string.command_button_down);

            case COMMAND_BUTTON_UP:
                return context.getString(R.string.command_button_up);

            case COMMAND_SEEKBAR:
                return context.getString(R.string.command_seekbar);

            case COMMAND_JOYSTICK:
                return context.getString(R.string.command_joystick);

            case COMMAND_TIMER:
                return context.getString(R.string.command_timer);

            default:
                return null;
        }
    }

    public interface LogUpdateListener {

        void onAddItem();

        void onClearList(int itemCount);
    }
}
