package com.krio.gadgetcontroller.logic.widget;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by krio on 07.04.16.
 */
public abstract class Widget {

    public static final String ATTR_CAPTION_TEXT = "captionText";

    WidgetType widgetType;

    long id;
    long panelId;
    int position;

    Map<String, Object> attrs = new HashMap<>();

    OnWidgetChangeListener changeListener;


    public Widget(WidgetType widgetType) {
        this.widgetType = widgetType;
    }


    public WidgetType getWidgetType() {
        return widgetType;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public long getPanelId() {
        return panelId;
    }

    public void setPanelId(long panelId) {
        this.panelId = panelId;
    }


    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        if (changeListener != null) {
            changeListener.onChangeWidgetPosition(position, id);
        }
    }


    public Map<String, Object> getAttributes() {
        return attrs;
    }

    public void setAttributes(Map<String, Object> attrs) {
        this.attrs = attrs;
        if (changeListener != null) {
            changeListener.onChangeWidgetAttrs(attrs, id);
        }
    }

    public String getCaption() {
        return (String) attrs.get(ATTR_CAPTION_TEXT);
    }

    public abstract boolean isCaptionVisible();


    public abstract Command getOutputCommand(CommandType commandType);

    public abstract List<Command> getOutputCommands();


    public abstract void performCommand(CommandType commandType, Map<String, Object> params);


    public void setChangeListener(OnWidgetChangeListener changeListener) {
        this.changeListener = changeListener;
    }

    public interface OnWidgetChangeListener {
        void onChangeWidgetPosition(int position, long widgetId);
        void onChangeWidgetAttrs(Map<String, Object> attrs, long widgetId);
    }
}
