package com.krio.gadgetcontroller.logic.connection.core;

import com.krio.gadgetcontroller.App;
import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.logic.connection.events.ConnectionEventHandler;
import com.krio.gadgetcontroller.logic.connection.events.ConnectionEventListener;
import com.krio.gadgetcontroller.ui.utils.DialogUtils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.MenuItem;

import java.util.List;

import javax.inject.Inject;

public class ConnectPerformer implements ConnectionEventListener {

    public interface OnConnectedListener {
        void onConnected();
    }

    public interface OnConnectionEventListener {
        void onConnecting();
        void onConnectionFailed();
    }

    @Nullable
    private OnConnectedListener connectedListener;

    @Nullable
    private OnConnectionEventListener connectionEventListener;

    @NonNull
    private final Context context;

    @NonNull
    private final Connection connection;

    @Nullable
    private final MenuItem menuItemLoader;

    @Nullable
    private final List<MenuItem> menuItemsForHide;

    @Inject
    ConnectionEventHandler connectionEventHandler;

    @Inject
    ConnectionWrapper connectionWrapper;

    public ConnectPerformer(@NonNull final Context context, @NonNull final Connection connection,
            @Nullable final MenuItem menuItemLoader, @Nullable List<MenuItem> menuItemsForHide) {

        this.context = context;
        this.connection = connection;

        this.menuItemLoader = menuItemLoader;
        this.menuItemsForHide = menuItemsForHide;

        App.getMainComponent().inject(this);

        connectionEventHandler.setEventListener(this);
        connectionWrapper.setConnection(connection);
    }

    public void setOnConnectionEventListener(
            @Nullable final OnConnectionEventListener connectionEventListener) {
        this.connectionEventListener = connectionEventListener;
    }

    public void performConnect(@NonNull final OnConnectedListener listener) {
        connectedListener = listener;
        connection.performConnect();
    }

    @Override
    public void onChangeState(final int state) {
        if (state == Connection.STATE_CONNECTED && connectedListener != null) {
            connectedListener.onConnected();
        }

        switch (state) {
            case Connection.STATE_CONNECTED:
                if (connectedListener != null) {
                    connectedListener.onConnected();
                }

                updateMenuItemsVisible(false);
                break;

            case Connection.STATE_CONNECTING:
                if (connectionEventListener != null) {
                    connectionEventListener.onConnecting();
                }

                updateMenuItemsVisible(true);
                break;

            case Connection.STATE_DISCONNECTED:
                updateMenuItemsVisible(false);
                break;
        }
    }

    private void updateMenuItemsVisible(boolean connectionInProgress) {
        if (menuItemLoader != null) {
            if (connectionInProgress) {
                menuItemLoader.setVisible(true);
                menuItemLoader.setActionView(R.layout.progress_bar);
            } else {
                menuItemLoader.setVisible(false);
            }
        }

        if (menuItemsForHide != null && !menuItemsForHide.isEmpty()) {
            for (MenuItem item : menuItemsForHide) {
                item.setVisible(!connectionInProgress);
            }
        }
    }

    @Override
    public void onSendData(final byte[] data) {
        // do nothing
    }

    @Override
    public void onReceiveData(final byte[] data, final int byteCount) {
        // do nothing
    }

    @Override
    public void onConnectionFailed() {
        if (connectionEventListener != null) {
            connectionEventListener.onConnectionFailed();
        }

        DialogUtils.showDialogConnectionFailed(context, () -> {
            connectionWrapper.getConnection().performConnect();
        });
    }

    @Override
    public void onConnectionLost() {
        // do nothing
    }

    @Override
    public void onRemoteEvent(final String event) {
        // do nothing
    }
}
