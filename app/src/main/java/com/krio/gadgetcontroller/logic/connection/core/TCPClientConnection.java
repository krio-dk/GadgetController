package com.krio.gadgetcontroller.logic.connection.core;

import android.support.annotation.NonNull;
import android.util.Log;

import com.krio.gadgetcontroller.logic.connection.events.ConnectionEventListener;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by krio on 05.07.16.
 */
public class TCPClientConnection extends Connection {

    private static final String TAG = "TCPClientConnection";

    private ExecutorService mExecutorService;
    private ConnectRunnable mConnectRunnable;
    private ConnectedRunnable mConnectedRunnable;

    public TCPClientConnection(@NonNull ConnectionEventListener eventListener, String host, int port) {
        super(eventListener);
        Log.d("krio", "new TCPClientConnection");
        init(host, port);
    }

    private void init(String host, int port) {
        mExecutorService = Executors.newSingleThreadExecutor();
        mConnectRunnable = new ConnectRunnable(host, port);
    }

    @Override
    protected void connect() {
        mExecutorService.submit(mConnectRunnable);
    }

    @Override
    protected void disconnect() {

        Log.d(TAG, "disconnect");


        if (getState() == STATE_CONNECTING) {
            mConnectRunnable.cancel();
        } else {
            mConnectedRunnable.cancel();
            mConnectedRunnable = null;
        }

    }

    @Override
    protected void send(byte[] data) {
        if (getState() == STATE_CONNECTED) {
            mConnectedRunnable.write(data);
        }
    }

    private class ConnectRunnable implements Runnable {

        Socket socket;
        String host;
        int port;

        private volatile boolean mCloseFlag;

        public ConnectRunnable(String host, int port) {
            this.host = host;
            this.port = port;
        }

        @Override
        public void run() {

            setState(STATE_CONNECTING);
            Log.d(TAG, "STATE_CONNECTING");

            socket = new Socket();
            mCloseFlag = false;

            try {
                socket.bind(null);
                socket.connect((new InetSocketAddress(host, port)), 500);

            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "Exception:  connecting canceled or connection failure");

                try {
                    socket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                setState(STATE_DISCONNECTED);
                Log.d(TAG, "STATE_DISCONNECTED");

                if (!mCloseFlag) {
                    eventListener.onConnectionFailed();
                    Log.d(TAG, "notifyConnectionFailed");
                }
                return;
            }

            Log.d(TAG, "connect ok");

            mConnectedRunnable = new ConnectedRunnable(socket);
            mExecutorService.submit(mConnectedRunnable);

        }

        public void cancel() {
            Log.d(TAG, "Cancel connecting");
            mCloseFlag = true;
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "Exception: can't close socket (Cancel connecting)");
            }
        }
    }

    private class ConnectedRunnable implements Runnable {

        private final Socket socket;

        private final InputStream inputStream;
        private final OutputStream outputStream;

        private volatile boolean mCloseFlag = false;

        public ConnectedRunnable(Socket socket) {
            Log.d(TAG, "new ConnectedRunnable");

            this.socket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "Exception: can't get streams");
            }

            inputStream = tmpIn;
            outputStream = tmpOut;
        }

        @Override
        public void run() {

            setState(STATE_CONNECTED);
            Log.d(TAG, "STATE_CONNECTED");

            byte[] buffer = new byte[1024];
            int byteCount;

            while (true) {
                try {
                    byteCount = inputStream.read(buffer);

                    if (byteCount == -1) {
                        throw new IOException();
                    }

                    eventListener.onReceiveData(buffer.clone(), byteCount);

                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d(TAG, "Exception: socket is closed");

                    setState(STATE_DISCONNECTED);
                    Log.d(TAG, "STATE_DISCONNECTED");

                    if (!mCloseFlag) {
                        eventListener.onConnectionLost();
                        Log.d(TAG, "notifyConnectionLost");
                    }
                    break;
                }
            }
        }

        public void write(byte[] buffer) {
            try {
                outputStream.write(buffer);
                eventListener.onSendData(buffer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void cancel() {
            Log.d(TAG, "Cancel connected");
            mCloseFlag = true;
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "Exception: can't close socket (Cancel connected)");
            }
        }
    }
}
