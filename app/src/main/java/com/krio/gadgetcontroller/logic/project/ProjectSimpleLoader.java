package com.krio.gadgetcontroller.logic.project;

import android.content.Context;
import android.util.Log;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.logic.command.InputCommand;
import com.krio.gadgetcontroller.logic.panel.Panel;
import com.krio.gadgetcontroller.logic.widget.Widget;
import com.krio.gadgetcontroller.logic.widgetbuilder.WidgetBuilder;
import com.krio.gadgetcontroller.logic.widgetbuilder.WidgetBuilderFactory;
import com.krio.gadgetcontroller.provider.command.CommandColumns;
import com.krio.gadgetcontroller.provider.command.CommandCursor;
import com.krio.gadgetcontroller.provider.command.CommandSelection;
import com.krio.gadgetcontroller.provider.commandparam.CommandParamColumns;
import com.krio.gadgetcontroller.provider.commandparam.CommandParamCursor;
import com.krio.gadgetcontroller.provider.commandparam.CommandParamSelection;
import com.krio.gadgetcontroller.provider.panel.PanelColumns;
import com.krio.gadgetcontroller.provider.panel.PanelCursor;
import com.krio.gadgetcontroller.provider.panel.PanelSelection;
import com.krio.gadgetcontroller.provider.project.ProjectColumns;
import com.krio.gadgetcontroller.provider.project.ProjectCursor;
import com.krio.gadgetcontroller.provider.project.ProjectSelection;
import com.krio.gadgetcontroller.provider.widget.WidgetColumns;
import com.krio.gadgetcontroller.provider.widget.WidgetCursor;
import com.krio.gadgetcontroller.provider.widget.WidgetSelection;
import com.krio.gadgetcontroller.provider.widgetattr.WidgetAttrColumns;
import com.krio.gadgetcontroller.provider.widgetattr.WidgetAttrCursor;
import com.krio.gadgetcontroller.provider.widgetattr.WidgetAttrSelection;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by krio on 02.06.16.
 */
public class ProjectSimpleLoader implements Project.ProjectLoader {

    Context context;
    boolean lazy;

    public ProjectSimpleLoader(Context context, boolean lazy) {
        Log.d("krio", "new ProjectSimpleLoader, lazy = " + lazy);
        this.context = context;
        this.lazy = lazy;
    }

    @Override
    public void load(Project project) {
        Log.d("krio", "load project = " + project.getId());
        loadProject(project);
        Log.d("krio", "load project done, name = " + project.getName() + ", position = " + project.getPosition());
        if (!lazy) {
            loadPanels(project);
            loadWidgets(project);
        }
    }

    /**
     * Selects name and token for project with specified ID.
     */
    private void loadProject(Project project) {
        ProjectSelection projectSelection = new ProjectSelection();
        projectSelection.id(project.getId());

        String[] projection = {
                ProjectColumns._ID,
                ProjectColumns.NAME,
                ProjectColumns.TOKEN,
                ProjectColumns.POSITION_ON_LIST
        };

        ProjectCursor projectCursor = projectSelection.query(context, projection);
        while (projectCursor.moveToNext()) {
            project.name = projectCursor.getName();
            project.token = projectCursor.getToken();
            project.position = projectCursor.getPositionOnList();
        }
        projectCursor.close();
    }

    /**
     * Selects panels for project.
     */
    private void loadPanels(Project project) {
        PanelSelection panelSelection = new PanelSelection();
        panelSelection.projectId(project.getId());

        String[] projection = {
                PanelColumns._ID,
                PanelColumns.CAPTION
        };

        PanelCursor panelCursor = panelSelection.query(context, projection);
        while (panelCursor.moveToNext()) {
            Panel panel = new Panel(panelCursor.getId(), panelCursor.getCaption());
            project.panelMap.put(panel.getId(), panel);
            panel.setChangeListener(project);
        }
        panelCursor.close();
    }

    /**
     * Selects widgets for project.<br>
     * Calls {@link #buildWidgetAttrs(long widgetId) buildWidgetAttrs} for selecting widget attributes.<br>
     * Calls {@link #buildCommands(WidgetBuilder widgetBuilder, long widgetId) buildCommands} for selecting widget commands.
     */
    private void loadWidgets(Project project) {

        WidgetSelection widgetSelection = new WidgetSelection();
        widgetSelection.panelProjectId(project.getId());

        String[] projection = {
                PanelColumns.PROJECT_ID,
                WidgetColumns._ID,
                WidgetColumns.PANEL_ID,
                WidgetColumns.POSITION_ON_PANEL,
                WidgetColumns.CAPTION,
                WidgetColumns.TYPE
        };

        WidgetCursor widgetCursor = widgetSelection.query(context, projection);

        while (widgetCursor.moveToNext()) {
            WidgetBuilder widgetBuilder = WidgetBuilderFactory.createWidgetBuilder(widgetCursor.getType());

            if (widgetBuilder != null) {

                Map<String, Object> attrs = buildWidgetAttrs(widgetCursor.getId());

                widgetBuilder.setId(widgetCursor.getId());
                widgetBuilder.setPanelId(widgetCursor.getPanelId());
                widgetBuilder.setPosition(widgetCursor.getPositionOnPanel());
                widgetBuilder.setAttrs(attrs);

                buildCommands(widgetBuilder, widgetCursor.getId());
                widgetBuilder.createWidget();

                Widget widget = widgetBuilder.getWidget();
                project.widgetMap.put(widget.getId(), widget);
                widget.setChangeListener(project);

                for (Command command : widget.getOutputCommands()) {
                    command.setChangeListener(project);
                }

                for (InputCommand command : widgetBuilder.getInputCommandList()) {
                    project.inputCommandMap.put(command.getCmd(), command);
                    command.setChangeListener(project);
                }
            }
        }
        widgetCursor.close();
    }

    /**
     * Returns an Bundle object stored widget attributes.
     *
     * @param widgetId widget ID
     * @return attributes for widget with specified ID
     */
    private Map<String, Object> buildWidgetAttrs(long widgetId) {
        Map<String, Object> attrs = new HashMap<>();

        WidgetAttrSelection widgetAttrSelection = new WidgetAttrSelection();
        widgetAttrSelection.widgetId(widgetId);

        String[] projection = {
                PanelColumns.PROJECT_ID,
                WidgetAttrColumns.WIDGET_ID,
                WidgetAttrColumns.NAME,
                WidgetAttrColumns.BOOLEAN_ATTR,
                WidgetAttrColumns.DOUBLE_ATTR,
                WidgetAttrColumns.INTEGER_ATTR,
                WidgetAttrColumns.STRING_ATTR,
                WidgetAttrColumns.TYPE
        };

        WidgetAttrCursor widgetAttrCursor = widgetAttrSelection.query(context, projection);
        while (widgetAttrCursor.moveToNext()) {
            switch (widgetAttrCursor.getType()) {
                case STRING:
                    attrs.put(widgetAttrCursor.getName(), widgetAttrCursor.getStringAttr());
                    break;

                case INEGER:
                    attrs.put(widgetAttrCursor.getName(), widgetAttrCursor.getIntegerAttr());
                    break;

                case DOUBLE:
                    attrs.put(widgetAttrCursor.getName(), widgetAttrCursor.getDoubleAttr());
                    break;

                case BOOLEAN:
                    attrs.put(widgetAttrCursor.getName(), widgetAttrCursor.getBooleanAttr());
                    break;
            }
        }
        widgetAttrCursor.close();
        return attrs;
    }

    /**
     * Selects commands for widget with specified ID.<br>
     * Calls {@link #buildCommandParams(long commandId) buildCommandParams} for selecting command attributes.
     *
     * @param widgetBuilder widgetBuilder
     * @param widgetId      widget ID
     */
    private void buildCommands(WidgetBuilder widgetBuilder, long widgetId) {
        CommandSelection commandSelection = new CommandSelection();
        commandSelection.widgetId(widgetId);

        String[] projection = {
                PanelColumns.PROJECT_ID,
                CommandColumns._ID,
                CommandColumns.WIDGET_ID,
                CommandColumns.CMD,
                CommandColumns.TYPE
        };

        CommandCursor commandCursor = commandSelection.query(context, projection);
        while (commandCursor.moveToNext()) {
            Map<String, Object> params = buildCommandParams(commandCursor.getId());
            Command command = widgetBuilder.createCommand(commandCursor.getType(), commandCursor.getCmd(), params);

            command.setId(commandCursor.getId());
            command.setWidgetId(widgetId);
        }
        commandCursor.close();
    }

    /**
     * Returns an Bundle object stored command parameters.
     *
     * @param commandId command ID
     * @return parameters for command with specified ID
     */
    private Map<String, Object> buildCommandParams(long commandId) {
        Map<String, Object> params = new HashMap<>();

        CommandParamSelection commandParamSelection = new CommandParamSelection();
        commandParamSelection.commandId(commandId);

        String[] projection = {
                PanelColumns.PROJECT_ID,
                CommandParamColumns.COMMAND_ID,
                CommandParamColumns.NAME,
                CommandParamColumns.BOOLEAN_PARAM,
                CommandParamColumns.DATE_PARAM,
                CommandParamColumns.DOUBLE_PARAM,
                CommandParamColumns.INTEGER_PARAM,
                CommandParamColumns.STRING_PARAM,
                CommandParamColumns.TYPE
        };

        CommandParamCursor commandParamCursor = commandParamSelection.query(context, projection);
        while (commandParamCursor.moveToNext()) {
            switch (commandParamCursor.getType()) {
                case STRING:
                    params.put(commandParamCursor.getName(), commandParamCursor.getStringParam());
                    break;

                case INEGER:
                    params.put(commandParamCursor.getName(), commandParamCursor.getIntegerParam());
                    break;

                case DOUBLE:
                    params.put(commandParamCursor.getName(), commandParamCursor.getDoubleParam());
                    break;

                case BOOLEAN:
                    params.put(commandParamCursor.getName(), commandParamCursor.getBooleanParam());
                    break;
            }
        }
        commandParamCursor.close();
        return params;
    }

}
