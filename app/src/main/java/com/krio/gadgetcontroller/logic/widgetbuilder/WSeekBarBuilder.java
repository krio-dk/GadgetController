package com.krio.gadgetcontroller.logic.widgetbuilder;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.logic.command.OutputCommand;
import com.krio.gadgetcontroller.logic.widget.WSeekBar;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;

import java.util.Map;

/**
 * Created by krio on 17.04.16.
 */
public class WSeekBarBuilder extends WidgetBuilder {

    Command sbCmd;

    public WSeekBarBuilder() {
        widgetType = WidgetType.SEEKBAR;
    }

    @Override
    public boolean createConcreteWidget() {
        if (sbCmd == null) {
            return false;
        }

        widget = new WSeekBar(sbCmd);
        return true;
    }

    @Override
    public Command createCommand(CommandType type, String cmd, Map<String, Object> params) {

        if (type != CommandType.COMMAND_SEEKBAR) {
            return null;
        }

        sbCmd = new OutputCommand(type, cmd);
        return sbCmd;
    }
}
