package com.krio.gadgetcontroller.logic.connection.log;


import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by krio on 28.04.16.
 */
public class LogItem {

    Date time;
    String value;
    ItemType itemType;

    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

    public LogItem(Date time, String value, ItemType itemType) {
        this.time = time;
        this.value = value;
        this.itemType = itemType;
    }

    public String getTime() {
        return sdf.format(time);
    }

    public String getValue() {
        return value;
    }

    public ItemType getItemType() {
        return itemType;
    }
}
