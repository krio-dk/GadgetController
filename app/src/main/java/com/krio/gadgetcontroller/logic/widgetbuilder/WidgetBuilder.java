package com.krio.gadgetcontroller.logic.widgetbuilder;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.logic.command.InputCommand;
import com.krio.gadgetcontroller.logic.widget.Widget;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by krio on 16.04.16.
 */
public abstract class WidgetBuilder {

    Widget widget;
    WidgetType widgetType;

    List<InputCommand> inputCommandList = new ArrayList<>();

    long id;
    long panelId;
    int position;

    Map<String, Object> attrs;

    public void setId(long id) {
        if (widget == null) {
            this.id = id;
        } else {
            widget.setId(id);
        }
    }

    public void setPanelId(long panelId) {
        if (widget == null) {
            this.panelId = panelId;
        } else {
            widget.setPanelId(panelId);
        }
    }

    public void setPosition(int position) {
        if (widget == null) {
            this.position = position;
        } else {
            widget.setPosition(position);
        }

    }

    public void setAttrs(Map<String, Object> attrs) {
        if (widget == null) {
            this.attrs = attrs;
        } else {
            widget.setAttributes(attrs);
        }

    }

    public Widget getWidget() {
        return widget;
    }

    public WidgetType getWidgetType() {
        return widgetType;
    }

    public List<InputCommand> getInputCommandList() {
        return inputCommandList;
    }

    public boolean createWidget () {

        if (widget != null) {
            return true;
        }

        if (createConcreteWidget()) {
            widget.setId(id);
            widget.setPanelId(panelId);
            widget.setPosition(position);
            widget.setAttributes(attrs);
            return true;

        } else {
            return false;
        }
    }

    protected abstract boolean createConcreteWidget();

    public abstract Command createCommand(CommandType type, String cmd, Map<String, Object> params);
}
