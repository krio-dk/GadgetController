package com.krio.gadgetcontroller.logic.widgetbuilder;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.logic.command.InputCommand;
import com.krio.gadgetcontroller.logic.widget.WLed;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;

import java.util.Map;

/**
 * Created by krio on 16.04.16.
 */
public class WLedBuilder extends WidgetBuilder {

    boolean offCmdPresent = false;

    public WLedBuilder() {
        widgetType = WidgetType.LED;
    }

    @Override
    public boolean createConcreteWidget() {
        widget = new WLed();
        return true;
    }

    @Override
    public Command createCommand(CommandType type, String cmd, Map<String, Object> params) {

        if (widget == null) {
            createWidget();
        }

        switch (type) {

            case COMMAND_LED_ENABLE:
                InputCommand onCmd = new InputCommand(widget, type, cmd);
                inputCommandList.add(onCmd);
                return onCmd;

            case COMMAND_LED_DISABLE:
                if (offCmdPresent) {
                    return null;
                }

                InputCommand offCmd = new InputCommand(widget, type, cmd);
                offCmdPresent = true;
                inputCommandList.add(offCmd);
                return offCmd;

            default:
                return null;
        }
    }
}
