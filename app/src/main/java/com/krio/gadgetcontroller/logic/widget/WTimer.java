package com.krio.gadgetcontroller.logic.widget;

import com.krio.gadgetcontroller.App;
import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;
import com.krio.gadgetcontroller.ui.utils.PreciseCountdown;

import org.joda.time.DateTime;
import org.joda.time.DurationFieldType;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by krio on 22.01.17.
 */
public class WTimer extends Widget {

    public static final String ATTR_INTERVAL = "timer_interval";

    Command cmd;

    private PreciseCountdown timer;

    private WTimerCallback callback;

    public interface WTimerCallback {
        void onTick(String formattedCountDownTime);

        void onSend();

        void onStop();
    }

    public WTimer(Command cmd) {
        super(WidgetType.TIMER);
        this.cmd = cmd;
    }

    @Override
    public boolean isCaptionVisible() {
        return true;
    }

    public int getTimerInterval() {
        return (int) attrs.get(ATTR_INTERVAL);
    }

    @Override
    public Command getOutputCommand(CommandType commandType) {
        switch (commandType) {
            case COMMAND_TIMER:
                return cmd;
            default:
                throw new UnsupportedOperationException();
        }
    }

    @Override
    public List<Command> getOutputCommands() {
        List<Command> commandList = new ArrayList<>();
        commandList.add(cmd);
        return commandList;
    }

    @Override
    public void performCommand(CommandType commandType, Map<String, Object> params) {
        switch (commandType) {
            case COMMAND_TIMER:
                cmd.execute();
                break;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public void start() {
        if (timer == null) {
            timer = new WCountDownTimer((int) attrs.get(ATTR_INTERVAL));
            timer.start();
        }
    }

    public void stop() {
        timer.cancel();
        timer = null;

        if (callback != null) {
            callback.onStop();
        }
    }

    @Override
    public void setAttributes(final Map<String, Object> attrs) {
        super.setAttributes(attrs);
        if (timer != null) {
            timer.cancel();
            timer = new WCountDownTimer((int) attrs.get(ATTR_INTERVAL));
            timer.start();
        }
    }

    public boolean isRun() {
        return timer != null;
    }

    public void setCallback(WTimerCallback callback) {
        this.callback = callback;
    }

    private final class WCountDownTimer extends PreciseCountdown {

        private WCountDownTimer(long timeoutInSeconds) {
            super(timeoutInSeconds * 1000, 1000);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (App.getMainComponent() != null) {
                if (callback != null) {
                    callback.onTick(getFormattedCountDownTime(millisUntilFinished));
                }
            } else {
                timer.cancel();
            }
        }

        @Override
        public void onFinished() {
            if (App.getMainComponent() != null) {
                if (callback != null) {
                    callback.onSend();
                }

                performCommand(CommandType.COMMAND_TIMER, null);

                timer.stop();
                timer.restart();
            } else {
                timer.cancel();
            }
        }
    }

    public static String getFormattedCountDownTime(long millisUntilFinished) {
        PeriodType type = PeriodType.forFields(new DurationFieldType[]{
                DurationFieldType.days(),
                DurationFieldType.hours(),
                DurationFieldType.minutes(),
                DurationFieldType.seconds()
        });

        long startTime = DateTime.now().getMillis();
        long endTime = millisUntilFinished + startTime;

        Period period = new Period(startTime, endTime, type);

        PeriodFormatter formatter = new PeriodFormatterBuilder()
                .appendDays()
                .appendSeparator("d. ")
                .appendHours()
                .appendSeparator(":")
                .printZeroAlways()
                .minimumPrintedDigits(2)
                .appendMinutes()
                .appendSeparator(":")
                .minimumPrintedDigits(2)
                .appendSeconds()
                .toFormatter();

        return formatter.print(period);
    }
}
