package com.krio.gadgetcontroller.logic.connection.events;


import com.krio.gadgetcontroller.logic.command.InputCommandParser;
import com.krio.gadgetcontroller.logic.connection.core.Connection;
import com.krio.gadgetcontroller.logic.connection.log.ConnectionLog;

import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.Log;

/**
 * Created by krio on 11.04.16.
 */
public class ConnectionEventHandler extends Handler {

    public static final int STATE_CHANGE = 1;
    public static final int RECEIVE_DATA = 2;
    public static final int SEND_DATA = 3;
    public static final int REMOTE_EVENT = 4;
    public static final int CONNECTION_FAILED = 5;
    public static final int CONNECTION_LOST = 6;

    private ConnectionEventListener eventListener = new NoEventListener();

    private InputCommandParser inputCommandParser;
    private ConnectionLog connectionLog;

    public ConnectionEventHandler(@NonNull InputCommandParser inputCommandParser, @NonNull ConnectionLog connectionLog) {
        Log.d("krio", "new ConnectionEventHandler");
        this.inputCommandParser = inputCommandParser;
        this.connectionLog = connectionLog;
    }

    public void setEventListener(@NonNull ConnectionEventListener eventListener) {
        this.eventListener = eventListener;
    }

    public void resetEventListener() {
        this.eventListener = new NoEventListener();
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case STATE_CHANGE:
                switch (msg.arg1) {
                    case Connection.STATE_DISCONNECTED:
                        onChangeState(Connection.STATE_DISCONNECTED);
                        break;

                    case Connection.STATE_CONNECTING:
                        onChangeState(Connection.STATE_CONNECTING);
                        break;

                    case Connection.STATE_CONNECTED:
                        onChangeState(Connection.STATE_CONNECTED);
                        break;
                }
                break;

            case RECEIVE_DATA:
                onReceiveData((byte[]) msg.obj, msg.arg1);
                break;

            case SEND_DATA:
                onSendData((byte[]) msg.obj);
                break;

            case CONNECTION_FAILED:
                onConnectionFailed();
                break;

            case CONNECTION_LOST:
                onConnectionLost();
                break;

            case REMOTE_EVENT:
                onRemoteEvent((String) msg.obj);
                break;
        }
    }

    private void onChangeState(int state) {
        connectionLog.notifyChangeState(state);
        eventListener.onChangeState(state);
    }

    private void onSendData(byte[] data) {
        connectionLog.notifySendData(data);
        eventListener.onSendData(data);
    }

    private void onReceiveData(byte[] data, int byteCount) {
        connectionLog.notifyReceiveData(data, byteCount);
        inputCommandParser.parseCommand(data, byteCount);
        eventListener.onReceiveData(data, byteCount);
    }

    private void onConnectionFailed() {
        connectionLog.notifyConnectionFailed();
        eventListener.onConnectionFailed();
    }

    private void onConnectionLost() {
        connectionLog.notifyConnectionLost();
        eventListener.onConnectionLost();
    }

    private void onRemoteEvent(String event) {
        connectionLog.notifyRemoteEvent(event);
        eventListener.onRemoteEvent(event);
    }
}
