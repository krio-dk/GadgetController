package com.krio.gadgetcontroller.logic.widgetbuilder;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.logic.widget.WLabel;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;

import java.util.Map;

/**
 * Created by krio on 17.04.16.
 */
public class WLabelBuilder extends WidgetBuilder {

    public WLabelBuilder() {
        widgetType = WidgetType.LABEL;
    }

    @Override
    public boolean createConcreteWidget() {
        widget = new WLabel();
        return true;
    }

    @Override
    public Command createCommand(CommandType type, String cmd, Map<String, Object> params) {
        return null;
    }
}
