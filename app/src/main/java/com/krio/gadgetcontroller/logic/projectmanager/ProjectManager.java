package com.krio.gadgetcontroller.logic.projectmanager;

import android.util.Log;

import com.krio.gadgetcontroller.logic.project.Project;
import com.krio.gadgetcontroller.logic.project.ProjectCreator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

/**
 * Created by krio on 02.06.16.
 */
public class ProjectManager extends Observable implements Project.OnChangeListener {

    DataBaseSynchronizer synchronizer;
    ProjectCreator projectCreator;

    Map<Long, Project> projectMap = new LinkedHashMap<>();

    public ProjectManager(DataBaseSynchronizer synchronizer, ProjectCreator projectCreator) {
        Log.d("krio", "new ProjectManager");
        this.synchronizer = synchronizer;
        this.projectCreator = projectCreator;
    }

    public Map<Long, Project> getProjectMap() {
        return projectMap;
    }

    public List<Project> getProjectList() {
        List<Project> projectList = new ArrayList<>(projectMap.values());
        Collections.sort(projectList, (lhs, rhs) -> lhs.getPosition() - rhs.getPosition());

        return projectList;
    }

    public Project getProject (long projectId) {
        return projectMap.get(projectId);
    }

    public void addProject(String name, String token) {
        long projectId = synchronizer.addProject(name, token);

        Project project = projectCreator.createProject(projectId);
        projectMap.put(project.getId(), project);

        project.setChangeListener(this);
        updateObservers(Notifier.TYPE_PROJECT_INSERTED, projectId);
    }

    public void deleteProject(long projectId) {
        synchronizer.deleteProject(projectId);
        projectMap.remove(projectId);
    }

    @Override
    public void onChangeName(String name, long projectId) {
        synchronizer.updateProjectName(name, projectId);
        updateObservers(Notifier.TYPE_PROJECT_CHANGED, projectId);
    }

    @Override
    public void onChangeToken(String token, long projectId) {
        synchronizer.updateProjectToken(token, projectId);
        updateObservers(Notifier.TYPE_PROJECT_CHANGED, projectId);
    }

    @Override
    public void onChangePosition(int position, long projectId) {
        synchronizer.updateProjectPosition(position, projectId);
    }

    private void updateObservers (int type, long projectId) {
        Notifier notifier = new Notifier(type, projectMap.get(projectId));

        setChanged();
        notifyObservers(notifier);
    }

    public class Notifier {

        public static final int TYPE_PROJECT_INSERTED = 1;
        public static final int TYPE_PROJECT_CHANGED = 2;

        int type;
        Project project;

        public Notifier(int type, Project project) {
            this.type = type;
            this.project = project;
        }

        public int getType() {
            return type;
        }

        public Project getProject() {
            return project;
        }
    }

    public interface ProjectManagerLoader {
        void load(ProjectManager projectManager);
    }

    public interface DataBaseSynchronizer {
        long addProject(String name, String token);

        void updateProjectName(String name, long projectId);

        void updateProjectPosition(int position, long projectId);

        void updateProjectToken(String token, long projectId);

        void deleteProject(long projectId);
    }
}
