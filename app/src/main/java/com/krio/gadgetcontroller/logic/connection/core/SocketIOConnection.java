package com.krio.gadgetcontroller.logic.connection.core;

import android.util.Log;

import com.krio.gadgetcontroller.logic.connection.events.ConnectionEventListener;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by krio on 11.04.16.
 */
public class SocketIOConnection extends Connection {

    private String serverUrl = "http://dkgadget.net:9595";

    private Socket socket;
    private String token;

    public SocketIOConnection(ConnectionEventListener eventListener, String token) {
        super(eventListener);
        Log.d("krio", "new SocketIOConnection");
        this.token = token;
        init();
    }

    private void init() {
        IO.Options opts = new IO.Options();
        opts.query = "token=" + token + "&type=app";
        opts.reconnection = false;

        try {
            socket = IO.socket(serverUrl, opts);

            addListener(Socket.EVENT_CONNECTING);
            addListener(Socket.EVENT_RECONNECTING);
            addListener(Socket.EVENT_ERROR);
            addListener(Socket.EVENT_CONNECT_ERROR);
            addListener(Socket.EVENT_CONNECT_TIMEOUT);
            addListener(Socket.EVENT_CONNECT);
            addListener(Socket.EVENT_DISCONNECT);
            addListener(Socket.EVENT_MESSAGE);
            addListener(REMOTE_EVENT_DEVICE_CONNECT);
            addListener(REMOTE_EVENT_DEVICE_DISCONNECT);

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void addListener(String event) {
        socket.on(event, new SocketEventListener(event));
    }

    @Override
    public void connect() {
        socket.connect();
    }

    @Override
    public void disconnect() {
        socket.disconnect();
    }

    @Override
    public void send(byte[] data) {
        socket.emit(Socket.EVENT_MESSAGE, data);
        eventListener.onSendData(data);
    }

    private class SocketEventListener implements Emitter.Listener {

        String event;

        public SocketEventListener(String event) {
            this.event = event;
        }

        @Override
        public void call(Object... args) {
            switch (event) {
                case Socket.EVENT_CONNECTING:
                case Socket.EVENT_RECONNECTING:
                    setState(STATE_CONNECTING);
                    break;

                case Socket.EVENT_ERROR:
                    setState(STATE_DISCONNECTED);
                    eventListener.onConnectionLost();
                    break;

                case Socket.EVENT_CONNECT_ERROR:
                case Socket.EVENT_CONNECT_TIMEOUT:
                    setState(STATE_DISCONNECTED);
                    eventListener.onConnectionFailed();
                    break;

                case Socket.EVENT_CONNECT:
                    setState(STATE_CONNECTED);
                    break;

                case Socket.EVENT_DISCONNECT:
                    setState(STATE_DISCONNECTED);
                    break;

                case Socket.EVENT_MESSAGE:
                    byte[] data = (byte[]) args[0];
                    eventListener.onReceiveData(data, data.length);
                    break;

                case REMOTE_EVENT_DEVICE_CONNECT:
                    eventListener.onRemoteEvent(REMOTE_EVENT_DEVICE_CONNECT);
                    break;

                case REMOTE_EVENT_DEVICE_DISCONNECT:
                    eventListener.onRemoteEvent(REMOTE_EVENT_DEVICE_DISCONNECT);
                    break;
            }
        }
    }
}
