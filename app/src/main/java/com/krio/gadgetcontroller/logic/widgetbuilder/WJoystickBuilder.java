package com.krio.gadgetcontroller.logic.widgetbuilder;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.logic.command.OutputCommand;
import com.krio.gadgetcontroller.logic.widget.WJoystick;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;

import java.util.Map;

/**
 * Created by krio on 17.04.16.
 */
public class WJoystickBuilder extends WidgetBuilder {

    Command jCmd;

    public WJoystickBuilder() {
        widgetType = WidgetType.JOYSTICK;
    }

    @Override
    public boolean createConcreteWidget() {
        if (jCmd == null) {
            return false;
        }

        widget = new WJoystick(jCmd);
        return true;
    }

    @Override
    public Command createCommand(CommandType type, String cmd, Map<String, Object> params) {

        if (type != CommandType.COMMAND_JOYSTICK) {
            return null;
        }

        jCmd = new OutputCommand(type, cmd);
        return jCmd;
    }
}
