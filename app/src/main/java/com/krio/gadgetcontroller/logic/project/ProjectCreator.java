package com.krio.gadgetcontroller.logic.project;

import android.util.Log;

/**
 * Created by krio on 02.06.16.
 */
public class ProjectCreator {

    Project.ProjectLoader projectLoader;
    Project.DataBaseSynchronizer synchronizer;

    public ProjectCreator(Project.ProjectLoader projectLoader, Project.DataBaseSynchronizer synchronizer) {
        Log.d("krio", "new ProjectCreator with synchronizer");
        this.projectLoader = projectLoader;
        this.synchronizer = synchronizer;
    }

    public ProjectCreator(Project.ProjectLoader projectLoader) {
        Log.d("krio", "new ProjectCreator");
        this.projectLoader = projectLoader;
    }

    public Project createProject(long projectId) {
        Project project = new Project(projectId, synchronizer);
        projectLoader.load(project);
        return project;
    }
}
