package com.krio.gadgetcontroller.logic.project;

import android.util.Log;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.logic.command.InputCommand;
import com.krio.gadgetcontroller.logic.panel.Panel;
import com.krio.gadgetcontroller.logic.widget.Widget;
import com.krio.gadgetcontroller.provider.command.CommandType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by krio on 21.04.16.
 */
public class Project implements Panel.PanelChangeListener, Widget.OnWidgetChangeListener, Command.OnCommandChangeListener {

    Long id;
    String name;
    String token;
    int position;

    Map<Long, Panel> panelMap;
    Map<Long, Widget> widgetMap;
    Map<String, InputCommand> inputCommandMap;

    DataBaseSynchronizer synchronizer;
    OnChangeListener changeListener;

    public void setChangeListener(OnChangeListener changeListener) {
        this.changeListener = changeListener;
    }

    public Project(Long id, DataBaseSynchronizer synchronizer) {
        Log.d("krio", "new Project (" + id + ")");
        this.id = id;
        this.synchronizer = synchronizer;

        panelMap = new LinkedHashMap<>();
        widgetMap = new LinkedHashMap<>();
        inputCommandMap = new LinkedHashMap<>();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        changeListener.onChangeName(name, id);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
        changeListener.onChangeToken(token, id);
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        changeListener.onChangePosition(position, id);
    }

    public void addPanel(String name) {
        long panelId = synchronizer.addPanel(name, id);

        Panel panel = new Panel(panelId, name);
        panelMap.put(panel.getId(), panel);

        panel.setChangeListener(this);
    }

    public void deletePanel(long panelId) {
        synchronizer.deletePanel(panelId);
        panelMap.remove(panelId);

        Iterator<Map.Entry<Long, Widget>> widgetMapIterator = widgetMap.entrySet().iterator();
        while(widgetMapIterator.hasNext()) {
            Map.Entry<Long, Widget> entry = widgetMapIterator.next();
            if(entry.getValue().getPanelId() == panelId) {
                widgetMapIterator.remove();
            }
        }

        Iterator<Map.Entry<String, InputCommand>> inputCommandMapIterator = inputCommandMap.entrySet().iterator();
        while(inputCommandMapIterator.hasNext()) {
            Map.Entry<String, InputCommand> entry = inputCommandMapIterator.next();
            if(entry.getValue().getWidget().getPanelId() == panelId) {
                inputCommandMapIterator.remove();
            }
        }
    }

    public Panel getPanel(long panelId) {
        return panelMap.get(panelId);
    }

    public int getPanelCount() {
        return panelMap.size();
    }

    public List<Panel> getPanelList() {
        return new ArrayList<>(panelMap.values());
    }

    public Map<Long, Widget> getWidgetMap() {
        return widgetMap;
    }

    public List<Widget> getPanelWidgets(long panelId) {
        List<Widget> panelWidgetList = new ArrayList<>();

        for (Widget widget : widgetMap.values()) {
            if (widget.getPanelId() == panelId) {
                panelWidgetList.add(widget);
            }
        }

        Collections.sort(panelWidgetList, (lhs, rhs) -> lhs.getPosition() - rhs.getPosition());

        return panelWidgetList;
    }

    public Widget getWidget(long widgetId) {
        return widgetMap.get(widgetId);
    }

    public void addWidget(Widget widget) {
        long widgetId = synchronizer.addWidget(widget);

        widget.setId(widgetId);
        widgetMap.put(widget.getId(), widget);

        widget.setChangeListener(this);
    }

    public void deleteWidget(long widgetId) {
        synchronizer.deleteWidget(widgetId);
        widgetMap.remove(widgetId);

        Iterator<Map.Entry<String, InputCommand>> inputCommandMapIterator = inputCommandMap.entrySet().iterator();
        while(inputCommandMapIterator.hasNext()) {
            Map.Entry<String, InputCommand> entry = inputCommandMapIterator.next();
            if(entry.getValue().getWidget().getId() == widgetId) {
                inputCommandMapIterator.remove();
            }
        }
    }

    public Map<String, InputCommand> getInputCommandMap() {
        return inputCommandMap;
    }

    public List<InputCommand> getInputCommandList(long widgetId) {
        List<InputCommand> inputCommands = new ArrayList<>();

        for (InputCommand command : inputCommandMap.values()) {
            if (command.getWidget().getId() == widgetId) {
                inputCommands.add(command);
            }
        }

        return inputCommands;
    }

    public Command getInputCommand(long widgetId, CommandType commandType) {
        List<InputCommand> inputCommands = getInputCommandList(widgetId);
        for (Command command : inputCommands) {
            if (command.getType() == commandType) {
                return command;
            }
        }

        return null;
    }


    public void addInputCommand(InputCommand command) {
        long commandId = synchronizer.addCommand(command);

        command.setId(commandId);
        inputCommandMap.put(command.getCmd(), command);

        command.setChangeListener(this);
    }

    public void deleteInputCommand(String cmd) {
        long commandId = inputCommandMap.get(cmd).getId();
        synchronizer.deleteCommand(commandId);
        inputCommandMap.remove(cmd);
    }

    @Override
    public void onChangeOutputCommand(String command, long commandId) {
        synchronizer.updateCommand(command, commandId);
    }

    @Override
    public void onChangeInputCommands(String oldCommand, String newCommand, long commandId) {
        InputCommand command = inputCommandMap.remove(oldCommand);
        inputCommandMap.put(newCommand, command);
        synchronizer.updateCommand(newCommand, commandId);
    }

    @Override
    public void onChangeCommandParams(Map<String, Object> params, long commandId) {
        synchronizer.updateCommandParams(params, commandId);
    }

    @Override
    public void onChangePanelName(String panelName, long panelId) {
        synchronizer.updatePanelName(panelName, panelId);
    }

    @Override
    public void onChangeWidgetPosition(int position, long widgetId) {
        synchronizer.updateWidgetPosition(position, widgetId);
    }

    @Override
    public void onChangeWidgetAttrs(Map<String, Object> attrs, long widgetId) {
        synchronizer.updateWidgetAttrs(attrs, widgetId);
    }

    public interface OnChangeListener {
        void onChangeName(String name, long projectId);

        void onChangeToken(String token, long projectId);

        void onChangePosition(int position, long projectId);
    }

    public interface ProjectLoader {
        void load(Project project);
    }

    public interface DataBaseSynchronizer {

        long addPanel(String name, long projectId);

        void updatePanelName(String panelName, long panelId);

        void deletePanel(long panelId);

        long addWidget(Widget widget);

        void updateWidgetPosition(int position, long widgetId);

        void updateWidgetAttrs(Map<String, Object> attrs, long widgetId);

        void deleteWidget(long widgetId);

        long addCommand(Command command);

        void updateCommand(String command, long commandId);

        void updateCommandParams(Map<String, Object> params, long commandId);

        void deleteCommand(long commandId);
    }
}
