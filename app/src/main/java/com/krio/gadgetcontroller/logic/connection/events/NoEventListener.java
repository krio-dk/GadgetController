package com.krio.gadgetcontroller.logic.connection.events;

/**
 * Created by krio on 11.06.16.
 */
public class NoEventListener implements ConnectionEventListener {

    @Override
    public void onChangeState(int state) {
    }

    @Override
    public void onSendData(byte[] data) {
    }

    @Override
    public void onReceiveData(byte[] data, int byteCount) {
    }

    @Override
    public void onConnectionFailed() {
    }

    @Override
    public void onConnectionLost() {
    }

    @Override
    public void onRemoteEvent(String event) {
    }
}
