package com.krio.gadgetcontroller.logic.widget;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by krio on 07.04.16.
 */
public class WSwitch extends Widget {

    Command swcOnCmd;
    Command swcOffCmd;

    boolean checked;

    public WSwitch(Command swcOnCmd, Command swcOffCmd) {
        super(WidgetType.SWITCH);
        this.swcOnCmd = swcOnCmd;
        this.swcOffCmd = swcOffCmd;
    }

    @Override
    public boolean isCaptionVisible() {
        return true;
    }

    @Override
    public Command getOutputCommand(CommandType commandType) {
        switch (commandType) {
            case COMMAND_SWITCH_ENABLE:
                return swcOnCmd;
            case COMMAND_SWITCH_DISABLE:
                return swcOffCmd;
            default:
                throw new UnsupportedOperationException();
        }
    }

    @Override
    public List<Command> getOutputCommands() {
        List<Command> commandList = new ArrayList<>();
        commandList.add(swcOffCmd);
        commandList.add(swcOnCmd);
        return commandList;
    }

    @Override
    public void performCommand(CommandType commandType, Map<String, Object> params) {
        switch (commandType) {
            case COMMAND_SWITCH_ENABLE:
                swcOnCmd.execute();
                break;
            case COMMAND_SWITCH_DISABLE:
                swcOffCmd.execute();
                break;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(final boolean checked) {
        this.checked = checked;
    }
}
