package com.krio.gadgetcontroller.logic.connection.log;

/**
 * Created by krio on 28.04.16.
 */
public enum ItemType {
    STATE_CHANGE,
    CONNECTION_PROBLEM,
    RECEIVE_DATA,
    SEND_DATA,
    EXECUTE_INPUT_COMMAND,
    EXECUTE_OUTPUT_COMMAND;
}
