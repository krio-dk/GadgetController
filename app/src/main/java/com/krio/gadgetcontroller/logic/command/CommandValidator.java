package com.krio.gadgetcontroller.logic.command;

import android.support.annotation.NonNull;

import com.krio.gadgetcontroller.logic.project.Project;
import com.krio.gadgetcontroller.logic.widget.Widget;

/**
 * Created by krio on 11.05.16.
 */
public class CommandValidator {

    Project project;

    public CommandValidator(Project project) {
        this.project = project;
    }

    public boolean validateInputCommand (@NonNull String cmd) {
        if (project.getInputCommandMap().get(cmd) != null) {
            return false;
        }

        return true;
    }

    public boolean validateOutputCommand (@NonNull String cmd) {
        // Теперь можно назначать одинаковые исходящие команды для виджетов
        /*
        for (Widget widget : project.getWidgetMap().values()) {
            for (Command command : widget.getOutputCommands()) {
                if (command.getCmd().equals(cmd)) {
                    return false;
                }
            }
        }
        */
        return true;
    }
}
