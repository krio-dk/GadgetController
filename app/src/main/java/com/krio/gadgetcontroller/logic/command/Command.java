package com.krio.gadgetcontroller.logic.command;

import com.krio.gadgetcontroller.provider.command.CommandType;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by krio on 07.04.16.
 */
public abstract class Command {

    CommandType commandType;

    String cmd;

    long id;
    long widgetId;

    Map<String, Object> params = new HashMap<>();

    OnCommandChangeListener changeListener;

    public void setChangeListener(OnCommandChangeListener changeListener) {
        this.changeListener = changeListener;
    }

    public Command(CommandType commandType, String cmd) {
        this.commandType = commandType;
        this.cmd = cmd;
    }

    public CommandType getType() {
        return commandType;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        onChangeCommand(this.cmd, cmd);
        this.cmd = cmd;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public long getWidgetId() {
        return widgetId;
    }

    public void setWidgetId(long widgetId) {
        this.widgetId = widgetId;
    }


    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public final void execute(Map<String, Object> params) {
        this.params = params;
        execute();
    }

    public abstract void execute();

    protected abstract void onChangeCommand(String oldCommand, String newCommand);

    public interface OnCommandChangeListener {
        void onChangeOutputCommand(String command, long commandId);

        void onChangeInputCommands(String oldCommand, String newCommand, long commandId);

        void onChangeCommandParams(Map<String, Object> params, long commandId);
    }
}
