package com.krio.gadgetcontroller.logic.connection.events;

/**
 * Created by krio on 23.04.16.
 */
public interface ConnectionEventListener {
    void onChangeState(int state);

    void onSendData(byte[] data);

    void onReceiveData(byte[] data, int byteCount);

    void onConnectionFailed();

    void onConnectionLost();

    void onRemoteEvent(String event);
}
