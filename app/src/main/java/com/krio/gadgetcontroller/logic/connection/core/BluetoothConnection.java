package com.krio.gadgetcontroller.logic.connection.core;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.krio.gadgetcontroller.logic.connection.events.ConnectionEventListener;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by krio on 11.04.16.
 */
public class BluetoothConnection extends Connection {

    private static final String TAG = "BluetoothConnection";

    private static final UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private ExecutorService mExecutorService;
    private ConnectRunnable mConnectRunnable;
    private ConnectedRunnable mConnectedRunnable;

    public BluetoothConnection(ConnectionEventListener eventListener, BluetoothDevice bluetoothDevice, boolean secure) {
        super(eventListener);
        Log.d("krio", "new BluetoothConnection");
        init(bluetoothDevice, secure);
    }

    private void init(BluetoothDevice bluetoothDevice, boolean secure) {
        mExecutorService = Executors.newSingleThreadExecutor();
        mConnectRunnable = new ConnectRunnable(bluetoothDevice, secure);
    }

    @Override
    public void connect() {
        mExecutorService.submit(mConnectRunnable);
    }

    @Override
    public void disconnect() {

        Log.d(TAG, "disconnect");

        if (getState() == STATE_CONNECTING) {
            mConnectRunnable.cancel();
        } else {
            mConnectedRunnable.cancel();
            mConnectedRunnable = null;
        }
    }

    @Override
    public void send(byte[] data) {
        if (getState() == STATE_CONNECTED) {
            mConnectedRunnable.write(data);
        }
    }

    private class ConnectRunnable implements Runnable {

        private final BluetoothDevice mDevice;
        private final boolean mSecure;

        private BluetoothSocket mSocket;
        private volatile boolean mCloseFlag;

        public ConnectRunnable(BluetoothDevice device, boolean secure) {
            Log.d(TAG, "new ConnectRunnable");
            mDevice = device;
            mSecure = secure;
        }

        private BluetoothSocket createSocket () {
            Log.d(TAG, "createSocket");

            BluetoothSocket socket = null;
            try {
                if (mSecure) {
                    socket = mDevice.createRfcommSocketToServiceRecord(SPP_UUID);
                } else {
                    socket = mDevice.createInsecureRfcommSocketToServiceRecord(SPP_UUID);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return socket;
        }

        @Override
        public void run() {

            setState(STATE_CONNECTING);
            Log.d(TAG, "STATE_CONNECTING");

            BluetoothAdapter.getDefaultAdapter().cancelDiscovery();

            mSocket = createSocket();
            mCloseFlag = false;

            try {
                mSocket.connect();
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "Exception:  connecting canceled or connection failure");

                try {
                    mSocket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                setState(STATE_DISCONNECTED);
                Log.d(TAG, "STATE_DISCONNECTED");

                if (!mCloseFlag) {
                    eventListener.onConnectionFailed();
                    Log.d(TAG, "notifyConnectionFailed");
                }
                return;
            }

            Log.d(TAG, "connect ok");

            mConnectedRunnable = new ConnectedRunnable(mSocket);
            mExecutorService.submit(mConnectedRunnable);
        }

        public void cancel() {
            Log.d(TAG, "Cancel connecting");
            mCloseFlag = true;
            try {
                if (mSocket != null) {
                    mSocket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "Exception: can't close socket (Cancel connecting)");
            }
        }
    }

    private class ConnectedRunnable implements Runnable {

        private final BluetoothSocket mSocket;
        private final InputStream mInStream;
        private final OutputStream mOutStream;

        private volatile boolean mCloseFlag = false;

        public ConnectedRunnable(BluetoothSocket socket) {
            Log.d(TAG, "new ConnectedRunnable");

            mSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "Exception: can't get streams");
            }

            mInStream = tmpIn;
            mOutStream = tmpOut;
        }

        @Override
        public void run() {

            setState(STATE_CONNECTED);
            Log.d(TAG, "STATE_CONNECTED");

            byte[] buffer = new byte[1024];
            int byteCount;

            while (true) {
                try {
                    byteCount = mInStream.read(buffer);
                    eventListener.onReceiveData(buffer.clone(), byteCount);

                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d(TAG, "Exception: socket is closed");

                    setState(STATE_DISCONNECTED);
                    Log.d(TAG, "STATE_DISCONNECTED");

                    if (!mCloseFlag) {
                        eventListener.onConnectionLost();
                        Log.d(TAG, "notifyConnectionLost");
                    }
                    break;
                }
            }
        }

        public void write(byte[] buffer) {
            try {
                mOutStream.write(buffer);
                eventListener.onSendData(buffer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void cancel() {
            Log.d(TAG, "Cancel connected");
            mCloseFlag = true;
            try {
                mSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "Exception: can't close socket (Cancel connected)");
            }
        }
    }
}
