package com.krio.gadgetcontroller.logic.connection.core;

import android.support.annotation.NonNull;

import com.krio.gadgetcontroller.logic.connection.events.ConnectionEventListener;

/**
 * Created by krio on 07.04.16.
 */
public abstract class Connection {

    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;

    public static final String REMOTE_EVENT_DEVICE_CONNECT = "device_connect";
    public static final String REMOTE_EVENT_DEVICE_DISCONNECT = "device_disconnect";

    private int state = STATE_DISCONNECTED;
    ConnectionEventListener eventListener;

    public Connection(@NonNull ConnectionEventListener eventListener) {
        this.eventListener = eventListener;
    }

    public final void performConnect() {
        if (state == STATE_CONNECTED || state == STATE_CONNECTING) {
            disconnect();
        }
        connect();
    }

    public final void performDisconnect() {
        if (state != STATE_DISCONNECTED) {
            disconnect();
        }
    }

    public final void performSend(byte[] data) {
        if (state == STATE_CONNECTED) {
            send(data);
        }
    }

    public final int getState() {
        return state;
    }

    protected final void setState(int state) {
        this.state = state;
        eventListener.onChangeState(state);
    }

    protected abstract void connect();

    protected abstract void disconnect();

    protected abstract void send(byte[] data);
}
