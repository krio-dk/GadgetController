package com.krio.gadgetcontroller.logic.connection.core;

import android.util.Log;

import com.krio.gadgetcontroller.logic.connection.events.ConnectionEventListener;

/**
 * Created by krio on 23.04.16.
 */
public class NoConnection extends Connection {

    public NoConnection(ConnectionEventListener eventListener) {
        super(eventListener);
        Log.d("krio", "new NoConnection");
    }

    @Override
    protected void connect() {

    }

    @Override
    protected void disconnect() {

    }

    @Override
    protected void send(byte[] data) {

    }
}
