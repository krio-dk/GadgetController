package com.krio.gadgetcontroller.logic.connection.core;

import android.bluetooth.BluetoothDevice;

import com.krio.gadgetcontroller.App;
import com.krio.gadgetcontroller.logic.connection.events.ConnectionEventListener;

/**
 * Created by krio on 23.04.16.
 */
public class ConnectionFactory {

    public static Connection createSocketIOConnection(String token) {
        return new SocketIOConnection(getEventListener(), token);
    }

    public static Connection createBluetoothConnection(BluetoothDevice bluetoothDevice) {
        return new BluetoothConnection(getEventListener(), bluetoothDevice, false);
    }

    public static Connection createTCPClientConnection(String host, int port) {
        return new TCPClientConnection(getEventListener(), host, port);
    }

    private static ConnectionEventListener getEventListener() {
        return App.getMainComponent().getConnectionEventListener();
    }
}
