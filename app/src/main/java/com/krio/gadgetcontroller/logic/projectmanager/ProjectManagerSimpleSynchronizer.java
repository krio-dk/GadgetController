package com.krio.gadgetcontroller.logic.projectmanager;

import android.content.ContentUris;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.krio.gadgetcontroller.R;
import com.krio.gadgetcontroller.provider.project.ProjectContentValues;
import com.krio.gadgetcontroller.provider.project.ProjectSelection;
import com.krio.gadgetcontroller.provider.panel.PanelContentValues;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by krio on 02.06.16.
 */
public class ProjectManagerSimpleSynchronizer implements ProjectManager.DataBaseSynchronizer {

    Context context;
    ExecutorService executorService = Executors.newSingleThreadExecutor();

    public ProjectManagerSimpleSynchronizer(Context context) {
        Log.d("krio", "new ProjectManagerSimpleSynchronizer");
        this.context = context;
    }

    @Override
    public long addProject(String name, String token) {
        long projectId = insertProject(name, token);
        insertPanel(projectId);

        return projectId;
    }

    @Override
    public void updateProjectName(String name, long projectId) {
        ProjectSelection projectSelection = new ProjectSelection();
        projectSelection.id(projectId);

        ProjectContentValues projectContentValues = new ProjectContentValues();
        projectContentValues.putName(name);
        projectContentValues.update(context, projectSelection);
    }

    @Override
    public void updateProjectPosition(int position, long projectId) {
        executorService.submit(() -> {
            ProjectSelection projectSelection = new ProjectSelection();
            projectSelection.id(projectId);

            ProjectContentValues projectContentValues = new ProjectContentValues();
            projectContentValues.putPositionOnList(position);

            projectContentValues.update(context, projectSelection);
        });
    }

    @Override
    public void updateProjectToken(String token, long projectId) {
        ProjectSelection projectSelection = new ProjectSelection();
        projectSelection.id(projectId);

        ProjectContentValues projectContentValues = new ProjectContentValues();
        projectContentValues.putToken(token);
        projectContentValues.update(context, projectSelection);
    }

    @Override
    public void deleteProject(long projectId) {
        ProjectSelection projectSelection = new ProjectSelection();
        projectSelection.id(projectId);
        projectSelection.delete(context);
    }

    private long insertProject(String projectName, String projectToken) {
        ProjectContentValues projectContentValues = new ProjectContentValues();
        projectContentValues.putName(projectName);
        projectContentValues.putToken(projectToken);
        projectContentValues.putPositionOnList(0);

        Uri uri = projectContentValues.insert(context);
        return ContentUris.parseId(uri);
    }

    private void insertPanel(long projectId) {
        PanelContentValues panelContentValues = new PanelContentValues();
        panelContentValues.putCaption(context.getString(R.string.header_new_panel));
        panelContentValues.putProjectId(projectId);

        panelContentValues.insert(context);
    }
}
