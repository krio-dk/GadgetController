package com.krio.gadgetcontroller.logic.panel;

/**
 * Created by krio on 18.11.15.
 */
public class Panel {

    public static final String CAPTION = "name";

    long id;
    String name;

    PanelChangeListener changeListener = new NoChangeListener();

    public Panel(long id, String name) {
        this.id = id;
        this.name = name;
    }


    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        changeListener.onChangePanelName(name, id);
    }

    public void setChangeListener(PanelChangeListener changeListener) {
        this.changeListener = changeListener;
    }

    public interface PanelChangeListener {
        void onChangePanelName(String panelName, long panelId);
    }

    class NoChangeListener implements PanelChangeListener {
        @Override
        public void onChangePanelName(String panelName, long panelId) {

        }
    }
}
