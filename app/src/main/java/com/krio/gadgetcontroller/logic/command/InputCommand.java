package com.krio.gadgetcontroller.logic.command;

import com.krio.gadgetcontroller.App;
import com.krio.gadgetcontroller.logic.widget.Widget;
import com.krio.gadgetcontroller.provider.command.CommandType;

/**
 * Created by krio on 07.04.16.
 */
public class InputCommand extends Command {

    Widget widget;

    public InputCommand(Widget widget, CommandType commandType, String cmd) {
        super(commandType, cmd);
        this.widget = widget;
    }

    public Widget getWidget() {
        return widget;
    }


    @Override
    public void execute() {
        widget.performCommand(commandType, params);
        App.getConnectionLog().notifyInputCommandExecute(this);
    }

    @Override
    protected void onChangeCommand(String oldCommand, String newCommand) {
        if (changeListener != null) {
            changeListener.onChangeInputCommands(oldCommand, newCommand, id);
        }
    }
}
