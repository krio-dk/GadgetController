package com.krio.gadgetcontroller.logic.project;

import android.content.ContentUris;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.logic.widget.Widget;
import com.krio.gadgetcontroller.provider.command.CommandContentValues;
import com.krio.gadgetcontroller.provider.command.CommandSelection;
import com.krio.gadgetcontroller.provider.commandparam.CommandParamContentValues;
import com.krio.gadgetcontroller.provider.commandparam.CommandParamSelection;
import com.krio.gadgetcontroller.provider.commandparam.CommandParamType;
import com.krio.gadgetcontroller.provider.widget.WidgetContentValues;
import com.krio.gadgetcontroller.provider.widget.WidgetSelection;
import com.krio.gadgetcontroller.provider.widgetattr.WidgetAttrContentValues;
import com.krio.gadgetcontroller.provider.widgetattr.WidgetAttrSelection;
import com.krio.gadgetcontroller.provider.widgetattr.WidgetAttrType;
import com.krio.gadgetcontroller.provider.panel.PanelContentValues;
import com.krio.gadgetcontroller.provider.panel.PanelSelection;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by krio on 02.06.16.
 */
public class ProjectSimpleSynchronizer implements Project.DataBaseSynchronizer {

    Context context;
    ExecutorService executorService = Executors.newSingleThreadExecutor();

    public ProjectSimpleSynchronizer(Context context) {
        Log.d("krio", "new ProjectSimpleSynchronizer");
        this.context = context;
    }


    @Override
    public long addPanel(String name, long projectId) {
        PanelContentValues panelContentValues = new PanelContentValues();
        panelContentValues.putProjectId(projectId);
        panelContentValues.putCaption(name);

        Uri uri = panelContentValues.insert(context);
        long panelId = ContentUris.parseId(uri);

        return panelId;
    }

    @Override
    public void updatePanelName(String name, long panelId) {
        PanelSelection panelSelection = new PanelSelection();
        panelSelection.id(panelId);

        PanelContentValues panelContentValues = new PanelContentValues();
        panelContentValues.putCaption(name);
        panelContentValues.update(context, panelSelection);
    }

    @Override
    public void deletePanel(long panelId) {
        PanelSelection panelSelection = new PanelSelection();
        panelSelection.id(panelId);
        panelSelection.delete(context);
    }

    @Override
    public long addWidget(Widget widget) {
        WidgetContentValues widgetContentValues = new WidgetContentValues();
        widgetContentValues.putCaption(widget.getCaption());
        widgetContentValues.putType(widget.getWidgetType());
        widgetContentValues.putPanelId(widget.getPanelId());
        widgetContentValues.putPositionOnPanel(widget.getPosition());

        Uri element_uri = widgetContentValues.insert(context.getContentResolver());
        long widgetId = ContentUris.parseId(element_uri);

        insertWidgetAttrs(widget.getAttributes(), widgetId);

        List<Command> commandList = widget.getOutputCommands();
        if (commandList != null) {
            for (Command cmd : commandList) {
                cmd.setWidgetId(widgetId);
            }
            insertCommands(commandList);
        }

        return widgetId;
    }

    @Override
    public void updateWidgetPosition(int position, long widgetId) {
        executorService.submit(() -> {
            WidgetSelection widgetSelection = new WidgetSelection();
            widgetSelection.id(widgetId);

            WidgetContentValues widgetContentValues = new WidgetContentValues();
            widgetContentValues.putPositionOnPanel(position);
            widgetContentValues.update(context, widgetSelection);
        });
    }

    @Override
    public void updateWidgetAttrs(Map<String, Object> attrs, long widgetId) {
        for (String attrName : attrs.keySet()) {
            WidgetAttrSelection widgetAttrSelection = new WidgetAttrSelection();
            widgetAttrSelection.widgetId(widgetId).and().name(attrName);

            WidgetAttrContentValues contentValues = new WidgetAttrContentValues();

            Object attrValue = attrs.get(attrName);

            switch (attrValue.getClass().getSimpleName()) {
                case "String":
                    contentValues.putStringAttr((String) attrValue);
                    break;

                case "Boolean":
                    contentValues.putBooleanAttr((Boolean) attrValue);
                    break;

                case "Integer":
                    contentValues.putIntegerAttr((Integer) attrValue);
                    break;

                case "Double":
                    contentValues.putDoubleAttr((Double) attrValue);
                    break;
            }
            contentValues.update(context, widgetAttrSelection);
        }
    }

    @Override
    public void deleteWidget(long widgetId) {
        WidgetSelection widgetSelection = new WidgetSelection();
        widgetSelection.id(widgetId);
        widgetSelection.delete(context);
    }

    @Override
    public long addCommand(Command command) {
        return insertCommand(command);
    }

    @Override
    public void updateCommand(String command, long commandId) {
        CommandSelection commandSelection = new CommandSelection();
        commandSelection.id(commandId);

        CommandContentValues commandContentValues = new CommandContentValues();
        commandContentValues.putCmd(command);
        commandContentValues.update(context, commandSelection);
    }

    @Override
    public void updateCommandParams(Map<String, Object> params, long commandId) {
        for (String paramName : params.keySet()) {
            CommandParamSelection commandParamSelection = new CommandParamSelection();
            commandParamSelection.commandId(commandId).and().name(paramName);

            CommandParamContentValues contentValues = new CommandParamContentValues();

            Object paramValue = params.get(paramName);

            switch (paramValue.getClass().getSimpleName()) {
                case "String":
                    contentValues.putStringParam((String) paramValue);
                    break;

                case "Boolean":
                    contentValues.putBooleanParam((Boolean) paramValue);
                    break;

                case "Integer":
                    contentValues.putIntegerParam((Integer) paramValue);
                    break;

                case "Double":
                    contentValues.putDoubleParam((Double) paramValue);
                    break;
            }
            contentValues.update(context, commandParamSelection);
        }
    }

    @Override
    public void deleteCommand(long commandId) {
        CommandSelection commandSelection = new CommandSelection();
        commandSelection.id(commandId);
        commandSelection.delete(context);
    }


    private void insertWidgetAttrs(Map<String, Object> attrs, long widgetId) {
        for (String key : attrs.keySet()) {
            WidgetAttrContentValues widgetAttrContentValues = new WidgetAttrContentValues();
            widgetAttrContentValues.putWidgetId(widgetId);
            widgetAttrContentValues.putName(key);

            Object value = attrs.get(key);

            switch (value.getClass().getSimpleName()) {
                case "String":
                    widgetAttrContentValues.putStringAttr((String) value);
                    widgetAttrContentValues.putType(WidgetAttrType.STRING);
                    break;

                case "Boolean":
                    widgetAttrContentValues.putBooleanAttr((Boolean) value);
                    widgetAttrContentValues.putType(WidgetAttrType.BOOLEAN);
                    break;

                case "Integer":
                    widgetAttrContentValues.putIntegerAttr((Integer) value);
                    widgetAttrContentValues.putType(WidgetAttrType.INEGER);
                    break;

                case "Double":
                    widgetAttrContentValues.putDoubleAttr((Double) value);
                    widgetAttrContentValues.putType(WidgetAttrType.DOUBLE);
                    break;
            }
            widgetAttrContentValues.insert(context);
        }
    }

    private void insertCommands(List<Command> commandList) {
        for (Command cmd : commandList) {
            insertCommand(cmd);
        }
    }

    private long insertCommand(Command command) {
        CommandContentValues commandContentValues = new CommandContentValues();
        commandContentValues.putWidgetId(command.getWidgetId());
        commandContentValues.putCmd(command.getCmd());
        commandContentValues.putType(command.getType());

        Uri command_uri = commandContentValues.insert(context.getContentResolver());
        long commandId = ContentUris.parseId(command_uri);

        insertCommandParams(command.getParams(), commandId);

        return commandId;
    }

    private void insertCommandParams(Map<String, Object> params, long commandId) {
        for (String key : params.keySet()) {
            CommandParamContentValues commandParamContentValues = new CommandParamContentValues();
            commandParamContentValues.putCommandId(commandId);
            commandParamContentValues.putName(key);

            Object value = params.get(key);
            switch (value.getClass().getSimpleName()) {
                case "String":
                    commandParamContentValues.putStringParam((String) value);
                    commandParamContentValues.putType(CommandParamType.STRING);
                    break;

                case "Boolean":
                    commandParamContentValues.putBooleanParam((Boolean) value);
                    commandParamContentValues.putType(CommandParamType.BOOLEAN);
                    break;

                case "Integer":
                    commandParamContentValues.putIntegerParam((Integer) value);
                    commandParamContentValues.putType(CommandParamType.INEGER);
                    break;

                case "Double":
                    commandParamContentValues.putDoubleParam((Double) value);
                    commandParamContentValues.putType(CommandParamType.DOUBLE);
                    break;
            }
            commandParamContentValues.insert(context.getContentResolver());
        }
    }

}
