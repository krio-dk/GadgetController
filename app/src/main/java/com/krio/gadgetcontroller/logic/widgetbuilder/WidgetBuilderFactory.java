package com.krio.gadgetcontroller.logic.widgetbuilder;

import com.krio.gadgetcontroller.provider.widget.WidgetType;

/**
 * Created by krio on 21.04.16.
 */
public class WidgetBuilderFactory {
    public static WidgetBuilder createWidgetBuilder(WidgetType widgetType) {
        switch (widgetType) {
            case BUTTON:
                return new WButtonBuilder();
            case LED:
                return new WLedBuilder();
            case SEEKBAR:
                return new WSeekBarBuilder();
            case SWITCH:
                return new WSwitchBuilder();
            case DISPLAY:
                return new WDisplayBuilder();
            case LABEL:
                return new WLabelBuilder();
            case JOYSTICK:
                return new WJoystickBuilder();
            case TIMER:
                return new WTimerBuilder();

            default:
                return null;
        }
    }
}
