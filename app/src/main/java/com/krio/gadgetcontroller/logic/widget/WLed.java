package com.krio.gadgetcontroller.logic.widget;

import com.krio.gadgetcontroller.logic.command.Command;
import com.krio.gadgetcontroller.provider.command.CommandType;
import com.krio.gadgetcontroller.provider.widget.WidgetType;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by krio on 07.04.16.
 */
public class WLed extends Widget {

    public static final String ATTR_COLOR = "color";

    boolean isEnabled = false;

    public WLed() {
        super(WidgetType.LED);
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public int getColor() {
        return (int) attrs.get(ATTR_COLOR);
    }

    @Override
    public boolean isCaptionVisible() {
        return true;
    }

    @Override
    public Command getOutputCommand(CommandType commandType) {
        return null;
    }

    @Override
    public List<Command> getOutputCommands() {
        return Collections.emptyList();
    }

    @Override
    public void performCommand(CommandType commandType, Map<String, Object> params) {
        switch (commandType) {
            case COMMAND_LED_ENABLE:
                isEnabled = true;
                break;
            case COMMAND_LED_DISABLE:
                isEnabled = false;
                break;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
